package com.master.testclass.data;

import org.junit.Ignore;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by subasteve on 4/27/2016.
 */
@Ignore
public class RandomAddRemoveFreeNumber implements Runnable  {

    /**
     * Returns a psuedo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimim value
     * @param max Maximim value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public static int randInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    final List<Integer> ACTIVE = new ArrayList<Integer>();
    final com.master.data.FreeNumber FREE_NUMBER;
    boolean running = false;

    public RandomAddRemoveFreeNumber(final com.master.data.FreeNumber FREE_NUMBER){
        this.FREE_NUMBER = FREE_NUMBER;
    }

    public final void start(){
        running = true;
        new Thread(this).start();
    }

    public final void stop(){
        running = false;
    }

    public final void run(){
        while (running){
            final boolean ADD = (randInt(1,2) == 1);
            if(ADD){
                ACTIVE.add(FREE_NUMBER.put());
            }else{
                boolean removed = false;
                for(final int TO_REMOVE : ACTIVE){
                    FREE_NUMBER.remove(TO_REMOVE);
                    ACTIVE.remove((Object)TO_REMOVE);
                    removed = true;
                    break;
                }
                if(!removed){
                    ACTIVE.add(FREE_NUMBER.put());
                }
            }
        }
    }
}
