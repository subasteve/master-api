package com.master.test.net;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Created by subasteve on 3/12/14.
 */
public class IP {
    final com.master.net.IP REFRENCE = new com.master.net.IP("127.0.0.1");
    final com.master.net.IP REFRENCE_2 = new com.master.net.IP("255.255.255.255");
    final com.master.net.IP REFRENCE_3 = new com.master.net.IP("127.0.0.1",true);
    final com.master.net.IP REFRENCE_4 = new com.master.net.IP("255.255.255.255",true);

    @Test
    public void TestToBytesFromStringReverse(){
        final int[] REFRENCE_BYTES = REFRENCE_3.toBytes();
        final int[] COMPARE_BYTES = com.master.net.IP.toBytes("127.0.0.1",true);
        assertArrayEquals("[TestToBytesFromStringReverse] 127.0.0.1 toBytes",REFRENCE_BYTES,COMPARE_BYTES);
        final int[] REFRENCE_BYTES_2 = REFRENCE_4.toBytes();
        final int[] COMPARE_BYTES_2 = com.master.net.IP.toBytes("255.255.255.255",true);
        assertArrayEquals("[TestToBytesFromStringReverse] 255.255.255.255 toBytes",REFRENCE_BYTES_2,COMPARE_BYTES_2);
    }

    @Test
    public void TestToBytesFromString(){
        final int[] REFRENCE_BYTES = REFRENCE.toBytes();
        final int[] COMPARE_BYTES = com.master.net.IP.toBytes("127.0.0.1");
        assertArrayEquals("[TestToBytesFromString] 127.0.0.1 toBytes",REFRENCE_BYTES,COMPARE_BYTES);
        final int[] REFRENCE_BYTES_2 = REFRENCE_2.toBytes();
        final int[] COMPARE_BYTES_2 = com.master.net.IP.toBytes("255.255.255.255");
        assertArrayEquals("[TestToBytesFromString] 255.255.255.255 toBytes",REFRENCE_BYTES_2,COMPARE_BYTES_2);
    }

    @Test
    public void TestToBytesFromInt(){
        final int[] REFRENCE_BYTES = REFRENCE.toBytes();
        final int[] COMPARE_BYTES = com.master.net.IP.toBytes(2130706433);
        assertArrayEquals("[TestToBytesFromString] 127.0.0.1 toBytes",REFRENCE_BYTES,COMPARE_BYTES);
        final int[] REFRENCE_BYTES_2 = REFRENCE_2.toBytes();
        final int[] COMPARE_BYTES_2 = com.master.net.IP.toBytes(-1);
        assertArrayEquals("[TestToBytesFromString] 255.255.255.255 toBytes",REFRENCE_BYTES_2,COMPARE_BYTES_2);
    }

    @Test
    public void TestToBytesFromIntReverse(){
        final int[] REFRENCE_BYTES = REFRENCE_3.toBytes();
        final int[] COMPARE_BYTES = com.master.net.IP.toBytes(2130706433,true);
        assertArrayEquals("[TestToBytesFromIntReverse] 127.0.0.1 toBytes",REFRENCE_BYTES,COMPARE_BYTES);
        final int[] REFRENCE_BYTES_2 = REFRENCE_4.toBytes();
        final int[] COMPARE_BYTES_2 = com.master.net.IP.toBytes(-1,true);
        assertArrayEquals("[TestToBytesFromIntReverse] 255.255.255.255 toBytes",REFRENCE_BYTES_2,COMPARE_BYTES_2);
    }

    @Test
    public void TestToIntFromString(){
        final int REFRENCE_INT = REFRENCE.toInt();
        final int COMPARE_INT = com.master.net.IP.toInt("127.0.0.1");
        assertEquals("[TestToBytesFromString] 127.0.0.1 toInt", REFRENCE_INT, COMPARE_INT);
        final int REFRENCE_INT_2 = REFRENCE_2.toInt();
        final int COMPARE_INT_2 = com.master.net.IP.toInt("255.255.255.255");
        assertEquals("[TestToBytesFromString] 255.255.255.255 toInt", REFRENCE_INT_2, COMPARE_INT_2);
    }

    @Test
    public void TestToIntFromStringReverse(){
        final int REFRENCE_INT = REFRENCE_3.toInt();
        final int COMPARE_INT = com.master.net.IP.toInt("127.0.0.1",true);
        assertEquals("[TestToIntFromStringReverse] 127.0.0.1 toInt", REFRENCE_INT, COMPARE_INT);
        final int REFRENCE_INT_2 = REFRENCE_4.toInt();
        final int COMPARE_INT_2 = com.master.net.IP.toInt("255.255.255.255",true);
        assertEquals("[TestToIntFromStringReverse] 255.255.255.255 toInt", REFRENCE_INT_2, COMPARE_INT_2);
    }

    @Test
    public void TestToIntFromBytes(){
        final int REFRENCE_INT = REFRENCE.toInt();
        final int COMPARE_INT = com.master.net.IP.toInt(new int[]{127,0,0,1});
        assertEquals("[TestToIntFromBytes] 127.0.0.1 toInt",REFRENCE_INT,COMPARE_INT);
        final int REFRENCE_INT_2 = REFRENCE_2.toInt();
        final int COMPARE_INT_2 = com.master.net.IP.toInt(new int[]{255,255,255,255});
        assertEquals("[TestToIntFromBytes] 255.255.255.255 toInt",REFRENCE_INT_2,COMPARE_INT_2);
    }

    @Test
    public void TestToIntFromBytesReverse(){
        final int REFRENCE_INT = REFRENCE_3.toInt();
        final int COMPARE_INT = com.master.net.IP.toInt(new int[]{127,0,0,1},true);
        assertEquals("[TestToIntFromBytesReverse] 127.0.0.1 toInt",REFRENCE_INT,COMPARE_INT);
        final int REFRENCE_INT_2 = REFRENCE_4.toInt();
        final int COMPARE_INT_2 = com.master.net.IP.toInt(new int[]{255,255,255,255},true);
        assertEquals("[TestToIntFromBytesReverse] 255.255.255.255 toInt",REFRENCE_INT_2,COMPARE_INT_2);
    }

}
