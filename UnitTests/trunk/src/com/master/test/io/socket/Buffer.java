package com.master.test.io.socket;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.master.data.UnsignedByte;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import java.util.Random;

import com.master.net.IP;
import com.master.net.MAC;

public final class Buffer{

	private com.master.io.socket.Buffer buffer = null;
	
	@Before
	public void setupBuffer(){
		buffer = new com.master.io.socket.Buffer(1024);
	}
	
	@After
	public void cleanupBuffer(){
		try{
			buffer.close();
		}catch(final Exception e){}
	}
	
	@Test
	public void TestByte(){
		for (int i = 0; i < 256; i++){
			buffer.put(i);
		}
		buffer.position(0);
		for (int i = 0; i < 256; i++){
			assertEquals(i, buffer.get());
		}
	}

	@Test
	public void TestUnsignedByte(){
		for (int i = 0; i < 256; i++){
			buffer.put(i);
		}
		buffer.position(0);
		for (int i = 0; i < 256; i++){
			assertEquals(i, buffer.getAsUnsignedByte().get());
		}
	}

	@Test
	public void TestUnsignedByte2(){
		for (int i = 0; i < 256; i++){
			buffer.put(i);
		}
		buffer.position(0);
		for (int i = 0; i < 256; i++){
			assertEquals(i, buffer.getAsUnsignedByte().getAsShort());
		}
	}

	@Test
	public void TestUnsignedByte3(){
		for (int i = 0; i < 256; i++){
			buffer.put(new UnsignedByte(i));
		}
		buffer.position(0);
		for (int i = 0; i < 256; i++){
			assertEquals(i, buffer.getAsUnsignedByte().get());
		}
	}

	@Test
	public void TestUnsignedByte4(){
		for (int i = 0; i < 256; i++){
			buffer.put(new UnsignedByte(i));
		}
		buffer.position(0);
		for (int i = 0; i < 256; i++){
			assertEquals(i, buffer.getAsUnsignedByte().getAsShort());
		}
	}


	@Test
	public void TestByteAsByte(){
		for (int i = -128; i < 128; i++){
			buffer.put(i);
		}
		buffer.position(0);
		for (int i = -128; i < 128; i++){
			assertEquals(i, buffer.getAsByte());
		}
	}
	
	@Test
	public void TestSignedByte(){
		for (int i = -128; i < 128; i++){
			buffer.put(i);
		}
		buffer.position(0);
		for (int i = -128; i < 128; i++){
			assertEquals(i, buffer.get(true));
		}
	}
	
	@Test
	public void TestBoolean(){
		boolean testBool = true;
		buffer.putBoolean(testBool);
		buffer.position(0);
		assertEquals(testBool, buffer.getBoolean());
	}
	
	@Test
	public void TestInteger(){
		Random random = new Random();
		for(int i = 0; i < 100; i++){
			int value = random.nextInt(Integer.MAX_VALUE);
			if(i % 2 == 0){
				value = -value;
			}
			buffer.position(0);
			buffer.putInteger(value);
			buffer.position(0);
			assertEquals(value, buffer.getInteger());
		}
	}
	
	@Test
	public void TestDouble(){
		for(int i = 0; i < 100; i++){
			double testDouble = randomDouble(0.0,Double.MAX_VALUE);
			buffer.position(0);
			buffer.putDouble(testDouble);
			buffer.position(0);
			assertEquals(testDouble, buffer.getDouble(), 0);
		}
	}

	@Test
	public void TestLong(){
		Random random = new Random();
		for(long i = 0; i < 100; i++){
			long value = randomLong(0, Long.MAX_VALUE);
			if(i % 2 == 0){
				value = -value;
			}
			buffer.position(0);
			buffer.putLong(value);
			buffer.position(0);
			assertEquals(value, buffer.getLong());
		}
	}
	
	@Test
	public void TestMAC(){
		String mac = "FF:FF:FF:FF:FF:FF";
		buffer.putMAC(new MAC(mac));
		buffer.position(0);
		assertEquals(mac, buffer.getMAC().toString());
	}

	@Test
	public void TestIP(){
		String ip = "127.0.0.1";
		buffer.writeIP(new IP(ip));
		buffer.position(0);
		assertEquals(ip, buffer.getIP().toString());
	}

	@Test
	public void testString(){
		String str = "Test String 1@#0034 Test*nG";
		buffer.putString(str);
		buffer.position(0);
		assertEquals(str, buffer.getString());
	}

	@Test
	public void testStringArray() throws Exception{
		String[] str = new String[3];
		str[0] = "Test String 1@#0034 Test*nG";
		str[1] = "127.0.0.1";
		str[2] = "FF:FF:FF:FF:FF:FF";
		buffer.putStringArray(str, true);
		buffer.position(0);
		String[] values_2 = buffer.getStringArray(true);
		assertEquals(str.length, values_2.length);
		for (int i = 0; i < values_2.length; i++){
			assertEquals(str[i], values_2[i]);
		}
	}

	@Test
	public void testStringArray2() throws Exception{
		String[] str = new String[3];
		str[0] = "Test String 1@#0034 Test*nG";
		str[1] = null;
		str[2] = "FF:FF:FF:FF:FF:FF";
		buffer.putStringArray(str, true);
		buffer.position(0);
		String[] values_2 = buffer.getStringArray(true);
		assertEquals(str.length, values_2.length);
		for (int i = 0; i < values_2.length; i++){
			final String STR = str[i] == null ? "" : str[i];
			assertEquals(STR, values_2[i]);
		}
	}

	@Test
	public void testStringArray3() throws Exception{
		String[] str = new String[3];
		str[0] = "Test String 1@#0034 Test*nG";
		str[1] = null;
		str[2] = "FF:FF:FF:FF:FF:FF";
		buffer.putStringArray(str, true, true);
		buffer.position(0);
		String[] values_2 = buffer.getStringArray(true);
		assertNotEquals(str.length, values_2.length);
		assertEquals(str[0], values_2[0]);
		assertEquals(str[2], values_2[1]);
	}

	@Test
	public void testStringArray4() throws Exception{
		buffer = new com.master.io.socket.Buffer(2024);
		String[] str = new String[258];
		for(int i = 0; i < str.length; i++){
			str[i] = Integer.toString(i);
		}
		buffer.putStringArray(str, true);
		buffer.position(0);
		String[] values_2 = buffer.getStringArray(true);
		assertEquals(255, values_2.length);
		assertNotEquals(str.length, values_2.length);
		for (int i = 0; i < values_2.length; i++){
			assertEquals(str[i], values_2[i]);
		}
	}

	@Test
	public void testShort(){
		Random random = new Random();
		for(int i = 0; i < 100; i++){
			int value = random.nextInt(Short.MAX_VALUE);
			if(i % 2 == 0){
				value = -value;
			}
			buffer.position(0);
			buffer.putShort(value);
			buffer.position(0);
			assertEquals(value, buffer.getShort());
		}
	}

	@Test
	public void testShortArray() throws Exception{
		Random rnd1 = new Random();
		int[] values = new int[5];
		for (int i = 0; i < values.length; i++){
			values[0] = rnd1.nextInt(Short.MAX_VALUE);
		}
		buffer.putShortArray(values,true);
		buffer.position(0);
		int[] values_2 = buffer.getShortArray(true);
		assertEquals(values.length, values_2.length);
		for (int i = 0; i < values.length; i++){
			assertEquals(values[i], values_2[i]);
		}
	}

	@Test
	public void testShortArrayAsShort() throws Exception{
		Random rnd1 = new Random();
		short[] values = new short[5];
		for (int i = 0; i < values.length; i++){
			values[0] = (short)rnd1.nextInt(Short.MAX_VALUE);
		}
		buffer.putShortArray(values,true);
		buffer.position(0);
		short[] values_2 = buffer.getShortArrayAsShort(true);
		assertEquals(values.length, values_2.length);
		for (int i = 0; i < values.length; i++){
			assertEquals(values[i], values_2[i]);
		}
	}

	@Test
	public void testByteArray() throws Exception{
		Random rnd1 = new Random();
		int[] tmpBuffer = new int[255];
		for (int i = 0; i < tmpBuffer.length; i++){
			tmpBuffer[i] = (byte)rnd1.nextInt(Byte.MAX_VALUE);
		}
		buffer.putByteArray(tmpBuffer,true);
		buffer.position(0);
		int[] newBuffer = buffer.getByteArray(true);
		assertEquals(tmpBuffer.length, newBuffer.length);
		for (int i = 0; i < newBuffer.length; i++){
			assertEquals(tmpBuffer[i], newBuffer[i]);
		}
	}

	@Test
	public void testByteArrayAsBytes() throws Exception{
		Random rnd1 = new Random();
		byte[] tmpBuffer = new byte[255];
		for (int i = 0; i < tmpBuffer.length; i++){
			tmpBuffer[i] = (byte)rnd1.nextInt(Byte.MAX_VALUE);
		}
		buffer.putByteArray(tmpBuffer,true);
		buffer.position(0);
		byte[] newBuffer = buffer.getByteArrayAsBytes(true);
		assertEquals(tmpBuffer.length, newBuffer.length);
		for (int i = 0; i < newBuffer.length; i++){
			assertEquals(tmpBuffer[i], newBuffer[i]);
		}
	}

	@Test
	public void testIntegerArray() throws Exception{
		Random rnd1 = new Random();
		int[] values = new int[5];
		for (int i = 0; i < values.length; i++){
			values[0] = rnd1.nextInt(Integer.MAX_VALUE);
		}
		buffer.putIntegerArray(values,true);
		buffer.position(0);
		int [] values_2 = buffer.getIntegerArray(true);
		assertEquals(values.length, values_2.length);
		for (int i = 0; i < values.length; i++){
			assertEquals(values[i], values_2[i]);
		}
	}

	@Test
	public void testLongArray() throws Exception{
		long[] values = new long[5];
		for (int i = 0; i < values.length; i++){
			values[0] = nextLong(Long.MAX_VALUE);
		}
		buffer.putLongArray(values,true);
		buffer.position(0);
		long[] values_2 = buffer.getLongArray(true);
		assertEquals(values.length, values_2.length);
		for (int i = 0; i < values.length; i++){
			assertEquals(values[i], values_2[i]);
		}
	}
	
	private double randomDouble(final double MIN, final double MAX){
		return MIN + (MAX - MIN) * new Random().nextDouble();
	}
	
	private long randomLong(final long MIN, final long MAX){
		return MIN + (MAX - MIN) * new Random().nextLong();
	}
	
	private long nextLong(long n) {
		Random rng = new Random();
	   // error checking and 2^x checking removed for simplicity.
	   long bits, val;
	   do {
		  bits = (rng.nextLong() << 1) >>> 1;
		  val = bits % n;
	   } while (bits-val+(n-1) < 0L);
	   return val;
	}

}
