package com.master.test.data;

import com.master.data.ReversibleFloatArray;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by subasteve on 1/30/17.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleFloatArrayTests {

    private static Random rand = new Random();

    public static float randFloat() {
        float min = 1f, max = Float.MAX_VALUE;

        float randomNum = rand.nextInt(((int)max - (int)min) + 1) + min;
        randomNum += 0.99213;

        return randomNum;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @PrepareForTest({ReversibleFloatArray.class})
    public void testConstructor() throws Exception {
        ReversibleFloatArray expected = PowerMockito.mock(ReversibleFloatArray.class);
        PowerMockito.whenNew(ReversibleFloatArray.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleFloatArray.class})
    public void testConstructor1() throws Exception {
        ReversibleFloatArray expected = PowerMockito.mock(ReversibleFloatArray.class);
        PowerMockito.whenNew(ReversibleFloatArray.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleFloatArray.class})
    public void testConstructor2() throws Exception {
        ReversibleFloatArray expected = PowerMockito.mock(ReversibleFloatArray.class);
        PowerMockito.whenNew(ReversibleFloatArray.class).withArguments(new float[] {randFloat()}).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final float[] VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        assertEquals(VALUE,FLOAT.getValue());
        assertTrue(Arrays.equals(VALUE,FLOAT.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        assertEquals(null,FLOAT.getValue());
        assertEquals(null,FLOAT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        assertEquals(false,FLOAT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final float[] VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        FLOAT.setValue(VALUE);
        assertEquals(true,FLOAT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final float[] VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        FLOAT.setValue(VALUE).save();
        assertEquals(false,FLOAT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final float[] VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        FLOAT.setValue(VALUE);
        assertEquals(VALUE,FLOAT.getValue());
        assertEquals(null,FLOAT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final float[] VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        FLOAT.setValue(VALUE).reset();
        assertEquals(null,FLOAT.getValue());
        assertEquals(null,FLOAT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final float[] VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        FLOAT.setValue(VALUE).save();
        assertEquals(VALUE,FLOAT.getValue());
        assertTrue(Arrays.equals(VALUE,FLOAT.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final float[] VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        FLOAT.setValue(VALUE).save().reset();
        assertEquals(VALUE,FLOAT.getValue());
        assertTrue(Arrays.equals(VALUE,FLOAT.getLastValue()));
    }

    @Test
    public void testSetValue() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final float[] NEW_VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        FLOAT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,FLOAT.getValue());
        assertTrue(Arrays.equals(VALUE,FLOAT.getLastValue()));
    }

    @Test
    public void testIsModifiedByPosition() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        final float CHANGED_VALUE = randFloat();
        FLOAT.setValue(CHANGED_VALUE, 0);
        assertEquals(true,FLOAT.isModified(0));
        assertEquals(false,FLOAT.isModified(1));
    }

    @Test
    public void testIsModifiedByPositionNull() throws Exception {
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        thrown.expect(NullPointerException.class);
        FLOAT.isModified(0);
    }

    @Test
    public void testIsModifiedByPositionNegative() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        FLOAT.isModified(-1);
    }

    @Test
    public void testIsModifiedByPositionGreaterThanArray() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        FLOAT.isModified(4);
    }

    @Test
    public void testGetValueLength() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        assertEquals(VALUE.length,FLOAT.getValueLength());
    }

    @Test
    public void testGetValueLengthNull() throws Exception {
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        assertEquals(-1,FLOAT.getValueLength());
    }

    @Test
    public void testGetLastValueLength() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        assertEquals(VALUE.length,FLOAT.getLastValueLength());
    }

    @Test
    public void testGetLastValueLengthNull() throws Exception {
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        assertEquals(-1,FLOAT.getLastValueLength());
    }

    @Test
    public void testSetValueByPosition() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        final float CHANGED_VALUE = randFloat();
        FLOAT.setValue(CHANGED_VALUE, 0);
        assertEquals(CHANGED_VALUE,FLOAT.getValue(0),1);
        assertNotEquals(CHANGED_VALUE,FLOAT.getLastValue(0));
        assertEquals(VALUE[1],FLOAT.getValue(1),1);
        assertEquals(VALUE[1],FLOAT.getLastValue(1),1);
    }

    @Test
    public void testSetValueByPositionNull() throws Exception {
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        thrown.expect(NullPointerException.class);
        FLOAT.setValue(randFloat(), 0);
    }

    @Test
    public void testSetValueByPositionNegative() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        FLOAT.setValue(randFloat(), -1);
    }

    @Test
    public void testSetValueByPositionGreaterThanArray() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        FLOAT.setValue(randFloat(), 4);
    }

    @Test
    public void testGetValueByPosition() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        assertEquals(VALUE[0],FLOAT.getValue(0),1);
        assertEquals(VALUE[1],FLOAT.getValue(1),1);
        assertEquals(VALUE[2],FLOAT.getValue(2),1);
        assertEquals(VALUE[3],FLOAT.getValue(3),1);
    }

    @Test
    public void testGetValueByPositionNull() throws Exception {
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        thrown.expect(NullPointerException.class);
        FLOAT.getValue(0);
    }

    @Test
    public void testGetValueByPositionNegative() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        FLOAT.getValue(-1);
    }

    @Test
    public void testGetValueByPositionGreaterThanArray() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        FLOAT.getValue(4);
    }

    @Test
    public void testGetLastValueByPosition() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        assertEquals(VALUE[0],FLOAT.getLastValue(0),1);
        assertEquals(VALUE[1],FLOAT.getLastValue(1),1);
        assertEquals(VALUE[2],FLOAT.getLastValue(2),1);
        assertEquals(VALUE[3],FLOAT.getLastValue(3),1);
    }

    @Test
    public void testGetLastValueByPositionNull() throws Exception {
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray();
        thrown.expect(NullPointerException.class);
        FLOAT.getLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionNegative() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        FLOAT.getLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionGreaterThanArray() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat(),randFloat(),randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        FLOAT.getLastValue(4);
    }

    @Test
    public void testNotModified() throws Exception {
        final float[] VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        assertEquals(false,FLOAT.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final float[] NEW_VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        FLOAT.setValue(NEW_VALUE);
        assertEquals(true,FLOAT.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final float[] NEW_VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        FLOAT.setValue(NEW_VALUE).save();
        assertEquals(false,FLOAT.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final float[] NEW_VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        FLOAT.setValue(NEW_VALUE).reset();
        assertTrue(Arrays.equals(VALUE,FLOAT.getValue()));
        assertTrue(Arrays.equals(VALUE,FLOAT.getLastValue()));
    }

    @Test
    public void testSave() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final float[] NEW_VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        FLOAT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,FLOAT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,FLOAT.getLastValue()));
    }

    @Test
    public void testSaveNull() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final float[] NEW_VALUE = null;
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        FLOAT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,FLOAT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,FLOAT.getLastValue()));
    }

    @Test
    public void testSaveReset() throws Exception {
        final float[] VALUE = new float[] {randFloat(),randFloat()};
        final float[] NEW_VALUE = new float[] {randFloat()};
        final ReversibleFloatArray FLOAT = new ReversibleFloatArray(VALUE);
        FLOAT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,FLOAT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,FLOAT.getLastValue()));
    }
}
