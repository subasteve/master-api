package com.master.test.data;

import com.master.data.ReversibleBoolean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 8/30/16.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleBooleanTests {

    public static boolean randBoolean() {
        int min = 1, max = 2;

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum == 1;
    }

    @Test
    @PrepareForTest({ReversibleBoolean.class})
    public void testConstructor() throws Exception {
        ReversibleBoolean expected = PowerMockito.mock(ReversibleBoolean.class);
        PowerMockito.whenNew(ReversibleBoolean.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleBoolean.class})
    public void testConstructor1() throws Exception {
        ReversibleBoolean expected = PowerMockito.mock(ReversibleBoolean.class);
        PowerMockito.whenNew(ReversibleBoolean.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleBoolean.class})
    public void testConstructor2() throws Exception {
        ReversibleBoolean expected = PowerMockito.mock(ReversibleBoolean.class);
        PowerMockito.whenNew(ReversibleBoolean.class).withArguments(randBoolean()).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final boolean VALUE = randBoolean();
        final ReversibleBoolean BOOL = new ReversibleBoolean(VALUE);
        assertEquals(VALUE,BOOL.getValue());
        assertEquals(VALUE,BOOL.getLastValue());
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleBoolean BOOL = new ReversibleBoolean();
        assertEquals(false,BOOL.getValue());
        assertEquals(false,BOOL.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleBoolean BOOL = new ReversibleBoolean();
        assertEquals(false,BOOL.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final boolean NEW_VALUE = true;
        final ReversibleBoolean BOOL = new ReversibleBoolean();
        BOOL.setValue(NEW_VALUE);
        assertEquals(true,BOOL.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final boolean NEW_VALUE = true;
        final ReversibleBoolean BOOL = new ReversibleBoolean();
        BOOL.setValue(NEW_VALUE).save();
        assertEquals(false,BOOL.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final boolean NEW_VALUE = true;
        final ReversibleBoolean BOOL = new ReversibleBoolean();
        BOOL.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertEquals(false,BOOL.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final boolean NEW_VALUE = true;
        final ReversibleBoolean BOOL = new ReversibleBoolean();
        BOOL.setValue(NEW_VALUE).reset();
        assertEquals(false,BOOL.getValue());
        assertEquals(false,BOOL.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final boolean NEW_VALUE = true;
        final ReversibleBoolean BOOL = new ReversibleBoolean();
        BOOL.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertEquals(NEW_VALUE,BOOL.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final boolean NEW_VALUE = true;
        final ReversibleBoolean BOOL = new ReversibleBoolean();
        BOOL.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertEquals(NEW_VALUE,BOOL.getLastValue());
    }

    @Test
    public void testSetValue() throws Exception {
        final boolean VALUE = true;
        final boolean NEW_VALUE = false;
        final ReversibleBoolean BOOL = new ReversibleBoolean(VALUE);
        BOOL.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertEquals(VALUE,BOOL.getLastValue());
    }

    @Test
    public void testNotModified() throws Exception {
        final boolean VALUE = true;
        final ReversibleBoolean BOOL = new ReversibleBoolean(VALUE);
        assertEquals(false,BOOL.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final boolean VALUE = true;
        final boolean NEW_VALUE = false;
        final ReversibleBoolean BOOL = new ReversibleBoolean(VALUE);
        BOOL.setValue(NEW_VALUE);
        assertEquals(true,BOOL.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final boolean VALUE = true;
        final boolean NEW_VALUE = false;
        final ReversibleBoolean BOOL = new ReversibleBoolean(VALUE);
        BOOL.setValue(NEW_VALUE).save();
        assertEquals(false,BOOL.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final boolean VALUE = true;
        final boolean NEW_VALUE = false;
        final ReversibleBoolean BOOL = new ReversibleBoolean(VALUE);
        BOOL.setValue(NEW_VALUE).reset();
        assertEquals(VALUE,BOOL.getValue());
        assertEquals(VALUE,BOOL.getLastValue());
    }

    @Test
    public void testSave() throws Exception {
        final boolean VALUE = true;
        final boolean NEW_VALUE = false;
        final ReversibleBoolean BOOL = new ReversibleBoolean(VALUE);
        BOOL.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertEquals(NEW_VALUE,BOOL.getLastValue());
    }

    @Test
    public void testSaveReset() throws Exception {
        final boolean VALUE = true;
        final boolean NEW_VALUE = false;
        final ReversibleBoolean BOOL = new ReversibleBoolean(VALUE);
        BOOL.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertEquals(NEW_VALUE,BOOL.getLastValue());
    }
}
