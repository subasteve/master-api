package com.master.test.data;

import com.master.data.ReversibleByte;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 8/30/16.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleByteTests {

    public static byte randByte(byte min, byte max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return (byte) randomNum;
    }

    @Test
    @PrepareForTest({ReversibleByte.class})
    public void testConstructor() throws Exception {
        ReversibleByte expected = PowerMockito.mock(ReversibleByte.class);
        PowerMockito.whenNew(ReversibleByte.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByte.class})
    public void testConstructor1() throws Exception {
        ReversibleByte expected = PowerMockito.mock(ReversibleByte.class);
        PowerMockito.whenNew(ReversibleByte.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByte.class})
    public void testConstructor2() throws Exception {
        ReversibleByte expected = PowerMockito.mock(ReversibleByte.class);
        PowerMockito.whenNew(ReversibleByte.class).withArguments(randByte((byte)1,Byte.MAX_VALUE)).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final byte VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        assertEquals(VALUE,BYTE.getValue());
        assertEquals(VALUE,BYTE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleByte BYTE = new ReversibleByte();
        assertEquals(-1,BYTE.getValue());
        assertEquals(-1,BYTE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleByte BYTE = new ReversibleByte();
        assertEquals(false,BYTE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte();
        BYTE.setValue(NEW_VALUE);
        assertEquals(true,BYTE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte();
        BYTE.setValue(NEW_VALUE).save();
        assertEquals(false,BYTE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte();
        BYTE.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertEquals(-1,BYTE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte();
        BYTE.setValue(NEW_VALUE).reset();
        assertEquals(-1,BYTE.getValue());
        assertEquals(-1,BYTE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte();
        BYTE.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertEquals(NEW_VALUE,BYTE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte();
        BYTE.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertEquals(NEW_VALUE,BYTE.getLastValue());
    }

    @Test
    public void testSetValue() throws Exception {
        final byte VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        BYTE.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertEquals(VALUE,BYTE.getLastValue());
    }

    @Test
    public void testUnsignedValue() throws Exception {
        final byte VALUE = (byte)220;
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        //Doesn't need to be set to read/write unsigned value
        //Just used to let user know what one should be used
        assertEquals(false, BYTE.isUnsigned());
        assertEquals(220,BYTE.getUnsignedValue());
        assertEquals(220,BYTE.getUnsignedLastValue());
    }

    @Test
    public void testUnsignedSet() throws Exception {
        final byte VALUE = (byte)220;
        final ReversibleByte BYTE = new ReversibleByte(VALUE,true);
        assertEquals(true, BYTE.isUnsigned());
        assertEquals(220,BYTE.getUnsignedValue());
        assertEquals(220,BYTE.getUnsignedLastValue());
    }

    @Test
    public void testUnsignedSet2() throws Exception {
        final byte VALUE = (byte)220;
        final ReversibleByte BYTE = new ReversibleByte(true);
        BYTE.setValue(VALUE).save();
        assertEquals(true, BYTE.isUnsigned());
        assertEquals(220,BYTE.getUnsignedValue());
        assertEquals(220,BYTE.getUnsignedLastValue());
    }

    @Test
    public void testNotModified() throws Exception {
        final byte VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        assertEquals(false,BYTE.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final byte VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        BYTE.setValue(NEW_VALUE);
        assertEquals(true,BYTE.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final byte VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        BYTE.setValue(NEW_VALUE).save();
        assertEquals(false,BYTE.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final byte VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        BYTE.setValue(NEW_VALUE).reset();
        assertEquals(VALUE,BYTE.getValue());
        assertEquals(VALUE,BYTE.getLastValue());
    }

    @Test
    public void testSave() throws Exception {
        final byte VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        BYTE.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertEquals(NEW_VALUE,BYTE.getLastValue());
    }

    @Test
    public void testSaveReset() throws Exception {
        final byte VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final byte NEW_VALUE = randByte((byte)1,Byte.MAX_VALUE);
        final ReversibleByte BYTE = new ReversibleByte(VALUE);
        BYTE.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertEquals(NEW_VALUE,BYTE.getLastValue());
    }
}
