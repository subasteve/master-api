package com.master.test.data;

import com.master.data.ReversibleShortArray;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by subasteve on 1/30/17.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleShortArrayTests {

    private static Random rand = new Random();

    public static short randShort() {
        int min = 1, max = Short.MAX_VALUE;
        
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return (short) randomNum;
    }

    public static boolean randBoolean() {
        int min = 1, max = 2;

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum == 1;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @PrepareForTest({ReversibleShortArray.class})
    public void testConstructor() throws Exception {
        ReversibleShortArray expected = PowerMockito.mock(ReversibleShortArray.class);
        PowerMockito.whenNew(ReversibleShortArray.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleShortArray.class})
    public void testConstructor1() throws Exception {
        ReversibleShortArray expected = PowerMockito.mock(ReversibleShortArray.class);
        PowerMockito.whenNew(ReversibleShortArray.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleShortArray.class})
    public void testConstructor2() throws Exception {
        ReversibleShortArray expected = PowerMockito.mock(ReversibleShortArray.class);
        PowerMockito.whenNew(ReversibleShortArray.class).withArguments(new short[] {randShort()}).thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleShortArray.class})
    public void testConstructor3() throws Exception {
        ReversibleShortArray expected = PowerMockito.mock(ReversibleShortArray.class);
        PowerMockito.whenNew(ReversibleShortArray.class).withArguments(new short[] {randShort()},randBoolean()).thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleShortArray.class})
    public void testConstructor4() throws Exception {
        ReversibleShortArray expected = PowerMockito.mock(ReversibleShortArray.class);
        PowerMockito.whenNew(ReversibleShortArray.class).withArguments(randBoolean()).thenReturn(expected);
    }


    @Test
    public void testConstructorWithValue() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertEquals(VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testConstructorIsNotUnsigned() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        assertFalse(SHORT.isUnsigned());
    }

    @Test
    public void testConstructorWithValueIsNotUnsigned() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertFalse(SHORT.isUnsigned());
        assertEquals(VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testConstructorWithUnsigned() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray(true);
        assertTrue(SHORT.isUnsigned());
    }

    @Test
    public void testConstructorWithValueAndUnsigned() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE,true);
        assertTrue(SHORT.isUnsigned());
        assertEquals(VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        assertEquals(null,SHORT.getValue());
        assertEquals(null,SHORT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        assertEquals(false,SHORT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        SHORT.setValue(VALUE);
        assertEquals(true,SHORT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        SHORT.setValue(VALUE).save();
        assertEquals(false,SHORT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        SHORT.setValue(VALUE);
        assertEquals(VALUE,SHORT.getValue());
        assertEquals(null,SHORT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        SHORT.setValue(VALUE).reset();
        assertEquals(null,SHORT.getValue());
        assertEquals(null,SHORT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        SHORT.setValue(VALUE).save();
        assertEquals(VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        SHORT.setValue(VALUE).save().reset();
        assertEquals(VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testSetValue() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final short[] NEW_VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        SHORT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testIsModifiedByPosition() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        final short CHANGED_VALUE = randShort();
        SHORT.setValue(CHANGED_VALUE, 0);
        assertEquals(true,SHORT.isModified(0));
        assertEquals(false,SHORT.isModified(1));
    }

    @Test
    public void testIsModifiedByPositionNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        thrown.expect(NullPointerException.class);
        SHORT.isModified(0);
    }

    @Test
    public void testIsModifiedByPositionNegative() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.isModified(-1);
    }

    @Test
    public void testIsModifiedByPositionGreaterThanArray() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.isModified(4);
    }

    @Test
    public void testGetValueLength() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertEquals(VALUE.length,SHORT.getValueLength());
    }

    @Test
    public void testGetValueLengthNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        assertEquals(-1,SHORT.getValueLength());
    }

    @Test
    public void testGetLastValueLength() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertEquals(VALUE.length,SHORT.getLastValueLength());
    }

    @Test
    public void testGetLastValueLengthNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        assertEquals(-1,SHORT.getLastValueLength());
    }

    @Test
    public void testSetValueByPosition() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        final short CHANGED_VALUE = randShort();
        SHORT.setValue(CHANGED_VALUE, 0);
        assertEquals(CHANGED_VALUE,SHORT.getValue(0));
        assertNotEquals(CHANGED_VALUE,SHORT.getLastValue(0));
        assertEquals(VALUE[1],SHORT.getValue(1));
        assertEquals(VALUE[1],SHORT.getLastValue(1));
    }

    @Test
    public void testGetValueByPositionUnsigned() throws Exception {
        final short[] VALUE = new short[] {(short)64535,(short)65435};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertEquals(VALUE[0],SHORT.getValue(0));
        assertNotEquals(VALUE[0],SHORT.getUnsignedValue(0));
        assertEquals(64535,SHORT.getUnsignedValue(0));
        assertEquals(VALUE[1],SHORT.getValue(1));
        assertNotEquals(VALUE[1],SHORT.getUnsignedValue(1));
        assertEquals(65435,SHORT.getUnsignedValue(1));
    }

    @Test
    public void testGetLastValueByPositionUnsigned() throws Exception {
        final short[] VALUE = new short[] {(short)64535,(short)65435};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertEquals(VALUE[0],SHORT.getLastValue(0));
        assertNotEquals(VALUE[0],SHORT.getUnsignedLastValue(0));
        assertEquals(64535,SHORT.getUnsignedLastValue(0));
        assertEquals(VALUE[1],SHORT.getLastValue(1));
        assertNotEquals(VALUE[1],SHORT.getUnsignedLastValue(1));
        assertEquals(65435,SHORT.getUnsignedLastValue(1));
    }

    @Test
    public void testGetValueByPositionUnsignedNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        thrown.expect(NullPointerException.class);
        SHORT.getUnsignedValue(0);
    }

    @Test
    public void testGetValueByPositionUnsignedNegative() throws Exception {
        final short[] VALUE = new short[] {(short)64535,(short)65435};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.getUnsignedValue(-1);
    }

    @Test
    public void testGetValueByPositionUnsignedGreaterThanArray() throws Exception {
        final short[] VALUE = new short[] {(short)64535,(short)65435};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.getUnsignedValue(3);
    }


    @Test
    public void testGetLastValueByPositionUnsignedNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        thrown.expect(NullPointerException.class);
        SHORT.getUnsignedLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionUnsignedNegative() throws Exception {
        final short[] VALUE = new short[] {(short)64535,(short)65435};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.getUnsignedLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionUnsignedGreaterThanArray() throws Exception {
        final short[] VALUE = new short[] {(short)64535,(short)65435};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.getUnsignedLastValue(3);
    }

    @Test
    public void testGetValueUnsigned() throws Exception {
        final short[] VALUE = new short[] {(short)64535,(short)65435};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        final int[] UNSIGNED_VALUE = SHORT.getUnsignedValue();
        assertNotEquals(UNSIGNED_VALUE[0],SHORT.getValue(0));
        assertEquals(UNSIGNED_VALUE[0],SHORT.getUnsignedValue(0));
        assertNotEquals(UNSIGNED_VALUE[1],SHORT.getValue(1));
        assertEquals(UNSIGNED_VALUE[1],SHORT.getUnsignedValue(1));
    }

    @Test
    public void testGetLastValueUnsigned() throws Exception {
        final short[] VALUE = new short[] {(short)64535,(short)65435};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        final int[] UNSIGNED_LAST_VALUE = SHORT.getUnsignedLastValue();
        assertNotEquals(UNSIGNED_LAST_VALUE[0],SHORT.getLastValue(0));
        assertEquals(UNSIGNED_LAST_VALUE[0],SHORT.getUnsignedLastValue(0));
        assertNotEquals(UNSIGNED_LAST_VALUE[1],SHORT.getLastValue(1));
        assertEquals(UNSIGNED_LAST_VALUE[1],SHORT.getUnsignedLastValue(1));
    }

    @Test
    public void testGetValueUnsignedNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        final int[] UNSIGNED_VALUE = SHORT.getUnsignedValue();
        assertEquals(null,UNSIGNED_VALUE);
    }


    @Test
    public void testGetLastValueUnsignedNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        final int[] UNSIGNED_LAST_VALUE = SHORT.getUnsignedLastValue();
        assertEquals(null,UNSIGNED_LAST_VALUE);
    }

    @Test
    public void testSetValueByPositionNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        thrown.expect(NullPointerException.class);
        SHORT.setValue(randShort(), 0);
    }

    @Test
    public void testSetValueByPositionNegative() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.setValue(randShort(), -1);
    }

    @Test
    public void testSetValueByPositionGreaterThanArray() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.setValue(randShort(), 4);
    }

    @Test
    public void testGetValueByPosition() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertEquals(VALUE[0],SHORT.getValue(0));
        assertEquals(VALUE[1],SHORT.getValue(1));
        assertEquals(VALUE[2],SHORT.getValue(2));
        assertEquals(VALUE[3],SHORT.getValue(3));
    }

    @Test
    public void testGetValueByPositionNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        thrown.expect(NullPointerException.class);
        SHORT.getValue(0);
    }

    @Test
    public void testGetValueByPositionNegative() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.getValue(-1);
    }

    @Test
    public void testGetValueByPositionGreaterThanArray() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.getValue(4);
    }

    @Test
    public void testGetLastValueByPosition() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertEquals(VALUE[0],SHORT.getLastValue(0));
        assertEquals(VALUE[1],SHORT.getLastValue(1));
        assertEquals(VALUE[2],SHORT.getLastValue(2));
        assertEquals(VALUE[3],SHORT.getLastValue(3));
    }

    @Test
    public void testGetLastValueByPositionNull() throws Exception {
        final ReversibleShortArray SHORT = new ReversibleShortArray();
        thrown.expect(NullPointerException.class);
        SHORT.getLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionNegative() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.getLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionGreaterThanArray() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort(),randShort(),randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        SHORT.getLastValue(4);
    }

    @Test
    public void testNotModified() throws Exception {
        final short[] VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        assertEquals(false,SHORT.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final short[] NEW_VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        SHORT.setValue(NEW_VALUE);
        assertEquals(true,SHORT.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final short[] NEW_VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        SHORT.setValue(NEW_VALUE).save();
        assertEquals(false,SHORT.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final short[] NEW_VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        SHORT.setValue(NEW_VALUE).reset();
        assertTrue(Arrays.equals(VALUE,SHORT.getValue()));
        assertTrue(Arrays.equals(VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testSave() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final short[] NEW_VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        SHORT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testSaveNull() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final short[] NEW_VALUE = null;
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        SHORT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,SHORT.getLastValue()));
    }

    @Test
    public void testSaveReset() throws Exception {
        final short[] VALUE = new short[] {randShort(),randShort()};
        final short[] NEW_VALUE = new short[] {randShort()};
        final ReversibleShortArray SHORT = new ReversibleShortArray(VALUE);
        SHORT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,SHORT.getLastValue()));
    }
}
