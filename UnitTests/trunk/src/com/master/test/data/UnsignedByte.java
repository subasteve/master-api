package com.master.test.data;

import com.master.data.ReversibleByte;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 11/10/16.
 */
public class UnsignedByte {

    private static Random rand = new Random();


    public static byte randByte(byte min, byte max) {
        return (byte) randInt((int)min,(int)max);
    }

    public static short randShort(short min, short max) {
        return (short) randInt((int)min,(int)max);
    }

    public static int randInt(int min, int max) {
        return rand.nextInt((max - min) + 1) + min;
    }

    @Test
    @PrepareForTest({com.master.data.UnsignedByte.class})
    public void testConstructor() throws Exception {
        com.master.data.UnsignedByte expected = PowerMockito.mock(com.master.data.UnsignedByte.class);
        PowerMockito.whenNew(com.master.data.UnsignedByte.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({com.master.data.UnsignedByte.class})
    public void testConstructor1() throws Exception {
        com.master.data.UnsignedByte expected = PowerMockito.mock(com.master.data.UnsignedByte.class);
        PowerMockito.whenNew(com.master.data.UnsignedByte.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByte.class})
    public void testConstructor2() throws Exception {
        com.master.data.UnsignedByte expected = PowerMockito.mock(com.master.data.UnsignedByte.class);
        PowerMockito.whenNew(com.master.data.UnsignedByte.class).withArguments(randShort((short)1,(short)255)).thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByte.class})
    public void testConstructor3() throws Exception {
        com.master.data.UnsignedByte expected = PowerMockito.mock(com.master.data.UnsignedByte.class);
        PowerMockito.whenNew(com.master.data.UnsignedByte.class).withArguments(randInt(1,255)).thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByte.class})
    public void testConstructor4() throws Exception {
        com.master.data.UnsignedByte expected = PowerMockito.mock(com.master.data.UnsignedByte.class);
        PowerMockito.whenNew(com.master.data.UnsignedByte.class).withArguments(randByte((byte)1,(byte)127)).thenReturn(expected);
    }

    @Test
    public void testConstructorSetCorrectValue() throws Exception {
        final short VALUE = randShort((short)1,(short)255);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.get());
    }

    @Test
    public void testConstructorSetCorrectValue2() throws Exception {
        final short VALUE = randShort((short)1,(short)255);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void testConstructor2SetCorrectValue() throws Exception {
        final int VALUE = randInt(1,255);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.get());
    }

    @Test
    public void testConstructor2SetCorrectValue2() throws Exception {
        final int VALUE = randInt(1,255);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsShort());
    }
    @Test
    public void testConstructor3SetCorrectValue() throws Exception {
        final byte VALUE = randByte((byte)1,(byte)127);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void testConstructor3SetCorrectValue2() throws Exception {
        final byte VALUE = randByte((byte)1,(byte)127);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.get());
    }


    @Test
    public void testConstructor3SetCorrectValue3() throws Exception {
        final byte VALUE = (byte) 255;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(255,UNSIGNED_BYTE.get());
    }

    @Test
    public void testConstructor3SetCorrectValue4() throws Exception {
        final byte VALUE = (byte) 255;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(255,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void testConstructor3SetCorrectValue5() throws Exception {
        final byte VALUE = (byte) 255;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsByte());
    }

    @Test
    public void testConstructor3SetCorrectValue6() throws Exception {
        final byte VALUE = randByte((byte)1,(byte)127);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsByte());
    }

    @Test
    public void testSetCorrectValue() throws Exception {
        final short VALUE = randShort((short)1,(short)255);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.get());
    }

    @Test
    public void testSetCorrectValue2() throws Exception {
        final short VALUE = randShort((short)1,(short)255);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void test2SetCorrectValue() throws Exception {
        final int VALUE = randInt(1,255);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.get());
    }


    @Test
    public void test2SetCorrectValue2() throws Exception {
        final int VALUE = randInt(1,255);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void test3SetCorrectValue() throws Exception {
        final byte VALUE = randByte((byte)1,(byte)127);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.get());
    }

    @Test
    public void test3SetCorrectValue2() throws Exception {
        final byte VALUE = randByte((byte)1,(byte)127);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void test3SetCorrectValue3() throws Exception {
        final byte VALUE = (byte) 255;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(255,UNSIGNED_BYTE.get());
    }

    @Test
    public void test3SetCorrectValue4() throws Exception {
        final byte VALUE = (byte) 255;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(255,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void test3SetCorrectValue5() throws Exception {
        final byte VALUE = (byte) 255;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsByte());
    }

    @Test
    public void test3SetCorrectValue6() throws Exception {
        final byte VALUE = randByte((byte)1,(byte)127);
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(VALUE,UNSIGNED_BYTE.getAsByte());
    }

    @Test
    public void testDefaultValues() throws Exception {
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        assertEquals(0,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void testDefaultValues2() throws Exception {
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        assertEquals(0,UNSIGNED_BYTE.get());
    }

    @Test
    public void testDefaultValues3() throws Exception {
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        assertEquals(0,UNSIGNED_BYTE.getAsByte());
    }

    @Test
    public void testSetNegativeValue() throws Exception {
        final short VALUE = -55;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(0,UNSIGNED_BYTE.get());
    }

    @Test
    public void testSetNegativeValue2() throws Exception {
        final short VALUE = -55;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(0,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void testSetNegativeValue3() throws Exception {
        final short VALUE = -55;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(0,UNSIGNED_BYTE.get());
    }

    @Test
    public void testSetNegativeValue4() throws Exception {
        final short VALUE = -55;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(0,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void test2SetNegativeValue() throws Exception {
        final int VALUE = -55;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(0,UNSIGNED_BYTE.get());
    }

    @Test
    public void test2SetNegativeValue2() throws Exception {
        final int VALUE = -55;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte();
        UNSIGNED_BYTE.set(VALUE);
        assertEquals(0,UNSIGNED_BYTE.getAsShort());
    }

    @Test
    public void test2SetNegativeValue3() throws Exception {
        final int VALUE = -55;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(0,UNSIGNED_BYTE.get());
    }

    @Test
    public void test2SetNegativeValue4() throws Exception {
        final int VALUE = -55;
        com.master.data.UnsignedByte UNSIGNED_BYTE = new com.master.data.UnsignedByte(VALUE);
        assertEquals(0,UNSIGNED_BYTE.getAsShort());
    }

}
