package com.master.test.data;

import com.master.data.ReversibleDoubleArray;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by subasteve on 1/30/17.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleDoubleArrayTests {

    private static Random rand = new Random();

    public static double randDouble() {
        double min = 1, max = Double.MAX_VALUE;

        double randomNum = rand.nextInt(((int)max - (int)min) + 1) + min;
        randomNum += 0.99213;

        return randomNum;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @PrepareForTest({ReversibleDoubleArray.class})
    public void testConstructor() throws Exception {
        ReversibleDoubleArray expected = PowerMockito.mock(ReversibleDoubleArray.class);
        PowerMockito.whenNew(ReversibleDoubleArray.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleDoubleArray.class})
    public void testConstructor1() throws Exception {
        ReversibleDoubleArray expected = PowerMockito.mock(ReversibleDoubleArray.class);
        PowerMockito.whenNew(ReversibleDoubleArray.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleDoubleArray.class})
    public void testConstructor2() throws Exception {
        ReversibleDoubleArray expected = PowerMockito.mock(ReversibleDoubleArray.class);
        PowerMockito.whenNew(ReversibleDoubleArray.class).withArguments(new double[] {randDouble()}).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final double[] VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        assertEquals(VALUE,DOUBLE.getValue());
        assertTrue(Arrays.equals(VALUE,DOUBLE.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        assertEquals(null,DOUBLE.getValue());
        assertEquals(null,DOUBLE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        assertEquals(false,DOUBLE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final double[] VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        DOUBLE.setValue(VALUE);
        assertEquals(true,DOUBLE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final double[] VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        DOUBLE.setValue(VALUE).save();
        assertEquals(false,DOUBLE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final double[] VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        DOUBLE.setValue(VALUE);
        assertEquals(VALUE,DOUBLE.getValue());
        assertEquals(null,DOUBLE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final double[] VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        DOUBLE.setValue(VALUE).reset();
        assertEquals(null,DOUBLE.getValue());
        assertEquals(null,DOUBLE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final double[] VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        DOUBLE.setValue(VALUE).save();
        assertEquals(VALUE,DOUBLE.getValue());
        assertTrue(Arrays.equals(VALUE,DOUBLE.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final double[] VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        DOUBLE.setValue(VALUE).save().reset();
        assertEquals(VALUE,DOUBLE.getValue());
        assertTrue(Arrays.equals(VALUE,DOUBLE.getLastValue()));
    }

    @Test
    public void testSetValue() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final double[] NEW_VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        DOUBLE.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,DOUBLE.getValue());
        assertTrue(Arrays.equals(VALUE,DOUBLE.getLastValue()));
    }

    @Test
    public void testIsModifiedByPosition() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        final double CHANGED_VALUE = randDouble();
        DOUBLE.setValue(CHANGED_VALUE, 0);
        assertEquals(true,DOUBLE.isModified(0));
        assertEquals(false,DOUBLE.isModified(1));
    }

    @Test
    public void testIsModifiedByPositionNull() throws Exception {
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        thrown.expect(NullPointerException.class);
        DOUBLE.isModified(0);
    }

    @Test
    public void testIsModifiedByPositionNegative() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        DOUBLE.isModified(-1);
    }

    @Test
    public void testIsModifiedByPositionGreaterThanArray() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        DOUBLE.isModified(4);
    }

    @Test
    public void testGetValueLength() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        assertEquals(VALUE.length,DOUBLE.getValueLength());
    }

    @Test
    public void testGetValueLengthNull() throws Exception {
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        assertEquals(-1,DOUBLE.getValueLength());
    }

    @Test
    public void testGetLastValueLength() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        assertEquals(VALUE.length,DOUBLE.getLastValueLength());
    }

    @Test
    public void testGetLastValueLengthNull() throws Exception {
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        assertEquals(-1,DOUBLE.getLastValueLength());
    }

    @Test
    public void testSetValueByPosition() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        final double CHANGED_VALUE = randDouble();
        DOUBLE.setValue(CHANGED_VALUE, 0);
        assertEquals(CHANGED_VALUE,DOUBLE.getValue(0),1);
        assertNotEquals(CHANGED_VALUE,DOUBLE.getLastValue(0));
        assertEquals(VALUE[1],DOUBLE.getValue(1),1);
        assertEquals(VALUE[1],DOUBLE.getLastValue(1),1);
    }

    @Test
    public void testSetValueByPositionNull() throws Exception {
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        thrown.expect(NullPointerException.class);
        DOUBLE.setValue(randDouble(), 0);
    }

    @Test
    public void testSetValueByPositionNegative() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        DOUBLE.setValue(randDouble(), -1);
    }

    @Test
    public void testSetValueByPositionGreaterThanArray() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        DOUBLE.setValue(randDouble(), 4);
    }

    @Test
    public void testGetValueByPosition() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        assertEquals(VALUE[0],DOUBLE.getValue(0),1);
        assertEquals(VALUE[1],DOUBLE.getValue(1),1);
        assertEquals(VALUE[2],DOUBLE.getValue(2),1);
        assertEquals(VALUE[3],DOUBLE.getValue(3),1);
    }

    @Test
    public void testGetValueByPositionNull() throws Exception {
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        thrown.expect(NullPointerException.class);
        DOUBLE.getValue(0);
    }

    @Test
    public void testGetValueByPositionNegative() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        DOUBLE.getValue(-1);
    }

    @Test
    public void testGetValueByPositionGreaterThanArray() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        DOUBLE.getValue(4);
    }

    @Test
    public void testGetLastValueByPosition() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        assertEquals(VALUE[0],DOUBLE.getLastValue(0),1);
        assertEquals(VALUE[1],DOUBLE.getLastValue(1),1);
        assertEquals(VALUE[2],DOUBLE.getLastValue(2),1);
        assertEquals(VALUE[3],DOUBLE.getLastValue(3),1);
    }

    @Test
    public void testGetLastValueByPositionNull() throws Exception {
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray();
        thrown.expect(NullPointerException.class);
        DOUBLE.getLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionNegative() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        DOUBLE.getLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionGreaterThanArray() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble(),randDouble(),randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        DOUBLE.getLastValue(4);
    }

    @Test
    public void testNotModified() throws Exception {
        final double[] VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        assertEquals(false,DOUBLE.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final double[] NEW_VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        DOUBLE.setValue(NEW_VALUE);
        assertEquals(true,DOUBLE.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final double[] NEW_VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        DOUBLE.setValue(NEW_VALUE).save();
        assertEquals(false,DOUBLE.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final double[] NEW_VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        DOUBLE.setValue(NEW_VALUE).reset();
        assertTrue(Arrays.equals(VALUE,DOUBLE.getValue()));
        assertTrue(Arrays.equals(VALUE,DOUBLE.getLastValue()));
    }

    @Test
    public void testSave() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final double[] NEW_VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        DOUBLE.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,DOUBLE.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,DOUBLE.getLastValue()));
    }

    @Test
    public void testSaveNull() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final double[] NEW_VALUE = null;
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        DOUBLE.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,DOUBLE.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,DOUBLE.getLastValue()));
    }

    @Test
    public void testSaveReset() throws Exception {
        final double[] VALUE = new double[] {randDouble(),randDouble()};
        final double[] NEW_VALUE = new double[] {randDouble()};
        final ReversibleDoubleArray DOUBLE = new ReversibleDoubleArray(VALUE);
        DOUBLE.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,DOUBLE.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,DOUBLE.getLastValue()));
    }
}
