package com.master.test.data;

import com.master.data.ReversibleFloat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 8/30/16.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleFloatTests {

    private static Random rand = new Random();

    public static float randFloat() {
        float min = 1f, max = Float.MAX_VALUE;

        float randomNum = rand.nextInt(((int)max - (int)min) + 1) + min;
        randomNum += 0.99213;

        return randomNum;
    }

    @Test
    @PrepareForTest({ReversibleFloat.class})
    public void testConstructor() throws Exception {
        ReversibleFloat expected = PowerMockito.mock(ReversibleFloat.class);
        PowerMockito.whenNew(ReversibleFloat.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleFloat.class})
    public void testConstructor1() throws Exception {
        ReversibleFloat expected = PowerMockito.mock(ReversibleFloat.class);
        PowerMockito.whenNew(ReversibleFloat.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleFloat.class})
    public void testConstructor2() throws Exception {
        ReversibleFloat expected = PowerMockito.mock(ReversibleFloat.class);
        PowerMockito.whenNew(ReversibleFloat.class).withArguments(randFloat()).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final float VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat(VALUE);
        assertEquals(VALUE,INT.getValue(),0.1);
        assertEquals(VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleFloat INT = new ReversibleFloat();
        assertEquals(-1,INT.getValue(),0.1);
        assertEquals(-1,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleFloat INT = new ReversibleFloat();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat();
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat();
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat();
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(-1,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat();
        INT.setValue(NEW_VALUE).reset();
        assertEquals(-1,INT.getValue(),0.1);
        assertEquals(-1,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat();
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(NEW_VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat();
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(NEW_VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testSetValue() throws Exception {
        final float VALUE = randFloat();
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testNotModified() throws Exception {
        final float VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat(VALUE);
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final float VALUE = randFloat();
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final float VALUE = randFloat();
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final float VALUE = randFloat();
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat(VALUE);
        INT.setValue(NEW_VALUE).reset();
        assertEquals(VALUE,INT.getValue(),0.1);
        assertEquals(VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testSave() throws Exception {
        final float VALUE = randFloat();
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(NEW_VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testSaveReset() throws Exception {
        final float VALUE = randFloat();
        final float NEW_VALUE = randFloat();
        final ReversibleFloat INT = new ReversibleFloat(VALUE);
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(NEW_VALUE,INT.getLastValue(),0.1);
    }
}
