package com.master.test.data;

import com.master.data.ReversibleLongArray;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by subasteve on 1/30/17.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleLongArrayTests {

    private static Random rand = new Random();

    public static long randLong() {
        int min = 1, max = Integer.MAX_VALUE;
        
        long randomNum = rand.nextInt((max - min) + 1) + min;
        long randomNum2 = rand.nextInt((max - min) + 1) + min;
        long randomNum3 = randomNum + randomNum2;


        return randomNum3;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @PrepareForTest({ReversibleLongArray.class})
    public void testConstructor() throws Exception {
        ReversibleLongArray expected = PowerMockito.mock(ReversibleLongArray.class);
        PowerMockito.whenNew(ReversibleLongArray.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleLongArray.class})
    public void testConstructor1() throws Exception {
        ReversibleLongArray expected = PowerMockito.mock(ReversibleLongArray.class);
        PowerMockito.whenNew(ReversibleLongArray.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleLongArray.class})
    public void testConstructor2() throws Exception {
        ReversibleLongArray expected = PowerMockito.mock(ReversibleLongArray.class);
        PowerMockito.whenNew(ReversibleLongArray.class).withArguments(new long[] {randLong()}).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final long[] VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        assertEquals(VALUE,LONG.getValue());
        assertTrue(Arrays.equals(VALUE,LONG.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleLongArray LONG = new ReversibleLongArray();
        assertEquals(null,LONG.getValue());
        assertEquals(null,LONG.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleLongArray LONG = new ReversibleLongArray();
        assertEquals(false,LONG.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final long[] VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray();
        LONG.setValue(VALUE);
        assertEquals(true,LONG.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final long[] VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray();
        LONG.setValue(VALUE).save();
        assertEquals(false,LONG.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final long[] VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray();
        LONG.setValue(VALUE);
        assertEquals(VALUE,LONG.getValue());
        assertEquals(null,LONG.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final long[] VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray();
        LONG.setValue(VALUE).reset();
        assertEquals(null,LONG.getValue());
        assertEquals(null,LONG.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final long[] VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray();
        LONG.setValue(VALUE).save();
        assertEquals(VALUE,LONG.getValue());
        assertTrue(Arrays.equals(VALUE,LONG.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final long[] VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray();
        LONG.setValue(VALUE).save().reset();
        assertEquals(VALUE,LONG.getValue());
        assertTrue(Arrays.equals(VALUE,LONG.getLastValue()));
    }

    @Test
    public void testSetValue() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final long[] NEW_VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        LONG.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,LONG.getValue());
        assertTrue(Arrays.equals(VALUE,LONG.getLastValue()));
    }

    @Test
    public void testIsModifiedByPosition() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        final long CHANGED_VALUE = randLong();
        LONG.setValue(CHANGED_VALUE, 0);
        assertEquals(true,LONG.isModified(0));
        assertEquals(false,LONG.isModified(1));
    }

    @Test
    public void testIsModifiedByPositionNull() throws Exception {
        final ReversibleLongArray LONG = new ReversibleLongArray();
        thrown.expect(NullPointerException.class);
        LONG.isModified(0);
    }

    @Test
    public void testIsModifiedByPositionNegative() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        LONG.isModified(-1);
    }

    @Test
    public void testIsModifiedByPositionGreaterThanArray() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        LONG.isModified(4);
    }

    @Test
    public void testGetValueLength() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        assertEquals(VALUE.length,LONG.getValueLength());
    }

    @Test
    public void testGetValueLengthNull() throws Exception {
        final ReversibleLongArray LONG = new ReversibleLongArray();
        assertEquals(-1,LONG.getValueLength());
    }

    @Test
    public void testGetLastValueLength() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        assertEquals(VALUE.length,LONG.getLastValueLength());
    }

    @Test
    public void testGetLastValueLengthNull() throws Exception {
        final ReversibleLongArray LONG = new ReversibleLongArray();
        assertEquals(-1,LONG.getLastValueLength());
    }

    @Test
    public void testSetValueByPosition() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        final long CHANGED_VALUE = randLong();
        LONG.setValue(CHANGED_VALUE, 0);
        assertEquals(CHANGED_VALUE,LONG.getValue(0));
        assertNotEquals(CHANGED_VALUE,LONG.getLastValue(0));
        assertEquals(VALUE[1],LONG.getValue(1));
        assertEquals(VALUE[1],LONG.getLastValue(1));
    }

    @Test
    public void testSetValueByPositionNull() throws Exception {
        final ReversibleLongArray LONG = new ReversibleLongArray();
        thrown.expect(NullPointerException.class);
        LONG.setValue(randLong(), 0);
    }

    @Test
    public void testSetValueByPositionNegative() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        LONG.setValue(randLong(), -1);
    }

    @Test
    public void testSetValueByPositionGreaterThanArray() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        LONG.setValue(randLong(), 4);
    }

    @Test
    public void testGetValueByPosition() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        assertEquals(VALUE[0],LONG.getValue(0));
        assertEquals(VALUE[1],LONG.getValue(1));
        assertEquals(VALUE[2],LONG.getValue(2));
        assertEquals(VALUE[3],LONG.getValue(3));
    }

    @Test
    public void testGetValueByPositionNull() throws Exception {
        final ReversibleLongArray LONG = new ReversibleLongArray();
        thrown.expect(NullPointerException.class);
        LONG.getValue(0);
    }

    @Test
    public void testGetValueByPositionNegative() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        LONG.getValue(-1);
    }

    @Test
    public void testGetValueByPositionGreaterThanArray() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        LONG.getValue(4);
    }

    @Test
    public void testGetLastValueByPosition() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        assertEquals(VALUE[0],LONG.getLastValue(0));
        assertEquals(VALUE[1],LONG.getLastValue(1));
        assertEquals(VALUE[2],LONG.getLastValue(2));
        assertEquals(VALUE[3],LONG.getLastValue(3));
    }

    @Test
    public void testGetLastValueByPositionNull() throws Exception {
        final ReversibleLongArray LONG = new ReversibleLongArray();
        thrown.expect(NullPointerException.class);
        LONG.getLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionNegative() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        LONG.getLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionGreaterThanArray() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong(),randLong(),randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        LONG.getLastValue(4);
    }

    @Test
    public void testNotModified() throws Exception {
        final long[] VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        assertEquals(false,LONG.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final long[] NEW_VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        LONG.setValue(NEW_VALUE);
        assertEquals(true,LONG.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final long[] NEW_VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        LONG.setValue(NEW_VALUE).save();
        assertEquals(false,LONG.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final long[] NEW_VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        LONG.setValue(NEW_VALUE).reset();
        assertTrue(Arrays.equals(VALUE,LONG.getValue()));
        assertTrue(Arrays.equals(VALUE,LONG.getLastValue()));
    }

    @Test
    public void testSave() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final long[] NEW_VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        LONG.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,LONG.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,LONG.getLastValue()));
    }

    @Test
    public void testSaveNull() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final long[] NEW_VALUE = null;
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        LONG.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,LONG.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,LONG.getLastValue()));
    }

    @Test
    public void testSaveReset() throws Exception {
        final long[] VALUE = new long[] {randLong(),randLong()};
        final long[] NEW_VALUE = new long[] {randLong()};
        final ReversibleLongArray LONG = new ReversibleLongArray(VALUE);
        LONG.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,LONG.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,LONG.getLastValue()));
    }
}
