package com.master.test.data;

import com.master.data.ReversibleBooleanArray;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by subasteve on 1/30/17.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleBooleanArrayTests {

    public static boolean randBoolean() {
        int min = 1, max = 2;

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum == 1;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @PrepareForTest({ReversibleBooleanArray.class})
    public void testConstructor() throws Exception {
        ReversibleBooleanArray expected = PowerMockito.mock(ReversibleBooleanArray.class);
        PowerMockito.whenNew(ReversibleBooleanArray.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleBooleanArray.class})
    public void testConstructor1() throws Exception {
        ReversibleBooleanArray expected = PowerMockito.mock(ReversibleBooleanArray.class);
        PowerMockito.whenNew(ReversibleBooleanArray.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleBooleanArray.class})
    public void testConstructor2() throws Exception {
        ReversibleBooleanArray expected = PowerMockito.mock(ReversibleBooleanArray.class);
        PowerMockito.whenNew(ReversibleBooleanArray.class).withArguments(new boolean[] {randBoolean()}).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        assertEquals(VALUE,BOOL.getValue());
        assertTrue(Arrays.equals(VALUE,BOOL.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        assertEquals(null,BOOL.getValue());
        assertEquals(null,BOOL.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        assertEquals(false,BOOL.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        BOOL.setValue(VALUE);
        assertEquals(true,BOOL.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        BOOL.setValue(VALUE).save();
        assertEquals(false,BOOL.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        BOOL.setValue(VALUE);
        assertEquals(VALUE,BOOL.getValue());
        assertEquals(null,BOOL.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        BOOL.setValue(VALUE).reset();
        assertEquals(null,BOOL.getValue());
        assertEquals(null,BOOL.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        BOOL.setValue(VALUE).save();
        assertEquals(VALUE,BOOL.getValue());
        assertTrue(Arrays.equals(VALUE,BOOL.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        BOOL.setValue(VALUE).save().reset();
        assertEquals(VALUE,BOOL.getValue());
        assertTrue(Arrays.equals(VALUE,BOOL.getLastValue()));
    }

    @Test
    public void testSetValue() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final boolean[] NEW_VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        BOOL.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertTrue(Arrays.equals(VALUE,BOOL.getLastValue()));
    }

    @Test
    public void testIsModifiedByPosition() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        final boolean CHANGED_VALUE = !VALUE[0];
        BOOL.setValue(CHANGED_VALUE, 0);
        assertEquals(true,BOOL.isModified(0));
        assertEquals(false,BOOL.isModified(1));
    }

    @Test
    public void testIsModifiedByPositionNull() throws Exception {
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        thrown.expect(NullPointerException.class);
        BOOL.isModified(0);
    }

    @Test
    public void testIsModifiedByPositionNegative() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BOOL.isModified(-1);
    }

    @Test
    public void testIsModifiedByPositionGreaterThanArray() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BOOL.isModified(4);
    }

    @Test
    public void testGetValueLength() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        assertEquals(VALUE.length,BOOL.getValueLength());
    }

    @Test
    public void testGetValueLengthNull() throws Exception {
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        assertEquals(-1,BOOL.getValueLength());
    }

    @Test
    public void testGetLastValueLength() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        assertEquals(VALUE.length,BOOL.getLastValueLength());
    }

    @Test
    public void testGetLastValueLengthNull() throws Exception {
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        assertEquals(-1,BOOL.getLastValueLength());
    }

    @Test
    public void testSetValueByPosition() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        final boolean CHANGED_VALUE = !VALUE[0];
        BOOL.setValue(CHANGED_VALUE, 0);
        assertEquals(CHANGED_VALUE,BOOL.getValue(0));
        assertEquals(!CHANGED_VALUE,BOOL.getLastValue(0));
        assertEquals(VALUE[1],BOOL.getValue(1));
        assertEquals(VALUE[1],BOOL.getLastValue(1));
    }

    @Test
    public void testSetValueByPositionNull() throws Exception {
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        thrown.expect(NullPointerException.class);
        BOOL.setValue(randBoolean(), 0);
    }

    @Test
    public void testSetValueByPositionNegative() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BOOL.setValue(randBoolean(), -1);
    }

    @Test
    public void testSetValueByPositionGreaterThanArray() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BOOL.setValue(randBoolean(), 4);
    }

    @Test
    public void testGetValueByPosition() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        assertEquals(VALUE[0],BOOL.getValue(0));
        assertEquals(VALUE[1],BOOL.getValue(1));
        assertEquals(VALUE[2],BOOL.getValue(2));
        assertEquals(VALUE[3],BOOL.getValue(3));
    }

    @Test
    public void testGetValueByPositionNull() throws Exception {
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        thrown.expect(NullPointerException.class);
        BOOL.getValue(0);
    }

    @Test
    public void testGetValueByPositionNegative() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BOOL.getValue(-1);
    }

    @Test
    public void testGetValueByPositionGreaterThanArray() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BOOL.getValue(4);
    }

    @Test
    public void testGetLastValueByPosition() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        assertEquals(VALUE[0],BOOL.getLastValue(0));
        assertEquals(VALUE[1],BOOL.getLastValue(1));
        assertEquals(VALUE[2],BOOL.getLastValue(2));
        assertEquals(VALUE[3],BOOL.getLastValue(3));
    }

    @Test
    public void testGetLastValueByPositionNull() throws Exception {
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray();
        thrown.expect(NullPointerException.class);
        BOOL.getLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionNegative() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BOOL.getLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionGreaterThanArray() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean(),randBoolean(),randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BOOL.getLastValue(4);
    }

    @Test
    public void testNotModified() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        assertEquals(false,BOOL.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final boolean[] NEW_VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        BOOL.setValue(NEW_VALUE);
        assertEquals(true,BOOL.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final boolean[] NEW_VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        BOOL.setValue(NEW_VALUE).save();
        assertEquals(false,BOOL.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final boolean[] NEW_VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        BOOL.setValue(NEW_VALUE).reset();
        assertTrue(Arrays.equals(VALUE,BOOL.getValue()));
        assertTrue(Arrays.equals(VALUE,BOOL.getLastValue()));
    }

    @Test
    public void testSave() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final boolean[] NEW_VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        BOOL.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,BOOL.getLastValue()));
    }

    @Test
    public void testSaveNull() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final boolean[] NEW_VALUE = null;
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        BOOL.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,BOOL.getLastValue()));
    }

    @Test
    public void testSaveReset() throws Exception {
        final boolean[] VALUE = new boolean[] {randBoolean(),randBoolean()};
        final boolean[] NEW_VALUE = new boolean[] {randBoolean()};
        final ReversibleBooleanArray BOOL = new ReversibleBooleanArray(VALUE);
        BOOL.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,BOOL.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,BOOL.getLastValue()));
    }
}
