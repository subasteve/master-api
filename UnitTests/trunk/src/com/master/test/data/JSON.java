package com.master.test.data;

import com.master.data.*;
import com.master.data.interfaces.UnhandledField;
import com.master.util.ProcessTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Subasteve on 7/10/2016.
 */
public class JSON implements UnhandledField {

    StringBuilder buffer;
    com.master.data.JSON jsonObj, jsonObj2;
    final ProcessTime TIMER = new ProcessTime();
    private List<Field> revertStringFields;
    private List<Field> revertShortFields;
    private List<Field> revertLongFields;
    private List<Field> revertFloatFields;
    private List<Field> revertDoubleFields;
    private List<Field> revertBooleanFields;
    private List<Field> revertByteFields;
    private List<Field> revertIntFields;

    private static final String EXPECTED_JSON = "{\"anInt\":1,\"anBoolean\":true,\"anShort\":3,\"anDouble\":4.4,\"anFloat\":5.5,\"anLong\":6,\"anByte\":7,\"anString\":\"str8\",\"revertInt\":{\"value\":9,\"lastValue\":9},\"revertByte\":{\"value\":10,\"lastValue\":10,\"UNSIGNED\":false},\"revertBoolean\":{\"value\":true,\"lastValue\":true},\"revertDouble\":{\"value\":12.12,\"lastValue\":12.12},\"revertFloat\":{\"value\":13.13,\"lastValue\":13.13},\"revertLong\":{\"value\":14,\"lastValue\":14},\"revertShort\":{\"value\":15,\"lastValue\":15,\"UNSIGNED\":false},\"revertString\":{\"value\":\"str16\",\"lastValue\":\"str16\"}}";
    private static final String EXPECTED_JSON_SIMPLE = "{\"anInt\":1,\"anBoolean\":true,\"anShort\":3,\"anDouble\":4.4,\"anFloat\":5.5,\"anLong\":6,\"anByte\":7,\"anString\":\"str8\"}";

    @Ignore
    public class JSONTest {
        int anInt = 1;
        boolean anBoolean = true;
        short anShort = 3;
        double anDouble = 4.4;
        float anFloat = 5.5f;
        long anLong = 6;
        byte anByte = 7;
        String anString = "str8";
        ReversibleInt revertInt = new ReversibleInt(9);
        ReversibleByte revertByte = new ReversibleByte((byte)10);
        ReversibleBoolean revertBoolean = new ReversibleBoolean(true);
        ReversibleDouble revertDouble = new ReversibleDouble(12.12);
        ReversibleFloat revertFloat = new ReversibleFloat(13.13f);
        ReversibleLong revertLong = new ReversibleLong(14);
        ReversibleShort revertShort = new ReversibleShort((short)15);
        ReversibleString revertString = new ReversibleString("str16");
    }

    @Before
    public void Setup(){
        final JSONTest TEST = new JSONTest();
        buffer = new StringBuilder();
        jsonObj = new com.master.data.JSON(buffer);
        jsonObj2 = new com.master.data.JSON(buffer, TEST);
        revertStringFields = jsonObj.setupFields(TEST.revertString);
        revertShortFields = jsonObj.setupFields(TEST.revertShort);
        revertLongFields = jsonObj.setupFields(TEST.revertLong);
        revertFloatFields = jsonObj.setupFields(TEST.revertFloat);
        revertDoubleFields = jsonObj.setupFields(TEST.revertDouble);
        revertBooleanFields = jsonObj.setupFields(TEST.revertBoolean);
        revertByteFields = jsonObj.setupFields(TEST.revertByte);
        revertIntFields = jsonObj.setupFields(TEST.revertInt);
    }

    @Test
    public void TestToJson() throws Exception {
        TIMER.start();
        buffer.append("{");
        jsonObj.createField(buffer, "anInt", 1, true);
        jsonObj.createField(buffer, "anBoolean", true, true);
        jsonObj.createField(buffer, "anShort", (short)3, true);
        jsonObj.createField(buffer, "anDouble", 4.4, true);
        jsonObj.createField(buffer, "anFloat", 5.5f, true);
        jsonObj.createField(buffer, "anLong", (long)6,true);
        jsonObj.createField(buffer, "anByte", (byte)7,true);
        jsonObj.createField(buffer, "anString", "str8");
        buffer.append("}");
        TIMER.stop();
        TIMER.writeProcessingTime();

        final String JSON = buffer.toString();
        System.out.println(JSON);

        assertEquals(EXPECTED_JSON_SIMPLE, JSON);
    }

    @Test
    public void TestToJson2() throws Exception {
        TIMER.start();
        buffer.append("{");
        jsonObj.createField("anInt", 1, true);
        jsonObj.createField("anBoolean", true, true);
        jsonObj.createField("anShort", (short)3, true);
        jsonObj.createField("anDouble", 4.4, true);
        jsonObj.createField("anFloat", 5.5f, true);
        jsonObj.createField("anLong", (long)6,true);
        jsonObj.createField("anByte", (byte)7,true);
        jsonObj.createField("anString", "str8");
        buffer.append("}");
        TIMER.stop();
        TIMER.writeProcessingTime();

        final String JSON = buffer.toString();
        System.out.println(JSON);

        assertEquals(EXPECTED_JSON_SIMPLE, JSON);
    }

    @Test
    public void TestToJson3() throws Exception {
        TIMER.start();
        jsonObj2.toJSON(this);
        TIMER.stop();
        TIMER.writeProcessingTime();

        final String JSON = buffer.toString();
        System.out.println(JSON);

        assertEquals(EXPECTED_JSON, JSON);
    }

    @Test
    public void TestToJson4() throws Exception {
        TIMER.start();
        jsonObj2.toJSON();
        TIMER.stop();
        TIMER.writeProcessingTime();

        final String JSON = buffer.toString();
        System.out.println(JSON);

        assertEquals(EXPECTED_JSON_SIMPLE, JSON);
    }

    @Override
    public boolean unhandledField(final Appendable PAGE, final Field FIELD, final Object CLASS_OBJ, final boolean FIRST) {
        if (FIELD.getType() == ReversibleString.class) {
            try {
                final ReversibleString REVERT_STRING = (ReversibleString) FIELD.get(CLASS_OBJ);
                if (!FIRST) {
                    PAGE.append(",");
                }
                PAGE.append("\"").append(FIELD.getName()).append("\":");
                jsonObj.toJSON(PAGE, REVERT_STRING, revertStringFields);
                return true;
            }catch (final Exception E){}
        }
        if (FIELD.getType() == ReversibleShort.class) {
            try {
                final ReversibleShort REVERT_SHORT = (ReversibleShort) FIELD.get(CLASS_OBJ);
                if (!FIRST) {
                    PAGE.append(",");
                }
                PAGE.append("\"").append(FIELD.getName()).append("\":");
                jsonObj.toJSON(PAGE, REVERT_SHORT, revertShortFields);
                return true;
            }catch (final Exception E){}
        }
        if (FIELD.getType() == ReversibleLong.class) {
            try {
                final ReversibleLong REVERT_LONG = (ReversibleLong) FIELD.get(CLASS_OBJ);
                if (!FIRST) {
                    PAGE.append(",");
                }
                PAGE.append("\"").append(FIELD.getName()).append("\":");
                jsonObj.toJSON(PAGE, REVERT_LONG, revertLongFields);
                return true;
            }catch (final Exception E){}
        }
        if (FIELD.getType() == ReversibleInt.class) {
            try {
                final ReversibleInt REVERT_INT = (ReversibleInt) FIELD.get(CLASS_OBJ);
                if (!FIRST) {
                    PAGE.append(",");
                }
                PAGE.append("\"").append(FIELD.getName()).append("\":");
                jsonObj.toJSON(PAGE, REVERT_INT, revertIntFields);
                return true;
            }catch (final Exception E){}
        }
        if (FIELD.getType() == ReversibleFloat.class) {
            try {
                final ReversibleFloat REVERT_FLOAT = (ReversibleFloat) FIELD.get(CLASS_OBJ);
                if (!FIRST) {
                    PAGE.append(",");
                }
                PAGE.append("\"").append(FIELD.getName()).append("\":");
                jsonObj.toJSON(PAGE, REVERT_FLOAT, revertFloatFields);
                return true;
            }catch (final Exception E){}
        }
        if (FIELD.getType() == ReversibleDouble.class) {
            try {
                final ReversibleDouble REVERT_DOUBLE = (ReversibleDouble) FIELD.get(CLASS_OBJ);
                if (!FIRST) {
                    PAGE.append(",");
                }
                PAGE.append("\"").append(FIELD.getName()).append("\":");
                jsonObj.toJSON(PAGE, REVERT_DOUBLE, revertDoubleFields);
                return true;
            }catch (final Exception E){}
        }
        if (FIELD.getType() == ReversibleByte.class) {
            try {
                final ReversibleByte REVERT_BYTE = (ReversibleByte) FIELD.get(CLASS_OBJ);
                if (!FIRST) {
                    PAGE.append(",");
                }
                PAGE.append("\"").append(FIELD.getName()).append("\":");
                jsonObj.toJSON(PAGE, REVERT_BYTE, revertByteFields);
                return true;
            }catch (final Exception E){}
        }
        if (FIELD.getType() == ReversibleBoolean.class) {
            try {
                final ReversibleBoolean REVERT_BOOLEAN = (ReversibleBoolean) FIELD.get(CLASS_OBJ);
                if (!FIRST) {
                    PAGE.append(",");
                }
                PAGE.append("\"").append(FIELD.getName()).append("\":");
                jsonObj.toJSON(PAGE, REVERT_BOOLEAN, revertBooleanFields);
                return true;
            }catch (final Exception E){}
        }
        return false;
    }
}
