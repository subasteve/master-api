package com.master.test.data;

import com.master.data.ReversibleDouble;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 8/30/16.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleDoubleTests {

    private static Random rand = new Random();

    public static double randDouble() {
        double min = 1, max = Double.MAX_VALUE;

        double randomNum = rand.nextInt(((int)max - (int)min) + 1) + min;
        randomNum += 0.99213;

        return randomNum;
    }

    @Test
    @PrepareForTest({ReversibleDouble.class})
    public void testConstructor() throws Exception {
        ReversibleDouble expected = PowerMockito.mock(ReversibleDouble.class);
        PowerMockito.whenNew(ReversibleDouble.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleDouble.class})
    public void testConstructor1() throws Exception {
        ReversibleDouble expected = PowerMockito.mock(ReversibleDouble.class);
        PowerMockito.whenNew(ReversibleDouble.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleDouble.class})
    public void testConstructor2() throws Exception {
        ReversibleDouble expected = PowerMockito.mock(ReversibleDouble.class);
        PowerMockito.whenNew(ReversibleDouble.class).withArguments(randDouble()).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final double VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble(VALUE);
        assertEquals(VALUE,INT.getValue(),0.1);
        assertEquals(VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleDouble INT = new ReversibleDouble();
        assertEquals(-1,INT.getValue(),0.1);
        assertEquals(-1,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleDouble INT = new ReversibleDouble();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble();
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble();
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble();
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(-1,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble();
        INT.setValue(NEW_VALUE).reset();
        assertEquals(-1,INT.getValue(),0.1);
        assertEquals(-1,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble();
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(NEW_VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble();
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(NEW_VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testSetValue() throws Exception {
        final double VALUE = randDouble();
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testNotModified() throws Exception {
        final double VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble(VALUE);
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final double VALUE = randDouble();
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final double VALUE = randDouble();
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final double VALUE = randDouble();
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble(VALUE);
        INT.setValue(NEW_VALUE).reset();
        assertEquals(VALUE,INT.getValue(),0.1);
        assertEquals(VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testSave() throws Exception {
        final double VALUE = randDouble();
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(NEW_VALUE,INT.getLastValue(),0.1);
    }

    @Test
    public void testSaveReset() throws Exception {
        final double VALUE = randDouble();
        final double NEW_VALUE = randDouble();
        final ReversibleDouble INT = new ReversibleDouble(VALUE);
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue(),0.1);
        assertEquals(NEW_VALUE,INT.getLastValue(),0.1);
    }
}
