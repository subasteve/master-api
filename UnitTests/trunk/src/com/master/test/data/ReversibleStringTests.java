package com.master.test.data;

import com.master.data.ReversibleString;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 8/30/16.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleStringTests {

    private static Random rand = new Random();

    public static String randString() {
        int min = 1, max = Integer.MAX_VALUE;

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return Integer.toString(randomNum);
    }

    @Test
    @PrepareForTest({ReversibleString.class})
    public void testConstructor() throws Exception {
        ReversibleString expected = PowerMockito.mock(ReversibleString.class);
        PowerMockito.whenNew(ReversibleString.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleString.class})
    public void testConstructor1() throws Exception {
        ReversibleString expected = PowerMockito.mock(ReversibleString.class);
        PowerMockito.whenNew(ReversibleString.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleString.class})
    public void testConstructor2() throws Exception {
        ReversibleString expected = PowerMockito.mock(ReversibleString.class);
        PowerMockito.whenNew(ReversibleString.class).withArguments(randString()).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final String VALUE = randString();
        final ReversibleString INT = new ReversibleString(VALUE);
        assertEquals(VALUE,INT.getValue());
        assertEquals(VALUE,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleString INT = new ReversibleString();
        assertEquals(null,INT.getValue());
        assertEquals(null,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleString INT = new ReversibleString();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString();
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString();
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString();
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(null,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString();
        INT.setValue(NEW_VALUE).reset();
        assertEquals(null,INT.getValue());
        assertEquals(null,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString();
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(NEW_VALUE,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString();
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(NEW_VALUE,INT.getLastValue());
    }

    @Test
    public void testSetValue() throws Exception {
        final String VALUE = randString();
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(VALUE,INT.getLastValue());
    }

    @Test
    public void testNotModified() throws Exception {
        final String VALUE = randString();
        final ReversibleString INT = new ReversibleString(VALUE);
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final String VALUE = randString();
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final String VALUE = randString();
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final String VALUE = randString();
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString(VALUE);
        INT.setValue(NEW_VALUE).reset();
        assertEquals(VALUE,INT.getValue());
        assertEquals(VALUE,INT.getLastValue());
    }

    @Test
    public void testSave() throws Exception {
        final String VALUE = randString();
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(NEW_VALUE,INT.getLastValue());
    }

    @Test
    public void testSaveReset() throws Exception {
        final String VALUE = randString();
        final String NEW_VALUE = randString();
        final ReversibleString INT = new ReversibleString(VALUE);
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(NEW_VALUE,INT.getLastValue());
    }
}
