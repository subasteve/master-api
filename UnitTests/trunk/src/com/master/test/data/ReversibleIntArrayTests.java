package com.master.test.data;

import com.master.data.ReversibleIntArray;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by subasteve on 1/30/17.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleIntArrayTests {

    private static Random rand = new Random();

    public static int randInt() {
        int min = 1, max = Integer.MAX_VALUE;

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @PrepareForTest({ReversibleIntArray.class})
    public void testConstructor() throws Exception {
        ReversibleIntArray expected = PowerMockito.mock(ReversibleIntArray.class);
        PowerMockito.whenNew(ReversibleIntArray.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleIntArray.class})
    public void testConstructor1() throws Exception {
        ReversibleIntArray expected = PowerMockito.mock(ReversibleIntArray.class);
        PowerMockito.whenNew(ReversibleIntArray.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleIntArray.class})
    public void testConstructor2() throws Exception {
        ReversibleIntArray expected = PowerMockito.mock(ReversibleIntArray.class);
        PowerMockito.whenNew(ReversibleIntArray.class).withArguments(new int[] {randInt()}).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final int[] VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        assertEquals(VALUE,INT.getValue());
        assertTrue(Arrays.equals(VALUE,INT.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleIntArray INT = new ReversibleIntArray();
        assertEquals(null,INT.getValue());
        assertEquals(null,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleIntArray INT = new ReversibleIntArray();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final int[] VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray();
        INT.setValue(VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final int[] VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray();
        INT.setValue(VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final int[] VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray();
        INT.setValue(VALUE);
        assertEquals(VALUE,INT.getValue());
        assertEquals(null,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final int[] VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray();
        INT.setValue(VALUE).reset();
        assertEquals(null,INT.getValue());
        assertEquals(null,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final int[] VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray();
        INT.setValue(VALUE).save();
        assertEquals(VALUE,INT.getValue());
        assertTrue(Arrays.equals(VALUE,INT.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final int[] VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray();
        INT.setValue(VALUE).save().reset();
        assertEquals(VALUE,INT.getValue());
        assertTrue(Arrays.equals(VALUE,INT.getLastValue()));
    }

    @Test
    public void testSetValue() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final int[] NEW_VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue());
        assertTrue(Arrays.equals(VALUE,INT.getLastValue()));
    }

    @Test
    public void testIsModifiedByPosition() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        final int CHANGED_VALUE = randInt();
        INT.setValue(CHANGED_VALUE, 0);
        assertEquals(true,INT.isModified(0));
        assertEquals(false,INT.isModified(1));
    }

    @Test
    public void testIsModifiedByPositionNull() throws Exception {
        final ReversibleIntArray INT = new ReversibleIntArray();
        thrown.expect(NullPointerException.class);
        INT.isModified(0);
    }

    @Test
    public void testIsModifiedByPositionNegative() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        INT.isModified(-1);
    }

    @Test
    public void testIsModifiedByPositionGreaterThanArray() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        INT.isModified(4);
    }

    @Test
    public void testGetValueLength() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        assertEquals(VALUE.length,INT.getValueLength());
    }

    @Test
    public void testGetValueLengthNull() throws Exception {
        final ReversibleIntArray INT = new ReversibleIntArray();
        assertEquals(-1,INT.getValueLength());
    }

    @Test
    public void testGetLastValueLength() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        assertEquals(VALUE.length,INT.getLastValueLength());
    }

    @Test
    public void testGetLastValueLengthNull() throws Exception {
        final ReversibleIntArray INT = new ReversibleIntArray();
        assertEquals(-1,INT.getLastValueLength());
    }

    @Test
    public void testSetValueByPosition() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        final int CHANGED_VALUE = randInt();
        INT.setValue(CHANGED_VALUE, 0);
        assertEquals(CHANGED_VALUE,INT.getValue(0));
        assertNotEquals(CHANGED_VALUE,INT.getLastValue(0));
        assertEquals(VALUE[1],INT.getValue(1));
        assertEquals(VALUE[1],INT.getLastValue(1));
    }

    @Test
    public void testSetValueByPositionNull() throws Exception {
        final ReversibleIntArray INT = new ReversibleIntArray();
        thrown.expect(NullPointerException.class);
        INT.setValue(randInt(), 0);
    }

    @Test
    public void testSetValueByPositionNegative() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        INT.setValue(randInt(), -1);
    }

    @Test
    public void testSetValueByPositionGreaterThanArray() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        INT.setValue(randInt(), 4);
    }

    @Test
    public void testGetValueByPosition() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        assertEquals(VALUE[0],INT.getValue(0));
        assertEquals(VALUE[1],INT.getValue(1));
        assertEquals(VALUE[2],INT.getValue(2));
        assertEquals(VALUE[3],INT.getValue(3));
    }

    @Test
    public void testGetValueByPositionNull() throws Exception {
        final ReversibleIntArray INT = new ReversibleIntArray();
        thrown.expect(NullPointerException.class);
        INT.getValue(0);
    }

    @Test
    public void testGetValueByPositionNegative() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        INT.getValue(-1);
    }

    @Test
    public void testGetValueByPositionGreaterThanArray() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        INT.getValue(4);
    }

    @Test
    public void testGetLastValueByPosition() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        assertEquals(VALUE[0],INT.getLastValue(0));
        assertEquals(VALUE[1],INT.getLastValue(1));
        assertEquals(VALUE[2],INT.getLastValue(2));
        assertEquals(VALUE[3],INT.getLastValue(3));
    }

    @Test
    public void testGetLastValueByPositionNull() throws Exception {
        final ReversibleIntArray INT = new ReversibleIntArray();
        thrown.expect(NullPointerException.class);
        INT.getLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionNegative() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        INT.getLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionGreaterThanArray() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt(),randInt(),randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        INT.getLastValue(4);
    }

    @Test
    public void testNotModified() throws Exception {
        final int[] VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final int[] NEW_VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final int[] NEW_VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final int[] NEW_VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        INT.setValue(NEW_VALUE).reset();
        assertTrue(Arrays.equals(VALUE,INT.getValue()));
        assertTrue(Arrays.equals(VALUE,INT.getLastValue()));
    }

    @Test
    public void testSave() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final int[] NEW_VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,INT.getLastValue()));
    }

    @Test
    public void testSaveNull() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final int[] NEW_VALUE = null;
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,INT.getLastValue()));
    }

    @Test
    public void testSaveReset() throws Exception {
        final int[] VALUE = new int[] {randInt(),randInt()};
        final int[] NEW_VALUE = new int[] {randInt()};
        final ReversibleIntArray INT = new ReversibleIntArray(VALUE);
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,INT.getLastValue()));
    }
}
