package com.master.test.data;

import com.master.data.ReversibleShort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 8/30/16.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleShortTests {

    private static Random rand = new Random();

    public static short randShort(short min, short max) {
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return (short) randomNum;
    }

    @Test
    @PrepareForTest({ReversibleShort.class})
    public void testConstructor() throws Exception {
        ReversibleShort expected = PowerMockito.mock(ReversibleShort.class);
        PowerMockito.whenNew(ReversibleShort.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleShort.class})
    public void testConstructor1() throws Exception {
        ReversibleShort expected = PowerMockito.mock(ReversibleShort.class);
        PowerMockito.whenNew(ReversibleShort.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleShort.class})
    public void testConstructor2() throws Exception {
        ReversibleShort expected = PowerMockito.mock(ReversibleShort.class);
        PowerMockito.whenNew(ReversibleShort.class).withArguments(randShort((short)1,Short.MAX_VALUE)).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final short VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        assertEquals(VALUE,SHORT.getValue());
        assertEquals(VALUE,SHORT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleShort SHORT = new ReversibleShort();
        assertEquals(-1,SHORT.getValue());
        assertEquals(-1,SHORT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleShort SHORT = new ReversibleShort();
        assertEquals(false,SHORT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort();
        SHORT.setValue(NEW_VALUE);
        assertEquals(true,SHORT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort();
        SHORT.setValue(NEW_VALUE).save();
        assertEquals(false,SHORT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort();
        SHORT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertEquals(-1,SHORT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort();
        SHORT.setValue(NEW_VALUE).reset();
        assertEquals(-1,SHORT.getValue());
        assertEquals(-1,SHORT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort();
        SHORT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertEquals(NEW_VALUE,SHORT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort();
        SHORT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertEquals(NEW_VALUE,SHORT.getLastValue());
    }

    @Test
    public void testSetValue() throws Exception {
        final short VALUE = randShort((short)1,Short.MAX_VALUE);
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        SHORT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertEquals(VALUE,SHORT.getLastValue());
    }

    @Test
    public void testUnsignedValue() throws Exception {
        final short VALUE = (short)65035;
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        //Doesn't need to be set to read/write unsigned value
        //Just used to let user know what one should be used
        assertEquals(false, SHORT.isUnsigned());
        assertEquals(65035,SHORT.getUnsignedValue());
        assertEquals(65035,SHORT.getUnsignedLastValue());
    }

    @Test
    public void testUnsignedSet() throws Exception {
        final short VALUE = (short)65035;
        final ReversibleShort SHORT = new ReversibleShort(VALUE,true);
        assertEquals(true, SHORT.isUnsigned());
        assertEquals(65035,SHORT.getUnsignedValue());
        assertEquals(65035,SHORT.getUnsignedLastValue());
    }

    @Test
    public void testUnsignedSet2() throws Exception {
        final short VALUE = (short)65035;
        final ReversibleShort SHORT = new ReversibleShort(true);
        SHORT.setValue(VALUE).save();
        assertEquals(true, SHORT.isUnsigned());
        assertEquals(65035,SHORT.getUnsignedValue());
        assertEquals(65035,SHORT.getUnsignedLastValue());
    }

    @Test
    public void testNotModified() throws Exception {
        final short VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        assertEquals(false,SHORT.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final short VALUE = randShort((short)1,Short.MAX_VALUE);
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        SHORT.setValue(NEW_VALUE);
        assertEquals(true,SHORT.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final short VALUE = randShort((short)1,Short.MAX_VALUE);
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        SHORT.setValue(NEW_VALUE).save();
        assertEquals(false,SHORT.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final short VALUE = randShort((short)1,Short.MAX_VALUE);
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        SHORT.setValue(NEW_VALUE).reset();
        assertEquals(VALUE,SHORT.getValue());
        assertEquals(VALUE,SHORT.getLastValue());
    }

    @Test
    public void testSave() throws Exception {
        final short VALUE = randShort((short)1,Short.MAX_VALUE);
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        SHORT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertEquals(NEW_VALUE,SHORT.getLastValue());
    }

    @Test
    public void testSaveReset() throws Exception {
        final short VALUE = randShort((short)1,Short.MAX_VALUE);
        final short NEW_VALUE = randShort((short)1,Short.MAX_VALUE);
        final ReversibleShort SHORT = new ReversibleShort(VALUE);
        SHORT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,SHORT.getValue());
        assertEquals(NEW_VALUE,SHORT.getLastValue());
    }
}
