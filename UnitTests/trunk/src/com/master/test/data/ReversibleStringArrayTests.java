package com.master.test.data;

import com.master.data.ReversibleStringArray;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by subasteve on 1/30/17.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleStringArrayTests {

    private static Random rand = new Random();

    public static String randString() {
        int min = 1, max = Integer.MAX_VALUE;

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return Integer.toString(randomNum);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @PrepareForTest({ReversibleStringArray.class})
    public void testConstructor() throws Exception {
        ReversibleStringArray expected = PowerMockito.mock(ReversibleStringArray.class);
        PowerMockito.whenNew(ReversibleStringArray.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleStringArray.class})
    public void testConstructor1() throws Exception {
        ReversibleStringArray expected = PowerMockito.mock(ReversibleStringArray.class);
        PowerMockito.whenNew(ReversibleStringArray.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleStringArray.class})
    public void testConstructor2() throws Exception {
        ReversibleStringArray expected = PowerMockito.mock(ReversibleStringArray.class);
        PowerMockito.whenNew(ReversibleStringArray.class).withArguments(new String[] {randString()}).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final String[] VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        assertEquals(VALUE,STRING.getValue());
        assertTrue(Arrays.equals(VALUE,STRING.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleStringArray STRING = new ReversibleStringArray();
        assertEquals(null,STRING.getValue());
        assertEquals(null,STRING.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleStringArray STRING = new ReversibleStringArray();
        assertEquals(false,STRING.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final String[] VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray();
        STRING.setValue(VALUE);
        assertEquals(true,STRING.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final String[] VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray();
        STRING.setValue(VALUE).save();
        assertEquals(false,STRING.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final String[] VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray();
        STRING.setValue(VALUE);
        assertEquals(VALUE,STRING.getValue());
        assertEquals(null,STRING.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final String[] VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray();
        STRING.setValue(VALUE).reset();
        assertEquals(null,STRING.getValue());
        assertEquals(null,STRING.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final String[] VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray();
        STRING.setValue(VALUE).save();
        assertEquals(VALUE,STRING.getValue());
        assertTrue(Arrays.equals(VALUE,STRING.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final String[] VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray();
        STRING.setValue(VALUE).save().reset();
        assertEquals(VALUE,STRING.getValue());
        assertTrue(Arrays.equals(VALUE,STRING.getLastValue()));
    }

    @Test
    public void testSetValue() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final String[] NEW_VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        STRING.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,STRING.getValue());
        assertTrue(Arrays.equals(VALUE,STRING.getLastValue()));
    }

    @Test
    public void testIsModifiedByPosition() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        final String CHANGED_VALUE = randString();
        STRING.setValue(CHANGED_VALUE, 0);
        assertEquals(true,STRING.isModified(0));
        assertEquals(false,STRING.isModified(1));
    }

    @Test
    public void testIsModifiedByPositionNull() throws Exception {
        final ReversibleStringArray STRING = new ReversibleStringArray();
        thrown.expect(NullPointerException.class);
        STRING.isModified(0);
    }

    @Test
    public void testIsModifiedByPositionNegative() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        STRING.isModified(-1);
    }

    @Test
    public void testIsModifiedByPositionGreaterThanArray() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        STRING.isModified(4);
    }

    @Test
    public void testGetValueLength() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        assertEquals(VALUE.length,STRING.getValueLength());
    }

    @Test
    public void testGetValueLengthNull() throws Exception {
        final ReversibleStringArray STRING = new ReversibleStringArray();
        assertEquals(-1,STRING.getValueLength());
    }

    @Test
    public void testGetLastValueLength() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        assertEquals(VALUE.length,STRING.getLastValueLength());
    }

    @Test
    public void testGetLastValueLengthNull() throws Exception {
        final ReversibleStringArray STRING = new ReversibleStringArray();
        assertEquals(-1,STRING.getLastValueLength());
    }

    @Test
    public void testSetValueByPosition() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        final String CHANGED_VALUE = randString();
        STRING.setValue(CHANGED_VALUE, 0);
        assertEquals(CHANGED_VALUE,STRING.getValue(0));
        assertNotEquals(CHANGED_VALUE,STRING.getLastValue(0));
        assertEquals(VALUE[1],STRING.getValue(1));
        assertEquals(VALUE[1],STRING.getLastValue(1));
    }

    @Test
    public void testSetValueByPositionNull() throws Exception {
        final ReversibleStringArray STRING = new ReversibleStringArray();
        thrown.expect(NullPointerException.class);
        STRING.setValue(randString(), 0);
    }

    @Test
    public void testSetValueByPositionNegative() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        STRING.setValue(randString(), -1);
    }

    @Test
    public void testSetValueByPositionGreaterThanArray() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        STRING.setValue(randString(), 4);
    }

    @Test
    public void testGetValueByPosition() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        assertEquals(VALUE[0],STRING.getValue(0));
        assertEquals(VALUE[1],STRING.getValue(1));
        assertEquals(VALUE[2],STRING.getValue(2));
        assertEquals(VALUE[3],STRING.getValue(3));
    }

    @Test
    public void testGetValueByPositionNull() throws Exception {
        final ReversibleStringArray STRING = new ReversibleStringArray();
        thrown.expect(NullPointerException.class);
        STRING.getValue(0);
    }

    @Test
    public void testGetValueByPositionNegative() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        STRING.getValue(-1);
    }

    @Test
    public void testGetValueByPositionGreaterThanArray() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        STRING.getValue(4);
    }

    @Test
    public void testGetLastValueByPosition() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        assertEquals(VALUE[0],STRING.getLastValue(0));
        assertEquals(VALUE[1],STRING.getLastValue(1));
        assertEquals(VALUE[2],STRING.getLastValue(2));
        assertEquals(VALUE[3],STRING.getLastValue(3));
    }

    @Test
    public void testGetLastValueByPositionNull() throws Exception {
        final ReversibleStringArray STRING = new ReversibleStringArray();
        thrown.expect(NullPointerException.class);
        STRING.getLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionNegative() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        STRING.getLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionGreaterThanArray() throws Exception {
        final String[] VALUE = new String[] {randString(),randString(),randString(),randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        STRING.getLastValue(4);
    }

    @Test
    public void testNotModified() throws Exception {
        final String[] VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        assertEquals(false,STRING.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final String[] NEW_VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        STRING.setValue(NEW_VALUE);
        assertEquals(true,STRING.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final String[] NEW_VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        STRING.setValue(NEW_VALUE).save();
        assertEquals(false,STRING.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final String[] NEW_VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        STRING.setValue(NEW_VALUE).reset();
        assertTrue(Arrays.equals(VALUE,STRING.getValue()));
        assertTrue(Arrays.equals(VALUE,STRING.getLastValue()));
    }

    @Test
    public void testSave() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final String[] NEW_VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        STRING.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,STRING.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,STRING.getLastValue()));
    }

    @Test
    public void testSaveNull() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final String[] NEW_VALUE = null;
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        STRING.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,STRING.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,STRING.getLastValue()));
    }

    @Test
    public void testSaveReset() throws Exception {
        final String[] VALUE = new String[] {randString(),randString()};
        final String[] NEW_VALUE = new String[] {randString()};
        final ReversibleStringArray STRING = new ReversibleStringArray(VALUE);
        STRING.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,STRING.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,STRING.getLastValue()));
    }
}
