package com.master.test.data;

import com.master.data.ReversibleLong;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 8/30/16.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleLongTests {

    private static Random rand = new Random();

    public static long randLong(int min, int max) {

        long randomNum = rand.nextInt((max - min) + 1) + min;
        long randomNum2 = rand.nextInt((max - min) + 1) + min;
        long randomNum3 = randomNum + randomNum2;


        return randomNum3;
    }

    @Test
    @PrepareForTest({ReversibleLong.class})
    public void testConstructor() throws Exception {
        ReversibleLong expected = PowerMockito.mock(ReversibleLong.class);
        PowerMockito.whenNew(ReversibleLong.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleLong.class})
    public void testConstructor1() throws Exception {
        ReversibleLong expected = PowerMockito.mock(ReversibleLong.class);
        PowerMockito.whenNew(ReversibleLong.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleLong.class})
    public void testConstructor2() throws Exception {
        ReversibleLong expected = PowerMockito.mock(ReversibleLong.class);
        PowerMockito.whenNew(ReversibleLong.class).withArguments(randLong(1,Integer.MAX_VALUE)).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final long VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong(VALUE);
        assertEquals(VALUE,LONG.getValue());
        assertEquals(VALUE,LONG.getLastValue());
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleLong LONG = new ReversibleLong();
        assertEquals(-1,LONG.getValue());
        assertEquals(-1,LONG.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleLong LONG = new ReversibleLong();
        assertEquals(false,LONG.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong();
        LONG.setValue(NEW_VALUE);
        assertEquals(true,LONG.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong();
        LONG.setValue(NEW_VALUE).save();
        assertEquals(false,LONG.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong();
        LONG.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,LONG.getValue());
        assertEquals(-1,LONG.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong();
        LONG.setValue(NEW_VALUE).reset();
        assertEquals(-1,LONG.getValue());
        assertEquals(-1,LONG.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong();
        LONG.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,LONG.getValue());
        assertEquals(NEW_VALUE,LONG.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong();
        LONG.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,LONG.getValue());
        assertEquals(NEW_VALUE,LONG.getLastValue());
    }

    @Test
    public void testSetValue() throws Exception {
        final long VALUE = randLong(1,Integer.MAX_VALUE);
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong(VALUE);
        LONG.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,LONG.getValue());
        assertEquals(VALUE,LONG.getLastValue());
    }

    @Test
    public void testNotModified() throws Exception {
        final long VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong(VALUE);
        assertEquals(false,LONG.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final long VALUE = randLong(1,Integer.MAX_VALUE);
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong(VALUE);
        LONG.setValue(NEW_VALUE);
        assertEquals(true,LONG.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final long VALUE = randLong(1,Integer.MAX_VALUE);
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong(VALUE);
        LONG.setValue(NEW_VALUE).save();
        assertEquals(false,LONG.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final long VALUE = randLong(1,Integer.MAX_VALUE);
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong(VALUE);
        LONG.setValue(NEW_VALUE).reset();
        assertEquals(VALUE,LONG.getValue());
        assertEquals(VALUE,LONG.getLastValue());
    }

    @Test
    public void testSave() throws Exception {
        final long VALUE = randLong(1,Integer.MAX_VALUE);
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong(VALUE);
        LONG.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,LONG.getValue());
        assertEquals(NEW_VALUE,LONG.getLastValue());
    }

    @Test
    public void testSaveReset() throws Exception {
        final long VALUE = randLong(1,Integer.MAX_VALUE);
        final long NEW_VALUE = randLong(1,Integer.MAX_VALUE);
        final ReversibleLong LONG = new ReversibleLong(VALUE);
        LONG.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,LONG.getValue());
        assertEquals(NEW_VALUE,LONG.getLastValue());
    }
}
