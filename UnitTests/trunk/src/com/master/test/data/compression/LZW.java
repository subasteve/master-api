package com.master.test.data.compression;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by subasteve on 1/2/14.
 */
public final class LZW {
    @Test
    public void TestCompression(){
        final String TEST_STRING = "~`!1@2#3$4%5^6&7*8(9)0_-+=qQwWeErRtTyYuUiIoOpP[{]}\\|aAsSdDfFgGhHjJkKlL;:\"'zZxXcCvVbBnNmM,<.>/?";
        final List<Integer> COMPRESSED = com.master.data.compression.LZW.compress(TEST_STRING);
        final String DECOMPRESSED = com.master.data.compression.LZW.decompress(COMPRESSED);
        Assert.assertTrue(TEST_STRING.equals(DECOMPRESSED));
    }
}
