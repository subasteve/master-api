package com.master.test.data;

import com.master.data.ReversibleByteArray;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by subasteve on 1/30/17.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleByteArrayTests {

    private static Random rand = new Random();

    public static byte randByte() {
        int min = 1, max = Byte.MAX_VALUE;

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return (byte) randomNum;
    }

    public static boolean randBoolean() {
        int min = 1, max = 2;

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum == 1;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @PrepareForTest({ReversibleByteArray.class})
    public void testConstructor() throws Exception {
        ReversibleByteArray expected = PowerMockito.mock(ReversibleByteArray.class);
        PowerMockito.whenNew(ReversibleByteArray.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByteArray.class})
    public void testConstructor1() throws Exception {
        ReversibleByteArray expected = PowerMockito.mock(ReversibleByteArray.class);
        PowerMockito.whenNew(ReversibleByteArray.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByteArray.class})
    public void testConstructor2() throws Exception {
        ReversibleByteArray expected = PowerMockito.mock(ReversibleByteArray.class);
        PowerMockito.whenNew(ReversibleByteArray.class).withArguments(new byte[] {randByte()}).thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByteArray.class})
    public void testConstructor3() throws Exception {
        ReversibleByteArray expected = PowerMockito.mock(ReversibleByteArray.class);
        PowerMockito.whenNew(ReversibleByteArray.class).withArguments(new byte[] {randByte()},randBoolean()).thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleByteArray.class})
    public void testConstructor4() throws Exception {
        ReversibleByteArray expected = PowerMockito.mock(ReversibleByteArray.class);
        PowerMockito.whenNew(ReversibleByteArray.class).withArguments(randBoolean()).thenReturn(expected);
    }


    @Test
    public void testConstructorWithValue() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertEquals(VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testConstructorIsNotUnsigned() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        assertFalse(BYTE.isUnsigned());
    }

    @Test
    public void testConstructorWithValueIsNotUnsigned() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertFalse(BYTE.isUnsigned());
        assertEquals(VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testConstructorWithUnsigned() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray(true);
        assertTrue(BYTE.isUnsigned());
    }

    @Test
    public void testConstructorWithValueAndUnsigned() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE,true);
        assertTrue(BYTE.isUnsigned());
        assertEquals(VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        assertEquals(null,BYTE.getValue());
        assertEquals(null,BYTE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        assertEquals(false,BYTE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        BYTE.setValue(VALUE);
        assertEquals(true,BYTE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        BYTE.setValue(VALUE).save();
        assertEquals(false,BYTE.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        BYTE.setValue(VALUE);
        assertEquals(VALUE,BYTE.getValue());
        assertEquals(null,BYTE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        BYTE.setValue(VALUE).reset();
        assertEquals(null,BYTE.getValue());
        assertEquals(null,BYTE.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        BYTE.setValue(VALUE).save();
        assertEquals(VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        BYTE.setValue(VALUE).save().reset();
        assertEquals(VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testSetValue() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final byte[] NEW_VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        BYTE.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testIsModifiedByPosition() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        final byte CHANGED_VALUE = randByte();
        BYTE.setValue(CHANGED_VALUE, 0);
        assertEquals(true,BYTE.isModified(0));
        assertEquals(false,BYTE.isModified(1));
    }

    @Test
    public void testIsModifiedByPositionNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        thrown.expect(NullPointerException.class);
        BYTE.isModified(0);
    }

    @Test
    public void testIsModifiedByPositionNegative() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.isModified(-1);
    }

    @Test
    public void testIsModifiedByPositionGreaterThanArray() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.isModified(4);
    }

    @Test
    public void testGetValueLength() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertEquals(VALUE.length,BYTE.getValueLength());
    }

    @Test
    public void testGetValueLengthNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        assertEquals(-1,BYTE.getValueLength());
    }

    @Test
    public void testGetLastValueLength() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertEquals(VALUE.length,BYTE.getLastValueLength());
    }

    @Test
    public void testGetLastValueLengthNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        assertEquals(-1,BYTE.getLastValueLength());
    }

    @Test
    public void testSetValueByPosition() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        final byte CHANGED_VALUE = randByte();
        BYTE.setValue(CHANGED_VALUE, 0);
        assertEquals(CHANGED_VALUE,BYTE.getValue(0));
        assertNotEquals(CHANGED_VALUE,BYTE.getLastValue(0));
        assertEquals(VALUE[1],BYTE.getValue(1));
        assertEquals(VALUE[1],BYTE.getLastValue(1));
    }

    @Test
    public void testGetValueByPositionUnsigned() throws Exception {
        final byte[] VALUE = new byte[] {(byte)220,(byte)240};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertEquals(VALUE[0],BYTE.getValue(0));
        assertNotEquals(VALUE[0],BYTE.getUnsignedValue(0));
        assertEquals(220,BYTE.getUnsignedValue(0));
        assertEquals(VALUE[1],BYTE.getValue(1));
        assertNotEquals(VALUE[1],BYTE.getUnsignedValue(1));
        assertEquals(240,BYTE.getUnsignedValue(1));
    }

    @Test
    public void testGetLastValueByPositionUnsigned() throws Exception {
        final byte[] VALUE = new byte[] {(byte)220,(byte)240};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertEquals(VALUE[0],BYTE.getLastValue(0));
        assertNotEquals(VALUE[0],BYTE.getUnsignedLastValue(0));
        assertEquals(220,BYTE.getUnsignedLastValue(0));
        assertEquals(VALUE[1],BYTE.getLastValue(1));
        assertNotEquals(VALUE[1],BYTE.getUnsignedLastValue(1));
        assertEquals(240,BYTE.getUnsignedLastValue(1));
    }

    @Test
    public void testGetValueByPositionUnsignedNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        thrown.expect(NullPointerException.class);
        BYTE.getUnsignedValue(0);
    }

    @Test
    public void testGetValueByPositionUnsignedNegative() throws Exception {
        final byte[] VALUE = new byte[] {(byte)220,(byte)240};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.getUnsignedValue(-1);
    }

    @Test
    public void testGetValueByPositionUnsignedGreaterThanArray() throws Exception {
        final byte[] VALUE = new byte[] {(byte)220,(byte)240};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.getUnsignedValue(3);
    }


    @Test
    public void testGetLastValueByPositionUnsignedNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        thrown.expect(NullPointerException.class);
        BYTE.getUnsignedLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionUnsignedNegative() throws Exception {
        final byte[] VALUE = new byte[] {(byte)220,(byte)240};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.getUnsignedLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionUnsignedGreaterThanArray() throws Exception {
        final byte[] VALUE = new byte[] {(byte)220,(byte)240};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.getUnsignedLastValue(3);
    }

    @Test
    public void testGetValueUnsigned() throws Exception {
        final byte[] VALUE = new byte[] {(byte)220,(byte)240};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        final int[] UNSIGNED_VALUE = BYTE.getUnsignedValue();
        assertNotEquals(UNSIGNED_VALUE[0],BYTE.getValue(0));
        assertEquals(UNSIGNED_VALUE[0],BYTE.getUnsignedValue(0));
        assertNotEquals(UNSIGNED_VALUE[1],BYTE.getValue(1));
        assertEquals(UNSIGNED_VALUE[1],BYTE.getUnsignedValue(1));
    }

    @Test
    public void testGetLastValueUnsigned() throws Exception {
        final byte[] VALUE = new byte[] {(byte)220,(byte)240};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        final int[] UNSIGNED_LAST_VALUE = BYTE.getUnsignedLastValue();
        assertNotEquals(UNSIGNED_LAST_VALUE[0],BYTE.getLastValue(0));
        assertEquals(UNSIGNED_LAST_VALUE[0],BYTE.getUnsignedLastValue(0));
        assertNotEquals(UNSIGNED_LAST_VALUE[1],BYTE.getLastValue(1));
        assertEquals(UNSIGNED_LAST_VALUE[1],BYTE.getUnsignedLastValue(1));
    }

    @Test
    public void testGetValueUnsignedNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        final int[] UNSIGNED_VALUE = BYTE.getUnsignedValue();
        assertEquals(null,UNSIGNED_VALUE);
    }


    @Test
    public void testGetLastValueUnsignedNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        final int[] UNSIGNED_LAST_VALUE = BYTE.getUnsignedLastValue();
        assertEquals(null,UNSIGNED_LAST_VALUE);
    }

    @Test
    public void testSetValueByPositionNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        thrown.expect(NullPointerException.class);
        BYTE.setValue(randByte(), 0);
    }

    @Test
    public void testSetValueByPositionNegative() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.setValue(randByte(), -1);
    }

    @Test
    public void testSetValueByPositionGreaterThanArray() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.setValue(randByte(), 4);
    }

    @Test
    public void testGetValueByPosition() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertEquals(VALUE[0],BYTE.getValue(0));
        assertEquals(VALUE[1],BYTE.getValue(1));
        assertEquals(VALUE[2],BYTE.getValue(2));
        assertEquals(VALUE[3],BYTE.getValue(3));
    }

    @Test
    public void testGetValueByPositionNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        thrown.expect(NullPointerException.class);
        BYTE.getValue(0);
    }

    @Test
    public void testGetValueByPositionNegative() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.getValue(-1);
    }

    @Test
    public void testGetValueByPositionGreaterThanArray() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.getValue(4);
    }

    @Test
    public void testGetLastValueByPosition() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertEquals(VALUE[0],BYTE.getLastValue(0));
        assertEquals(VALUE[1],BYTE.getLastValue(1));
        assertEquals(VALUE[2],BYTE.getLastValue(2));
        assertEquals(VALUE[3],BYTE.getLastValue(3));
    }

    @Test
    public void testGetLastValueByPositionNull() throws Exception {
        final ReversibleByteArray BYTE = new ReversibleByteArray();
        thrown.expect(NullPointerException.class);
        BYTE.getLastValue(0);
    }

    @Test
    public void testGetLastValueByPositionNegative() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.getLastValue(-1);
    }

    @Test
    public void testGetLastValueByPositionGreaterThanArray() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte(),randByte(),randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        BYTE.getLastValue(4);
    }

    @Test
    public void testNotModified() throws Exception {
        final byte[] VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        assertEquals(false,BYTE.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final byte[] NEW_VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        BYTE.setValue(NEW_VALUE);
        assertEquals(true,BYTE.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final byte[] NEW_VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        BYTE.setValue(NEW_VALUE).save();
        assertEquals(false,BYTE.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final byte[] NEW_VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        BYTE.setValue(NEW_VALUE).reset();
        assertTrue(Arrays.equals(VALUE,BYTE.getValue()));
        assertTrue(Arrays.equals(VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testSave() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final byte[] NEW_VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        BYTE.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testSaveNull() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final byte[] NEW_VALUE = null;
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        BYTE.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,BYTE.getLastValue()));
    }

    @Test
    public void testSaveReset() throws Exception {
        final byte[] VALUE = new byte[] {randByte(),randByte()};
        final byte[] NEW_VALUE = new byte[] {randByte()};
        final ReversibleByteArray BYTE = new ReversibleByteArray(VALUE);
        BYTE.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,BYTE.getValue());
        assertTrue(Arrays.equals(NEW_VALUE,BYTE.getLastValue()));
    }
}
