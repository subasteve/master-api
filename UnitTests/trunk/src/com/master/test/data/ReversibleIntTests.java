package com.master.test.data;

import com.master.data.ReversibleInt;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by subasteve on 8/30/16.
 */
@RunWith(PowerMockRunner.class)
public class ReversibleIntTests {

    private static Random rand = new Random();

    public static int randInt(int min, int max) {
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    @Test
    @PrepareForTest({ReversibleInt.class})
    public void testConstructor() throws Exception {
        ReversibleInt expected = PowerMockito.mock(ReversibleInt.class);
        PowerMockito.whenNew(ReversibleInt.class).withNoArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleInt.class})
    public void testConstructor1() throws Exception {
        ReversibleInt expected = PowerMockito.mock(ReversibleInt.class);
        PowerMockito.whenNew(ReversibleInt.class).withAnyArguments().thenReturn(expected);
    }

    @Test
    @PrepareForTest({ReversibleInt.class})
    public void testConstructor2() throws Exception {
        ReversibleInt expected = PowerMockito.mock(ReversibleInt.class);
        PowerMockito.whenNew(ReversibleInt.class).withArguments(randInt(1,Integer.MAX_VALUE)).thenReturn(expected);
    }

    @Test
    public void testConstructorWithValue() throws Exception {
        final int VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt(VALUE);
        assertEquals(VALUE,INT.getValue());
        assertEquals(VALUE,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValue() throws Exception {
        final ReversibleInt INT = new ReversibleInt();
        assertEquals(-1,INT.getValue());
        assertEquals(-1,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueNotModified() throws Exception {
        final ReversibleInt INT = new ReversibleInt();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueModified() throws Exception {
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt();
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveNotModified() throws Exception {
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt();
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testConstructorWithNoValueSetValue() throws Exception {
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt();
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(-1,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueReset() throws Exception {
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt();
        INT.setValue(NEW_VALUE).reset();
        assertEquals(-1,INT.getValue());
        assertEquals(-1,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSave() throws Exception {
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt();
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(NEW_VALUE,INT.getLastValue());
    }

    @Test
    public void testConstructorWithNoValueSetValueSaveReset() throws Exception {
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt();
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(NEW_VALUE,INT.getLastValue());
    }

    @Test
    public void testSetValue() throws Exception {
        final int VALUE = randInt(1,Integer.MAX_VALUE);
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(VALUE,INT.getLastValue());
    }

    @Test
    public void testNotModified() throws Exception {
        final int VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt(VALUE);
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testModified() throws Exception {
        final int VALUE = randInt(1,Integer.MAX_VALUE);
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt(VALUE);
        INT.setValue(NEW_VALUE);
        assertEquals(true,INT.isModified());
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
        final int VALUE = randInt(1,Integer.MAX_VALUE);
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(false,INT.isModified());
    }

    @Test
    public void testReset() throws Exception {
        final int VALUE = randInt(1,Integer.MAX_VALUE);
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt(VALUE);
        INT.setValue(NEW_VALUE).reset();
        assertEquals(VALUE,INT.getValue());
        assertEquals(VALUE,INT.getLastValue());
    }

    @Test
    public void testSave() throws Exception {
        final int VALUE = randInt(1,Integer.MAX_VALUE);
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt(VALUE);
        INT.setValue(NEW_VALUE).save();
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(NEW_VALUE,INT.getLastValue());
    }

    @Test
    public void testSaveReset() throws Exception {
        final int VALUE = randInt(1,Integer.MAX_VALUE);
        final int NEW_VALUE = randInt(1,Integer.MAX_VALUE);
        final ReversibleInt INT = new ReversibleInt(VALUE);
        INT.setValue(NEW_VALUE).save().reset();
        assertEquals(NEW_VALUE,INT.getValue());
        assertEquals(NEW_VALUE,INT.getLastValue());
    }
}
