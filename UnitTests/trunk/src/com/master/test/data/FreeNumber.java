package com.master.test.data;

import com.master.testclass.data.RandomAddRemoveFreeNumber;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by subasteve on 4/27/2016.
 */
public class FreeNumber {

    @Test
    public final void testThreadSafe(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber();
        final RandomAddRemoveFreeNumber GEN1 = new RandomAddRemoveFreeNumber(FREE_NUMBER);
        final RandomAddRemoveFreeNumber GEN2 = new RandomAddRemoveFreeNumber(FREE_NUMBER);
        final RandomAddRemoveFreeNumber GEN3 = new RandomAddRemoveFreeNumber(FREE_NUMBER);
        final RandomAddRemoveFreeNumber GEN4 = new RandomAddRemoveFreeNumber(FREE_NUMBER);
        GEN1.start();
        GEN2.start();
        GEN3.start();
        GEN4.start();
        try {
            Thread.sleep(1000);
        }catch (final Exception E){}
        assertTrue(FREE_NUMBER.getSize() > 4);
    }


    @Test
    public final void testAdd(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber();
        assertEquals(0, FREE_NUMBER.getSize());
        FREE_NUMBER.put();
        assertEquals(1, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddRemove(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber();
        assertEquals(0, FREE_NUMBER.getSize());
        FREE_NUMBER.put();
        assertEquals(1, FREE_NUMBER.getSize());
        FREE_NUMBER.remove(0);
        assertEquals(0, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddID(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber();
        assertEquals(0, FREE_NUMBER.getSize());
        final int ID = FREE_NUMBER.put();
        assertEquals(ID+1, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddRemoveID(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber();
        assertEquals(0, FREE_NUMBER.getSize());
        final int ID = FREE_NUMBER.put();
        assertEquals(ID+1, FREE_NUMBER.getSize());
        FREE_NUMBER.remove(ID);
        assertEquals(0, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddOffset(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber(1);
        assertEquals(1, FREE_NUMBER.getSize());
        FREE_NUMBER.put();
        assertEquals(2, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddRemoveOffset(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber(1);
        assertEquals(1, FREE_NUMBER.getSize());
        FREE_NUMBER.put();
        assertEquals(2, FREE_NUMBER.getSize());
        FREE_NUMBER.remove(1);
        assertEquals(1, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddIDOffset(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber(1);
        assertEquals(1, FREE_NUMBER.getSize());
        final int ID = FREE_NUMBER.put();
        assertEquals(ID+1, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddRemoveIDOffset(){
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber(1);
        assertEquals(1, FREE_NUMBER.getSize());
        final int ID = FREE_NUMBER.put();
        assertEquals(ID+1, FREE_NUMBER.getSize());
        FREE_NUMBER.remove(ID);
        assertEquals(1, FREE_NUMBER.getSize());
    }


    @Test
    public final void testAddOffsetRandom(){
        final int START_POS = RandomAddRemoveFreeNumber.randInt(1,Integer.MAX_VALUE-1);
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber(START_POS);
        assertEquals(START_POS, FREE_NUMBER.getSize());
        FREE_NUMBER.put();
        assertEquals(START_POS+1, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddRemoveOffsetRandom(){
        final int START_POS = RandomAddRemoveFreeNumber.randInt(1,Integer.MAX_VALUE-1);
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber(START_POS);
        assertEquals(START_POS, FREE_NUMBER.getSize());
        FREE_NUMBER.put();
        assertEquals(START_POS+1, FREE_NUMBER.getSize());
        FREE_NUMBER.remove(START_POS);
        assertEquals(START_POS, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddIDOffsetRandom(){
        final int START_POS = RandomAddRemoveFreeNumber.randInt(1,Integer.MAX_VALUE-1);
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber(START_POS);
        assertEquals(START_POS, FREE_NUMBER.getSize());
        final int ID = FREE_NUMBER.put();
        assertEquals(ID+1, FREE_NUMBER.getSize());
    }

    @Test
    public final void testAddRemoveIDOffsetRandom(){
        final int START_POS = RandomAddRemoveFreeNumber.randInt(1,Integer.MAX_VALUE-1);
        final com.master.data.FreeNumber FREE_NUMBER = new com.master.data.FreeNumber(START_POS);
        assertEquals(START_POS, FREE_NUMBER.getSize());
        final int ID = FREE_NUMBER.put();
        assertEquals(ID+1, FREE_NUMBER.getSize());
        FREE_NUMBER.remove(ID);
        assertEquals(START_POS, FREE_NUMBER.getSize());
    }
}
