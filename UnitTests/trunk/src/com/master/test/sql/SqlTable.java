package com.master.test.sql;

import com.master.sql.builder.SqlColumn;
import com.master.sql.builder.SqlDataType;
import com.master.sql.builder.SqlForeignKey;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by subasteve on 5/17/2014.
 */
public class SqlTable {

    @Test
    public final void TestCreateTable(){
        final com.master.sql.builder.SqlTable TABLE = new com.master.sql.builder.SqlTable("UserBoolean",true);
        try{
            /*
            SqlColumn[] COLUMNS = new SqlColumn[]{
                    new SqlColumn("ID",8,SqlDataType.MEDIUMINT,true,false,true,true),
                    new SqlColumn("username",20,SqlDataType.VARCHAR,false,true),
                    new SqlColumn("firstName",50,SqlDataType.VARCHAR,false,true,true),
                    new SqlColumn("lastName",50,SqlDataType.VARCHAR,false,true),
                    new SqlColumn("email",250,SqlDataType.VARCHAR,false,true),
                    new SqlColumn("passHash",32,SqlDataType.VARCHAR)
            };
            */
            SqlColumn[] COLUMNS = new SqlColumn[]{
                    new SqlColumn("ID",SqlDataType.BIGINT,true,false,true),
                    new SqlColumn("UserID",8,SqlDataType.MEDIUMINT,false,false,true),
                    new SqlColumn("variable",25,SqlDataType.VARCHAR,false,false,true),
                    new SqlColumn("value",1,SqlDataType.BIT,false,false,false)
            };
            TABLE.setColumns(COLUMNS);
            TABLE.getForeignKeys().add(new SqlForeignKey("UserID","User","ID"));
            final String QUERY = TABLE.toString();
            Assert.assertNotNull(QUERY);
        }catch (final Exception E){

        }
    }
}
