package com.master.test.cache;

import com.master.cache.CacheEntry;
import com.master.data.ReversibleBoolean;
import com.master.data.ReversibleInt;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class LocalCache {

    private static Random rand = new Random();

    public static int randInt(int min, int max) {
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    com.master.cache.LocalCache cache = null;

    @Before
    public void setupCache() {
        cache = new com.master.cache.LocalCache(10000);
    }

    @Test
    @PrepareForTest({com.master.cache.LocalCache.class})
    public void testConstructor() throws Exception {
        com.master.cache.LocalCache expected = PowerMockito.mock(com.master.cache.LocalCache.class);
        PowerMockito.whenNew(com.master.cache.LocalCache.class).withArguments(randInt(1, Integer.MAX_VALUE)).thenReturn(expected);
    }

    @Test
    public void testPut() throws Exception {
        cache.put("Key","Value",-1);
        assertEquals("Value",cache.get("Key").getEntry());
    }

    @Test
    public void testRemove() throws Exception {
        cache.put("Key","Value",-1);
        cache.remove("Key");
        assertEquals(null,cache.get("Key"));
    }

    @Test
    public void testRemoveBool() throws Exception {
        cache.put("Key","Value",-1);
        assertEquals(true,cache.remove("Key"));
    }

    @Test
    public void testClear() throws Exception {
        cache.put("Key","Value",-1);
        cache.clear();
        assertEquals(null,cache.get("Key"));
    }

    @Test
    public void testGetNonExistantValue() throws Exception {
        assertEquals(null,cache.get("Key"));
    }

    @Test
    public void testRemoveEmpty() throws Exception {
        assertEquals(false,cache.remove("Key"));
    }

    @Test
    public void testSizeAfterRemove() throws Exception {
        cache.put("Key","Value",-1);
        cache.remove("Key");
        assertEquals(0,cache.size());
    }

    @Test
    public void testSizeAfterClear() throws Exception {
        cache.put("Key","Value",-1);
        cache.clear();
        assertEquals(0,cache.size());
    }

    @Test
    public void testInitialSize() throws Exception {
        assertEquals(0,cache.size());
    }

    @Test
    public void testSize() throws Exception {
        cache.put("Key","Value",-1);
        assertEquals(1,cache.size());
    }

    @Test
    public void testExpires() throws Exception {
        cache.put("Key","Value",1);
        Thread.sleep(2000);
        assertEquals(null,cache.get("Key"));
    }

    @Test
    public void testSizeRemoveOldest() throws Exception {
        cache = new com.master.cache.LocalCache(3);
        cache.put("Key","Value",-1);
        cache.put("Key2","Value_2",-1);
        cache.put("Key3","Value_3",-1);
        cache.put("Key4","Value_4",-1);
        assertEquals(null,cache.get("Key"));
    }

    @Test
    public void testGetAll() throws Exception {
        cache.put("Key","Value",-1);
        cache.put("Key2","Value_2",-1);
        final List<String> FINDER = new ArrayList<String>();
        FINDER.add("Key");
        FINDER.add("Key2");
        final Map<String,String> VALUES = cache.getAll(FINDER);

        assertEquals("Value",VALUES.get("Key"));
        assertEquals("Value_2",VALUES.get("Key2"));
    }

    @Test
    public void testGetAllObject() throws Exception {
        final ReversibleBoolean BOOLEAN = new ReversibleBoolean(false);
        final ReversibleBoolean BOOLEAN_1 = new ReversibleBoolean(true);
        cache.put("Key",BOOLEAN,-1);
        cache.put("Key2",BOOLEAN_1,-1);
        final List<String> FINDER = new ArrayList<String>();
        FINDER.add("Key");
        FINDER.add("Key2");
        final Map<String,ReversibleBoolean> VALUES = cache.getAll(FINDER);

        assertEquals(BOOLEAN,VALUES.get("Key"));
        assertEquals(BOOLEAN_1,VALUES.get("Key2"));

        assertEquals(false,BOOLEAN.getValue());
        assertEquals(true,BOOLEAN_1.getValue());
    }

    @Test
    public void testObjects() throws Exception {
        final ReversibleBoolean BOOLEAN = new ReversibleBoolean(false);
        final ReversibleInt INT = new ReversibleInt(42);
        cache.put("Key", BOOLEAN,-1);
        cache.put("Key2", INT,-1);

        assertEquals(BOOLEAN,cache.get("Key").getEntry());
        assertEquals(INT,cache.get("Key2").getEntry());

        final CacheEntry<ReversibleBoolean> CACHED_VALUE = cache.get("Key");
        final CacheEntry<ReversibleInt> CACHED_VALUE_2 = cache.get("Key2");

        assertEquals(false,CACHED_VALUE.getEntry().getValue());
        assertEquals(42,CACHED_VALUE_2.getEntry().getValue());
    }

}
