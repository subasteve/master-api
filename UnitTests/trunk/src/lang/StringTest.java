package com.master.test.lang;

import com.master.util.ProcessTime;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

/**
 * Created by subasteve on 9/21/13.
 */
public class StringTest {

    private final String combineStringsWithStrings(final String STR1, final String STR2){
        return STR1+STR2;
    }

    private final String combineStringsBuilder(final String STR1, final String STR2){
        return new StringBuilder(STR1.length()+STR2.length()).append(STR1).append(STR2).toString();
    }

    private final String combineStringsBuffer(final String STR1, final String STR2){
        return new StringBuffer(STR1.length()+STR2.length()).append(STR1).append(STR2).toString();
    }

    @Test
    @Ignore("Local Tests Only")
    public void TestString(){
        final ProcessTime TIMER = new ProcessTime();
        final int TEST_AMOUNT = Integer.MAX_VALUE/16;
        System.out.println("Combine Strings with StringBuilder Should be Fastest...");
        TIMER.start();
        for(int i = 0; i < TEST_AMOUNT; i++){
            combineStringsBuilder("Tasty","Pasty");
        }
        TIMER.stop();
        final long STRING_BUILDER_TIME = TIMER.getProcessingTime();
        TIMER.writeProcessingTime();
        System.out.println("Combine Strings with StringBuffer...");
        TIMER.start();
        for(int i = 0; i < TEST_AMOUNT; i++){
            combineStringsBuffer("Tasty","Pasty");
        }
        TIMER.stop();
        final long STRING_BUFFER_TIME = TIMER.getProcessingTime();
        TIMER.writeProcessingTime();
        System.out.println("Combine Strings with String + String...");
        TIMER.start();
        for(int i = 0; i < TEST_AMOUNT; i++){
            combineStringsWithStrings("Tasty","Pasty");
        }
        TIMER.stop();
        final long STRING_PLUS_STRING_TIME = TIMER.getProcessingTime();
        TIMER.writeProcessingTime();
        assertEquals(true, (STRING_BUILDER_TIME < STRING_BUFFER_TIME && STRING_BUILDER_TIME < STRING_PLUS_STRING_TIME));
    }
}
