package com.master.util;

import java.lang.ProcessBuilder;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public final class Command implements Runnable{

	private final String COMMAND;
	private final String[] COMMANDS;
	private final CreateString output;
	Process process = null;
	
	public Command(final String COMMAND){
		this.COMMAND = COMMAND;
		this.COMMANDS = null;
		output = null;
		(new Thread(this, COMMAND)).start();
	}
	
	public Command(final String COMMAND, final boolean CREATE_STRING){
		this.COMMAND = COMMAND;
		this.COMMANDS = null;
		if(CREATE_STRING){
			output = new CreateString();
		}else{
			output = null;
		}
		(new Thread(this, COMMAND)).start();
	}
	
	public Command(final String[] COMMANDS, final boolean CREATE_STRING){
		this.COMMAND = null;
		this.COMMANDS = COMMANDS;
		if(CREATE_STRING){
			output = new CreateString();
		}else{
			output = null;
		}
		(new Thread(this, COMMANDS[0])).start();
	}
	
	public Command(final String COMMAND, final boolean CREATE_STRING, final boolean IN_NEW_THREAD){
		this.COMMAND = COMMAND;
		this.COMMANDS = null;
		if(CREATE_STRING){
			output = new CreateString();
		}else{
			output = null;
		}
		if(IN_NEW_THREAD){
			(new Thread(this, COMMAND)).start();
		}else{
			run();
		}
	}
	
	public Command(final String[] COMMANDS, final boolean CREATE_STRING, final boolean IN_NEW_THREAD){
		this.COMMAND = null;
		this.COMMANDS = COMMANDS;
		if(CREATE_STRING){
			output = new CreateString();
		}else{
			output = null;
		}
		if(IN_NEW_THREAD){
			(new Thread(this, COMMANDS[0])).start();
		}else{
			run();
		}
	}
	
	public final CreateString getOutput(){
		return output;
	}
	
	public final void destory(){
		if(process != null){
			process.destroy();
		}
	}
	
	public final Process getProccess(){
		return process;
	}
	
	public final void run(){
		try{
			if(COMMANDS != null){
				process = new ProcessBuilder(COMMANDS).start();
			}else{
				process = new ProcessBuilder(COMMAND).start();
			}
			final BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = br.readLine()) != null) {
				if(!line.equals("")){
					if(output != null){
						output.put(line);
					}else{
						System.out.println(line);
					}
				}
		    }
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(process != null){
				process.destroy();
			}
		}
	}
	
}
