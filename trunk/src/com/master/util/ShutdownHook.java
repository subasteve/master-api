package com.master.util;

import java.util.Map;
import java.util.HashMap;
import com.master.util.interfaces.Shutdown;

public final class ShutdownHook extends Thread{

	private final Map<Integer, Shutdown> STORED_SHUTDOWN = new HashMap<Integer, Shutdown>();
	private int lastIndex = 0;
	
	public ShutdownHook(){
		Runtime.getRuntime().addShutdownHook(this);
	}
	
	public final void put(final Shutdown SHUTDOWN){
		STORED_SHUTDOWN.put(lastIndex++,SHUTDOWN);
	}
	
	public final void put(final Shutdown SHUTDOWN, final int POS){
		STORED_SHUTDOWN.put(POS,SHUTDOWN);
	}
	
	public final void run(){
		for(final Shutdown SHUTDOWN : STORED_SHUTDOWN.values()){
			SHUTDOWN.shutdown();
		}
	}
	
}