package com.master.util;

public final class ProcessTime{
	
	private final Logger LOGGER;
	private final String OPERATION;
	private long startTime = 0;
	private long stopTime = 0;
	
	public ProcessTime(final String OPERATION, final Logger LOGGER){
		this.LOGGER = LOGGER;
		this.OPERATION = OPERATION;
	}
	
	public ProcessTime(final Logger LOGGER){
		this.LOGGER = LOGGER;
		OPERATION = "Operation";
	}
	
	public ProcessTime(final String OPERATION){
		this.OPERATION = OPERATION;
		LOGGER = null;
	}
	
	public ProcessTime(){
		LOGGER = null;
		OPERATION = "Operation";
	}
	
	public final void start(){
		startTime = System.currentTimeMillis();
	}
	
	public final void stop(){
		stopTime = System.currentTimeMillis();
	}
	
	public final String getStringProcessingTime(){
		if(stopTime == 0 || startTime == 0){
			return "No start or stop called...";
		}
		final long difference = stopTime-startTime;
		return getStringProcessingTime(difference);
	}
	
	public final String getStringProcessingTime(final long difference){
		if(difference > 1000){
			final int seconds = (int)(difference/1000);
			if(seconds >= 60){
				final int minutes = (int)(seconds/60);
				if(minutes >= 60){
					final int hours = (int)(minutes/60);
					if(hours >= 24){
						final int days = (int)(hours/24);
						return OPERATION+" took "+days+"day & "+(hours-(days*24))+"hr & "+(minutes-(hours*60))+"min & "+(seconds-(minutes*60))+"s & "+(difference-(seconds*1000))+"ms";
					}else{
						return OPERATION+" took "+hours+"hr & "+(minutes-(hours*60))+"min & "+(seconds-(minutes*60))+"s & "+(difference-(seconds*1000))+"ms";
					}
				}else{
					return OPERATION+" took "+minutes+"min & "+(seconds-(minutes*60))+"s & "+(difference-(seconds*1000))+"ms";
				}
			}else{
				return OPERATION+" took "+seconds+"s & "+(difference-(seconds*1000))+"ms";
			}
		}else{
			return OPERATION+" took "+difference+"ms";
		}
	}
	
	public final long getProcessingTime(){
		if(stopTime == 0 || startTime == 0){
			return -1;
		}
		return stopTime-startTime;
	}
	
	public final long getCurrentProcessingTime(){
		if(startTime == 0){
			return -1;
		}
		return System.currentTimeMillis()-startTime;
	}
	
	public final void writeProcessingTime(){
		if(LOGGER != null){
			LOGGER.writeLine(getStringProcessingTime());
		}else{
			System.out.println(getStringProcessingTime());
		}
	}
	
}