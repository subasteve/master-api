package com.master.util;

import java.util.Timer;
import java.util.TimerTask;

//will be used for farming and delays
public abstract class UpTime{

	private int Seconds, Minutes, Hours, Days;
	private int countSeconds, countMinutes, countHours, countDays;
	private int counterSeconds, counterMinutes, counterHours, counterDays;
    Timer timer;
	
	public abstract void onSecondsChange();
	public abstract void onMinutesChange();
	public abstract void onHoursChange();
	public abstract void onDaysChange();
	
	public UpTime(){
		reset();
		SecondsTimer();
	}
	
	public final void reset(){
		Seconds = 0;
		Minutes = 0;
		Hours = 0;
		Days = 0;
	}
	
    public final void SecondsTimer() {
        timer = new Timer();
        timer.schedule(new RemindTask(),
                       0,        //initial delay
                       1000);  //subsequent rate
    }
	
	public final int getSeconds(){
		return Seconds;
	}
	
	public final int getMinutes(){
		return Minutes;
	}
	
	public final int getHours(){
		return Hours;
	}
	
	public final int getDays(){
		return Days;
	}
	
	public final String getUpTime(){
        final StringBuilder buf = new StringBuilder(100);
		buf.append("Server UpTime: ");
		if(Days > 0){
			buf.append(Days);
			if(Days == 1){
				buf.append(" Day ");
			}else{
				buf.append(" Days ");
			}
		}if(Hours > 0){
			buf.append(Hours);
			if(Hours == 1){
				buf.append(" Hour ");
			}else{
				buf.append(" Hours ");
			}
		}if(Minutes > 0){
			buf.append(Minutes);
			if(Minutes == 1){
				buf.append(" Minute ");
			}else{
				buf.append(" Minutes ");
			}
		}if(Seconds > 0){
			buf.append(Seconds);
			if(Seconds == 1){
				buf.append(" Second ");
			}else{
				buf.append(" Seconds ");
			}
		}
		return buf.toString();
	}
	
	public final void resetCounter(){
		counterSeconds = 0;
		counterMinutes = 0;
		counterHours = 0;
		counterDays = 0;
	}
	
	public final void resetCount(){
		countSeconds = 0;
		countMinutes = 0;
		countHours = 0;
		countDays = 0;
	}
	
	public final void printOutEvery(final int Seconds, final int Minutes, final int Hours, final int Days){
		resetCounter();
		resetCount();
		countSeconds = Seconds;
		countMinutes = Minutes;
		countHours = Hours;
		countDays = Days;
	}
	
	class RemindTask extends TimerTask{
		public final void run(){
			Seconds++;
			counterSeconds++;
			if(countSeconds == counterSeconds && countMinutes == counterMinutes && countHours == counterHours && countDays == counterDays){
				System.out.println(getUpTime());
				resetCounter();
			}
			onSecondsChange();
			if (Seconds >= 60){
				Minutes++;
				counterMinutes++;
				Seconds = 0;	
				counterSeconds = 0;
				onMinutesChange();
			}
			if (Minutes >= 60){
				Hours++;
				counterHours++;
				Minutes = 0;
				counterMinutes = 0;
				onHoursChange();
			}
			if (Hours >= 24){
				Days++;
				counterDays++;
				Hours = 0;
				counterHours = 0;
				onDaysChange();
			}
        }
    }
}
