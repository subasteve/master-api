// TODO: FINISH CONVERTING TO BYTEBUFFER FROM STRING
package com.master.util;

import com.master.util.error.StringNotFoundException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public final class SearchableString{

	private ByteBuffer buffer = null;
	
	public SearchableString(){}
	
	public SearchableString(final String STRING){
		setString(STRING);
	}
	
	public SearchableString(final byte[] BYTES){
		buffer = ByteBuffer.allocateDirect(BYTES.length);
		buffer.put(BYTES);
		buffer.flip();
	}
	
	public SearchableString(final ByteBuffer BUFFER){
		buffer = BUFFER;
	}
	
	public final void setString(final String STRING){
		final byte[] BYTES = STRING.getBytes();
		buffer = ByteBuffer.allocateDirect(BYTES.length);
		buffer.put(BYTES);
		buffer.flip();
	}
	
	public final void setBuffer(final ByteBuffer BUFFER){
		buffer = BUFFER;
	}
	
	public final ByteBuffer getByteBuffer(){
		return buffer;
	}
	
	public final void setBuffer(final ByteBuffer BUFFER, final boolean REMOVE_FORMATTING, final boolean NEW_LINE_SPACE){
		if(REMOVE_FORMATTING){
			final String encoding = System.getProperty("file.encoding");
			final CharBuffer CHAR_BUFFER = Charset.forName(encoding).decode(BUFFER);
			int amount = 0;
			for(int i = 0; i < CHAR_BUFFER.length(); i++){
				final char CHAR = CHAR_BUFFER.get(i);
				if(CHAR != '\t' && CHAR != '\r'){
					if(!NEW_LINE_SPACE && CHAR != '\n'){
						amount++;
					}else if(NEW_LINE_SPACE){
						amount++;
					}
				}
			}
			buffer = ByteBuffer.allocateDirect(amount);
			amount = 0;
			for(int i = 0; i < CHAR_BUFFER.length(); i++){
				final char CHAR = CHAR_BUFFER.get(i);
				if(CHAR != '\t' && CHAR != '\r'){
					if(NEW_LINE_SPACE && CHAR == '\n'){
						buffer.put(amount++,(byte)' ');
					}else if((!NEW_LINE_SPACE && CHAR != '\n') || NEW_LINE_SPACE){
						buffer.put(amount++,(byte)CHAR);
					}
				}
			}
		}else{
			buffer = BUFFER;
		}
	}
	
	/*
	public final byte[] getBytes(final StringElement ELEMENT_1, final int OFFSET_1){
		return getBytes(ELEMENT_1,null,OFFSET_1,0);
	}
	
	public final byte[] getBytes(final StringElement ELEMENT_1, final int OFFSET_1, final boolean FROM_START){
		return getBytes(null,ELEMENT_1,0,OFFSET_1);
	}
	
	public final byte[] getBytes(final StringElement ELEMENT_1, final StringElement ELEMENT_2, final int OFFSET_1, final int OFFSET_2){
		final int POSITION = (ELEMENT_1 != null) ? ELEMENT_1.getStartPos()+OFFSET_1 : 0;
		final int END = (ELEMENT_2 != null) ? ELEMENT_2.getStartPos()+OFFSET_2-POSITION : buffer.limit();
		return getBytes(POSITION,END);
	}
	
	*/
	public final byte[] getBytes(final int OFFSET, final int LENGTH){
		buffer.rewind();
		buffer.position(OFFSET);
		final byte[] BYTES = new byte[LENGTH];
		buffer.get(BYTES);
		return BYTES;
	}
	
	public final byte[] getBytes(final int LENGTH){
		final byte[] BYTES = new byte[LENGTH];
		buffer.get(BYTES);
		return BYTES;
	}
	
	public final int findAmount(final String INDEX_1){
		return findAmount(INDEX_1.getBytes(),false);
	}
	
	public final int findAmount(final String INDEX_1, final boolean IGNORE_CASE){
		return findAmount(INDEX_1.getBytes(),IGNORE_CASE);
	}
	
	public final int findAmount(final byte[] INDEX_1){
		return findAmount(INDEX_1,false);
	}

	public final int findAmount(final byte[] INDEX_1, final boolean IGNORE_CASE){
		int amount = 0;
		while(buffer.hasRemaining()){
			for(int i = 0; i < INDEX_1.length; i++){
				final byte BYTE = buffer.get();
				if(IGNORE_CASE){
					if((byte)(Character.toLowerCase((char)BYTE) & 0xFF) == (byte)(Character.toLowerCase((char)INDEX_1[i]) & 0xFF)){
						if(i == INDEX_1.length-1){
							amount++;
						}
					}else{
						break;
					}
				}else if(BYTE == INDEX_1[i]){
					if(i == INDEX_1.length-1){
						amount++;
					}
				}else{
					break;
				}
			}
		}
		return amount;
	}
	
	public final StringElement findIndex(final String INDEX_1){
		return findIndex(INDEX_1.getBytes(),false);
	}
	
	public final StringElement findIndex(final String INDEX_1, final boolean IGNORE_CASE){
		return findIndex(INDEX_1.getBytes(),IGNORE_CASE);
	}
	
	public final StringElement findIndex(final byte[] INDEX_1){
		return findIndex(INDEX_1,false);
	}
	
	public final StringElement findIndex(final byte[] INDEX_1, final boolean IGNORE_CASE){
		while(buffer.hasRemaining()){
			for(int i = 0; i < INDEX_1.length; i++){
				final byte BYTE = buffer.get();
				if(IGNORE_CASE){
					if((byte)(Character.toLowerCase((char)BYTE) & 0xFF) == (byte)(Character.toLowerCase((char)INDEX_1[i]) & 0xFF)){
						if(i == INDEX_1.length-1){
							return new StringElement(buffer.position()-i,INDEX_1);
						}
					}else{
						break;
					}
				}else if(BYTE == INDEX_1[i]){
					if(i == INDEX_1.length-1){
						return new StringElement(buffer.position()-i,INDEX_1);
					}
				}else{
					break;
				}
			}
		}
		return null;
	}
	
	public final StringElement[] findIndexs(final String INDEX_1){
		return findIndexs(INDEX_1.getBytes(),false);
	}
	
	public final StringElement[] findIndexs(final String INDEX_1, final boolean IGNORE_CASE){
		return findIndexs(INDEX_1.getBytes(),IGNORE_CASE);
	}
	
	public final StringElement[] findIndexs(final byte[] INDEX_1){
		return findIndexs(INDEX_1,false);
	}
	
	public final StringElement[] findIndexs(final byte[] INDEX_1, final boolean IGNORE_CASE){
		final StringElement[] ELEMENTS = new StringElement[findAmount(INDEX_1)];
		for(int i = 0; i < ELEMENTS.length; i++){
			final StringElement ELEMENT = findIndex(INDEX_1,IGNORE_CASE);
			if(ELEMENT != null){
				position(ELEMENT.getEndPos());
				ELEMENTS[i] = ELEMENT;
			}
		}
		return ELEMENTS;
	}
	
	public final String toString(){
		return toString(0);
	}
	
	public final String toString(final int OFFSET){
		return toString(OFFSET,buffer.limit()-OFFSET);
	}
	
	public final String toString(final int OFFSET, final int LENGTH){
		final byte[] BYTES = new byte[LENGTH];
		buffer.position(OFFSET);
		buffer.get(BYTES);
		return new String(BYTES);
	}
	
	public final byte[] toBytes(){
		return toBytes(0);
	}
	
	public final byte[] toBytes(final int OFFSET){
		return toBytes(OFFSET,buffer.limit()-OFFSET);
	}
	
	public final byte[] toBytes(final int OFFSET, final int LENGTH){
		final byte[] BYTES = new byte[LENGTH];
		buffer.position(OFFSET);
		buffer.get(BYTES);
		return BYTES;
	}
	
	/*
	private final boolean checkIndex(final String INDEX_1){
		return checkIndex(INDEX_1,null);
	}
	
	private final boolean checkIndex(final String INDEX_1, final String INDEX_2){
		return checkIndex(INDEX_1,INDEX_2);
	}
	
	private final boolean checkIndex(final String INDEX_1, final String INDEX_2){
		if(INDEX_1 != null && INDEX_2 != null){
			return (contains(INDEX_1,false) && contains(INDEX_2,false));
		}else if(INDEX_1 != null){
			return contains(INDEX_1,false);
		}else if(INDEX_2 != null){
			return contains(INDEX_2,false);
		}
		return false;
	}
	*/
	
	public final String removeEscape(String str){
		str = str.replace( "\"", "" );
		str = str.replace( "'", "" );
		return str;
	}
	
	public final SearchableString position(final String STRING) throws StringNotFoundException{
		return position(findIndex(STRING.getBytes()));
	}
	
	public final SearchableString position(final byte[] STRING) throws StringNotFoundException{
		return position(findIndex(STRING));
	}
	
	public final SearchableString position(final StringElement ELEMENT) throws StringNotFoundException{
		if(ELEMENT == null) throw new StringNotFoundException();
		buffer.position(ELEMENT.getEndPos()-1);
		//System.out.println(ELEMENT.toString()+" Offset: "+ELEMENT.getEndPos());
		return this;
	}
	
	public final SearchableString position(final int OFFSET){
		buffer.position(OFFSET);
		//System.out.println("Offset: "+OFFSET);
		return this;
	}
	
	public final int position(){
		return buffer.position();
	}
	
	/*
	public final ByteBuffer toLowerCase(){
		return toLowerCase(0);
	}
	
	public final ByteBuffer toLowerCase(final int OFFSET){
		buffer.rewind();
		buffer.position(OFFSET);
		final ByteBuffer BUFFER = ByteBuffer.allocateDirect(buffer.limit()-OFFSET);
		while(buffer.hasRemaining()){
			BUFFER.put((byte)(Character.toLowerCase((char)buffer.get()) & 0xFF));
		}
		BUFFER.flip();
		return BUFFER;
	}
	*/
	
	public final boolean contains(final String STRING){
		return contains(STRING,false);
	}
	
	public final boolean contains(final String STRING, final boolean IGNORE_CASE){
		if(STRING == null || STRING.equals("")){
			return false;
		}
        return (findIndex(STRING,IGNORE_CASE) != null);
	}
	
	
	/* TODO: MAKE REPLACE ALL */
	public final boolean replace(final String STRING, final String WITH_STRING) throws NullPointerException{
		return replace(STRING.getBytes(),WITH_STRING.getBytes(),false);
	}
	
	public final boolean replace(final String STRING, final String WITH_STRING, final boolean IGNORE_CASE) throws NullPointerException{
		return replace(STRING.getBytes(),WITH_STRING.getBytes(),IGNORE_CASE);
	}
	
	public final boolean replace(final byte[] STRING, final byte[] WITH_STRING, final boolean IGNORE_CASE) throws NullPointerException{
		try{
			if(STRING == null){
				throw new NullPointerException();
			}
			final StringElement ELEMENT = findIndex(STRING,IGNORE_CASE);
			if(ELEMENT != null){
				if(WITH_STRING == null){
					final ByteBuffer BUFFER = ByteBuffer.allocateDirect(buffer.limit()-STRING.length);
					buffer.rewind();
					while(buffer.hasRemaining()){
						if(ELEMENT.getStartPos() > buffer.position() || buffer.position() > ELEMENT.getEndPos()){
							BUFFER.put(buffer.get());
						}
					}
					buffer = BUFFER;
					return true;
				}else{
					final ByteBuffer BUFFER = ByteBuffer.allocateDirect(buffer.limit()+(WITH_STRING.length-STRING.length));
					buffer.rewind();
					while(buffer.hasRemaining()){
						final byte BYTE = buffer.get();
						if(ELEMENT.getStartPos() == buffer.position()){
							for (int i = 0; i < WITH_STRING.length; i++){
								BUFFER.put(WITH_STRING[i]);
							}
							buffer.position(ELEMENT.getEndPos()-1);
						}else{
							BUFFER.put(BYTE);
						}
					}
					buffer = BUFFER;
					buffer.position(ELEMENT.getEndPos()-1);
					return true;
				}
			}
		}catch(final Exception e){ e.printStackTrace(); }
		return false;
	}
	/*
	public final boolean replace(final int START_POS, final String STRING, final String WITH_STRING, final boolean IGNORE_CASE) throws NullPointerException{
		if(STRING == null || STRING.equals("")){
			throw new NullPointerException();
		}
		final int POSITION = findIndex(START_POS,STRING,IGNORE_CASE);
		if(IGNORE_CASE){
			final int POSTION = string.substring(START_POS).toLowerCase().indexOf(STRING);
			if(POSTION > -1){
				final String BEFORE_INSERT = string.substring(0,POSTION);
				final String AFTER_INSERT = string.substring(POSTION+STRING.length());
				if(WITH_STRING == null || WITH_STRING.equals("")){
					string = BEFORE_INSERT+AFTER_INSERT;
				}else{
					string = BEFORE_INSERT+WITH_STRING+AFTER_INSERT;
				}
				return true;
			}else{
				return false;
			}
		}else{
			final int POSTION = string.substring(START_POS).indexOf(STRING);
			if(POSTION > -1){
				final String BEFORE_INSERT = string.substring(0,POSTION);
				final String AFTER_INSERT = string.substring(POSTION+STRING.length());
				if(WITH_STRING == null || WITH_STRING.equals("")){
					string = BEFORE_INSERT+AFTER_INSERT;
				}else{
					string = BEFORE_INSERT+WITH_STRING+AFTER_INSERT;
				}
				return true;
			}else{
				return false;
			}
		}
	}
	*/
	
	private final String makeNumberSafe(String str){
		str = str.replace( ",", "" );
		str = str.replace( "#", "" );
		return str;
	}
	
	public final int getInt(final int LENGTH){
		final int CURRENT_POSITION = position();
		return Integer.parseInt(makeNumberSafe(toString(CURRENT_POSITION,LENGTH).trim()));
	}
	
	public final int getInt(final String INDEX) throws StringNotFoundException{
		final int CURRENT_POSITION = position();
		final StringElement ELEMENT = findIndex(INDEX);
		if(ELEMENT == null) throw new StringNotFoundException();
		return Integer.parseInt(makeNumberSafe(toString(CURRENT_POSITION,ELEMENT.getStartPos()-1-CURRENT_POSITION).trim()));
		/*
		int integer = 0;
		for(int i = CURRENT_POSITION; i < ELEMENT.getEndPos()-ELEMENT.getLength()-1-CURRENT_POSITION; i++){
			integer += (buffer.get());
		}
		return integer;
		*/
	}
	
	public final double getDouble(final String INDEX) throws StringNotFoundException{
		final int CURRENT_POSITION = position();
		final StringElement ELEMENT = findIndex(INDEX);
		if(ELEMENT == null) throw new StringNotFoundException();
		return Double.parseDouble(makeNumberSafe(toString(CURRENT_POSITION,ELEMENT.getStartPos()-1-CURRENT_POSITION).trim()));
	}
	
	/*
	public final int getInt(final String INDEX) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getInt(INDEX,null,false,false);
	}

	public final int getInt(final String INDEX, final boolean OFFSET_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
        return getInt(INDEX,null,OFFSET_SELF,false);
    }
	
	public final int getInt(final String INDEX_1, final String INDEX_2) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getInt(INDEX_1,INDEX_2,false,false);
	}
	
	public final int getInt(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getInt(INDEX_1,INDEX_2,OFFSET_1_SELF,false);
	}
	
	public final int getInt(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF, final boolean OFFSET_2_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
		final String STRING = makeNumberSafe(getString(INDEX_1,INDEX_2,OFFSET_1_SELF,OFFSET_2_SELF));
		if(STRING == null || STRING.equals("")){
			throw new NullPointerException();
		}
		return Integer.parseInt(STRING);
	}
	*/
	/*
	public final int[] getInts(final String INDEX_1, final String INDEX_2) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getInts(INDEX_1,INDEX_2,false,false);
	}
	
	public final int[] getInts(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getInts(INDEX_1,INDEX_2,OFFSET_1_SELF,false);
	}
	
	public final int[] getInts(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF, final boolean OFFSET_2_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
		final CreateString STRINGS = getStrings(INDEX_1,INDEX_2,OFFSET_1_SELF,OFFSET_2_SELF);
		final int[] INTEGERS = new int[STRINGS.size()];
		int tmp = 0;
		for(final String INTEGER : STRINGS.values()){
			INTEGERS[tmp++] = Integer.parseInt(makeNumberSafe(INTEGER));
		}
		return INTEGERS;
	}
	*/
	/*
	public final double getDouble(final String INDEX) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getDouble(INDEX,null,false,false);
	}
	
	public final double getDouble(final String INDEX_1, final String INDEX_2) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getDouble(INDEX_1,INDEX_2,false,false);
	}
	
	public final double getDouble(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getDouble(INDEX_1,INDEX_2,OFFSET_1_SELF,false);
	}
	
	public final double getDouble(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF, final boolean OFFSET_2_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
		final String STRING = makeNumberSafe(getString(INDEX_1,INDEX_2,OFFSET_1_SELF,OFFSET_2_SELF));
		if(STRING == null || STRING.equals("")){
			throw new NullPointerException();
		}
		return Double.parseDouble(STRING);
	}
	*/
	/*
	public final double[] getDoubles(final String INDEX_1, final String INDEX_2) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getDoubles(INDEX_1,INDEX_2,false,false);
	}
	
	public final double[] getDoubles(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
		return getDoubles(INDEX_1,INDEX_2,OFFSET_1_SELF,false);
	}
	
	public final double[] getDoubles(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF, final boolean OFFSET_2_SELF) throws StringNotFoundException, NullPointerException, NumberFormatException{
		final CreateString STRINGS = getStrings(INDEX_1,INDEX_2,OFFSET_1_SELF,OFFSET_2_SELF);
		if(STRINGS.isEmpty()){
			throw new StringNotFoundException();
		}
		final double[] DOUBLES = new double[STRINGS.size()];
		int tmp = 0;
		for(final String DOUBLE : STRINGS.values()){
			DOUBLES[tmp++] = Double.parseDouble(makeNumberSafe(DOUBLE));
		}
		return DOUBLES;
	}
	*/
	
	public final void add(final String STRING){
		final byte[] STRING_BYTES = STRING.getBytes();
		final ByteBuffer BUFFER = ByteBuffer.allocateDirect(buffer.limit()+STRING.length());
		final int CURRENT_POSITION = position();
		BUFFER.put(getBytes(0,CURRENT_POSITION));
		BUFFER.put(STRING_BYTES);
		BUFFER.put(getBytes(CURRENT_POSITION));
		BUFFER.flip();
		buffer = BUFFER;
	}
	
	public final String getString(final int LENGTH){
		final int CURRENT_POSITION = position();
		return toString(CURRENT_POSITION,LENGTH);
	}
	
	public final String getString(final String INDEX){
		final int CURRENT_POSITION = position();
		final StringElement ELEMENT = findIndex(INDEX);
		if(ELEMENT == null) return null;
		return toString(CURRENT_POSITION,ELEMENT.getStartPos()-1-CURRENT_POSITION);
	}
	
	public final String getString(final byte[] INDEX){
		final int CURRENT_POSITION = position();
		final StringElement ELEMENT = findIndex(INDEX);
		if(ELEMENT == null) return null;
		return toString(CURRENT_POSITION,ELEMENT.getStartPos()-1-CURRENT_POSITION);
	}
	
	/*
	public final String getString(final String INDEX) throws StringNotFoundException, NullPointerException{
		return getString(INDEX.getBytes(), null, 0, 0);
	}
	
	public final String getString(final String INDEX_1, final String INDEX_2) throws StringNotFoundException, NullPointerException{
		return getString(INDEX_1.getBytes(), INDEX_2.getBytes(), 0, 0);
	}
	
	public final String getString(final String INDEX_1, final String INDEX_2, final int OFFSET_1) throws StringNotFoundException, NullPointerException{
		return getString(INDEX_1.getBytes(), INDEX_2.getBytes(), OFFSET_1, 0);
	}
	
	public final String getString(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF) throws StringNotFoundException, NullPointerException{
		return getString(INDEX_1.getBytes(), INDEX_2.getBytes(), (OFFSET_1_SELF ? INDEX_1.length() : 0), 0);
	}
	
	public final String getString(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF, final boolean OFFSET_2_SELF) throws StringNotFoundException, NullPointerException{
		return getString(INDEX_1.getBytes(), INDEX_2.getBytes(), (OFFSET_1_SELF ? INDEX_1.length() : 0), (OFFSET_2_SELF ? INDEX_2.length() : 0));
	}
	
	public final String getString(final String INDEX_1, final String INDEX_2, final int OFFSET_1, final int OFFSET_2) throws StringNotFoundException, NullPointerException{
		return getString(INDEX_1.getBytes(),INDEX_2.getBytes(),OFFSET_1,OFFSET_2);
	}
	
	public final String getString(final byte[] INDEX_1, final byte[] INDEX_2, final int OFFSET_1, final int OFFSET_2) throws StringNotFoundException, NullPointerException{
		if(INDEX_1 == null && INDEX_2 == null) throw new NullPointerException();
		final StringElement ELEMENT_1 = findIndex(INDEX_1);
		if(ELEMENT_1 == null && INDEX_1 != null) throw new StringNotFoundException();
		if(ELEMENT_1 != null) {
			position(ELEMENT_1.getStartPos()+OFFSET_1);
		}
		final StringElement ELEMENT_2 = findIndex(INDEX_2);
		if(ELEMENT_2 == null && INDEX_2 != null) throw new StringNotFoundException();
		if(INDEX_1 != null && INDEX_2 != null){
			return new String(getBytes(ELEMENT_1,ELEMENT_2,OFFSET_1,OFFSET_2-1));
		}else if(INDEX_1 != null){
			return new String(getBytes(ELEMENT_1,OFFSET_1));
		}else if(INDEX_2 != null){
			return new String(getBytes(ELEMENT_2,OFFSET_2,true));
		}
		return null;
	}
	*/
	/*
	public final StringElement getStringElement(final String INDEX) throws StringNotFoundException, NullPointerException{
		return getStringElement(INDEX, null, 0, 0);
	}
	
	public final StringElement getStringElement(final String INDEX_1, final String INDEX_2) throws StringNotFoundException, NullPointerException{
		return getStringElement(INDEX_1, INDEX_2, 0, 0);
	}
	
	public final StringElement getStringElement(final String INDEX_1, final String INDEX_2, final int OFFSET_1) throws StringNotFoundException, NullPointerException{
		return getStringElement(INDEX_1, INDEX_2, OFFSET_1, 0);
	}
	
	public final StringElement getStringElement(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF) throws StringNotFoundException, NullPointerException{
		return getStringElement(INDEX_1, INDEX_2, OFFSET_1_SELF, false);
	}
	
	public final StringElement getStringElement(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF, final boolean OFFSET_2_SELF) throws StringNotFoundException, NullPointerException{
		return getStringElement(INDEX_1, INDEX_2, (OFFSET_1_SELF ? INDEX_1.length() : 0), (OFFSET_2_SELF ? INDEX_2.length() : 0));
	}
	
	public final StringElement getStringElement(final String INDEX_1, final String INDEX_2, final int OFFSET_1, final int OFFSET_2) throws StringNotFoundException, NullPointerException{
		return getStringElement(0,INDEX_1,INDEX_2,OFFSET_1,OFFSET_2);
	}
	
	public final StringElement getStringElement(final String INDEX_1, final String INDEX_2, final int OFFSET_1, final int OFFSET_2) throws StringNotFoundException, NullPointerException{
		if(INDEX_1 == null && INDEX_2 == null) throw new NullPointerException();
		final StringElement ELEMENT_1 = findIndex(INDEX_1);
		if(ELEMENT_1 == null && INDEX_1 != null) throw new StringNotFoundException();
		final StringElement ELEMENT_2 = findIndex((ELEMENT_1 != null) ? ELEMENT_1.getStartPos() : START_POS,INDEX_2);
		if(ELEMENT_2 == null && INDEX_2 != null) throw new StringNotFoundException();
		final int POSITION = (ELEMENT_1 != null) ? ELEMENT_1.getStartPos()+OFFSET_1 : (ELEMENT_2 != null) ? ELEMENT_2.getStartPos()+OFFSET_2 : 0;
		if(INDEX_1 != null && INDEX_2 != null){
			return new StringElement(POSITION,getBytes(POSITION,ELEMENT_2.getStartPos()+OFFSET_2));
		}else if(INDEX_1 != null){
			return new StringElement(POSITION,getBytes(POSITION,buffer.limit()));
		}else if(INDEX_2 != null){
			return new StringElement(POSITION,getBytes(0,POSITION));
		}
		return null;
	}
	*/
	/*
	public final int stringPos(final String INDEX, final boolean OFFSET_SELF) throws StringNotFoundException, NullPointerException{
		return stringPos(INDEX,(OFFSET_SELF ? INDEX.length() : 0));
	}
	
	public final int stringPos(final String INDEX, final int OFFSET) throws StringNotFoundException, NullPointerException{
		checkIndex(INDEX);
		return string.indexOf(INDEX)+OFFSET;
	}
	
	public final int stringPos(final int START_POS, final String INDEX, final int OFFSET) throws StringNotFoundException, NullPointerException{
		checkIndex(START_POS,INDEX);
		return START_POS+string.substring(START_POS).indexOf(INDEX)+OFFSET;
	}
	
	public final int stringPos(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF) throws StringNotFoundException, NullPointerException{
		return stringPos(0,INDEX_1,INDEX_2,(OFFSET_1_SELF ? INDEX_1.length() : 0),0);
	}
	
	public final int stringPos(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF, final boolean OFFSET_2_SELF) throws StringNotFoundException, NullPointerException{
		return stringPos(0,INDEX_1,INDEX_2,(OFFSET_1_SELF ? INDEX_1.length() : 0),(OFFSET_2_SELF ? INDEX_2.length() : 0));
	}
	
	public final int stringPos(final String INDEX_1, final String INDEX_2, final int OFFSET_1, final int OFFSET_2) throws StringNotFoundException, NullPointerException{
		return stringPos(0,INDEX_1,INDEX_2,OFFSET_1,OFFSET_2);
	}
	
	public final int stringPos(final int START_POS, final String INDEX_1, final String INDEX_2, final int OFFSET_1, final int OFFSET_2) throws StringNotFoundException, NullPointerException{
		checkIndex(START_POS,INDEX_1,INDEX_2);
		final int POSTION = START_POS+string.substring(START_POS).indexOf(INDEX_1)+OFFSET_1;
		String subString = string.substring(POSTION);
		if(!subString.contains(INDEX_2)){
			throw new StringNotFoundException();
		}
		return (POSTION+subString.indexOf(INDEX_2)+OFFSET_2);
	}
	*/
	/*
	public final CreateString getStrings(final String INDEX) throws StringNotFoundException, NullPointerException{
		return getStrings(INDEX, null, 0, 0);
	}
	
	public final CreateString getStrings(final String INDEX_1, final String INDEX_2) throws StringNotFoundException, NullPointerException{
		return getStrings(INDEX_1, INDEX_2, 0, 0);
	}
	
	public final CreateString getStrings(final String INDEX_1, final String INDEX_2, final int OFFSET_1) throws StringNotFoundException, NullPointerException{
		return getStrings(INDEX_1, INDEX_2, OFFSET_1, 0);
	}
	
	public final CreateString getStrings(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF) throws StringNotFoundException, NullPointerException{
		return getStrings(INDEX_1, INDEX_2, OFFSET_1_SELF, false);
	}
	
	public final CreateString getStrings(final String INDEX_1, final String INDEX_2, final boolean OFFSET_1_SELF, final boolean OFFSET_2_SELF) throws StringNotFoundException, NullPointerException{
		return getStrings(INDEX_1, INDEX_2, (OFFSET_1_SELF ? INDEX_1.length() : 0), (OFFSET_2_SELF ? INDEX_2.length() : 0));
	}
	*/
	/*
	public final CreateString getStrings(final String INDEX_1, final String INDEX_2, final int OFFSET_1, final int OFFSET_2) throws StringNotFoundException, NullPointerException{
		final CreateString STRINGS = new CreateString();
		int lastIndex = 0;
		do{
			try{
				final StringElement stringElement = getStringElement(lastIndex,INDEX_1,INDEX_2,OFFSET_1,OFFSET_2);
				if(stringElement == null){
					break;
				}
				//System.out.println("["+stringElement.getStartPos()+"]"+stringElement.toString()+"["+stringElement.getEndPos()+"]");
				STRINGS.put(stringElement.toString());
				lastIndex = stringElement.getEndPos();
				*/
				/*
				if(INDEX_1 != null && INDEX_2 != null){
					final int TMP = stringPos(lastIndex,INDEX_1,INDEX_2,OFFSET_1,OFFSET_2);
					STRINGS.put(string.substring(stringPos(lastIndex,INDEX_1,OFFSET_1),TMP));
					lastIndex = TMP;
				}else if(INDEX_1 != null){
					lastIndex = stringPos(lastIndex,INDEX_1,OFFSET_1);
					STRINGS.put(string.substring(lastIndex));
				}else if(INDEX_2 != null){
					lastIndex = stringPos(lastIndex,INDEX_2,OFFSET_2);
					STRINGS.put(string.substring(0,lastIndex));
				}
				*/
				/*
			}catch(final Exception e){
				break;
			}
		}while(true);
		if(STRINGS.isEmpty()){
			throw new StringNotFoundException();
		}
		return STRINGS;
	}
	*/
}
