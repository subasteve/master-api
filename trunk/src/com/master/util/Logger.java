package com.master.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Calendar;

import java.util.Map;
import java.util.HashMap;

import com.master.data.FreeNumber;
import com.master.util.interfaces.LoggerHook;

public class Logger implements LoggerHook{

	private BufferedWriter out = null;
	private final Map<Integer, LoggerHook> HOOKS = new HashMap<Integer, LoggerHook>();
	private static final FreeNumber uniqueIDGen = new FreeNumber();
    private boolean toFile = true, toConsole = true, appendTimeStamp = false;
	private LogLevel logLevel = LogLevel.Info;
	private boolean useColor = false, logColor = false;
	

	public Logger(final String PATH){
		setFile(PATH);
		toConsole = false;
		appendTimeStamp = false;
		writeLine("Logger Started");
	}
	
	public Logger(final String PATH, final boolean CONSOLE){
        setFile(PATH);
		toConsole = CONSOLE;
		appendTimeStamp = false;
		writeLine("Logger Started");
	}
	
	public Logger(final String PATH, final boolean CONSOLE, final boolean APPEND){
        setFile(PATH,APPEND);
		toConsole = CONSOLE;
		appendTimeStamp = false;
		writeLine("Logger Started");
	}
	
	public Logger(final String PATH, final boolean CONSOLE, final boolean APPEND, final boolean TIME_STAMP){
        setFile(PATH,APPEND);
		appendTimeStamp = TIME_STAMP;
		toConsole = CONSOLE;
		writeLine("Logger Started");
	}
	
	public Logger(final String PATH, final boolean CONSOLE, final boolean APPEND, final boolean TIME_STAMP, final boolean START_MSG){
        setFile(PATH,APPEND);
		appendTimeStamp = TIME_STAMP;
		toConsole = CONSOLE;
		if(START_MSG){
			writeLine("Logger Started");
		}
	}

    public Logger(final boolean CONSOLE){
        appendTimeStamp = false;
        toConsole = CONSOLE;
        toFile = false;
        writeLine("Logger Started");
    }

    public Logger(final boolean CONSOLE, final boolean TIME_STAMP){
        appendTimeStamp = TIME_STAMP;
        toConsole = CONSOLE;
        toFile = false;
        writeLine("Logger Started");
    }

    public Logger(final boolean CONSOLE, final boolean TIME_STAMP, final boolean START_MSG){
        appendTimeStamp = TIME_STAMP;
        toConsole = CONSOLE;
        toFile = false;
        if(START_MSG){
            writeLine("Logger Started");
        }
    }

    public Logger loadConfig(final LoadIni SETTINGS) throws Exception {
        return toFile(SETTINGS.loadBoolean("logToFile")).toConsole(SETTINGS.loadBoolean("console")).setLevel(SETTINGS.loadString("logLevel")).useColor(SETTINGS.loadBoolean("colorConsole")).logColor(SETTINGS.loadBoolean("colorLog"));
    }

    public Logger generateConfig(final LoadIni SETTINGS){
	if(SETTINGS != null){
		boolean changes = false;
		if(!SETTINGS.hasProperty("logToFile")){
			SETTINGS.setProperty("logToFile", "false");
			changes = true;
		}
		if(!SETTINGS.hasProperty("console")){
			SETTINGS.setProperty("console", "true");
			changes = true;
		}
		if(!SETTINGS.hasProperty("logLevel")){
			SETTINGS.setProperty("logLevel", "info");
			changes = true;
		}
		if(!SETTINGS.hasProperty("colorConsole")){
			SETTINGS.setProperty("colorConsole", "false");
			changes = true;
		}
		if(!SETTINGS.hasProperty("colorLog")){
			SETTINGS.setProperty("colorLog", "false");
			changes = true;
		}
		if(changes){
			SETTINGS.save();
		}
	}
	return this;
    }
	
	public Logger(){
		toConsole = true;
		appendTimeStamp = false;
		writeLine("Logger Started");
	}
	
	public final int addHook(final LoggerHook HOOK){
		final int ID = uniqueIDGen.put();
		HOOKS.put(ID,HOOK);
		return ID;
	}
	
	public final Logger removeHook(final int ID){
		HOOKS.remove(ID);
		uniqueIDGen.remove(ID);
        return this;
	}

    public final Logger setFile(final String PATH){
        setFile(PATH,true);
    	return this;
    }

    public final Logger setFile(final String PATH, final boolean APPEND){
        try{
            out = new BufferedWriter(new FileWriter(PATH,APPEND));
        }catch(Exception e){
            e.printStackTrace();
        }
        return this;
    }

    public final Logger toFile(final boolean BOOL){
        toFile = BOOL;
        return this;
    }

    public final Logger toConsole(final boolean BOOL){
        toConsole = BOOL;
        return this;
    }

    public final Logger useColor(final boolean BOOL){
    	useColor = BOOL;
    	return this;
    }

    public final Logger logColor(final boolean BOOL){
    	logColor = BOOL;
    	return this;
    }

    public final Logger appendTimeStamp(final boolean BOOL){
        appendTimeStamp = BOOL;
        return this;
    }
	
	public final Logger setLevel(final LogLevel LEVEL){
		logLevel = LEVEL;
        return this;
	}
	
	public final Logger setLevel(final String LEVEL){
		if(LEVEL.equalsIgnoreCase("error")){
			logLevel = LogLevel.Error;
		}else if(LEVEL.equalsIgnoreCase("warn")){
			logLevel = LogLevel.Warn;
		}else if(LEVEL.equalsIgnoreCase("info")){
			logLevel = LogLevel.Info;
		}else if(LEVEL.equalsIgnoreCase("fine")){
			logLevel = LogLevel.Fine;
		}else if(LEVEL.equalsIgnoreCase("verbose")){
			logLevel = LogLevel.Verbose;
		}
        return this;
	}
	
	public final LogLevel getLevel(){
		return logLevel;
	}
	
	public final void destruct(){
		if(out != null){
			try{
				out.close();
			}catch(Exception e){}
		}
	}
	
	public final boolean isAM(){
		final Calendar CALENDAR = Calendar.getInstance();
        return (CALENDAR.get(Calendar.AM_PM) == Calendar.AM);
	}
	
	public final int getHour(){
		final Calendar CALENDAR = Calendar.getInstance();
		return CALENDAR.get(Calendar.HOUR);
	}
	
	public final int getMinute(){
		final Calendar CALENDAR = Calendar.getInstance();
		return CALENDAR.get(Calendar.MINUTE);
	}
	
	public final int getSecond(){
		final Calendar CALENDAR = Calendar.getInstance();
		return CALENDAR.get(Calendar.SECOND);
	}
	
	public final int[] getHourMinuteSecond(){
		final Calendar CALENDAR = Calendar.getInstance();
		final int[] ARRAY = {CALENDAR.get(Calendar.HOUR),CALENDAR.get(Calendar.MINUTE),CALENDAR.get(Calendar.SECOND)};
		return ARRAY;
	}
	
	public final int getMonth(){
		final Calendar CALENDAR = Calendar.getInstance();
		return CALENDAR.get(Calendar.MONTH);
	}
	
	public final int getDay(){
		final Calendar CALENDAR = Calendar.getInstance();
		return CALENDAR.get(Calendar.DAY_OF_MONTH);
	}
	
	public final int getYear(){
		final Calendar CALENDAR = Calendar.getInstance();
		return CALENDAR.get(Calendar.YEAR);
	}
	
	public final int[] getMonthDayYear(){
		final Calendar CALENDAR = Calendar.getInstance();
		final int[] ARRAY = {CALENDAR.get(Calendar.MONTH),CALENDAR.get(Calendar.DAY_OF_MONTH),CALENDAR.get(Calendar.YEAR)};
		return ARRAY;
	}
	
	public final int[] getDate(){
		final Calendar CALENDAR = Calendar.getInstance();
		final int[] ARRAY = {CALENDAR.get(Calendar.MONTH),CALENDAR.get(Calendar.DAY_OF_MONTH),CALENDAR.get(Calendar.YEAR),CALENDAR.get(Calendar.HOUR),CALENDAR.get(Calendar.MINUTE),CALENDAR.get(Calendar.SECOND)};
		return ARRAY;
	}
	
	public final String getTimeStamp(){
		final StringBuilder BUFFER = new StringBuilder(10);
		final Calendar CALENDAR = Calendar.getInstance();
		BUFFER.append("[");
		BUFFER.append(CALENDAR.get(Calendar.HOUR));
		BUFFER.append(":");
		BUFFER.append(CALENDAR.get(Calendar.MINUTE));
		BUFFER.append(":");
		BUFFER.append(CALENDAR.get(Calendar.SECOND));
		BUFFER.append("]");
		return BUFFER.toString();
	}
	
	public synchronized final Logger writeTimeStamp(){
		if(appendTimeStamp){
			if(toFile && out != null){
				try{
					out.write(getTimeStamp());
				}catch(final Exception e){}
			}
			if(toConsole){
				System.out.print(getTimeStamp());
			}
		}
        return this;
	}
	
	public final boolean canWriteLog(final LogLevel LEVEL){
		if(logLevel == LogLevel.None){
			return false;
		}else if(logLevel == LogLevel.Error && LEVEL != LogLevel.Error){
			return false;
		}else if(logLevel == LogLevel.Warn && (LEVEL != LogLevel.Error && LEVEL != LogLevel.Warn)){
			return false;
		}else if(logLevel == LogLevel.Info && (LEVEL != LogLevel.Error && LEVEL != LogLevel.Warn && LEVEL != LogLevel.Info)){
			return false;
		}else if(logLevel == LogLevel.Fine && (LEVEL != LogLevel.Error && LEVEL != LogLevel.Warn && LEVEL != LogLevel.Info && LEVEL != LogLevel.Fine)){
			return false;
		}else if(logLevel == LogLevel.Verbose && (LEVEL != LogLevel.Error && LEVEL != LogLevel.Warn && LEVEL != LogLevel.Info && LEVEL != LogLevel.Fine && LEVEL != LogLevel.Verbose)){
			return false;
		}
		return true;
	}
	
	public synchronized final Logger write(final CreateString STRINGS){
		return write(STRINGS,true);
	}
	
	public synchronized final Logger write(final SearchableString STRING){
		return write(STRING,LogLevel.Info,null);
	}

	public synchronized final Logger write(final SearchableString STRING, final Color COLOR){
		return write(STRING,LogLevel.Info,COLOR);
	}

	public synchronized final Logger write(final SearchableString STRING, final LogLevel LEVEL){
		return write(STRING,LEVEL,null);
	}
	
	public synchronized final Logger write(final SearchableString STRING, final LogLevel LEVEL, final Color COLOR){
		if(!canWriteLog(LEVEL)){
			return this;
		}
		for(final LoggerHook HOOK : HOOKS.values()){
			HOOK.write(STRING,LEVEL,COLOR);
		}
		writeTimeStamp();
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					out.write(COLOR.getEscapeCode());
				}
				out.write(STRING.toString());
				if(useColor && logColor && COLOR != null){
					final String END_COLOR = COLOR.getEndEscapeCode();
					if(END_COLOR != null){
						out.write(END_COLOR);
					}
				}
				out.flush();
			}catch(Exception e){}
		}
		if(toConsole){
			if(useColor && COLOR != null){
				System.out.print(COLOR.getEscapeCode());
			}
			System.out.print(STRING.toString());
			if(useColor && COLOR != null){
				final String END_COLOR = COLOR.getEndEscapeCode();
				if(END_COLOR != null){
					System.out.print(END_COLOR);
				}
			}
		}
        return this;
	}
	
	public synchronized final Logger write(final CreateString STRINGS, final boolean NEWLINE){
		return write(STRINGS,NEWLINE,LogLevel.Info,null);
	}

	public synchronized final Logger write(final CreateString STRINGS, final boolean NEWLINE, final Color COLOR){
		return write(STRINGS,NEWLINE,LogLevel.Info,COLOR);
	}

	public synchronized final Logger write(final CreateString STRINGS, final boolean NEWLINE, final LogLevel LEVEL){
		return write(STRINGS,NEWLINE,LEVEL,null);	
	}
	
	public synchronized final Logger write(final CreateString STRINGS, final boolean NEWLINE, final LogLevel LEVEL, final Color COLOR){
		if(!canWriteLog(LEVEL)){
			return this;
		}
		for(final LoggerHook HOOK : HOOKS.values()){
			HOOK.write(STRINGS,NEWLINE,LEVEL,COLOR);
		}
		writeTimeStamp();
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					out.write(COLOR.getEscapeCode());
				}
			}catch(Exception e){}
		}
		if(useColor && COLOR != null){
			System.out.print(COLOR.getEscapeCode());
		}
		for(final String STRING : STRINGS.values()){
			if(toFile && out != null){
				try{
					out.write(STRING);
					if(NEWLINE){
						out.write("\n");
					}
				}catch(Exception e){}
			}
			if(toConsole){
				if(NEWLINE){
					System.out.println(STRING);
				}else{
					System.out.print(STRING);
				}
			}
		}
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					final String END_COLOR = COLOR.getEndEscapeCode();
					if(END_COLOR != null){
						out.write(END_COLOR);
					}
				}
				out.flush();
			}catch(Exception e){}
		}
		if(toConsole){
			if(useColor && COLOR != null){
				final String END_COLOR = COLOR.getEndEscapeCode();
				if(END_COLOR != null){
					System.out.print(END_COLOR);
				}
			}
		}
        return this;
	}

	public synchronized final Logger write(final Color COLOR){
		return write(COLOR,LogLevel.Info);
	}

	public synchronized final Logger write(final Color COLOR, final LogLevel LEVEL){
		if(!canWriteLog(LEVEL)){
			return this;
		}
		for(final LoggerHook HOOK : HOOKS.values()){
			HOOK.write(LEVEL,COLOR);
		}
		writeTimeStamp();
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					out.write(COLOR.getEscapeCode());
				}
				out.flush();
			}catch(Exception e){}
		}
		if(toConsole){
			if(useColor && COLOR != null){
				System.out.print(COLOR.getEscapeCode());
			}
		}
        return this;
	}
	
	public synchronized final Logger write(final String STRING){
		return write(STRING,LogLevel.Info);
	}

	public synchronized final Logger write(final String STRING, final Color COLOR){
		return write(STRING,LogLevel.Info,COLOR);
	}

	public synchronized final Logger write(final String STRING, final LogLevel LEVEL){
		return write(STRING,LEVEL,null);
	}
	
	public synchronized final Logger write(final String STRING, final LogLevel LEVEL, final Color COLOR){
		if(!canWriteLog(LEVEL)){
			return this;
		}
		for(final LoggerHook HOOK : HOOKS.values()){
			HOOK.write(STRING,LEVEL,COLOR);
		}
		writeTimeStamp();
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					out.write(COLOR.getEscapeCode());
				}
				out.write(STRING);
				if(useColor && logColor && COLOR != null){
					final String END_COLOR = COLOR.getEndEscapeCode();
					if(END_COLOR != null){
						out.write(END_COLOR);
					}
				}
				out.flush();
			}catch(Exception e){}
		}
		if(toConsole){
			if(useColor && COLOR != null){
				System.out.print(COLOR.getEscapeCode());
			}
			System.out.print(STRING);
			if(useColor && COLOR != null){
				final String END_COLOR = COLOR.getEndEscapeCode();
				if(END_COLOR != null){
					System.out.print(END_COLOR);
				}
			}
		}
        return this;
	}
	
	public synchronized final Logger write(final String... STRINGS){
		return write(LogLevel.Info,null, STRINGS);
	}

	public synchronized final Logger write(final Color COLOR, final String... STRINGS){
		return write(LogLevel.Info,COLOR, STRINGS);
	}

	public synchronized final Logger write(final LogLevel LEVEL, final String... STRINGS){
		return write(LEVEL,null, STRINGS);
	}
	
	public synchronized final Logger write(final LogLevel LEVEL, final Color COLOR, final String... STRINGS){
		if(!canWriteLog(LEVEL)){
			return this;
		}
		for(final LoggerHook HOOK : HOOKS.values()){
			HOOK.write(LEVEL,COLOR,STRINGS);
		}
		writeTimeStamp();
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					out.write(COLOR.getEscapeCode());
				}
			}catch(Exception e){}
		}
		if(useColor && COLOR != null){
			System.out.print(COLOR.getEscapeCode());
		}
		for(final String STRING : STRINGS){
			if(toFile && out != null){
				try{
					out.write(STRING);
				}catch(final Exception e){}
			}
			if(toConsole){
				System.out.print(STRING);
			}
		}
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					final String END_COLOR = COLOR.getEndEscapeCode();
					if(END_COLOR != null){
						out.write(END_COLOR);
					}
				}
				out.flush();
			}catch(Exception e){}
		}
		if(toConsole){
			if(useColor && COLOR != null){
				final String END_COLOR = COLOR.getEndEscapeCode();
				if(END_COLOR != null){
					System.out.print(END_COLOR);
				}
			}
		}
        return this;
	}
	
	public synchronized final Logger writeLine(final String STRING){
		return writeLine(STRING,LogLevel.Info,null);
	}

	public synchronized final Logger writeLine(final String STRING, final Color COLOR){
		return writeLine(STRING,LogLevel.Info,COLOR);
	}

	public synchronized final Logger writeLine(final String STRING, final LogLevel LEVEL){
		return writeLine(STRING,LEVEL,null);
	}
	
	public synchronized final Logger writeLine(final String STRING, final LogLevel LEVEL, final Color COLOR){
		if(!canWriteLog(LEVEL)){
			return this;
		}
		for(final LoggerHook HOOK : HOOKS.values()){
			HOOK.writeLine(STRING,LEVEL,COLOR);
		}
		writeTimeStamp();
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					out.write(COLOR.getEscapeCode());
				}
				out.write(STRING);
				if(useColor && logColor && COLOR != null){
					final String END_COLOR = COLOR.getEndEscapeCode();
					if(END_COLOR != null){
						out.write(END_COLOR);
					}
				}
				out.write("\n");
				out.flush();
			}catch(final Exception e){}
		}
		if(toConsole){
			if(useColor && COLOR != null){
				System.out.print(COLOR.getEscapeCode());
			}
			System.out.println(STRING);
			if(useColor && COLOR != null){
				final String END_COLOR = COLOR.getEndEscapeCode();
				if(END_COLOR != null){
					System.out.print(END_COLOR);
				}
			}
		}
        return this;
	}
	
	public synchronized final Logger writeLine(final String... STRINGS){
		return writeLine(LogLevel.Info,null,STRINGS);
	}

	public synchronized final Logger writeLine(final LogLevel LEVEL, final String... STRINGS){
		return writeLine(LEVEL,null,STRINGS);
	}

	public synchronized final Logger writeLine(final Color COLOR, final String... STRINGS){
		return writeLine(LogLevel.Info,COLOR,STRINGS);
	}
	
	public synchronized final Logger writeLine(final LogLevel LEVEL, final Color COLOR, final String... STRINGS){
		if(!canWriteLog(LEVEL)){
			return this;
		}
		for(final LoggerHook HOOK : HOOKS.values()){
			HOOK.writeLine(LEVEL,COLOR,STRINGS);
		}
		writeTimeStamp();
		if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					out.write(COLOR.getEscapeCode());
				}
			}catch(Exception e){}
		}
		if(useColor && COLOR != null){
			System.out.print(COLOR.getEscapeCode());
		}
		for(final String STRING : STRINGS){
			if(toFile && out != null){
				try{
					out.write(STRING);
					out.write("\n");
				}catch(Exception e){}
			}
			if(toConsole){
				System.out.println(STRING);
			}
		}
        	if(toFile && out != null){
			try{
				if(useColor && logColor && COLOR != null){
					final String END_COLOR = COLOR.getEndEscapeCode();
					if(END_COLOR != null){
						out.write(END_COLOR);
					}
				}
				out.flush();
			}catch(Exception e){}
		}
		if(toConsole){
			if(useColor && COLOR != null){
				final String END_COLOR = COLOR.getEndEscapeCode();
				if(END_COLOR != null){
					System.out.print(END_COLOR);
				}
			}
		}
        	return this;
	}

    public Logger writeError(final String MSG){
    	if(useColor){
		return write("[", LogLevel.Error).write("ERROR", LogLevel.Error, Color.RedLine).write("] ", LogLevel.Error).writeLine(MSG, LogLevel.Error);
	}
        return writeLine("[ERROR] "+MSG, LogLevel.Error);
    }

    public Logger writeError(final String MSG, final Exception E){
        return writeError(MSG).writeError(E);
    }
	
	public Logger writeError(final Exception E){
		if(!canWriteLog(LogLevel.Error)){
			return this;
		}
		for(final LoggerHook HOOK : HOOKS.values()){
			HOOK.writeError(E);
		}
		final StringBuilder buffer = new StringBuilder(1000);
		StackTraceElement[] stackElements = E.getStackTrace();
		for (int lcv = stackElements.length-1; lcv > -1; lcv--){
			buffer.append(stackElements[lcv].getFileName());
			buffer.append("->");
			buffer.append(stackElements[lcv].getMethodName());
			buffer.append("():");
			buffer.append(stackElements[lcv].getLineNumber());
			buffer.append("\n");
		}
		buffer.append(E.getMessage());
		buffer.append(" ");
		buffer.append(E.getCause());
		buffer.append("\n");
		write(buffer.toString(),Color.RedLine);
        return this;
	}

}
