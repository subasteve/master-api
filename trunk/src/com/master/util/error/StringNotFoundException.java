package com.master.util.error;

public class StringNotFoundException extends Exception{

	public StringNotFoundException(){
		super();
	}
	
	public StringNotFoundException(final String MESSAGE){
		super(MESSAGE);
	}
	
	public StringNotFoundException(final Throwable CAUSE){
		super(CAUSE);
	}
	
	public StringNotFoundException(final String MESSAGE, final Throwable CAUSE){
		super(MESSAGE,CAUSE);
	}
	
}