package com.master.util;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

public final class CreateString{
	
	private int totalLength = 0;
	private int lastIndex = 0;
	private final Map<Integer, String> STORED_STRINGS = new HashMap<Integer, String>();
	
	public CreateString(final String STRING){
		put(STRING);
	}
	
	public CreateString(final String... STRINGS){
		put(STRINGS);
	}
	
	public CreateString(){}
	
	public final void clear(){
		lastIndex = 0;
		STORED_STRINGS.clear();
	}
	
	public final Collection<String> values(){
		return STORED_STRINGS.values();
	}

	public final String get(final int INDEX){
		return STORED_STRINGS.get(INDEX);
	}

	public final SearchableString getSearchableString(final int INDEX){
		return new SearchableString(STORED_STRINGS.get(INDEX));
	}
	
	public final boolean isEmpty(){
		return STORED_STRINGS.isEmpty();
	}
	
	public final int size(){
		return STORED_STRINGS.size();
	}
	
	public final CreateString removeLast(){
		STORED_STRINGS.remove(lastIndex--);
		return this;
	}
	
	public final int getLastIndex(){
		return lastIndex;
	}
	
	public final CreateString remove(final int INDEX){
		STORED_STRINGS.remove(INDEX);
		return this;
	}

	public final CreateString merge(final int INDEX_1, final int INDEX_2){
		if(INDEX_1 == INDEX_2){
			return this;
		}
		final String STRING_1 = STORED_STRINGS.get(INDEX_1);
		final String STRING_2 = STORED_STRINGS.get(INDEX_2);
		put(INDEX_1,new StringBuilder(STRING_1.length()+STRING_2.length()).append(STRING_1).append(STRING_2).toString());
		STORED_STRINGS.remove(INDEX_2);
		return this;
	}
	
	public final CreateString put(final String STRING){
		if(STRING == null){
			return this;
		}
		totalLength += STRING.length();
		STORED_STRINGS.put(lastIndex++, STRING);
		return this;
	}
	
	public final CreateString put(final char CHAR){
		totalLength++;
		STORED_STRINGS.put(lastIndex++, String.valueOf(CHAR));
		return this;
	}
	
	public final CreateString put(final String... STRINGS){
		for(final String STRING : STRINGS){
			if(STRING != null){
				totalLength += STRING.length();
				STORED_STRINGS.put(lastIndex++, STRING);
			}
		}
		return this;
	}
	
	public final CreateString put(final ByteBuffer BUFFER){
		final byte[] BYTES = new byte[BUFFER.limit()];
		BUFFER.get(BYTES);
		totalLength += BYTES.length;
		STORED_STRINGS.put(lastIndex++, new String(BYTES));
		return this;
	}

	public final CreateString put(final CharBuffer BUFFER, final boolean TRIM){
		final char[] CHARS = new char[BUFFER.limit()];
		BUFFER.get(CHARS);
		totalLength += CHARS.length;
		STORED_STRINGS.put(lastIndex++, TRIM ?  String.valueOf(CHARS).trim() : String.valueOf(CHARS));
		return this;
	}
	
	public final CreateString append(final ByteBuffer BUFFER){
		final byte[] BYTES = new byte[BUFFER.limit()];
		BUFFER.get(BYTES);
		totalLength += BYTES.length;
		final String STORED_STRING = STORED_STRINGS.get(lastIndex);
		if(STORED_STRING != null){
			final StringBuilder STRING_BUFFER = new StringBuilder(totalLength);
			STRING_BUFFER.append(STORED_STRING);
			STRING_BUFFER.append(new String(BYTES));
			STORED_STRINGS.put(lastIndex, STRING_BUFFER.toString());
		}else{
			STORED_STRINGS.put(lastIndex, new String(BYTES));
		}
		return this;
	}

	public final CreateString append(final CharBuffer BUFFER){
		final char[] CHARS = new char[BUFFER.limit()];
		BUFFER.get(CHARS);
		totalLength += CHARS.length;
		final String STORED_STRING = STORED_STRINGS.get(lastIndex);
		if(STORED_STRING != null){
			STORED_STRINGS.put(lastIndex, new StringBuffer(totalLength).append(STORED_STRING).append(String.valueOf(CHARS)).toString());
		}else{
			STORED_STRINGS.put(lastIndex, String.valueOf(CHARS));
		}
		return this;
	}

	public final boolean containsKey(final int KEY){
		return STORED_STRINGS.containsKey(KEY);
	}

	
	/* TODO: Divide up by newlines */
	public final void reDivide(){
		int tmp = 0;
		for(final String STRING : STORED_STRINGS.values()){
			System.out.println("["+tmp+++"]"+STRING);
		}
	}
	
	public final CreateString put(final int INTEGER){
		STORED_STRINGS.put(lastIndex++, Integer.toString(INTEGER));
		return this;
	}

	public final CreateString addTabsToStart(final int INDEX, final int AMOUNT){
		final String STORED_STRING = STORED_STRINGS.get(INDEX);
		if(STORED_STRING != null){
			final StringBuilder BUFFER = new StringBuilder(STORED_STRING.length()+AMOUNT);
			totalLength += AMOUNT;
			for(int i = 0; i < AMOUNT; i++){
				BUFFER.append('\t');
			}
			BUFFER.append(STORED_STRING);
			STORED_STRINGS.put(INDEX, BUFFER.toString());
		}
		return this;
	}
	
	public final CreateString put(final int INDEX, final String STRING){
		final String STORED_STRING = STORED_STRINGS.get(INDEX);
		if(STORED_STRING != null){
			final int LENGTH = totalLength-STORED_STRING.length();
			totalLength = LENGTH+STRING.length();
		}else{
			totalLength += STRING.length();
		}
		STORED_STRINGS.put(INDEX, STRING);
		return this;
	}
	
	public static final String toString(final String... STRINGS){
		int tmp = 0;
		for(final String STRING : STRINGS){
			if(STRING != null){
				tmp += STRING.length();
			}
		}
		final StringBuilder BUFFER = new StringBuilder(tmp);
		for(final String STRING : STRINGS){
			if(STRING != null){
				BUFFER.append(STRING);
			}
		}
		return BUFFER.toString();
	}
	
	public final String toString(){
		return toString(false);
	}

	public final String toString(final boolean NEWLINE){
		final StringBuilder BUFFER = new StringBuilder(totalLength+(STORED_STRINGS.size()*2));
		int tmp = 0;
		int found = 0;
		do{
			final int KEY = tmp++;
			if(STORED_STRINGS.containsKey(KEY)){
				final String STRING = STORED_STRINGS.get(KEY);
				BUFFER.append(STRING);
				if(NEWLINE){
					BUFFER.append("\r\n");
				}
				found++;
				if(found >= STORED_STRINGS.size()){
					break;
				}
			}
		}while(true);
		return BUFFER.toString();
	}
	
	public final byte[] toBytes(){
		final byte[] BYTES = new byte[totalLength];
		int tmp = 0;
		int tmp2 = 0;
		for(int i = 0; i < STORED_STRINGS.size();){
			final int KEY = tmp2++;
			if(STORED_STRINGS.containsKey(KEY)){
				final String STRING = STORED_STRINGS.get(KEY);
				final byte[] BYTES_2 = STRING.getBytes();
				for(int x = 0; x < BYTES_2.length; x++){
					BYTES[tmp++] = BYTES_2[x];
				}
				i++;
			}
		}
		return BYTES;
	}
	
	public final ByteBuffer toByteBuffer(){
		return ByteBuffer.wrap(toBytes());
	}
	
}
