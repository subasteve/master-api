package com.master.util;

import java.awt.Image;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.File;
import javax.imageio.ImageIO;
import java.util.Calendar;
import java.awt.Toolkit;
import java.awt.Robot;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

public class ScreenShot{

	//public ScreenShot(String appName,int heigth, int width, int[] pixels){
		//saveImage(appName,createImage(new MemoryImageSource(width,heigth,pixels,0,width)));
	//}

	Robot robot;
	Toolkit toolkit;
	Rectangle screenBounds;
	
	private void saveImage(String appName,Image image){
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);
		File file = new File(appName+".png");
		try{
			BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
			// Copy image to buffered image
			Graphics g = bimage.createGraphics();
			// Paint the image onto the buffered image
			g.drawImage(image, 0, 0, null);
			g.dispose();
			ImageIO.write(bimage, "png", file);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public ScreenShot(String appName,Image image){
		robot = null;
		toolkit = null;
		screenBounds = null;
		saveImage(appName,image);
	}
	
	public ScreenShot(){
		try{
			robot = new Robot();
			robot.setAutoWaitForIdle(false);
		}catch(Exception e){
			e.printStackTrace();
		}
		toolkit = Toolkit.getDefaultToolkit();
		screenBounds = new Rectangle(toolkit.getScreenSize());
	}


	public final BufferedImage getScreen(){
		return robot.createScreenCapture(screenBounds);
	}
	
	public final int getHeight(){
		return screenBounds.height;
	}
	
	public final int getWidth(){
		return screenBounds.width;
	}
	
	public final BufferedImage reduceSizeBy(final BufferedImage img, final double scale){
		  int w = (int) (img.getWidth() * scale);
		  int h = (int) (img.getHeight() * scale);
		  BufferedImage outImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);                
		  AffineTransform trans = new AffineTransform();
		  trans.scale(scale, scale);
		  Graphics2D g = outImage.createGraphics();
		  g.drawImage(img, trans, null);
		  g.dispose();
		  return outImage;
	}
	
	public final int[] getPixels(final BufferedImage img){
		final int width = img.getWidth(), height = img.getHeight();
		int[] pixels = new int[width * height];
		PixelGrabber pg = new PixelGrabber(img, 0, 0, width, height, pixels, 0, width);
		try{
			pg.grabPixels();
		}catch(Exception e){
			e.printStackTrace();
		}
		return pixels;
	}
	
	 final int[] getValues(final BufferedImage img, int bitOffset/* 24 - Alpha, 16 - Red, 8 - Blue, 0 - Green */) {
         final int width = img.getWidth(), height = img.getHeight();
         int[] pixels = new int[width * height];
         int[] values = new int[width * height];
         PixelGrabber pg = new PixelGrabber(img, 0, 0, width, height, pixels, 0, width);
         try {
             pg.grabPixels();
             for (int j = 0; j < height; j++) {
                 for (int i = 0; i < width; i++) {
                     values[j * width + i] = ((pixels[j * width + i] >> bitOffset) & 0xff);
                 }
             }
         } catch (Exception e) {
             e.printStackTrace();
         }
         return values;
     }
	
	public ScreenShot(String Name){
		try{
			robot = new Robot();
			System.out.println("AutoDelay: "+robot.getAutoDelay());
			robot.setAutoWaitForIdle(false);
			toolkit = Toolkit.getDefaultToolkit();
			screenBounds = new Rectangle(toolkit.getScreenSize());
			final BufferedImage screen = robot.createScreenCapture(screenBounds);
			File file = new File(Name+".png");
			ImageIO.write(screen, "png", file);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
