package com.master.util;

public final class StringElement{
	
	private final int START_POS;
	private final byte[] STRING;
	private final int END_POS;
	
	public StringElement(final int START_POS, final String STRING){
		this.START_POS = START_POS;
		this.STRING = STRING.getBytes();
		this.END_POS = START_POS+this.STRING.length;
	}
	
	public StringElement(final int START_POS, final byte[] STRING){
		this.START_POS = START_POS;
		this.STRING = STRING;
		this.END_POS = START_POS+this.STRING.length;
	}
	
	public final int getStartPos(){
		return START_POS;
	}
	
	public final String toString(){
		return new String(STRING);
	}
	
	public final byte[] toBytes(){
		return STRING;
	}
	
	public final int getEndPos(){
		return END_POS;
	}
	
	public final int getLength(){
		return STRING.length;
	}
	
}