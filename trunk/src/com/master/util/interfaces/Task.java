package com.master.util.interfaces;

public interface Task{

	void start();
	void run(final long CYCLE_TIME);
	void stop();
	boolean isStarted();
	
}