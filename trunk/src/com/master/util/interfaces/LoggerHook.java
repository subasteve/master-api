package com.master.util.interfaces;

import com.master.util.Logger;
import com.master.util.SearchableString;
import com.master.util.CreateString;

public interface LoggerHook{

	public static enum LogLevel { Verbose, Fine, Info, Warn, Error, None }

	public static enum Color {
		Reset ("\u001B[0m"),
		Black (30),
		BlackLine (30,Reset),
		Red (31),
		RedLine (31,Reset),
		Green (32),
		GreenLine (32,Reset),
		Yellow (33),
		YellowLine (33,Reset),
		Blue (34),
		BlueLine (34,Reset),
		Purple (35),
		PurpleLine (35,Reset),
		Cyan (36),
		CyanLine (36,Reset),
		White (37),
		WhiteLine (37,Reset);

		final int BACKGROUND, TEXT_COLOR;
		final String ESCAPE_CODE, END_ESCAPE_CODE;

		Color(final String ESCAPE_CODE){
			TEXT_COLOR = -1;
			BACKGROUND = -1;
			this.ESCAPE_CODE = ESCAPE_CODE;
			END_ESCAPE_CODE = null;
		}
		Color(final int TEXT_COLOR){
			this.TEXT_COLOR = TEXT_COLOR;
			BACKGROUND = 40;
			ESCAPE_CODE = generateEscapeCode();
			END_ESCAPE_CODE = null;
		}
		Color(final int TEXT_COLOR, final Color END_COLOR){
			this.TEXT_COLOR = TEXT_COLOR;
			BACKGROUND = 40;
			ESCAPE_CODE = generateEscapeCode();
			END_ESCAPE_CODE = END_COLOR.getEscapeCode();
		}
		Color(final int TEXT_COLOR, final int BACKGROUND){
			this.TEXT_COLOR = TEXT_COLOR;
			this.BACKGROUND = BACKGROUND;
			ESCAPE_CODE = generateEscapeCode();
			END_ESCAPE_CODE = null;
		}
		Color(final int TEXT_COLOR, final int BACKGROUND, final Color END_COLOR){
			this.TEXT_COLOR = TEXT_COLOR;
			this.BACKGROUND = BACKGROUND;
			ESCAPE_CODE = generateEscapeCode();
			END_ESCAPE_CODE = END_COLOR.getEscapeCode();
		}
		private String generateEscapeCode(){
			return new StringBuilder().append("\u001B[").append(TEXT_COLOR).append(";").append(BACKGROUND).append(";1m").toString();
		}
		public String getEscapeCode(){
			return ESCAPE_CODE;
		}
		public String getEndEscapeCode(){
			return END_ESCAPE_CODE;
		}
	}

    Logger write(final SearchableString STRING, final LogLevel LEVEL, final Color COLOR);
    Logger write(final CreateString STRINGS, final boolean NEWLINE, final LogLevel LEVEL, final Color COLOR);
    Logger write(final String STRING, final LogLevel LEVEL, final Color COLOR);
    Logger write(final LogLevel LEVEL, final Color COLOR, final String... STRINGS);
    Logger writeLine(final String STRING, final LogLevel LEVEL, final Color COLOR);
    Logger writeLine(final LogLevel LEVEL, final Color COLOR, final String... STRINGS);
	Logger writeError(final Exception e);

}
