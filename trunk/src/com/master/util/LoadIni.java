package com.master.util;

import com.master.util.error.StringNotFoundException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.io.File;

public final class LoadIni extends Properties{

	private long timeStamp;
    private final List<String> TOP_COMMENTS = new ArrayList<String>();
    private final Map<String,String[]> COMMENTS = new HashMap<String,String[]>();

    public final Logger getLogger() {
        return logger;
    }

    public final void setLogger(final Logger LOGGER) {
        logger = LOGGER;
    }

    private Logger logger = null;
	private final String file;
	private final File f;

	public LoadIni(final String file){
		super();
		this.file = file;
		f = new File("./"+file);
		timeStamp = 0;
		reload();
	}
	
	public LoadIni(final String file, final boolean bool){
		super();
		this.file = file;
		f = new File("./"+file);
		timeStamp = 0;
		if(bool){
			reload();
		}
	}

    public final String[] getLoadedCommentsArea(){
        return (String[]) TOP_COMMENTS.toArray();
    }
	
	public final boolean reload(){
		if(timeStamp != f.lastModified()){
			timeStamp = f.lastModified();
			try{
                boolean commentsArea = true;
                TOP_COMMENTS.clear();
                final List<String> LINES = Files.readAllLines(Paths.get(file), java.nio.charset.Charset.defaultCharset());
                final List<String> COMMENTS = new ArrayList<String>();
                for (final String line : LINES){
                    final String LINE = line.trim();
                    if(commentsArea){
                        if(!LINE.equals("") && LINE.startsWith("#")){
                            TOP_COMMENTS.add(LINE);
                        }else{
                            commentsArea = false;
                        }
                    }else{
                        if(LINE.startsWith("#")){
                            COMMENTS.add(LINE.substring(2,LINE.length()));
                        }else if (LINE.indexOf("=") > -1){
                            final String VAR = LINE.substring(0,LINE.indexOf("=")).trim();
                            final String VALUE = LINE.substring(LINE.indexOf("=")+1,LINE.length()).trim();
                            final String[] PROP_COMMENTS = new String[COMMENTS.size()];
                            int tmp = 0;
                            for (final String COMMENT : COMMENTS){
                                PROP_COMMENTS[tmp++] = COMMENT;
                            }
                            setProperty(VAR, VALUE, PROP_COMMENTS);
                            COMMENTS.clear();
                        }
                    }
                }
				return true;
			}catch(final Exception ex) {
                if(logger != null){
                    logger.writeError("Error loading File: "+file,ex);
                }
			}
		}
		return false;
	}

    public final LoadIni save(){
        return save(null);
    }
	
	public final LoadIni save(final String[] COMMENTS){
		try{
            final StringBuilder BUFFER = new StringBuilder();
            if(COMMENTS != null && COMMENTS.length > 0){
                for(final String COMMENT : COMMENTS){
                    BUFFER.append("# ").append(COMMENT).append("\n");
                }
                BUFFER.append("\n");
            }else{
                BUFFER.append("# \n\n");
            }
            final Enumeration<?> NAMES = propertyNames();
            while (NAMES.hasMoreElements()) {
                final String NAME = (String) NAMES.nextElement();
                final String STR = getProperty(NAME).trim();
                final String[] PROP_COMMENTS = this.COMMENTS.get(NAME);
                if(PROP_COMMENTS != null){
                    for(final String COMMENT : PROP_COMMENTS){
                        BUFFER.append("# ").append(COMMENT).append("\n");
                    }
                }
                BUFFER.append(NAME).append(" = ").append(STR).append("\n");
            }
            Files.write(Paths.get(file), BUFFER.toString().getBytes("utf-8"), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
			timeStamp = f.lastModified();
		}catch(final Exception ex) {
            if(logger != null){
                logger.writeError("Error Saving property: "+file,ex);
            }
		}
        return this;
	}

    @Override
    public final LoadIni setProperty(final String KEY, final String VALUE){
        return setProperty(KEY,VALUE,null);
    }

    public final LoadIni setProperty(final String KEY, final String VALUE, final String[] PROP_COMMENTS){
        super.setProperty(KEY,VALUE);
        COMMENTS.put(KEY,PROP_COMMENTS);
        return this;
    }

	public final boolean hasProperty(final String S){
		Enumeration<?> enumeration = propertyNames();
		while (enumeration.hasMoreElements()) {
			if(S.equals((String) enumeration.nextElement())){
				return true;
			}
		}
		return false;
	}
	
	public final boolean loadBoolean(final String S) throws StringNotFoundException {
		if(!hasProperty(S)){
			throw new StringNotFoundException(S+" Not Found");
		}
		final String STR = getProperty(S).trim();
        if(logger != null){
            logger.writeLine(Logger.LogLevel.Fine,"loadBoolean["+S+"] "+STR);
        }
		if(STR.equalsIgnoreCase("true")) {
            return true;
        } else if (STR.equalsIgnoreCase("false")) {
            return false;
        } else {
            return false;
        }
	}
	
	public final String loadString(final String S) throws StringNotFoundException {
        	if(!hasProperty(S)){
			throw new StringNotFoundException(S+" Not Found");
		}
        	final String STR = getProperty(S).trim();
        if(logger != null){
            logger.writeLine(Logger.LogLevel.Fine,"loadString["+S+"] "+STR);
        }
		return STR;
	}
	
	public final int loadInt(final String S) throws StringNotFoundException {
		if(!hasProperty(S)){
			throw new StringNotFoundException(S+" Not Found");
		}
		final String STR = getProperty(S).trim();
        if(logger != null){
            logger.writeLine(Logger.LogLevel.Fine,"loadInt["+S+"] "+STR);
        }
		return Integer.parseInt(STR);
	}

    	public final double loadDouble(final String S) throws StringNotFoundException {
		if(!hasProperty(S)){
			throw new StringNotFoundException(S+" Not Found");
		}
            final String STR = getProperty(S).trim();
        if(logger != null){
            logger.writeLine(Logger.LogLevel.Fine,"loadInt["+S+"] "+STR);
        }
        return Double.parseDouble(STR);
    }
	
	public final int[] loadIntArray(final String S) throws StringNotFoundException {
        	if(!hasProperty(S)){
			throw new StringNotFoundException(S+" Not Found");
		}
        	final String STR = getProperty(S).trim();
		return fromStringArray(STR);
	}
	
	public final double[] loadDoubleArray(final String S) throws StringNotFoundException {
		if(!hasProperty(S)){
			throw new StringNotFoundException(S+" Not Found");
		}
		final String STR = getProperty(S).trim();
		return doubleFromStringArray(STR);
	}
	
	public final String[] loadStringArray(final String S) throws StringNotFoundException {
		if(!hasProperty(S)){
			throw new StringNotFoundException(S+" Not Found");
		}
		final String STR = getProperty(S).trim();
		return stringFromStringArray(STR);
	}
	
	public final String[] stringFromStringArray(final String str){
        final String[] str2 = str.split(",");
		if (str2.length > 1){
	        final String array[] = new String[str2.length];
            for (int i = 0; i < str2.length; i++){
				try{
					array[i] = str2[i];
				}catch(Exception e){array[i] = "";}
            }
			return array;
        }
		return null;
    }
	
	public final String getStringFromIntArray(final int[] x){
		final StringBuilder buf = new StringBuilder(x.length*2);
        if (x != null){
            for (int i = 0; i < x.length; i++){
                if (i == 0){
                    buf.append(x[i]);
                }else{
                    buf.append(",");
					buf.append(x[i]);
                }
            }
        }
        return buf.toString();
    }
	
	public final String getStringFromDoubleArray(final double[] x){
		final StringBuilder buf = new StringBuilder(x.length*2);
        if (x != null){
            for (int i = 0; i < x.length; i++){
                if (i == 0){
                    buf.append(x[i]);
                }else{
                    buf.append(",");
					buf.append(x[i]);
                }
            }
        }
        return buf.toString();
    }
	
	public final String getStringFromStringArray(final String[] x){
		final StringBuilder buf = new StringBuilder(x.length*2);
        String returnString = "";
        if (x != null){
            for (int i = 0; i < x.length; i++){
                if (i == 0){
                    buf.append(x[i]);
                }else{
                    buf.append(",");
					buf.append(x[i]);
                }
            }
        }
        return buf.toString();
    }
	
	public final int[] fromStringArray(final String str){
        final String[] str2 = str.split(",");
		if (str2.length > 1){
	        final int array[] = new int[str2.length];
            for (int i = 0; i < str2.length; i++){
				try{
					array[i] = Integer.parseInt(str2[i]);
				}catch(Exception e){array[i] = -1;}
            }
			return array;
        }
		return null;
    }
	
	public final double[] doubleFromStringArray(final String str){
        final String[] str2 = str.split(",");
		if (str2.length > 1){
	        final double array[] = new double[str2.length];
            for (int i = 0; i < str2.length; i++){
				try{
					array[i] = Double.parseDouble(str2[i]);
				}catch(Exception e){array[i] = -1;}
            }
			return array;
        }
		return null;
    }
}
