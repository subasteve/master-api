package com.master.util;

import java.util.Hashtable;

public final class JavaLoader extends ClassLoader{

	public final synchronized void reloadJar(final String str){
		jar.read(str);
	}
	
	public final void newInstance(String str){
		try{
			loadClass(str,true).newInstance();
		}catch(final Exception e){ e.printStackTrace(); }
	}

    public final synchronized Class loadClass(final String s, final boolean resolve){
		try{
			final Class class1 = (Class)classes.get(s);
			if(class1 != null)
				return class1;
			if(jar != null){
				final byte[] abyte0 = jar.get(s);
				if(abyte0 != null){
					final Class class2 = defineClass(s, abyte0, 0, abyte0.length);
					if(resolve)
						resolveClass(class2);
					classes.put(s, class2);
					return class2;
				}else{
					return super.findSystemClass(s);
				}
			}else{
				return super.findSystemClass(s);
			}
		}catch(final Exception e){ e.printStackTrace(); }
		return null;
    }

    public JavaLoader(String jarFile){
        classes = new Hashtable();
		jar = new JarLoader(jarFile);
    }

    Hashtable classes;
    JarLoader jar;
}
