package com.master.util;

import java.io.*;
import java.util.Map;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class JarLoader {

	private Map<String,byte[]> classes = new HashMap<String,byte[]>();

    public final byte[] get(final String s){
        try{
            return classes.get(s);
        }catch(final Exception exception){
            exception.printStackTrace();
        }
        return null;
    }
	
	public final void read(final String s){
        try{
            final File file = new File(s);
            if(file.exists()){
				final int length = (int)file.length();
				final DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
				byte[] abyte0 = new byte[length];
				datainputstream.readFully(abyte0, 0, length);
				datainputstream.close();
				getClasses(abyte0);
			}
        }catch(final Exception e){
			e.printStackTrace();
        }
    }

    public JarLoader(final String str){
		read(str);
    }

    private final void getClasses(final byte abyte0[]){
        try{
            final ZipInputStream zipinputstream = new ZipInputStream(new ByteArrayInputStream(abyte0));
            final byte abyte1[] = new byte[1000];
            do{
                final ZipEntry zipentry = zipinputstream.getNextEntry();
                if(zipentry == null)
                    break;
                String s = zipentry.getName();
                if(s.endsWith(".class")){
                    s = s.substring(0, s.length() - 6);
                    s = s.replace('/', '.');
                    final ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
                    do{
                        final int i = zipinputstream.read(abyte1, 0, 1000);
                        if(-1 == i)
                            break;
                        bytearrayoutputstream.write(abyte1, 0, i);
                    } while(true);
                    final byte abyte2[] = bytearrayoutputstream.toByteArray();
                    classes.put(s, abyte2);
                }
            } while(true);
            zipinputstream.close();
        }catch(final Exception e){ e.printStackTrace(); }
    }
}
