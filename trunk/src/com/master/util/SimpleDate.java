package com.master.util;

import java.util.Calendar;
import java.util.Date;
import com.master.io.interfaces.BufferInterface;
import com.master.io.socket.Buffer;

public final class SimpleDate implements BufferInterface{

	private int YEAR;
	private long MILLS;
	private byte HOURS, MINUTES, SECONDS, MONTH, DAY;
	
	public SimpleDate(){
		final Calendar CALENDAR = Calendar.getInstance();
		MONTH = (byte)CALENDAR.get(Calendar.MONTH);
		DAY = (byte)CALENDAR.get(Calendar.DAY_OF_MONTH);
		YEAR = CALENDAR.get(Calendar.YEAR);
		HOURS = (byte)CALENDAR.get(Calendar.HOUR);
		MINUTES = (byte)CALENDAR.get(Calendar.MINUTE);
		SECONDS = (byte)CALENDAR.get(Calendar.SECOND);
		MILLS = CALENDAR.getTime().getTime();
	}
	
	public SimpleDate(final SimpleDate DATE){ //DIFF
		final Calendar CALENDAR = Calendar.getInstance();
		MONTH = (byte)Math.abs(DATE.getMonths()-CALENDAR.get(Calendar.MONTH));
		DAY = (byte)Math.abs(DATE.getDays()-CALENDAR.get(Calendar.DAY_OF_MONTH));
		YEAR = Math.abs(DATE.getYears()-CALENDAR.get(Calendar.YEAR));
		HOURS = (byte)Math.abs(DATE.getHours()-CALENDAR.get(Calendar.HOUR));
		MINUTES = (byte)Math.abs(DATE.getMinutes()-CALENDAR.get(Calendar.MINUTE));
		SECONDS = (byte)Math.abs(DATE.getSeconds()-CALENDAR.get(Calendar.SECOND));
		MILLS = Math.abs(DATE.getMills()-CALENDAR.getTime().getTime());
	}
	
	public final void put(final Buffer BUFFER){
		BUFFER.putLong(MILLS);
	}
	
	public final void get(final Buffer BUFFER){
		final Date DATE = new Date(BUFFER.getLong());
		MONTH = (byte)DATE.getMonth();
		DAY = (byte)DATE.getDay();
		YEAR = DATE.getYear();
		HOURS = (byte)DATE.getHours();
		MINUTES = (byte)DATE.getMinutes();
		SECONDS = (byte)DATE.getSeconds();
		MILLS = DATE.getTime();
	}
	
	public final long getMills(){
		return MILLS;
	}
	
	public final int getHours(){
		return HOURS;
	}
	
	public final int getMinutes(){
		return MINUTES;
	}
	
	public final int getSeconds(){
		return SECONDS;
	}
	
	public final int getMonths(){
		return MONTH;
	}
	
	public final int getDays(){
		return DAY;
	}
	
	public final int getYears(){
		return YEAR;
	}
	
	public final boolean sameDayMonthYear(final SimpleDate DATE){
        return (DATE.MONTH == MONTH && DATE.DAY == DAY && DATE.YEAR == YEAR);
	}
	
}