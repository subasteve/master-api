package com.master.google;

import java.net.URLEncoder;
import com.master.util.CreateString;

public final class OAuth2{
	
	private final String BASE_URL;
	private final String REDIRECT_URI;
	private final String CLIENT_ID;
	private final String SCOPE;
	private final String RESPONSE_TYPE = "response_type=code";
	
	public OAuth2(final String BASE_URL, final String REDIRECT_URI, final String CLIENT_ID, final String SCOPE){
		this.BASE_URL = BASE_URL;
		this.REDIRECT_URI = URLEncoder.encode(REDIRECT_URI);
		this.CLIENT_ID = CLIENT_ID;
		this.SCOPE = URLEncoder.encode(SCOPE);
	}
	
	public final String toString(){
		return new CreateString(BASE_URL,"?","redirect_uri=",REDIRECT_URI,"&client_id=",CLIENT_ID,"&scope=",SCOPE,"&",RESPONSE_TYPE).toString();
	}

}