package com.master.google;

import com.master.net.WebPage;
import com.master.util.SearchableString;
import java.io.IOException;

public class Weather{

	private int zip = 66502, currentTemp = -1, high = -1, low = -1, humidity = -1;
	private final WebPage WEB_PAGE = new WebPage();

	public Weather(final int zip){
		this.zip = zip;
	}

	public final void setZip(final int zip){
		this.zip = zip;
	}

	public final void update() throws IOException{
		final SearchableString results = WEB_PAGE.get("www.google.com","/ig/api?weather="+zip);
		try{
			currentTemp = results.position("<current_conditions>").position("<temp_f data=\"").getInt("\"");
			humidity = results.position("<humidity data=\"Humidity: ").getInt("%");
			//currentTemp = results.getInt("<current_conditions>","<temp_f data=\"","\"",true);
			//humidity = results.getInt("<current_conditions>","<humidity data=\"Humidity: ","%",true);
		}catch(final Exception e){ e.printStackTrace(); }
		try{
			low = results.position("<forecast_conditions>").position("<low data=\"").getInt("\"");
			high = results.position("<high data=\"").getInt("\"");
			//high = results.getInt("<forecast_conditions>","<high data=\"","\"",true);
			//low = results.getInt("<forecast_conditions>","<low data=\"","\"",true);
		}catch(final Exception e){ e.printStackTrace(); }
	}
	
	public final int getHumidity(){
		return humidity;
	}
	
	public final int getHumidity(final boolean UPDATE) throws IOException{
		if(UPDATE){
			update();
		}
		return humidity;
	}

	public final int getTemp(){
		return currentTemp;
	}

	public final int getTemp(final boolean UPDATE) throws IOException{
		if(UPDATE){
			update();
		}
		return currentTemp;
	}

	public final int getHigh(){
		return high;
	}

	public final int getHigh(final boolean UPDATE) throws IOException{
		if(UPDATE){
			update();
		}
		return high;
	}

	public final int getLow(){
		return low;
	}

	public final int getLow(final boolean UPDATE) throws IOException{
		if(UPDATE){
			update();
		}
		return low;
	}

}
