package com.master.google;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;

import com.master.util.error.StringNotFoundException;
import com.master.util.SearchableString;
import com.master.net.WebPage;
import com.master.util.CreateString;

public final class Search{

	private final String GOOGLE_SEARCH_URL = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=8&filter=0&safe=off";
	private final Map<Integer, Result> RESULTS = new HashMap<Integer, Result>();
	
	public Search(final String QUERY){
		int tmp = 0;
		try{
			final WebPage WEB_PAGE = new WebPage();
			SearchableString SEARCH_STRING = new SearchableString(WEB_PAGE.get(GOOGLE_SEARCH_URL+"&q="+QUERY+"&userip="+randomIP()).toString());
			SEARCH_STRING.position(0);
			try{
				do{
					RESULTS.put(tmp,new Result(SEARCH_STRING));
					tmp++;
				}while(true);
			}catch(final Exception ex){}
			SEARCH_STRING.position(0);
			try{
				do{
					final String START = SEARCH_STRING.position("\"start\":\"").getString("\"");
					SearchableString SEARCH_STRING_2 = new SearchableString(WEB_PAGE.get(GOOGLE_SEARCH_URL+"&q="+QUERY+"&start="+START+"&userip="+randomIP()).toString());
					SEARCH_STRING_2.position(0);
					try{
						do{
							RESULTS.put(tmp,new Result(SEARCH_STRING_2));
							tmp++;
						}while(true);
					}catch(final Exception exx){}
				}while(true);
			}catch(final Exception ex){}
		}catch(final Exception e){}	
	}
	
	private int random(int range) {
		return (int)(java.lang.Math.random() * (range+1));
	}
	
	private final String randomIP(){
		final CreateString STRING = new CreateString();
		STRING.put(random(255)).put(".").put(random(255)).put(".").put(random(255)).put(".").put(random(255));
		return STRING.toString();
	}
	
	public final Collection<Result> values(){
		return RESULTS.values();
	}
	
	public final int size(){
		return RESULTS.size();
	}
	
	public final String toString(){
		final CreateString STRING = new CreateString();
		for(final Result RESULT : RESULTS.values()){
			STRING.put(RESULT.toString()).put("\r\n");
		}
		return STRING.toString();
	}
	
	public class Result{
	
		private final String URL, TITLE;
		
		public Result(final SearchableString SEARCH_STRING) throws StringNotFoundException{
			URL = SEARCH_STRING.position("\"url\":\"").getString("\"");
			TITLE = SEARCH_STRING.position("\"titleNoFormatting\":\"").getString("\"");
		}
		
		public String getURL(){
			return URL;
		}
		
		public String getTitle(){
			return TITLE;
		}
		
		public String toString(){
			return TITLE+" "+URL;
		}
	
	}
}
