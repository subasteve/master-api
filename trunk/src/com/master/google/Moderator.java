package com.master.google;

import com.master.util.SearchableString;
import com.master.util.error.StringNotFoundException;
import com.master.net.WebPage;
import java.io.IOException;

public final class Moderator{

	private final int SERIES_ID;
	private final String SUBMISSIONS_URL;
	private final String TOPICS_URL;
	private final SearchableString SEARCH_STRING = new SearchableString();
	private boolean debug = false;

	public Moderator(final int SERIES_ID){
		this.SERIES_ID = SERIES_ID;
		TOPICS_URL = "www.googleapis.com/moderator/v1/series/"+SERIES_ID+"/topics";
		SUBMISSIONS_URL = "www.googleapis.com/moderator/v1/series/"+SERIES_ID+"/submissions";
	}

	public Moderator(final int SERIES_ID, final boolean DEBUG){
        this.SERIES_ID = SERIES_ID;
		debug = DEBUG;
		TOPICS_URL = "www.googleapis.com/moderator/v1/series/"+SERIES_ID+"/topics";
        SUBMISSIONS_URL = "www.googleapis.com/moderator/v1/series/"+SERIES_ID+"/submissions";
    }

	public void setDebug(final boolean DEBUG){
		debug = DEBUG;
	}

	//public final String accessToken(){
		//http://accounts.google.com/o/oauth2/
	//}

	public final void test() throws IOException,StringNotFoundException{
		for(final Topic TOPIC : getTopics()){
            System.out.println(TOPIC.toString());
        }
		for(final Submission SUBMISSION : getSubmissions()){
			System.out.println(SUBMISSION.toString());
		}
	}

	public final Submission[] getSubmissions() throws IOException,StringNotFoundException{
		SEARCH_STRING.setString(new WebPage(SUBMISSIONS_URL,debug).getSource().toString());
		final String ENTRIES = SEARCH_STRING.position("{\"data\":{\"kind\":\"moderator#submissionList\",\"items\":[").getString("]}}");
		if(debug){
			System.out.println("DATA: "+ENTRIES);
		}
		if(ENTRIES.contains("},{")){ //has more than one object
			final String[] ENTRIES_ARRAY = ENTRIES.split("[}]+[,]+[{]");
			final Submission[] SUBMISSIONS = new Submission[ENTRIES_ARRAY.length];
			int tmp = 0;
			for(final String SUBMISSION : ENTRIES_ARRAY){
				if(debug){
					System.out.println("Parse Submission: "+SUBMISSION);
				}
				SUBMISSIONS[tmp++] = new Submission(SUBMISSION);
			}
			return SUBMISSIONS;
		}else{
			final Submission[] SUBMISSIONS = new Submission[1];
			SUBMISSIONS[0] = new Submission(ENTRIES);
			return SUBMISSIONS;
		}
	}

	public final Topic[] getTopics() throws IOException,StringNotFoundException{
		SEARCH_STRING.setString(new WebPage(TOPICS_URL,debug).getSource().toString());
		final String ENTRIES = SEARCH_STRING.position("{\"data\":{\"kind\":\"moderator#topicList\",\"items\":[").getString("}}]}}");
		if(debug){
            System.out.println("DATA: "+ENTRIES);
        }
        if(ENTRIES.contains("},{")){ //has more than one object
            final String[] ENTRIES_ARRAY = ENTRIES.split("[}]+[,]+[{]");
            final Topic[] TOPICS = new Topic[ENTRIES_ARRAY.length];
            int tmp = 0;
            for(final String TOPIC : ENTRIES_ARRAY){
                if(debug){
                    System.out.println("Parse Topic: "+TOPIC);
                }
                TOPICS[tmp++] = new Topic(TOPIC);
            }
            return TOPICS;
        }else{
            final Topic[] TOPICS = new Topic[1];
            TOPICS[0] = new Topic(ENTRIES);
			return TOPICS;
        }
	}

	private final class Topic{

		private final int TOPIC_ID;
		private final String TITLE;
		private final String DESCRIPTION;
		private final int PLUS_VOTES;
		private final int MINUS_VOTES;
		private final int NONE_VOTES;
		private final int USERS;
		private final int SUBMISSIONS;

		public Topic(final String DATA) throws StringNotFoundException{
			SEARCH_STRING.setString(DATA);
			this.TOPIC_ID = SEARCH_STRING.position("topicId\":\"").getInt("\"");
			this.TITLE = SEARCH_STRING.position("name\":\"").getString("\"");
			this.DESCRIPTION = SEARCH_STRING.position("description\":\"").getString("\"");
			if(SEARCH_STRING.contains("counters")){
				this.PLUS_VOTES = SEARCH_STRING.position("plusVotes\":").getInt(",");
                this.MINUS_VOTES = SEARCH_STRING.position("minusVotes\":").getInt(",");
                this.NONE_VOTES = SEARCH_STRING.position("noneVotes\":").getInt(",");
				this.USERS = SEARCH_STRING.position("users\":").getInt(",");
				this.SUBMISSIONS = SEARCH_STRING.position("submissions\":").getInt(",");
			}else{
				this.PLUS_VOTES = -1;
				this.MINUS_VOTES = -1;
				this.NONE_VOTES = -1;
				this.USERS = -1;
				this.SUBMISSIONS = -1;
			}
		}

		public final int getID(){
			return TOPIC_ID;
		}

		public final String getTitle(){
			return TITLE;
		}

		public final String getDescription(){
			return DESCRIPTION;
		}

		public final int getPlusVotes(){
            return PLUS_VOTES;
        }

        public final int getMinusVotes(){
            return MINUS_VOTES;
        }

        public final int getNoneVotes(){
            return NONE_VOTES;
        }

		public final int getUsers(){
			return USERS;
		}

		public final int getSubmissions(){
			return SUBMISSIONS;
		}

		public final boolean isEmpty(){
            return (USERS == -1 && SUBMISSIONS == -1);
		}

		public final String toString(){
			if(isEmpty()){
				return TITLE+" Description: "+DESCRIPTION+" ID: "+TOPIC_ID;
			}else{
				return TITLE+" Description: "+DESCRIPTION+" ID: "+TOPIC_ID+" Votes[ +"+PLUS_VOTES+", -"+MINUS_VOTES+", "+NONE_VOTES+" ] Users: "+USERS+" Submissions: "+SUBMISSIONS;
			}
		}

	}

	private final class Submission{

		private final int SUBMISSION_ID;
		private final int TOPIC_ID;
		private final String TEXT;
		private final int PLUS_VOTES;
		private final int MINUS_VOTES;
		private final int NONE_VOTES;
		private final String AUTHOR;

		public Submission(final String DATA) throws StringNotFoundException{
			SEARCH_STRING.setString(DATA);
			this.SUBMISSION_ID = SEARCH_STRING.position("submissionId\":\"").getInt("\"");
			this.TOPIC_ID = SEARCH_STRING.position("topicId\":\"").getInt("\"");
			this.TEXT = SEARCH_STRING.position("text\":\"").getString("\"");
			this.PLUS_VOTES = SEARCH_STRING.position("plusVotes\":").getInt(",");
			this.MINUS_VOTES = SEARCH_STRING.position("minusVotes\":").getInt(",");
			this.NONE_VOTES = SEARCH_STRING.position("noneVotes\":").getInt("}");
			this.AUTHOR = SEARCH_STRING.position("displayName\":\"").getString("\"");
		}

		public final int getID(){
			return SUBMISSION_ID;
		}

		public final int getTopicID(){
			return TOPIC_ID;
		}

		public final String getText(){
			return TEXT;
		}

		public final int getPlusVotes(){
			return PLUS_VOTES;
		}

		public final int getMinusVotes(){
			return MINUS_VOTES;
		}

		public final int getNoneVotes(){
			return NONE_VOTES;
		}

		public final String getAuthor(){
			return AUTHOR;
		}

		public final String toString(){
			return TEXT+" ID: "+SUBMISSION_ID+" TOPIC-ID: "+TOPIC_ID+" Votes[ +"+PLUS_VOTES+", -"+MINUS_VOTES+", "+NONE_VOTES+" ] Author: "+AUTHOR;
		}

	}

}
