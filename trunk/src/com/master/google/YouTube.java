package com.master.google;

import com.master.util.Logger;
import com.master.net.WebPage;
import com.master.net.Host;
import com.master.util.CreateString;
import com.master.util.SearchableString;
import com.master.util.StringElement;
import java.net.URLDecoder;
import java.io.IOException;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.ArrayList;

import java.io.Serializable;

public final class YouTube implements com.master.data.interfaces.JSON,Serializable{

	private transient static final String TIMES[] = { "time=today", "time=this_week", "time=this_month", "" };
	private transient static final String ORDER_BY[] = { "orderby=relevance", "orderby=rating", "orderby=viewCount", "orderby=published", "" };
	private transient static final String CATEGORIES[] = {"Any","Film","Autos","Music","Animals","Sports","Travel","Games","Comedy","People","News","Entertainment","Education","Howto","Nonprofit","Tech",/*"Movies",*/"Shows","Trailers"};
	private transient static final String REGION_IDS[] = {/* Argentina */ "AR", /* Australia */ 	"AU", /* Brazil */ "BR", /* Canada */ "CA", /* Czech Republic */ "CZ", /* France */ "FR", /* Germany */ "DE", /* Great Britain */ "GB", /* Hong Kong */ "HK", /* India */ "IN", /* Ireland */ "IE", /* Israel */ "IL", /* Italy */ "IT", /* Japan */ "JP", /* Mexico */ "MX", /* Netherlands */ "NL", /* New Zealand */ "NZ", /* Poland */ "PL", /* Russia */ "RU", /* South Africa */ "ZA", /* South Korea */ "KR", /* Spain */ "ES", /* Sweden */ "SE", /* Taiwan */ "TW", /* United States */ "US"};
	private transient static final String STANDARD_FEEDS[] = {"top_rated", "top_favorites", "most_viewed", "most_shared", "most_popular", "most_recent", "most_discussed", "most_responded", "recently_featured", "on_the_web"};
	private transient static final String STANDARD_URL = "http://gdata.youtube.com/feeds/api/standardfeeds/";
	private transient static final String USER_URL = "http://gdata.youtube.com/feeds/api/users/";
	private transient static final String VIDEO_URL = "http://gdata.youtube.com/feeds/api/videos/";
	private transient static final Host HOST = new Host("www.youtube.com","74.125.225.167");
	private transient static final Host HOST2 = new Host("gdata.youtube.com","74.125.225.206");
	private transient final Logger LOGGER;
	private final String VIDEO_ID;
	private int videoDuration = -1;
	private String videoTitle = null;
	public enum FormatType { FLV, MP4, MP4_3D, HTML5, HTML5_3D, GP3, Unknown }
	
	private final Map<Integer, Format> FORMATS = new HashMap<Integer, Format>();
	private transient int lastIndex = 0;
	
	public YouTube(final String VIDEO_ID) throws IOException{
		this.VIDEO_ID = VIDEO_ID;
		LOGGER = null;
		getVideo(null);
	}
	
	public YouTube(final String VIDEO_ID, final WebPage WEB_PAGE) throws IOException{
		this.VIDEO_ID = VIDEO_ID;
		LOGGER = null;
		getVideo(WEB_PAGE);
	}
	
	private final void getVideo(WebPage webPage) throws IOException{
		if(LOGGER != null){
			LOGGER.writeLine("Finding Video["+VIDEO_ID+"]");
		}
		boolean mainPage = false;
		if(webPage == null){
		 	webPage = new WebPage();
		}
		
		SearchableString SEARCH_STRING = webPage.get(HOST2,String.format("/feeds/api/videos/%1$s",VIDEO_ID));
		if(LOGGER != null){
			LOGGER.write(SEARCH_STRING.toString(),Logger.LogLevel.Verbose);
		}
		if(SEARCH_STRING.position(0).contains("<yt:duration")){
			try{
				videoDuration = SEARCH_STRING.position(0).position("<yt:duration seconds='").getInt("'");
			}catch(final Exception e){
				if(LOGGER != null){
					LOGGER.writeError(e);
				}
			}
		}
		
		SEARCH_STRING = webPage.get(HOST,"/get_video_info?video_id="+VIDEO_ID+"&asv=3&el=detailpage&hl=en_US");
		if(LOGGER != null){
			LOGGER.write(SEARCH_STRING.toString(),Logger.LogLevel.Verbose);
		}
		if(SEARCH_STRING.position(0).contains("status=fail")){
			try{
				int CODE = SEARCH_STRING.position(0).position("errorcode=").getInt("&");
				if(LOGGER != null){
					LOGGER.writeLine("Fail["+CODE+"] "+SEARCH_STRING.position(0).position("reason=").getString("&"));
				}
				if(CODE == 150){
					SEARCH_STRING = webPage.get(HOST,"/watch?v="+VIDEO_ID);
					if(LOGGER != null){
						LOGGER.write(SEARCH_STRING.toString(),Logger.LogLevel.Verbose);
					}
					mainPage = true;
				}
			}catch(final Exception e){
				if(LOGGER != null){
					LOGGER.writeError(e);
				}
			}
		}
		try{
			videoTitle = URLDecoder.decode(SEARCH_STRING.position(0).position("title=").getString("&"), "UTF-8");
			if(LOGGER != null){
				LOGGER.writeLine(String.format("Video[%1$s] %2$s",VIDEO_ID,videoTitle));
			}
		}catch(final Exception e){
			if(LOGGER != null){
				LOGGER.writeError(e);
			}
		}
		String FMT_URL_MAP = null;
		try{
			FMT_URL_MAP = URLDecoder.decode(SEARCH_STRING.position(0).position("url_encoded_fmt_stream_map=").getString("&").replaceAll("\\n", "").replaceAll("\\r", ""), "UTF-8");
		}catch(final Exception e){
			try{
				FMT_URL_MAP = URLDecoder.decode(SEARCH_STRING.position(0).position("\"url_encoded_fmt_stream_map\": \"").getString("\"").replaceAll("\\n", "").replaceAll("\\r", ""), "UTF-8");
			}catch(final Exception ez){
				if(LOGGER != null){
					LOGGER.writeError(e);
					LOGGER.writeError(ez);
				}
			}
		}
		try{
			LOGGER.writeLine("Decoded String: "+URLDecoder.decode(URLDecoder.decode(FMT_URL_MAP, "UTF-8"), "UTF-8"),Logger.LogLevel.Verbose);
		}catch(final Exception e){
			if(LOGGER != null){
				LOGGER.writeError(e);
			}
		}
		final Map<Integer, String> URLS = new HashMap<Integer, String>();
		int tmp = 0;
		final String SAVE_STRING = SEARCH_STRING.toString();
		SEARCH_STRING.setString(FMT_URL_MAP);
		try{
			if(LOGGER != null){
				LOGGER.writeLine("String: "+FMT_URL_MAP, Logger.LogLevel.Verbose);
			}
			do{
				final int START_POSITION = SEARCH_STRING.position();
				final String SIG = URLDecoder.decode(SEARCH_STRING.position(START_POSITION).position("sig=").getString("&"));
				//final int FORMAT = Integer.parseInt(SEARCH_STRING.position("itag=").getString("&"));
				try{
					final StringElement NEXT = SEARCH_STRING.position(START_POSITION).position("url=").findIndex(",");
					final StringElement AND = SEARCH_STRING.position(START_POSITION).position("url=").findIndex("&");
					if(NEXT.getStartPos() < AND.getStartPos()){
						final String URL = URLDecoder.decode(SEARCH_STRING.position(START_POSITION).position("url=").getString(","));
						URLS.put(tmp++,String.format("%1$s&signature=%2$s",URL,SIG));
						if(LOGGER != null){
							LOGGER.writeLine("URL: "+String.format("%1$s&signature=%2$s",URL,SIG),Logger.LogLevel.Verbose);
						}
					}else{
						final String URL = URLDecoder.decode(SEARCH_STRING.position(START_POSITION).position("url=").getString("&"));
						URLS.put(tmp++,String.format("%1$s&signature=%2$s",URL,SIG));
						if(LOGGER != null){
							LOGGER.writeLine("URL: "+String.format("%1$s&signature=%2$s",URL,SIG),Logger.LogLevel.Verbose);
						}
					}
				}catch(final Exception e){
				
				}
			}while(true);
		}catch(final Exception e){
			if(LOGGER != null){
				LOGGER.writeError(e);
			}
		}
		SEARCH_STRING.setString(SAVE_STRING);
		String FMT_LIST = null;
		try{
			FMT_LIST = URLDecoder.decode(SEARCH_STRING.position(0).position("fmt_list=").getString("&").replaceAll("\\n", "").replaceAll("\\r", "").replaceAll("\\\\",""), "UTF-8");
		}catch(final Exception e){
			try{
				FMT_LIST = URLDecoder.decode(SEARCH_STRING.position(0).position("\"fmt_list\": \"").getString("\"").replaceAll("\\n", "").replaceAll("\\r", "").replaceAll("\\\\",""), "UTF-8");
			}catch(final Exception ez){
				if(LOGGER != null){
					LOGGER.writeError(e);
					LOGGER.writeError(ez);
				}
			}
		}	
		try{
			if(LOGGER != null){
				LOGGER.writeLine(FMT_LIST,Logger.LogLevel.Verbose);
			}
			final String[] FORMATS_2 = FMT_LIST.split(",");
			for(final String STRING2 : FORMATS_2){
				try{
					FORMATS.put(lastIndex, new Format(STRING2,URLS.get(lastIndex++)));
				}catch(final Exception e){
					if(LOGGER != null){
						LOGGER.writeError(e);
					}
				 }
			}
		}catch(final Exception e){
			if(LOGGER != null){
				LOGGER.writeError(e);
			}
		}
		if(LOGGER != null){
			LOGGER.writeLine("Video["+VIDEO_ID+"] Done");
		}
	}
	
	public YouTube(final String VIDEO_ID, final Logger LOGGER) throws IOException{
		this.VIDEO_ID = VIDEO_ID;
		this.LOGGER = LOGGER;
		getVideo(null);
	}
	
	public static int random(final int LOW,final int HIGH){
		return (int) (Math.floor(Math.random()*(1+HIGH-LOW))+LOW);
	}
	
	public static int random(final int HIGH){
		return (int) Math.floor(Math.random()*(1+HIGH));
	}
	
	public static final String randomTime(){
		return TIMES[random(3)];
	}
	
	public static final String randomOrderby(boolean relevance){
		return ORDER_BY[random(4)];
	}
	
	public static final String[] getTimeSearch(){
		return TIMES;
	}
	
	public static final String[] getOrderbySearch(){
		return ORDER_BY;
	}
	
	public final Collection<Format> values(){
		return FORMATS.values();
	}
	
	public final Collection<Format> filter(final FormatType... TYPES){
		final Collection<Format> COLLECTION = new ArrayList<Format>();
		for(final Format FORMAT : FORMATS.values()){
			for(final FormatType TYPE : TYPES){
				if(FORMAT.getType() == TYPE){
					COLLECTION.add(FORMAT);
					break;
				}
			}
		}
		return COLLECTION;
	}
	
	public final String getTitle(){
		return videoTitle;
	}
	
	public final String getID(){
		return VIDEO_ID;
	}
	
	public final int getDuration(){
		return videoDuration;
	}
	
	public final String toString(){
		final CreateString STRING = new CreateString();
		STRING.put(String.format("[%1$s] %2$s Duration: %3$d ",VIDEO_ID,videoTitle,videoDuration));
		for(final Format FORMAT : FORMATS.values()){
			STRING.put(FORMAT.toString()).put("\r\n");
		}
		return STRING.toString();
	}
	
	public final void toJSON(final StringBuffer PAGE){
		int tmp = 0;
		PAGE.append("{");
		PAGE.append("\"id\":\"").append(VIDEO_ID).append("\",");
		PAGE.append("\"title\":\"").append(videoTitle).append("\",");
		PAGE.append("\"formats\":");
		PAGE.append("[");
		for(final Format FORMAT : FORMATS.values()){
			FORMAT.toJSON(PAGE);
			if(++tmp < FORMATS.size()){
				PAGE.append(",");
			}
		}
		PAGE.append("]}");
	}
	
	public final class Format implements com.master.data.interfaces.JSON,Serializable{
	
		private final int FORMAT_ID;
		private final String URL;
		private final int WIDTH, HEIGHT;
		private final FormatType TYPE;
		
		public Format(final String DATA, final String URL){
			this.URL = URL;
			final String[] VALUES = DATA.split("/");
			FORMAT_ID = Integer.parseInt(VALUES[0]);
			WIDTH = Integer.parseInt(VALUES[1].substring(0,VALUES[1].indexOf("x")));
			HEIGHT = Integer.parseInt(VALUES[1].substring(VALUES[1].indexOf("x")+1));
			if(FORMAT_ID == 5 || FORMAT_ID == 6 || FORMAT_ID == 34 || FORMAT_ID == 35){ //FLV
				TYPE = FormatType.FLV;
			}else if(FORMAT_ID == 18 || FORMAT_ID == 22 || FORMAT_ID == 37 || FORMAT_ID == 38){ //MP4
				TYPE = FormatType.MP4;
			}else if(FORMAT_ID == 83 || FORMAT_ID == 82 || FORMAT_ID == 85 || FORMAT_ID == 84){ //MP4 3D
				TYPE = FormatType.MP4_3D;
			}else if(FORMAT_ID == 43 || FORMAT_ID == 44 || FORMAT_ID == 45 || FORMAT_ID == 46){ //HTML5
				TYPE = FormatType.HTML5;
			}else if(FORMAT_ID == 100 || FORMAT_ID == 101 || FORMAT_ID == 102){ //HTML5 3D
				TYPE = FormatType.HTML5_3D;
			}else if(FORMAT_ID == 13 || FORMAT_ID == 17 || FORMAT_ID == 36){ //3GP
				TYPE = FormatType.GP3;
			}else{
				TYPE = FormatType.Unknown;
			}
			if(LOGGER != null){
				LOGGER.writeLine(toString());
			}
		}
		
		public final int getWidth(){
			return WIDTH;
		}
		
		public final int getHeight(){
			return HEIGHT;
		}
		
		public final FormatType getType(){
			return TYPE;
		}
		
		public final int getFormatID(){
			return FORMAT_ID;
		}
		
		public final String getURL(){
			return URL;
		}
		
		public final String toString(){
			return "Format: "+FORMAT_ID+" Type: "+TYPE+" Width: "+WIDTH+" Height: "+HEIGHT+"\r\nURL: "+URL;
		}
		
		public final void toJSON(final StringBuffer PAGE){
			PAGE.append("{\"format\":").append(FORMAT_ID).append(",");
			PAGE.append("\"type\":\"").append(TYPE).append("\",");
			PAGE.append("\"width\":").append(WIDTH).append(",");
			PAGE.append("\"height\":").append(HEIGHT).append(",");
			PAGE.append("\"url\":\"").append(URL).append("\"");
			PAGE.append("}");
		}
	
	}
	
	public final class Settings{
		
		private String queryURL = null;
		private String query = null;
		private String orderby = null;
		private String time = null;
		private String category = null;
		private String author = null;
		private String standardfeed = null;
		
		public Settings(){}
		
		public final void setQueryURL(final String URL){
			queryURL = URL;
		}
		
		public final void setQuery(final String QUERY){
			query = QUERY;
		}
		
		public final void setOrderby(final String ORDER_BY){
			orderby = ORDER_BY;
		}
		
		public final void setTime(final String TIME){
			time = TIME;
		}
		
		public final void setCategory(final String CATEGORY){
			category = CATEGORY;
		}
		
		public final void setAuthor(final String AUTHOR){
			author = AUTHOR;
		}
		
		public final void setStandardfeed(final String STANDARD_FEED){
			standardfeed = STANDARD_FEED;
		}
		
		public final YouTube playRandomVideo(){
		/*
			final CreateString CS = new CreateString();
			String url = "";
			if(queryURL == null){
				url = queryURL;
			}else{
				
			}
			*/
			return null;
		}
		
	}

}
