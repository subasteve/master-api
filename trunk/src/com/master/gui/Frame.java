package com.master.gui;

import java.awt.image.BufferedImage;
import java.awt.Graphics;
import java.awt.Color;

import java.awt.GraphicsEnvironment;
import java.awt.GraphicsDevice;

public class Frame extends java.awt.Frame{

	private BufferedImage image = null;
	private int xOffset = 0, yOffset = 0;

	public Frame(final String TITLE, final boolean RESIZEABLE, final boolean VISIBLE, final int WIDTH, int HEIGHT){
		super(TITLE);
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        	GraphicsDevice[] devices = env.getScreenDevices();
      		setSize(WIDTH,HEIGHT);
      		for (int i = 0; i < devices.length; i++) {
      			if(devices[i].isFullScreenSupported()){
      				setUndecorated(true);
      				setResizable(false);
      				//GraphicsConfiguration[] gc = devices[i].getConfigurations();
      				devices[i].setFullScreenWindow(this);
            			validate();
	      			//for (int x=0; x < gc.length; x++) {
			 		//System.out.println(gc[x].getBounds().toString());
				//}
			}
      		}
		//setResizable(RESIZEABLE);
		//setVisible(VISIBLE);
	}
	
	public final void setImage(final BufferedImage IMAGE, final int X_OFFSET, final int Y_OFFSET){
		image = IMAGE;
		xOffset = X_OFFSET;
		yOffset = Y_OFFSET;
	}
	
	public void update(final Graphics GRAPHICS){
		paint(GRAPHICS);
	}
	
	public final void paint(final Graphics GRAPHICS){
		GRAPHICS.setColor(Color.BLACK);
		GRAPHICS.fillRect( 0, 0, getWidth(), getHeight());
		if(image != null){
			GRAPHICS.drawImage(image, xOffset, yOffset, null);
		}
		GRAPHICS.dispose();
	}
	
}
