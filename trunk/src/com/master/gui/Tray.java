package com.master.gui;

import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.Image;
import java.awt.AWTException;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

public final class Tray{

	private final SystemTray TRAY;
	private final TrayIcon ICON;
	private boolean added = false;
	
	public Tray(final Image IMAGE, final String TITLE) throws AWTException,UnsupportedOperationException{
		if(SystemTray.isSupported()){
			TRAY = SystemTray.getSystemTray();
			ICON = new TrayIcon(IMAGE, TITLE);
			TRAY.add(ICON);
			added = true;
		}else{
			throw new UnsupportedOperationException();
		}
	}
	
	public final void setImage(final Image IMAGE){
		ICON.setImage(IMAGE);
	}
	
	public final void setTitle(final String TITLE){
		ICON.setToolTip(TITLE);
	}
	
	public final void remove(){
		if(added){
			TRAY.remove(ICON);
			added = false;
		}
	}
	
	public final void add() throws AWTException{
		if(!added){
			TRAY.add(ICON);
			added = true;
		}
	}
	
	public final void addActionListener(final ActionListener ACTION_LISTENER){
		ICON.addActionListener(ACTION_LISTENER);
	}
	
	public final void removeActionListener(final ActionListener ACTION_LISTENER){
		ICON.removeActionListener(ACTION_LISTENER);
	}
	
	public final void addMouseListener(final MouseListener MOUSE_LISTENER){
		ICON.addMouseListener(MOUSE_LISTENER);
	}
	
	public final void removeMouseListener(final MouseListener MOUSE_LISTENER){
		ICON.removeMouseListener(MOUSE_LISTENER);
	}
	
	public final void displayMessage(final String CAPTION, final String TEXT, final TrayIcon.MessageType TYPE){
		ICON.displayMessage(CAPTION,TEXT,TYPE);
	}
	
}