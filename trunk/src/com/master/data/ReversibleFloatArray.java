package com.master.data;

import java.util.Arrays;

/**
 * Created by subasteve on 1/30/17.
 */
public class ReversibleFloatArray {

    private float[] value = null, lastValue = null;

    public ReversibleFloatArray(){
    }

    public ReversibleFloatArray(final float[] VALUE){
        setValue(VALUE);
        save();
    }

    public final float[] getValue(){
        return value;
    }

    public final int getValueLength(){
        return value != null ? value.length : -1;
    }

    public final float getValue(final int POS){
        return value[POS];
    }

    public final ReversibleFloatArray setValue(final float[] VALUE){
        value = VALUE;
        return this;
    }

    public final ReversibleFloatArray setValue(final float VALUE, final int POS){
        value[POS] = VALUE;
        return this;
    }

    public final float[] getLastValue(){
        return lastValue;
    }

    public final int getLastValueLength(){
        return lastValue != null ? lastValue.length : -1;
    }

    public final float getLastValue(final int POS){
        return lastValue[POS];
    }

    public final boolean isModified(){
        return !Arrays.equals(value, lastValue);
    }

    public final boolean isModified(final int POS){
        return value[POS] != lastValue[POS];
    }

    public final ReversibleFloatArray save(){
        if(value != null){
            lastValue = Arrays.copyOf(value, value.length);
        }else{
            lastValue = null;
        }
        return this;
    }

    public final ReversibleFloatArray reset(){
        final boolean MODIFIED = isModified();
        if(lastValue != null && MODIFIED){
            value = Arrays.copyOf(lastValue, lastValue.length);
        }else if(lastValue == null && MODIFIED){
            value = null;
        }
        return this;
    }

}
