package com.master.data;

import java.util.Arrays;

/**
 * Created by subasteve on 1/30/17.
 */
public class ReversibleStringArray {

    private String[] value = null, lastValue = null;

    public ReversibleStringArray(){
    }

    public ReversibleStringArray(final String[] VALUE){
        setValue(VALUE);
        save();
    }

    public final String[] getValue(){
        return value;
    }

    public final int getValueLength(){
        return value != null ? value.length : -1;
    }

    public final String getValue(final int POS){
        return value[POS];
    }

    public final ReversibleStringArray setValue(final String[] VALUE){
        value = VALUE;
        return this;
    }

    public final ReversibleStringArray setValue(final String VALUE, final int POS){
        value[POS] = VALUE;
        return this;
    }

    public final String[] getLastValue(){
        return lastValue;
    }

    public final int getLastValueLength(){
        return lastValue != null ? lastValue.length : -1;
    }

    public final String getLastValue(final int POS){
        return lastValue[POS];
    }

    public final boolean isModified(){
        return !Arrays.equals(value, lastValue);
    }

    public final boolean isModified(final int POS){
        return value[POS] != lastValue[POS];
    }

    public final ReversibleStringArray save(){
        if(value != null){
            lastValue = Arrays.copyOf(value, value.length);
        }else{
            lastValue = null;
        }
        return this;
    }

    public final ReversibleStringArray reset(){
        final boolean MODIFIED = isModified();
        if(lastValue != null && MODIFIED){
            value = Arrays.copyOf(lastValue, lastValue.length);
        }else if(lastValue == null && MODIFIED){
            value = null;
        }
        return this;
    }

}
