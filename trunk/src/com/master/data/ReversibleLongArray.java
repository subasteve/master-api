package com.master.data;

import java.util.Arrays;

/**
 * Created by subasteve on 1/30/17.
 */
public class ReversibleLongArray {

    private long[] value = null, lastValue = null;

    public ReversibleLongArray(){
    }

    public ReversibleLongArray(final long[] VALUE){
        setValue(VALUE);
        save();
    }

    public final long[] getValue(){
        return value;
    }

    public final int getValueLength(){
        return value != null ? value.length : -1;
    }

    public final long getValue(final int POS){
        return value[POS];
    }

    public final ReversibleLongArray setValue(final long[] VALUE){
        value = VALUE;
        return this;
    }

    public final ReversibleLongArray setValue(final long VALUE, final int POS){
        value[POS] = VALUE;
        return this;
    }

    public final long[] getLastValue(){
        return lastValue;
    }

    public final int getLastValueLength(){
        return lastValue != null ? lastValue.length : -1;
    }

    public final long getLastValue(final int POS){
        return lastValue[POS];
    }

    public final boolean isModified(){
        return !Arrays.equals(value, lastValue);
    }

    public final boolean isModified(final int POS){
        return value[POS] != lastValue[POS];
    }

    public final ReversibleLongArray save(){
        if(value != null){
            lastValue = Arrays.copyOf(value, value.length);
        }else{
            lastValue = null;
        }
        return this;
    }

    public final ReversibleLongArray reset(){
        final boolean MODIFIED = isModified();
        if(lastValue != null && MODIFIED){
            value = Arrays.copyOf(lastValue, lastValue.length);
        }else if(lastValue == null && MODIFIED){
            value = null;
        }
        return this;
    }

}
