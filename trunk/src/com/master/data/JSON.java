package com.master.data;

import com.master.data.interfaces.UnhandledField;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public final class JSON {


    private final Object CLASS_OBJ;
    private final List<Field> FIELDS;
    private final Appendable PAGE;

    public JSON(){
        PAGE = null;
        CLASS_OBJ = null;
        FIELDS = null;
    }

    public JSON(final Appendable PAGE, final Object CLASS_OBJ){
        this.PAGE = PAGE;
        this.CLASS_OBJ = CLASS_OBJ;
        FIELDS = setupFields(CLASS_OBJ);
    }

    public JSON(final Appendable PAGE){
        this.PAGE = PAGE;
        CLASS_OBJ = null;
        FIELDS = null;
    }

    public JSON(final Object CLASS_OBJ){
        PAGE = null;
        this.CLASS_OBJ = CLASS_OBJ;
        FIELDS = setupFields(CLASS_OBJ);
    }

    public final Appendable getAppendable(){
        return PAGE;
    }

    public final List<Field> setupFields(final Object CLASS_OBJ){
        if(CLASS_OBJ != null) {
            final List<Field> FIELDS = new ArrayList();
            for (final Field FIELD : CLASS_OBJ.getClass().getDeclaredFields()) {
                final int MODIFIERS = FIELD.getModifiers();
                if (!Modifier.isTransient(MODIFIERS)) {
                    FIELD.setAccessible(true);
                    FIELDS.add(FIELD);
                }
            }
            for (final Field FIELD : CLASS_OBJ.getClass().getSuperclass().getDeclaredFields()) {
                final int MODIFIERS = FIELD.getModifiers();
                if (!Modifier.isTransient(MODIFIERS)) {
                    FIELD.setAccessible(true);
                    FIELDS.add(FIELD);
                }
            }
            return FIELDS;
        }
        return null;
    }

    public final JSON toJSON() {
        return toJSON(PAGE,CLASS_OBJ,FIELDS,null);
    }

    public final JSON toJSON(final Appendable PAGE) {
        return toJSON(PAGE,CLASS_OBJ,FIELDS,null);
    }

    public final JSON toJSON(final UnhandledField UNHANDLED_FIELD) {
        return toJSON(PAGE,CLASS_OBJ,FIELDS,UNHANDLED_FIELD);
    }

    public final JSON toJSON(final Appendable PAGE, final UnhandledField UNHANDLED_FIELD) {
        return toJSON(PAGE,CLASS_OBJ,FIELDS,UNHANDLED_FIELD);
    }

    public final JSON toJSON(final Appendable PAGE, final Object CLASS_OBJ, final List<Field> FIELDS) {
        return toJSON(PAGE,CLASS_OBJ,FIELDS,null);
    }

    public final JSON toJSON(final Appendable PAGE, final Object CLASS_OBJ, final List<Field> FIELDS, final UnhandledField UNHANDLED_FIELD) {
        if(CLASS_OBJ != null && PAGE != null){
            try {
                startObject(PAGE);
            }catch (final Exception E){}
            boolean first = true;
            for (final Field FIELD : FIELDS) {
                final Class TYPE = FIELD.getType();
                if (FIELD.getType().isArray()) {
                    final Class COMPONENT_TYPE = TYPE.getComponentType();
                    try {
                        if (COMPONENT_TYPE == String.class) {
                            createArray(PAGE, FIELD.getName(), (String[]) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(COMPONENT_TYPE == boolean.class){
                            createArray(PAGE, FIELD.getName(), (boolean[]) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(COMPONENT_TYPE == short.class){
                            createArray(PAGE, FIELD.getName(), (short[]) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(COMPONENT_TYPE == int.class){
                            createArray(PAGE, FIELD.getName(), (int[]) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(COMPONENT_TYPE == long.class){
                            createArray(PAGE, FIELD.getName(), (long[]) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(COMPONENT_TYPE == double.class){
                            createArray(PAGE, FIELD.getName(), (double[]) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(COMPONENT_TYPE == float.class){
                            createArray(PAGE, FIELD.getName(), (float[]) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(COMPONENT_TYPE == byte.class){
                            createArray(PAGE, FIELD.getName(), (byte[]) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else{
                            if(UNHANDLED_FIELD != null){
                                if(UNHANDLED_FIELD.unhandledField(PAGE,FIELD,CLASS_OBJ,first)){
                                    first = false;
                                }
                            }
                        }
                    }catch (final Exception E){}
                }else{
                    try {
                        if (TYPE == String.class) {
                            createField(PAGE, FIELD.getName(), (String) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(TYPE == boolean.class){
                            createField(PAGE, FIELD.getName(), (boolean) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(TYPE == short.class){
                            createField(PAGE, FIELD.getName(), (short) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(TYPE == int.class){
                            createField(PAGE, FIELD.getName(), (int) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(TYPE == long.class){
                            createField(PAGE, FIELD.getName(), (long) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(TYPE == double.class){
                            createField(PAGE, FIELD.getName(), (double) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(TYPE == float.class){
                            createField(PAGE, FIELD.getName(), (float) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else if(TYPE == byte.class){
                            createField(PAGE, FIELD.getName(), (byte) FIELD.get(CLASS_OBJ),false,!first);
                            first = false;
                        }else{
                            if(UNHANDLED_FIELD != null){
                                if(UNHANDLED_FIELD.unhandledField(PAGE,FIELD,CLASS_OBJ,first)){
                                    first = false;
                                }
                            }
                        }
                    }catch (final Exception E){}
                }
            }
            try {
                endObject(PAGE);
            }catch (final Exception E){}
        }
        return this;
    }

    public final JSON nextToken(final boolean NEXT) {
        return nextToken(PAGE,NEXT);
    }

    public final JSON nextToken(final Appendable PAGE, final boolean NEXT) {
        if(PAGE != null) {
            try {
                if (NEXT) {
                    PAGE.append(",");
                }
            } catch (final Exception E) {

            }
        }
        return this;
    }

    public final JSON startArray() {
        return startArray(PAGE,null,false);
    }

    public final JSON startArray(final boolean BEFORE) {
        return startArray(PAGE,null,BEFORE);
    }

    public final JSON startArray(final String FIELD) {
        return startArray(PAGE,FIELD,false);
    }

    public final JSON startArray(final String FIELD, final boolean BEFORE) {
        return startArray(PAGE,FIELD,BEFORE);
    }

    public final JSON startArray(final Appendable PAGE) {
        return startArray(PAGE,null,false);
    }

    public final JSON startArray(final Appendable PAGE, final String FIELD) {
        return startArray(PAGE,FIELD,false);
    }

    public final JSON startArray(final Appendable PAGE, final boolean BEFORE) {
        return startArray(PAGE,null,BEFORE);
    }

    public final JSON startArray(final Appendable PAGE, final String FIELD, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                nextToken(PAGE, BEFORE);
                if (FIELD != null) {
                    PAGE.append("\"").append(FIELD).append("\":[");
                } else {
                    PAGE.append("[");
                }
            } catch (final Exception E) {

            }
        }
        return this;
    }

    public final JSON endArray() {
        return endArray(PAGE,false);
    }

    public final JSON endArray(final boolean NEXT) {
        return endArray(PAGE,NEXT);
    }

    public final JSON endArray(final Appendable PAGE) {
        return endArray(PAGE,false);
    }

    public final JSON endArray(final Appendable PAGE, final boolean NEXT) {
        if(PAGE != null) {
            try {
                PAGE.append("]");
                nextToken(PAGE, NEXT);
            } catch (final Exception E) {

            }
        }
        return this;
    }


    public final JSON startField() {
        return startField(PAGE,null,false);
    }

    public final JSON startField(final String FIELD) {
        return startField(PAGE,FIELD,false);
    }

    public final JSON startField(final boolean BEFORE) {
        return startField(PAGE,null,BEFORE);
    }

    public final JSON startField(final String FIELD, final boolean BEFORE) {
        return startField(PAGE,FIELD,BEFORE);
    }

    public final JSON startField(final Appendable PAGE) {
        return startField(PAGE,null,false);
    }

    public final JSON startField(final Appendable PAGE, final String FIELD) {
        return startField(PAGE,FIELD,false);
    }

    public final JSON startField(final Appendable PAGE, final String FIELD, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                nextToken(PAGE, BEFORE);
                if (FIELD != null) {
                    PAGE.append("\"").append(FIELD).append("\":");
                }
            } catch (final Exception E) {

            }
        }
        return this;
    }

    public final JSON startObject() {
        return startObject(PAGE, null, false);
    }

    public final JSON startObject(final boolean BEFORE) {
        return startObject(PAGE, null, BEFORE);
    }

    public final JSON startObject(final String FIELD) {
        return startObject(PAGE, FIELD, false);
    }

    public final JSON startObject(final String FIELD, final boolean BEFORE) {
        return startObject(PAGE, FIELD, BEFORE);
    }

    public final JSON startObject(final Appendable PAGE) {
        return startObject(PAGE, null, false);
    }

    public final JSON startObject(final Appendable PAGE, final boolean BEFORE) {
        return startObject(PAGE, null, BEFORE);
    }

    public final JSON startObject(final Appendable PAGE, final String FIELD) {
        return startObject(PAGE, FIELD, false);
    }

    public final JSON startObject(final Appendable PAGE, final String FIELD, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                nextToken(PAGE, BEFORE);
                if (FIELD != null) {
                    PAGE.append("\"").append(FIELD).append("\":{");
                } else {
                    PAGE.append("{");
                }
            } catch (final Exception E) {

            }
        }
        return this;
    }

    public final JSON endObject() {
        return endObject(PAGE,false);
    }

    public final JSON endObject(final boolean NEXT) {
        return endObject(PAGE,NEXT);
    }

    public final JSON endObject(final Appendable PAGE) {
        return endObject(PAGE,false);
    }

    public final JSON endObject(final Appendable PAGE, final boolean NEXT) {
        if(PAGE != null) {
            try {
                PAGE.append("}");
                nextToken(PAGE, NEXT);
            } catch (final Exception E) {

            }
        }
        return this;
    }

    public final JSON createArray(final String[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final String[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String FIELD, final String[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final String[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final String[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final String[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final String[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startArray(PAGE, FIELD, BEFORE);
                if (STRING_ARRAY != null) {
                    boolean first = true;
                    for (int i = 0; i < STRING_ARRAY.length; i++) {
                        if (!first) {
                            nextToken(PAGE, true);
                        } else {
                            first = false;
                        }
                        PAGE.append("\"").append(STRING_ARRAY[i]).append("\"");
                    }
                }
                endArray(PAGE, NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createArray(final byte[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final byte[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final byte[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final byte[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final byte[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final byte[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String FIELD, final byte[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final byte[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final byte[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final byte[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final byte[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startArray(PAGE,FIELD,BEFORE);
                if (STRING_ARRAY != null) {
                    boolean first = true;
                    for (int i = 0; i < STRING_ARRAY.length; i++) {
                        if(!first){
                            nextToken(PAGE,true);
                        }else{
                            first = false;
                        }
                        PAGE.append(Byte.toString(STRING_ARRAY[i]));
                    }
                }
                endArray(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createArray(final short[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final short[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final short[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final short[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final short[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final short[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String FIELD, final short[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final short[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final short[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final short[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final short[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startArray(PAGE,FIELD,BEFORE);
                if (STRING_ARRAY != null) {
                    boolean first = true;
                    for (int i = 0; i < STRING_ARRAY.length; i++) {
                        if(!first){
                            nextToken(PAGE,true);
                        }else{
                            first = false;
                        }
                        PAGE.append(Short.toString(STRING_ARRAY[i]));
                    }
                }
                endArray(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createArray(final int[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final int[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final int[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final int[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final int[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final int[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String FIELD, final int[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final int[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final int[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final int[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final int[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startArray(PAGE,FIELD,BEFORE);
                if (STRING_ARRAY != null) {
                    boolean first = true;
                    for (int i = 0; i < STRING_ARRAY.length; i++) {
                        if(!first){
                            nextToken(PAGE,true);
                        }else{
                            first = false;
                        }
                        PAGE.append(Integer.toString(STRING_ARRAY[i]));
                    }
                }
                endArray(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createArray(final double[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final double[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final double[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final double[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final double[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final double[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String FIELD, final double[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final double[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final double[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final double[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final double[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startArray(PAGE,FIELD,BEFORE);
                if (STRING_ARRAY != null) {
                    boolean first = true;
                    for (int i = 0; i < STRING_ARRAY.length; i++) {
                        if(!first){
                            nextToken(PAGE,true);
                        }else{
                            first = false;
                        }
                        PAGE.append(Double.toString(STRING_ARRAY[i]));
                    }
                }
                endArray(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createArray(final float[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final float[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final float[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final float[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final float[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final float[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String FIELD, final float[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final float[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final float[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final float[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public  final JSON createArray(final Appendable PAGE, final String FIELD, final float[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startArray(PAGE,FIELD,BEFORE);
                if (STRING_ARRAY != null) {
                    boolean first = true;
                    for (int i = 0; i < STRING_ARRAY.length; i++) {
                        if(!first){
                            nextToken(PAGE,true);
                        }else{
                            first = false;
                        }
                        PAGE.append(Float.toString(STRING_ARRAY[i]));
                    }
                }
                endArray(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createArray(final long[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final long[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final long[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final long[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final long[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final long[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String FIELD, final long[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final long[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final long[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final long[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final long[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startArray(PAGE,FIELD,BEFORE);
                if (STRING_ARRAY != null) {
                    boolean first = true;
                    for (int i = 0; i < STRING_ARRAY.length; i++) {
                        if(!first){
                            nextToken(PAGE,true);
                        }else{
                            first = false;
                        }
                        PAGE.append(Long.toString(STRING_ARRAY[i]));
                    }
                }
                endArray(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createArray(final boolean[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final boolean[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final boolean[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final boolean[] STRING_ARRAY) {
        return createArray(PAGE, null, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final boolean[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, null, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final boolean[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final String FIELD, final boolean[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final String FIELD, final boolean[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, BEFORE);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final boolean[] STRING_ARRAY) {
        return createArray(PAGE, FIELD, STRING_ARRAY, false, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final boolean[] STRING_ARRAY, final boolean NEXT) {
        return createArray(PAGE, FIELD, STRING_ARRAY, NEXT, false);
    }

    public final JSON createArray(final Appendable PAGE, final String FIELD, final boolean[] STRING_ARRAY, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startArray(PAGE,FIELD,BEFORE);
                if (STRING_ARRAY != null) {
                    boolean first = true;
                    for (int i = 0; i < STRING_ARRAY.length; i++) {
                        if(!first){
                            nextToken(PAGE,true);
                        }else{
                            first = false;
                        }
                        PAGE.append(Boolean.toString(STRING_ARRAY[i]));
                    }
                }
                endArray(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createField(final String VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    //Had to rename to createStringField since boolean method conflict
    public final JSON createStringField(final String VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createStringField(final String VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, null, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createStringField(final Appendable PAGE, final String VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final String VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final String FIELD, final String VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final String VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, FIELD, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final String VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final String VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final String VALUE, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startField(PAGE,FIELD,BEFORE);
                PAGE.append("\"").append(VALUE).append("\"");
                nextToken(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createField(final byte VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final byte VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final byte VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, null, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final byte VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final byte VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final byte VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final String FIELD, final byte VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final byte VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, FIELD, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final byte VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final byte VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final byte VALUE, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startField(PAGE,FIELD,BEFORE);
                PAGE.append(Byte.toString(VALUE));
                nextToken(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createField(final short VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final short VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final short VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, null, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final short VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final short VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final short VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final String FIELD, final short VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final short VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, FIELD, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final short VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final short VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final short VALUE, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startField(PAGE,FIELD,BEFORE);
                PAGE.append(Short.toString(VALUE));
                nextToken(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createField(final int VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final int VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final int VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, null, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final int VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final int VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final int VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final String FIELD, final int VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final int VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, FIELD, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final int VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final int VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final int VALUE, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startField(PAGE,FIELD,BEFORE);
                PAGE.append(Integer.toString(VALUE));
                nextToken(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createField(final long VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final long VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final long VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, null, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final long VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final long VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final long VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final String FIELD, final long VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final long VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, FIELD, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final long VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final long VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final long VALUE, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startField(PAGE,FIELD,BEFORE);
                PAGE.append(Long.toString(VALUE));
                nextToken(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createField(final double VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final double VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final double VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, null, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final double VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final double VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final double VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final String FIELD, final double VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final double VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, FIELD, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final double VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final double VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final double VALUE, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startField(PAGE,FIELD,BEFORE);
                PAGE.append(Double.toString(VALUE));
                nextToken(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createField(final float VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final float VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final float VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, null, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final float VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final float VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final float VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final String FIELD, final float VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final float VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, FIELD, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final float VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final float VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final float VALUE, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startField(PAGE,FIELD,BEFORE);
                PAGE.append(Float.toString(VALUE));
                nextToken(PAGE,NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

    public final JSON createField(final boolean VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final boolean VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final boolean VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, null, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final boolean VALUE) {
        return createField(PAGE, null, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final boolean VALUE, final boolean NEXT) {
        return createField(PAGE, null, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final boolean VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final String FIELD, final boolean VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final String FIELD, final boolean VALUE, final boolean NEXT, final boolean BEFORE) {
        return createField(PAGE, FIELD, VALUE, NEXT, BEFORE);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final boolean VALUE) {
        return createField(PAGE, FIELD, VALUE, false, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final boolean VALUE, final boolean NEXT) {
        return createField(PAGE, FIELD, VALUE, NEXT, false);
    }

    public final JSON createField(final Appendable PAGE, final String FIELD, final boolean VALUE, final boolean NEXT, final boolean BEFORE) {
        if(PAGE != null) {
            try {
                startField(PAGE, FIELD, BEFORE);
                PAGE.append(Boolean.toString(VALUE));
                nextToken(PAGE, NEXT);
            } catch (final Exception E) {
            }
        }
        return this;
    }

}
