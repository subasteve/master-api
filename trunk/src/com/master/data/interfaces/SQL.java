package com.master.data.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface SQL {

	public void fromSQL(final ResultSet RESULTS) throws SQLException;
	public void toSQL();
	
}
