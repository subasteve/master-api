package com.master.data.interfaces;

import com.master.net.CacheServer;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by subasteve on 10/17/13.
 */
public interface SQLToCache {

    public void fromSQLToCache(final ResultSet RESULTS, final CacheServer CACHE) throws SQLException;

}