package com.master.data.interfaces;

import com.master.cache.LocalCache;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by subasteve on 10/17/13.
 */
public interface SQLToLocalCache {

    public void fromSQLToCache(final ResultSet RESULTS, final LocalCache CACHE) throws SQLException;

}