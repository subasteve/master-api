package com.master.data.interfaces;

import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Created by subasteve on 9/13/16.
 */
public interface UnhandledField {

    boolean unhandledField(final Appendable PAGE, final Field FIELD, final Object CLASS_OBJ, final boolean FIRST);

}
