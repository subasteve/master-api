package com.master.data;

import java.util.Arrays;

/**
 * Created by subasteve on 1/30/17.
 */
public class ReversibleBooleanArray {

    private boolean[] value = null, lastValue = null;

    public ReversibleBooleanArray(){
    }

    public ReversibleBooleanArray(final boolean[] VALUE){
        setValue(VALUE);
        save();
    }

    public final boolean[] getValue(){
        return value;
    }

    public final int getValueLength(){
        return value != null ? value.length : -1;
    }

    public final boolean getValue(final int POS){
        return value[POS];
    }

    public final ReversibleBooleanArray setValue(final boolean[] VALUE){
        value = VALUE;
        return this;
    }

    public final ReversibleBooleanArray setValue(final boolean VALUE, final int POS){
        value[POS] = VALUE;
        return this;
    }

    public final boolean[] getLastValue(){
        return lastValue;
    }

    public final int getLastValueLength(){
        return lastValue != null ? lastValue.length : -1;
    }

    public final boolean getLastValue(final int POS){
        return lastValue[POS];
    }

    public final boolean isModified(){
        return !Arrays.equals(value, lastValue);
    }

    public final boolean isModified(final int POS){
        return value[POS] != lastValue[POS];
    }

    public final ReversibleBooleanArray save(){
        if(value != null){
            lastValue = Arrays.copyOf(value, value.length);
        }else{
            lastValue = null;
        }
        return this;
    }

    public final ReversibleBooleanArray reset(){
        final boolean MODIFIED = isModified();
        if(lastValue != null && MODIFIED){
            value = Arrays.copyOf(lastValue, lastValue.length);
        }else if(lastValue == null && MODIFIED){
            value = null;
        }
        return this;
    }

}
