package com.master.data;

/**
 * Created by subasteve on 8/30/16.
 */
public final class ReversibleDouble {

    private double value = -1, lastValue = -1;

    public ReversibleDouble(){
    }

    public ReversibleDouble(final double VALUE){
        setValue(VALUE);
        save();
    }

    public final double getValue(){
        return value;
    }

    public final ReversibleDouble setValue(final double VALUE){
        value = VALUE;
        return this;
    }

    public final double getLastValue(){
        return lastValue;
    }

    public final boolean isModified(){
        return value != lastValue;
    }

    public final ReversibleDouble save(){
        lastValue = value;
        return this;
    }

    public final ReversibleDouble reset(){
        if(isModified()){
            value = lastValue;
        }
        return this;
    }
}
