package com.master.data;

/**
 * Created by subasteve on 8/30/16.
 */
public final class ReversibleInt {

    private int value = -1, lastValue = -1;

    public ReversibleInt(){
    }

    public ReversibleInt(final int VALUE){
        setValue(VALUE);
        save();
    }

    public final int getValue(){
        return value;
    }

    public final ReversibleInt setValue(final int VALUE){
        value = VALUE;
        return this;
    }

    public final int getLastValue(){
        return lastValue;
    }

    public final boolean isModified(){
        return value != lastValue;
    }

    public final ReversibleInt save(){
        lastValue = value;
        return this;
    }

    public final ReversibleInt reset(){
        if(isModified()){
            value = lastValue;
        }
        return this;
    }
}
