package com.master.data;

/**
 * Created by subasteve on 8/30/16.
 */
public final class ReversibleShort {

    private short value = -1, lastValue = -1;
    private final boolean UNSIGNED;

    public ReversibleShort(){
        UNSIGNED = false;
    }

    public ReversibleShort(final boolean UNSIGNED){
        this.UNSIGNED = UNSIGNED;
    }


    public ReversibleShort(final short VALUE){
        UNSIGNED = false;
        setValue(VALUE);
        save();
    }

    public ReversibleShort(final short VALUE, final boolean UNSIGNED){
        this.UNSIGNED = UNSIGNED;
        setValue(VALUE);
        save();
    }


    public final short getValue(){
        return value;
    }

    public final ReversibleShort setValue(final short VALUE){
        value = VALUE;
        return this;
    }

    public final int getUnsignedValue(){
        return value == -1 ? -1 : value & 0xffff;
    }

    public final short getLastValue(){
        return lastValue;
    }

    public final int getUnsignedLastValue(){
        return lastValue == -1 ? -1 : lastValue & 0xffff;
    }

    public final boolean isModified(){
        return value != lastValue;
    }

    public final boolean isUnsigned(){
        return UNSIGNED;
    }

    public final ReversibleShort save(){
        lastValue = value;
        return this;
    }

    public final ReversibleShort reset(){
        if(isModified()){
            value = lastValue;
        }
        return this;
    }
}
