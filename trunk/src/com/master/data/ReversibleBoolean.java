package com.master.data;

/**
 * Created by subasteve on 8/30/16.
 */
public class ReversibleBoolean {

    private boolean value = false, lastValue = false;

    public ReversibleBoolean(){
    }

    public ReversibleBoolean(final boolean VALUE){
        setValue(VALUE);
        save();
    }

    public final boolean getValue(){
        return value;
    }

    public final ReversibleBoolean setValue(final boolean VALUE){
        value = VALUE;
        return this;
    }

    public final boolean getLastValue(){
        return lastValue;
    }

    public final boolean isModified(){
        return value != lastValue;
    }

    public final ReversibleBoolean save(){
        lastValue = value;
        return this;
    }

    public final ReversibleBoolean reset(){
        if(isModified()){
            value = lastValue;
        }
        return this;
    }

}
