package com.master.data;

import java.util.Arrays;

/**
 * Created by subasteve on 1/30/17.
 */
public class ReversibleDoubleArray {

    private double[] value = null, lastValue = null;

    public ReversibleDoubleArray(){
    }

    public ReversibleDoubleArray(final double[] VALUE){
        setValue(VALUE);
        save();
    }

    public final double[] getValue(){
        return value;
    }

    public final int getValueLength(){
        return value != null ? value.length : -1;
    }

    public final double getValue(final int POS){
        return value[POS];
    }

    public final ReversibleDoubleArray setValue(final double[] VALUE){
        value = VALUE;
        return this;
    }

    public final ReversibleDoubleArray setValue(final double VALUE, final int POS){
        value[POS] = VALUE;
        return this;
    }

    public final double[] getLastValue(){
        return lastValue;
    }

    public final int getLastValueLength(){
        return lastValue != null ? lastValue.length : -1;
    }

    public final double getLastValue(final int POS){
        return lastValue[POS];
    }

    public final boolean isModified(){
        return !Arrays.equals(value, lastValue);
    }

    public final boolean isModified(final int POS){
        return value[POS] != lastValue[POS];
    }

    public final ReversibleDoubleArray save(){
        if(value != null){
            lastValue = Arrays.copyOf(value, value.length);
        }else{
            lastValue = null;
        }
        return this;
    }

    public final ReversibleDoubleArray reset(){
        final boolean MODIFIED = isModified();
        if(lastValue != null && MODIFIED){
            value = Arrays.copyOf(lastValue, lastValue.length);
        }else if(lastValue == null && MODIFIED){
            value = null;
        }
        return this;
    }

}
