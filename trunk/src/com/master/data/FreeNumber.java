package com.master.data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
* Thread safe free number
* Allowing for unique numbers for reference to objects
* @author Subasteve
* @version 1.0
*/
public final class FreeNumber{

	private final Map<Integer,Integer> NUMBERS = new ConcurrentHashMap<Integer,Integer>();
	private final Map<Integer,Integer> OPEN_SLOTS = new ConcurrentHashMap<Integer,Integer>();
	private final int START_POS;
	private int size = 0;

	public FreeNumber(){
		START_POS = 0;
	}

	public FreeNumber(final int START_POS){
		this.START_POS = START_POS;
		size = START_POS;
	}
	
	public final void reset(){
		NUMBERS.clear();
		OPEN_SLOTS.clear();
		size = START_POS;
	}
	
	public final int put(){
		if(!OPEN_SLOTS.isEmpty()){
			int taskID = -1;
			for(final int TASK_ID : OPEN_SLOTS.values()){
				taskID = TASK_ID;
				break;
			}
			if(taskID != -1){
				OPEN_SLOTS.remove(taskID);
				return taskID;
			}
		}
		if(NUMBERS.get(size) == null){
			NUMBERS.put(size, size);
			return size++;
		}
		return -1;
	}
	
	public final boolean put(final int ID){
		if(NUMBERS.get(ID) == null){
			NUMBERS.put(ID,ID);
			return true;
		}
		return false;
	}
	
	public final boolean isEmpty(final int ID){
		return (NUMBERS.get(ID) == null);
	}
	
	public final int getSize(){
		return size-OPEN_SLOTS.size();
	}
	
	public final boolean remove(final int ID){
		if(ID < -1) return false;
		if(NUMBERS.get(ID) != null){
			final int NUMBER = NUMBERS.get(ID);
			NUMBERS.remove(ID);
			OPEN_SLOTS.put(ID,ID);
			return true;
		}
		return false;
	}

}