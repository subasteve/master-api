package com.master.data;

/**
 * Created by subasteve on 8/30/16.
 */
public final class ReversibleLong {

    private long value = -1, lastValue = -1;

    public ReversibleLong(){
    }

    public ReversibleLong(final long VALUE){
        setValue(VALUE);
        save();
    }

    public final long getValue(){
        return value;
    }

    public final ReversibleLong setValue(final long VALUE){
        value = VALUE;
        return this;
    }

    public final long getLastValue(){
        return lastValue;
    }

    public final boolean isModified(){
        return value != lastValue;
    }

    public final ReversibleLong save(){
        lastValue = value;
        return this;
    }

    public final ReversibleLong reset(){
        if(isModified()){
            value = lastValue;
        }
        return this;
    }
}
