package com.master.data;

/**
 * Created by subasteve on 8/30/16.
 */
public class ReversibleString {

    private String value = null, lastValue = null;

    public ReversibleString(){
    }

    public ReversibleString(final String VALUE){
        setValue(VALUE);
        save();
    }

    public final String getValue(){
        return value;
    }

    public final ReversibleString setValue(final String VALUE){
        value = VALUE;
        return this;
    }

    public final String getLastValue(){
        return lastValue;
    }

    public final boolean isModified(){
        return value != lastValue;
    }

    public final ReversibleString save(){
        lastValue = value;
        return this;
    }

    public final ReversibleString reset(){
        if(isModified()){
            value = lastValue;
        }
        return this;
    }

}
