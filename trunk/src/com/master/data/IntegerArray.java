package com.master.data;

import java.util.Map;
import java.util.HashMap;
import com.master.net.socket.stream.in.In;
import com.master.net.socket.stream.out.Out;
import java.io.IOException;

public final class IntegerArray{

	private final Map<Integer, Int> STORED_INTEGERS = new HashMap<Integer, Int>();
	private int lastIndex = 0;

	public IntegerArray(){}

	public IntegerArray(final In IN) throws IOException{
		final int SIZE = IN.readInteger();
		for(int i = 0; i < SIZE; i++){
			put(IN.readInteger());
		}
	}

	public final IntegerArray put(final int VALUE){
		STORED_INTEGERS.put(lastIndex, new Int(VALUE,lastIndex));
		lastIndex++;
		return this;
	}

	public final IntegerArray put(final int VALUE, final int INDEX){
		STORED_INTEGERS.put(INDEX, new Int(VALUE,INDEX));
		return this;
	}

	public final int get(final int INDEX) throws NullPointerException{
		final Int INT = STORED_INTEGERS.get(INDEX);
		if(INT == null){
			throw new NullPointerException();
		}
		return INT.getValue();
	}

	public final int size(){
		return STORED_INTEGERS.size();
	}

	public final IntegerArray remove(final int INDEX){
		STORED_INTEGERS.remove(INDEX);
		return this;
	}

	public final void writeTo(final Out OUT) throws IOException{
		OUT.writeInteger(STORED_INTEGERS.size());
		for(final Int INT : STORED_INTEGERS.values()){
			INT.writeTo(OUT);
		}
	}

}
