package com.master.data;

/**
 * Created by subasteve on 8/30/16.
 */
public class ReversibleByte {

    private byte value = -1, lastValue = -1;
    private final boolean UNSIGNED;

    public ReversibleByte(){
        UNSIGNED = false;
    }

    public ReversibleByte(final boolean UNSIGNED){
        this.UNSIGNED = UNSIGNED;
    }

    public ReversibleByte(final byte VALUE){
        UNSIGNED = false;
        setValue(VALUE);
        save();
    }

    public ReversibleByte(final byte VALUE, final boolean UNSIGNED){
        this.UNSIGNED = UNSIGNED;
        setValue(VALUE);
        save();
    }

    public final byte getValue(){
        return value;
    }

    public final int getUnsignedValue(){
        return value == -1 ? -1 : value & 0xff;
    }

    public final ReversibleByte setValue(final byte VALUE){
        value = VALUE;
        return this;
    }

    public final byte getLastValue(){
        return lastValue;
    }

    public final int getUnsignedLastValue(){
        return lastValue == -1 ? -1 : lastValue & 0xff;
    }

    public final boolean isModified(){
        return value != lastValue;
    }

    public final boolean isUnsigned(){
        return UNSIGNED;
    }

    public final ReversibleByte save(){
        lastValue = value;
        return this;
    }

    public final ReversibleByte reset(){
        if(isModified()){
            value = lastValue;
        }
        return this;
    }

}
