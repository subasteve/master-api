package com.master.data;

import static java.lang.Math.abs;

/**
 * Created by subasteve on 11/10/16.
 */
public class UnsignedByte {

    private byte value = 0;

    public UnsignedByte(){

    }

    public UnsignedByte(final byte VALUE){
        value = VALUE;
    }

    private void setupValue(final short VALUE){
        if(VALUE > 0 && VALUE < 256) {
            value = (byte) VALUE;
        }
    }

    private void setupValue(final int VALUE){
        if(VALUE > 0 && VALUE < 256) {
            value = (byte) VALUE;
        }
    }

    public UnsignedByte(final short VALUE){
        setupValue(VALUE);
    }

    public UnsignedByte(final int VALUE){
        setupValue(VALUE);
    }

    public final byte getAsByte(){
        return value;
    }

    public final short getAsShort(){
        return (short)get();
    }

    public final int get(){
        return Byte.toUnsignedInt(value);
    }

    public final UnsignedByte set(final short VALUE){
        setupValue(VALUE);
        return this;
    }

    public final UnsignedByte set(final int VALUE){
        setupValue(VALUE);
        return this;
    }

    public final UnsignedByte set(final byte VALUE){
        value = VALUE;
        return this;
    }

}
