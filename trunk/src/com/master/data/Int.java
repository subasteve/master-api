package com.master.data;

import com.master.net.socket.stream.in.In;
import com.master.net.socket.stream.out.Out;
import java.io.IOException;

public final class Int{

	private int integer;
	private int index;

	public Int(final int INT, final int INDEX){
		integer = INT;
		index = INDEX;
	}

	public Int(final int INT){
		integer = INT;
		index = -1;
	}

	public Int(final String INT_STRING) throws NumberFormatException{
		integer = Integer.parseInt(INT_STRING);
		index = -1;
	}

	public Int(final String INT_STRING, final int INDEX) throws NumberFormatException{
		integer = Integer.parseInt(INT_STRING);
		index = INDEX;
        }

	public Int(final In IN) throws IOException{
		integer = IN.readInteger();
	}

	public Int(final In IN, final int INDEX) throws IOException{
		integer = IN.readInteger();
		index = INDEX;
	}

	public int getValue(){
		return integer;
	}

	public int getIndex(){
		return index;
	}

	public void setValue(final int INT){
		integer = INT;
	}

	public void setIndex(final int INDEX){
		index = INDEX;
	}

	public void writeTo(final Out OUT) throws IOException{
		OUT.writeInteger(integer);
	}

	public String toString(){
		return Integer.toString(integer);
	}

}
