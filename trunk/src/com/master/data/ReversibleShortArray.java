package com.master.data;

import java.util.Arrays;

/**
 * Created by subasteve on 1/30/17.
 */
public class ReversibleShortArray {

    private short[] value = null, lastValue = null;
    private final boolean UNSIGNED;

    public ReversibleShortArray(){
        UNSIGNED = false;
    }

    public ReversibleShortArray(final boolean UNSIGNED){
        this.UNSIGNED = UNSIGNED;
    }

    public ReversibleShortArray(final short[] VALUE){
        UNSIGNED = false;
        setValue(VALUE);
        save();
    }

    public ReversibleShortArray(final short[] VALUE, final boolean UNSIGNED){
        this.UNSIGNED = UNSIGNED;
        setValue(VALUE);
        save();
    }

    private final int[] createUnsignedArray(final short[] ARRAY){
        if(ARRAY != null){
            final int[] VALUES = new int[ARRAY.length];
            for(int i = 0; i < ARRAY.length; i++){
                VALUES[i] = ARRAY[i] == -1 ? -1 : ARRAY[i] & 0xffff;
            }
            return VALUES;
        }
        return null;
    }

    public final short[] getValue(){
        return value;
    }

    public final int[] getUnsignedValue(){
        return createUnsignedArray(value);
    }

    public final int getValueLength(){
        return value != null ? value.length : -1;
    }

    public final short getValue(final int POS){
        return value[POS];
    }

    public final int getUnsignedValue(final int POS){
        return value[POS] == -1 ? -1 : value[POS] & 0xffff;
    }

    public final ReversibleShortArray setValue(final short[] VALUE){
        value = VALUE;
        return this;
    }

    public final ReversibleShortArray setValue(final short VALUE, final int POS){
        value[POS] = VALUE;
        return this;
    }

    public final short[] getLastValue(){
        return lastValue;
    }

    public final int[] getUnsignedLastValue(){
        return createUnsignedArray(lastValue);
    }

    public final int getLastValueLength(){
        return lastValue != null ? lastValue.length : -1;
    }

    public final short getLastValue(final int POS){
        return lastValue[POS];
    }

    public final int getUnsignedLastValue(final int POS){
        return lastValue[POS] == -1 ? -1 : lastValue[POS] & 0xffff;
    }

    public final boolean isModified(){
        return !Arrays.equals(value, lastValue);
    }

    public final boolean isModified(final int POS){
        return value[POS] != lastValue[POS];
    }

    public final boolean isUnsigned(){
        return UNSIGNED;
    }

    public final ReversibleShortArray save(){
        if(value != null){
            lastValue = Arrays.copyOf(value, value.length);
        }else{
            lastValue = null;
        }
        return this;
    }

    public final ReversibleShortArray reset(){
        final boolean MODIFIED = isModified();
        if(lastValue != null && MODIFIED){
            value = Arrays.copyOf(lastValue, lastValue.length);
        }else if(lastValue == null && MODIFIED){
            value = null;
        }
        return this;
    }

}
