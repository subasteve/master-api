package com.master.data;

import java.util.Arrays;

/**
 * Created by subasteve on 1/30/17.
 */
public class ReversibleIntArray {

    private int[] value = null, lastValue = null;

    public ReversibleIntArray(){
    }

    public ReversibleIntArray(final int[] VALUE){
        setValue(VALUE);
        save();
    }

    public final int[] getValue(){
        return value;
    }

    public final int getValueLength(){
        return value != null ? value.length : -1;
    }

    public final int getValue(final int POS){
        return value[POS];
    }

    public final ReversibleIntArray setValue(final int[] VALUE){
        value = VALUE;
        return this;
    }

    public final ReversibleIntArray setValue(final int VALUE, final int POS){
        value[POS] = VALUE;
        return this;
    }

    public final int[] getLastValue(){
        return lastValue;
    }

    public final int getLastValueLength(){
        return lastValue != null ? lastValue.length : -1;
    }

    public final int getLastValue(final int POS){
        return lastValue[POS];
    }

    public final boolean isModified(){
        return !Arrays.equals(value, lastValue);
    }

    public final boolean isModified(final int POS){
        return value[POS] != lastValue[POS];
    }

    public final ReversibleIntArray save(){
        if(value != null){
            lastValue = Arrays.copyOf(value, value.length);
        }else{
            lastValue = null;
        }
        return this;
    }

    public final ReversibleIntArray reset(){
        final boolean MODIFIED = isModified();
        if(lastValue != null && MODIFIED){
            value = Arrays.copyOf(lastValue, lastValue.length);
        }else if(lastValue == null && MODIFIED){
            value = null;
        }
        return this;
    }

}
