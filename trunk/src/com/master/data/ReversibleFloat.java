package com.master.data;

/**
 * Created by subasteve on 8/30/16.
 */
public final class ReversibleFloat {

    private float value = -1, lastValue = -1;

    public ReversibleFloat(){
    }

    public ReversibleFloat(final float VALUE){
        setValue(VALUE);
        save();
    }

    public final float getValue(){
        return value;
    }

    public final ReversibleFloat setValue(final float VALUE){
        value = VALUE;
        return this;
    }

    public final float getLastValue(){
        return lastValue;
    }

    public final boolean isModified(){
        return value != lastValue;
    }

    public final ReversibleFloat save(){
        lastValue = value;
        return this;
    }

    public final ReversibleFloat reset(){
        if(isModified()){
            value = lastValue;
        }
        return this;
    }
}
