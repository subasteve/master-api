package com.master.geometry.precisioninteger;

import com.master.io.socket.Buffer;
import com.master.net.socket.stream.in.In;
import com.master.net.socket.stream.out.Out;
import java.io.IOException;

import java.nio.BufferUnderflowException;
import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;

public final class Point{

	private int x = -1, y = -1;

	public Point(){}

	public Point(final int X, final int Y){
		x = X;
		y = Y;
	}

	public Point(final Point POINT){
		x = POINT.getX();
		y = POINT.getY();
	}

	public Point(final In IN) throws IOException{
		x = IN.readInteger();
		y = IN.readInteger();
	}
	
	public Point(final Buffer BUFFER) throws BufferUnderflowException{
		x = BUFFER.getInteger();
		y = BUFFER.getInteger();
	}

	public final boolean equals(final Point POINT){
		return (x == POINT.getX() && y == POINT.getY());
	}

	public final int getX(){
		return x;
	}

	public final int getY(){
		return y;
	}

	public final void setX(final int X){
		x = X;
	}

	public final void setY(final int Y){
		y = Y;
	}

	public final void set(final int X, final int Y){
		x = X;
		y = Y;
	}

	public final void set(final Point POINT){
		x = POINT.getX();
		y = POINT.getY();
	}

	public final void translateX(final int X){
		x+=X;
	}

	public final void translateY(final int Y){
		y+=Y;
	}

	public final void translate(final int X, final int Y){
		x+=X;
		y+=Y;
	}
	
	//Distance Formula - straight line to point
	public final int distanceTo(final Point POINT){
		return (int) Math.sqrt(Math.pow(x - POINT.getX(), 2) + Math.pow(y - POINT.getY(), 2));
	}
	
	public final int distanceTo(final int X, final int Y){
		return (int) Math.sqrt(Math.pow(x - X, 2) + Math.pow(y - Y, 2));
	}
	
	//X,Y 
	public final Point distanceToXY(final Point POINT){
		return new Point(x - POINT.getX(),y - POINT.getY());
	}
	
	public final Point distanceToXY(final int X, final int Y){
		return new Point(x - X,y - Y);
	}

	public final String toString(){
		return new StringBuffer("Point [").append(x).append(",").append(y).append("]").toString();
	}

	public final void put(final Out OUT) throws IOException{
		OUT.writeInteger(x);
		OUT.writeInteger(y);
	}
	
	public final void put(final Buffer BUFFER) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		BUFFER.putInteger(x).putInteger(y).putToChannel();
	}

}
