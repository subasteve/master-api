package com.master.geometry.precisioninteger;

import com.master.io.socket.Buffer;
import com.master.net.socket.stream.in.In;
import com.master.net.socket.stream.out.Out;
import java.io.IOException;

import java.nio.BufferUnderflowException;
import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;

public final class Dimension{

	private int width = -1, height = -1;

	public Dimension(){}

	public Dimension(final int WIDTH, final int HEIGHT){
		width = WIDTH;
		height = HEIGHT;
	}

	public Dimension(final Dimension DIMENSION){
		width = DIMENSION.getWidth();
		height = DIMENSION.getHeight();
	}

	public Dimension(final In IN) throws IOException{
		width = IN.readInteger();
		height = IN.readInteger();
	}
	
	public Dimension(final Buffer BUFFER) throws BufferUnderflowException{
		width = BUFFER.getInteger();
		height = BUFFER.getInteger();
	}

	public final boolean equals(final Dimension DIMENSION){
		return(width == DIMENSION.getWidth() && height == DIMENSION.getHeight());
	}

	public final int getWidth(){
		return width;
	}

	public final int getHeight(){
		return height;
	}

	public final void setWidth(final int WIDTH){
		width = WIDTH;
	}

	public final void setHeight(final int HEIGHT){
		height = HEIGHT;
	}

	public final void set(final int WIDTH, final int HEIGHT){
		width = WIDTH;
		height = HEIGHT;
	}

	public final void set(final Dimension DIMENSION){
		width = DIMENSION.getWidth();
		height = DIMENSION.getHeight();
	}

	public final void translateWidth(final int WIDTH){
		width+=WIDTH;
	}

	public final void translateHeight(final int HEIGHT){
		height+=HEIGHT;
	}

	public final void translate(final int WIDTH, final int HEIGHT){
		width+=WIDTH;
		height+=HEIGHT;
	}

	public final String toString(){
		return new StringBuffer("Dimension [").append(width).append(",").append(height).append("]").toString();
	}

	public final void put(final Out OUT) throws IOException{
		OUT.writeInteger(width);
		OUT.writeInteger(height);
	}
	
	public final void put(final Buffer BUFFER) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		BUFFER.putInteger(width).putInteger(height).putToChannel();
	}

}
