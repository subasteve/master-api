package com.master.geometry;

import com.master.io.socket.Buffer;
import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;

public final class Rectangle{

	// TopLeft--------TopRight
	// |        Area         |
	// BottomLeft--BottomRight
	// X,Y
	// 0,0-----------------2,0
	// |        Area         |
	// 0,2-----------------2,2
	private int topLeftX, topLeftY, topRightX, topRightY, bottomLeftX, bottomLeftY, bottomRightX, bottomRightY, width, height, area;

	public Rectangle(){}
	
	public Rectangle(final int X, final int Y, final int SIZE){
		setArea(X,Y,SIZE);
	}
	
	public Rectangle(final int X, final int Y, final int HEIGHT, final int WIDTH){
		setArea(X,Y,HEIGHT,WIDTH);
	}
	
	public final int getXOrgin(){
		return topLeftX;
	}
	
	public final int getYOrgin(){
		return topLeftY;
	}
	
	public final void get(final Buffer BUFFER){
		width = BUFFER.getInteger();
		height = BUFFER.getInteger();
		if(width != 0 && height != 0){
			area = width*height;
		}else if(width == 0 && height == 0){
			area = 1; //Coord
		}else{
			area = width+height;
		}
		topLeftX = BUFFER.getInteger();
		topLeftY = BUFFER.getInteger();
		topRightX = topLeftX+width;
		topRightY = topLeftY;
		bottomLeftX = topLeftX;
		bottomLeftY = topLeftY+height;
		bottomRightX = topLeftX+width;
		bottomRightY = topLeftY+height;
	}
	
	public final void put(final Buffer BUFFER) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		BUFFER.putInteger(width).putInteger(height).putInteger(topLeftX).putInteger(topLeftY).putToChannel();
	}
	
	public final void move(final int TOP_LEFT_X, final int TOP_LEFT_Y){
		topLeftX = TOP_LEFT_X;
		topLeftY = TOP_LEFT_Y;
		topRightX = TOP_LEFT_X+width;
		topRightY = TOP_LEFT_Y;
		bottomLeftX = TOP_LEFT_X;
		bottomLeftY = TOP_LEFT_Y+height;
		bottomRightX = TOP_LEFT_X+width;
		bottomRightY = TOP_LEFT_Y+height;
	}
	
	public final void setArea(final int TOP_LEFT_X, final int TOP_LEFT_Y, final int SIZE_SQUARED){
		width = SIZE_SQUARED;
		height = SIZE_SQUARED;
		if(width != 0 && height != 0){
			area = width*height;
		}else if(width == 0 && height == 0){
			area = 1; //Coord
		}else{
			area = width+height;
		}
		topLeftX = TOP_LEFT_X;
		topLeftY = TOP_LEFT_Y;
		topRightX = TOP_LEFT_X+SIZE_SQUARED;
		topRightY = TOP_LEFT_Y;
		bottomLeftX = TOP_LEFT_X;
		bottomLeftY = TOP_LEFT_Y+SIZE_SQUARED;
		bottomRightX = TOP_LEFT_X+SIZE_SQUARED;
		bottomRightY = TOP_LEFT_Y+SIZE_SQUARED;
	}	
	
	public final void setArea(final int TOP_LEFT_X, final int TOP_LEFT_Y, final int HEIGHT, final int WIDTH){
		width = WIDTH;
		height = HEIGHT;
		if(width != 0 && height != 0){
			area = width*height;
		}else if(width == 0 && height == 0){
			area = 1; //Coord
		}else{
			area = width+height;
		}
		topLeftX = TOP_LEFT_X;
		topLeftY = TOP_LEFT_Y;
		topRightX = TOP_LEFT_X+WIDTH;
		topRightY = TOP_LEFT_Y;
		bottomLeftX = TOP_LEFT_X;
		bottomLeftY = TOP_LEFT_Y+HEIGHT;
		bottomRightX = TOP_LEFT_X+WIDTH;
		bottomRightY = TOP_LEFT_Y+HEIGHT;
	}
	
	public final int getArea(){
		return area;
	}
	
	public final int getWidth(){
		return width;
	}
	
	public final int getHeight(){
		return height;
	}

	public final void growX(final int SIZE, final boolean MODIFY_ORGIN){
		if(MODIFY_ORGIN){
			topLeftX -= SIZE;
			bottomLeftX -= SIZE;
		}
		topRightX += SIZE;
		bottomRightX += SIZE;
	}
	
	public final void shrinkX(final int SIZE, final boolean MODIFY_ORGIN){
		if(MODIFY_ORGIN){
			topLeftX += SIZE;
			bottomLeftX += SIZE;
		}
		topRightX -= SIZE;
		bottomRightX -= SIZE;
	}
	
	public final void growY(final int SIZE, final boolean MODIFY_ORGIN){
		if(MODIFY_ORGIN){
			topLeftY -= SIZE;
			bottomLeftY -= SIZE;
		}
		topRightY += SIZE;
		bottomRightY += SIZE;
	}
	
	public final void shrinkY(final int SIZE, final boolean MODIFY_ORGIN){
		if(MODIFY_ORGIN){
			topLeftY += SIZE;
			bottomLeftY += SIZE;
		}
		topRightY -= SIZE;
		bottomRightY -= SIZE;
	}
	
	public final void grow(final int SIZE, final boolean MODIFY_ORGIN){
		if(MODIFY_ORGIN){
			topLeftX -= SIZE;
			bottomLeftX -= SIZE;
			topLeftY -= SIZE;
			bottomLeftY -= SIZE;
		}
		topRightX += SIZE;
		bottomRightX += SIZE;
		topRightY += SIZE;
		bottomRightY += SIZE;
	}
	
	public final void grow(final int X, final int Y, final boolean MODIFY_ORGIN){
		if(MODIFY_ORGIN){
			topLeftX -= X;
			bottomLeftX -= X;
			topLeftY -= Y;
			bottomLeftY -= Y;
		}
		topRightX += X;
		bottomRightX += X;
		topRightY += Y;
		bottomRightY += Y;
	}
	
	public final void shrink(final int SIZE, final boolean MODIFY_ORGIN){
		if(MODIFY_ORGIN){
			topLeftX += SIZE;
			bottomLeftX += SIZE;
			topLeftY += SIZE;
			bottomLeftY += SIZE;
		}
		topRightX -= SIZE;
		bottomRightX -= SIZE;
		topRightY -= SIZE;
		bottomRightY -= SIZE;
	}
	
	public final void shrink(final int X, final int Y, final boolean MODIFY_ORGIN){
		if(MODIFY_ORGIN){
			topLeftX += X;
			bottomLeftX += X;
			topLeftY += Y;
			bottomLeftY += Y;
		}
		topRightX -= X;
		bottomRightX -= X;
		topRightY -= Y;
		bottomRightY -= Y;
	}

}