package com.master.graphics;

import java.io.File;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Graphics2D;
import java.awt.Color;
import java.net.URLDecoder;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.awt.FontFormatException;
import java.awt.image.BufferedImage;

/* TODO: REPLACE TOTALY WITH CUSTOM NIO VERSION */
public final class Font{

	private java.awt.Font font = null;
	private int paddingImageWidth = 0, paddingImageHeight = 0, paddingStringWidth = 0, paddingStringHeight = 0;
	
	public Font(final File FILE) throws FontFormatException,IOException{
		font = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, FILE);
	}
	
	public Font(final File FILE, final int SIZE) throws FontFormatException,IOException{
		font = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, FILE).deriveFont((float)SIZE);
	}
	
	public final void setSize(final int SIZE){
		font = font.deriveFont((float)SIZE);
	}
	
	public final void setImagePadding(final int WIDTH, final int HEIGHT){
		paddingImageWidth = WIDTH;
		paddingImageHeight = HEIGHT;
	}
	
	public final void setStringPadding(final int WIDTH, final int HEIGHT){
		paddingStringWidth = WIDTH;
		paddingStringHeight = HEIGHT;
	}
	
	public final BufferedImage draw(final String STRING){
		return draw(STRING,null,Color.BLACK);
	}
	
	public final BufferedImage draw(final String STRING, final Point POINT){
		return draw(STRING,POINT,Color.BLACK);
	}
	
	public final BufferedImage draw(final String STRING, final Point POINT, final Color COLOR){
		final FontMetrics FONT_MATRICES = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).createGraphics().getFontMetrics(font);
		final int WIDTH = FONT_MATRICES.stringWidth(STRING);
		final int HEIGHT = FONT_MATRICES.getAscent();
		final BufferedImage IMAGE = new BufferedImage(WIDTH+paddingImageWidth, HEIGHT+paddingImageHeight, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D GRAPHICS_2D = IMAGE.createGraphics();
		GRAPHICS_2D.setFont(font);
		GRAPHICS_2D.setColor(COLOR);
		GRAPHICS_2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
		if(POINT != null){
			GRAPHICS_2D.drawString(STRING,(float)POINT.getX()+paddingStringWidth,(float)POINT.getY()+paddingStringHeight);
		}else{
			GRAPHICS_2D.drawString(STRING,paddingStringWidth,HEIGHT+paddingStringHeight);
		}
		GRAPHICS_2D.dispose();
		return IMAGE;
	}
	
	public final BufferedImage draw(final String STRING, final Color COLOR, final String ENCODING) throws UnsupportedEncodingException{
		return draw(URLDecoder.decode(STRING, ENCODING),null,COLOR);
	}
	
	public final BufferedImage draw(final String STRING, final Color COLOR, final Point POINT, final String ENCODING) throws UnsupportedEncodingException{
		return draw(URLDecoder.decode(STRING, ENCODING),POINT,COLOR);
	}
	
	public final void draw(final Graphics GRAPHICS, final String STRING, final Point POINT){
		draw(GRAPHICS, STRING, POINT, Color.BLACK);
	}
	
	public final void draw(final Graphics GRAPHICS, final String STRING, final Point POINT, final Color COLOR){
		final FontMetrics FONT_MATRICES = GRAPHICS.getFontMetrics(font);
		final int WIDTH = FONT_MATRICES.stringWidth(STRING);
		final int HEIGHT = FONT_MATRICES.getAscent();
		GRAPHICS.setFont(font);
		GRAPHICS.setColor(COLOR);
		GRAPHICS.drawString(STRING, (int)POINT.getX()+paddingStringWidth,(int)POINT.getY()+paddingStringHeight);
		GRAPHICS.dispose();
	}
	
	public final void draw(final Graphics GRAPHICS, final String STRING, final Point POINT, final Color COLOR, final String ENCODING) throws UnsupportedEncodingException{
		draw(GRAPHICS, URLDecoder.decode(STRING, ENCODING), POINT, COLOR);
	}
	
	public final void draw(final Graphics2D GRAPHICS_2D, final String STRING, final Point POINT){
		draw(GRAPHICS_2D, STRING, POINT, Color.BLACK);
	}
	
	public final void draw(final Graphics2D GRAPHICS_2D, final String STRING, final Point POINT, final Color COLOR){
		final FontMetrics FONT_MATRICES = GRAPHICS_2D.getFontMetrics(font);
		final int WIDTH = FONT_MATRICES.stringWidth(STRING);
		final int HEIGHT = FONT_MATRICES.getAscent();
		GRAPHICS_2D.setFont(font);
		GRAPHICS_2D.setColor(COLOR);
		GRAPHICS_2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
		GRAPHICS_2D.drawString(STRING, (float)POINT.getX()+paddingStringWidth,(float)POINT.getY()+paddingStringHeight);
		GRAPHICS_2D.dispose();
	}
	
	public final void draw(final Graphics2D GRAPHICS_2D, final String STRING, final Point POINT, final Color COLOR, final String ENCODING) throws UnsupportedEncodingException{
		draw(GRAPHICS_2D, URLDecoder.decode(STRING, ENCODING), POINT, COLOR);
	}
}