package com.master.sql;

import java.sql.DriverManager;

public final class MSSql extends SQL{

	//MSSQL VARS
	private final String CONNECTION, USER, PASS;

	public MSSql(final String SERVER, final String DB, final int PORT){
		CONNECTION = new StringBuilder().append("jdbc:sqlserver://").append(SERVER).append(":").append(PORT).append(";databaseName=").append(DB).append(";integratedSecurity=true;").toString();
		USER = null;
		PASS = null;
		try{
			Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver"); 
			reconnect();
		}catch(final Exception E){
			ErrorHandler(E);
		}
	}
	
	public MSSql(final String SERVER, final String DB, final int PORT, final String USER, final String PASS){
		CONNECTION = new StringBuilder().append("jdbc:sqlserver://").append(SERVER).append(":").append(PORT).append(";databaseName=").append(DB).append(";").toString();
		this.USER = USER;
		this.PASS = PASS;
		try{
			Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
			reconnect();
		}catch(final Exception E){
			ErrorHandler(E);
		}
	}
	
	public final void reconnect(){
		try{
			if(USER == null && PASS == null){
				setConnection(DriverManager.getConnection(CONNECTION, USER, PASS));
			}else{
				setConnection(DriverManager.getConnection(CONNECTION));
			}
		} catch (Exception e) {
			ErrorHandler(e);
		}
	}

}
