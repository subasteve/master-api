package com.master.sql;

import com.master.util.error.StringNotFoundException;

import java.sql.DriverManager;
import com.master.util.LoadIni;
import com.master.util.Logger;

public final class MySql extends SQL{

	//MySQL VARS
	private final String MySQL;
	private final String MySQLUser;
	private final String MySQLPass;

	public MySql(final String user, final String pass, final String server, final String database){
		MySQL = "jdbc:mysql://"+server+"/"+database;
		MySQLUser = user;
		MySQLPass = pass;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            reconnect();
        }catch (final Exception E) {
            ErrorHandler(E);
        }
	}

    public MySql(final LoadIni INI, final Logger LOGGER) throws StringNotFoundException {
        setLogger(LOGGER);
        MySQL = "jdbc:mysql://"+INI.loadString("dbserver")+"/"+INI.loadString("database");
        MySQLUser = INI.loadString("dbuser");
        MySQLPass = INI.loadString("dbpass");
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            reconnect();
        }catch (final Exception E) {
            ErrorHandler(E);
        }
    }

	public MySql(final LoadIni INI) throws StringNotFoundException {
        MySQL = "jdbc:mysql://"+INI.loadString("dbserver")+"/"+INI.loadString("database");
        MySQLUser = INI.loadString("dbuser");
        MySQLPass = INI.loadString("dbpass");
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            reconnect();
        }catch (final Exception E) {
            ErrorHandler(E);
        }
    }

	public final void reconnect(){
		try{
			setConnection(DriverManager.getConnection(MySQL, MySQLUser, MySQLPass));
		} catch (Exception e) {
			ErrorHandler(e);
		}
	}

    public final String toString(){
        return new StringBuilder().append("[MySql] Host[").append(MySQL).append("] User[").append(MySQLUser).append("]").toString();
    }

}
