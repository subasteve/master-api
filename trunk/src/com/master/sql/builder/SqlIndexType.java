package com.master.sql.builder;

/**
 * Created by subasteve on 5/17/2014.
 */
//TODO: KEY_BLOCK_SIZE & WITH PARSER
public enum SqlIndexType {
    BTREE("BTREE"),
    HASH("HASH"),
    DEFAULT(null);

    private final String NAME;

    private SqlIndexType(final String NAME){
        this.NAME = NAME;
    }

    public final String toString(){
        return NAME;
    }

    public final String toString(final String COLUMN){
        if(NAME != null){
            return new StringBuilder().append("INDEX USING ").append(NAME).append(" (").append(COLUMN).append(")").toString();
        }else{
            return new StringBuilder().append("INDEX(").append(COLUMN).append(")").toString();
        }
    }

    public final String toString(final String COLUMN, final int WIDTH){
        if(NAME != null) {
            return new StringBuilder().append("INDEX USING ").append(NAME).append(" (").append(COLUMN).append("(").append(WIDTH).append(")").append(")").toString();
        }else{
            return new StringBuilder().append("INDEX(").append(COLUMN).append("(").append(WIDTH).append(")").append(")").toString();
        }
    }
}
