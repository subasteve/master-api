package com.master.sql.builder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by subasteve on 5/17/2014.
 */
public class SqlForeignKey {

    private final Map<String,String> COLUMN_TO_REFERENCED_COLUMN;
    private final String REFERENCED_TABLE;
    private SqlReferenceOption deleteOption, updateOption;

    public SqlForeignKey(final String ID, final String REFERENCED_TABLE, final String REFERENCED_COLUMN){
        COLUMN_TO_REFERENCED_COLUMN = new HashMap<String, String>();
        this.REFERENCED_TABLE = REFERENCED_TABLE;
        COLUMN_TO_REFERENCED_COLUMN.put(ID,REFERENCED_COLUMN);
    }

    public SqlForeignKey(final String REFERENCED_TABLE, final Map<String,String> COLUMN_TO_REFERENCED_COLUMN){
        this.COLUMN_TO_REFERENCED_COLUMN = COLUMN_TO_REFERENCED_COLUMN;
        this.REFERENCED_TABLE = REFERENCED_TABLE;
    }

    public final void setOnDelete(final SqlReferenceOption DELETE_OPTION){
        deleteOption = DELETE_OPTION;
    }

    public final SqlReferenceOption getOnDelete(){
        return deleteOption;
    }

    public final void setOnUpdate(final SqlReferenceOption UPDATE_OPTION){
        updateOption = UPDATE_OPTION;
    }

    public final SqlReferenceOption getOnUpdate(){
        return updateOption;
    }

    public final String toString(){
        final StringBuilder BUFFER = new StringBuilder();
        BUFFER.append("FOREIGN KEY (");
        int tmp = 0;
        for (final String COLUMN : COLUMN_TO_REFERENCED_COLUMN.keySet()){
            BUFFER.append(COLUMN);
            if(++tmp < COLUMN_TO_REFERENCED_COLUMN.size()){
                BUFFER.append(",");
            }
        }
        BUFFER.append(")");
        BUFFER.append(" REFERENCES ").append(REFERENCED_TABLE).append("(");
        tmp = 0;
        for (final String COLUMN : COLUMN_TO_REFERENCED_COLUMN.values()){
            BUFFER.append(COLUMN);
            if(++tmp < COLUMN_TO_REFERENCED_COLUMN.size()){
                BUFFER.append(",");
            }
        }
        BUFFER.append(")");
        if(deleteOption != null){
            BUFFER.append("ON DELETE ").append(deleteOption);
        }
        if(updateOption != null){
            BUFFER.append("ON UPDATE ").append(updateOption);
        }
        return BUFFER.toString();
    }
}
