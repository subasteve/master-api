package com.master.sql.builder;

/**
 * Created by subasteve on 5/17/2014.
 */
public enum SqlReferenceOption {
    RESTRICT("RESTRICT"),
    CASCADE("CASCADE"),
    SET_NULL("SET NULL"),
    NO_ACTION("NO ACTION");

    private final String NAME;

    private SqlReferenceOption(final String NAME){
        this.NAME = NAME;
    }

    public final String toString(){
        return NAME;
    }
}
