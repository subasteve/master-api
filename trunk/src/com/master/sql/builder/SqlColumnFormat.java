package com.master.sql.builder;

/**
 * Created by subasteve on 5/17/2014.
 */
public enum SqlColumnFormat {
    FIXED("FIXED"),
    DYNAMIC("DYNAMIC"),
    DEFAULT("DEFAULT");

    private final String NAME;

    private SqlColumnFormat(final String NAME){
        this.NAME = NAME;
    }

    public final String toString(){
        return NAME;
    }
}
