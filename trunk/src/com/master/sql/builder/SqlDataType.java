package com.master.sql.builder;

/**
 * Created by subasteve on 5/16/2014.
 */
public enum SqlDataType {
    //TODO: CHAR BINARY & VARCHAR BINARY & ENUM & SET
    BIT("BIT",true),
    INT("INT",true,true,true),
    INTEGER("INTEGER",true,true,true),
    TINYINT("TINYINT",true,true,true),
    SMALLINT("SMALLINT",true,true,true),
    MEDIUMINT("MEDIUMINT",true,true,true),
    BIGINT("BIGINT",true,true,true),
    DECIMAL("DECIMAL",true,true,true,true),
    DEC("DEC",true,true,true,true),
    NUMERIC("NUMERIC",true,true,true,true),
    FIXED("FIXED",true,true,true,true),
    FLOAT("FLOAT",true,true,true,true),
    DOUBLE("DOUBLE",true,true,true,true),
    DOUBLE_PRECISION("DOUBLE PRECISION",true,true,true,true),
    REAL("REAL",true,true,true,true),
    DATE("DATE"),
    DATETIME("DATETIME"),
    TIMESTAMP("TIMESTAMP"),
    TIME("TIME"),
    YEAR("YEAR",true),
    VARCHAR("VARCHAR",true),
    CHAR("CHAR",true),
    BINARY("BINARY",true),
    VARBINARY("VARBINARY",true),
    SERIAL("SERIAL"),
    BOOL("BOOL"),
    BOOLEAN("BOOLEAN"),
    TINYBLOB("TINYBLOB"),
    BLOB("BLOB"),
    MEDIUMBLOB("MEDIUMBLOB"),
    LONGBLOB("LONGBLOB"),
    TINYTEXT("TINYTEXT"),
    TEXT("TEXT"),
    MEDIUMTEXT("MEDIUMTEXT"),
    LONGTEXT("LONGTEXT");

    private String NAME;
    private final boolean ALLOW_WIDTH, ALLOW_DECIMAL_WIDTH, ALLOW_UNSIGNED, ALLOW_ZEROFILL;

    private SqlDataType(final String NAME){
        this.NAME = NAME;
        this.ALLOW_WIDTH = false;
        this.ALLOW_DECIMAL_WIDTH = false;
        this.ALLOW_UNSIGNED = false;
        this.ALLOW_ZEROFILL = false;
    }

    private SqlDataType(final String NAME, final boolean ALLOW_WIDTH){
        this.NAME = NAME;
        this.ALLOW_WIDTH = ALLOW_WIDTH;
        this.ALLOW_DECIMAL_WIDTH = false;
        this.ALLOW_UNSIGNED = false;
        this.ALLOW_ZEROFILL = false;
    }

    private SqlDataType(final String NAME, final boolean ALLOW_WIDTH, final boolean ALLOW_UNSIGNED, final boolean ALLOW_ZEROFILL){
        this.NAME = NAME;
        this.ALLOW_WIDTH = ALLOW_WIDTH;
        this.ALLOW_DECIMAL_WIDTH = false;
        this.ALLOW_UNSIGNED = ALLOW_UNSIGNED;
        this.ALLOW_ZEROFILL = ALLOW_ZEROFILL;
    }

    private SqlDataType(final String NAME, final boolean ALLOW_WIDTH, final boolean ALLOW_DECIMAL_WIDTH, final boolean ALLOW_UNSIGNED, final boolean ALLOW_ZEROFILL){
        this.NAME = NAME;
        this.ALLOW_WIDTH = ALLOW_WIDTH;
        this.ALLOW_DECIMAL_WIDTH = ALLOW_DECIMAL_WIDTH;
        this.ALLOW_UNSIGNED = ALLOW_UNSIGNED;
        this.ALLOW_ZEROFILL = ALLOW_ZEROFILL;
    }

    public boolean allowWidth(){
        return ALLOW_WIDTH;
    }

    public boolean allowDecimalWidth(){
        return ALLOW_DECIMAL_WIDTH;
    }

    public boolean allowUnsigned(){
        return ALLOW_UNSIGNED;
    }

    public boolean allowZerofill(){
        return ALLOW_ZEROFILL;
    }

    public final String toString(){
        return NAME;
    }

    public final String toString(final int WIDTH){
        return toString(WIDTH,-1,false,false,false,false,false);
    }

    public final String toString(final int WIDTH, final boolean UNSIGNED){
        return toString(WIDTH,-1,UNSIGNED,false,false,false,false);
    }

    public final String toString(final int WIDTH, final boolean UNSIGNED, final boolean ZEROFILL){
        return toString(WIDTH,-1,UNSIGNED,ZEROFILL,false,false,false);
    }

    public final String toString(final int WIDTH, final boolean UNSIGNED, final boolean ZEROFILL, final boolean NOT_NULL){
        return toString(WIDTH,-1,UNSIGNED,ZEROFILL,NOT_NULL,false,false);
    }

    public final String toString(final int WIDTH, final boolean UNSIGNED, final boolean ZEROFILL, final boolean NOT_NULL, final boolean AUTO_INCREMENT){
        return toString(WIDTH,-1,UNSIGNED,ZEROFILL,NOT_NULL,AUTO_INCREMENT,false);
    }

    public final String toString(final int WIDTH, final boolean UNSIGNED, final boolean ZEROFILL, final boolean NOT_NULL, final boolean AUTO_INCREMENT, final boolean UNIQUE){
        return toString(WIDTH,-1,UNSIGNED,ZEROFILL,NOT_NULL,AUTO_INCREMENT,UNIQUE);
    }

    public final String toString(final int WIDTH, final int DECIMAL){
        return toString(WIDTH,DECIMAL,false,false,false,false,false);
    }

    public final String toString(final int WIDTH, final int DECIMAL, final boolean UNSIGNED){
        return toString(WIDTH,DECIMAL,UNSIGNED,false,false,false,false);
    }

    public final String toString(final int WIDTH, final int DECIMAL, final boolean UNSIGNED, final boolean ZEROFILL){
        return toString(WIDTH,DECIMAL,UNSIGNED,ZEROFILL,false,false,false);
    }

    public final String toString(final int WIDTH, final int DECIMAL, final boolean UNSIGNED, final boolean ZEROFILL, final boolean NOT_NULL){
        return toString(WIDTH,DECIMAL,UNSIGNED,ZEROFILL,NOT_NULL,false,false);
    }

    public final String toString(final int WIDTH, final int DECIMAL, final boolean UNSIGNED, final boolean ZEROFILL, final boolean NOT_NULL, final boolean AUTO_INCREMENT){
        return toString(WIDTH,DECIMAL,UNSIGNED,ZEROFILL,NOT_NULL,AUTO_INCREMENT,false);
    }

    public final String toString(final int WIDTH, final int DECIMAL, final boolean UNSIGNED, final boolean ZEROFILL, final boolean NOT_NULL, final boolean AUTO_INCREMENT, final boolean UNIQUE){
        final StringBuilder BUFFER = new StringBuilder();
        BUFFER.append(NAME);
        if(WIDTH != -1 && DECIMAL != -1) {
            BUFFER.append("(").append(WIDTH).append(",").append(DECIMAL).append(")");
        }else if(WIDTH != -1) {
            BUFFER.append("(").append(WIDTH).append(")");
        }else if(DECIMAL != -1){
            BUFFER.append("(").append(DECIMAL).append(")");
        }
        if(UNSIGNED){
            BUFFER.append(" UNSIGNED");
        }
        if(ZEROFILL){
            BUFFER.append(" ZEROFILL");
        }
        if(NOT_NULL){
            BUFFER.append(" NOT NULL");
        }
        if(AUTO_INCREMENT){
            BUFFER.append(" AUTO_INCREMENT");
        }
        if(UNIQUE){
            BUFFER.append(" UNIQUE");
        }
        return BUFFER.toString();
    }
}
