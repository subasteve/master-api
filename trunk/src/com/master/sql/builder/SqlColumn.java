package com.master.sql.builder;

/**
 * Created by subasteve on 5/17/2014.
 */
public class SqlColumn {

    private SqlColumnFormat format = null;
    private SqlStorage storage = null;
    private SqlIndexType index = null;
    private final SqlDataType DATA_TYPE;
    private String comment = null, defaultValue = null;
    private final String NAME;
    private final int WIDTH, DECIMAL_WIDTH;
    private final boolean PRIMARY_KEY, UNIQUE, NOT_NULL, AUTO_INCREMENT, UNSIGNED, ZEROFILL;

    public final String getName(){
        return NAME;
    }

    public final SqlColumn setIndex(final SqlIndexType INDEX){
        index = INDEX;
        return this;
    }

    public final SqlIndexType getIndex(){
        return index;
    }

    public final SqlColumn setDefaultValue(final String VALUE){
        defaultValue = VALUE;
        return this;
    }

    public final String getDefaultValue(){
        return defaultValue;
    }

    public final SqlColumn setComment(final String COMMENT){
        comment = COMMENT;
        return this;
    }

    public final String getComment(){
        return comment;
    }

    public final SqlColumn setFormat(final SqlColumnFormat FORMAT){
        format = FORMAT;
        return this;
    }

    public final SqlColumn setStorage(final SqlStorage STORAGE){
        storage = STORAGE;
        return this;
    }

    public final SqlColumnFormat getFormat(){
        return format;
    }

    public final SqlStorage getStorage(){
        return storage;
    }

    public final int getWidth(){
        return WIDTH;
    }

    public final int getDecimalWidth(){
        return DECIMAL_WIDTH;
    }

    public final boolean hasPrimaryKey(){
        return PRIMARY_KEY;
    }

    public final boolean hasIndex(){
        return index != null;
    }

    public final boolean hasUnique(){
        return UNIQUE;
    }

    public final boolean isNotNull(){
        return NOT_NULL;
    }

    public final boolean isAutoIncrement(){
        return AUTO_INCREMENT;
    }

    public final boolean isUnsigned(){
        return UNSIGNED;
    }

    public final boolean isZerofill(){
        return ZEROFILL;
    }

    public SqlColumn(final String NAME, final SqlDataType DATA_TYPE){
        this.NAME = NAME;
        this.WIDTH = -1;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = false;
        this.UNIQUE = false;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY){
        this.NAME = NAME;
        this.WIDTH = -1;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = false;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE){
        this.NAME = NAME;
        this.WIDTH = -1;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL){
        this.NAME = NAME;
        this.WIDTH = -1;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT){
        this.NAME = NAME;
        this.WIDTH = -1;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT, final boolean UNSIGNED) throws Exception{
        if(!DATA_TYPE.allowUnsigned() && UNSIGNED){
            throw new Exception("UNSIGNED Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = -1;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = UNSIGNED;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT, final boolean UNSIGNED, final boolean ZEROFILL) throws Exception{
        if(!DATA_TYPE.allowUnsigned() && UNSIGNED){
            throw new Exception("UNSIGNED Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowZerofill() && ZEROFILL){
            throw new Exception("ZEROFILL Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = -1;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = UNSIGNED;
        this.ZEROFILL = ZEROFILL;
    }

    public SqlColumn(final String NAME, final int WIDTH, final SqlDataType DATA_TYPE) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = false;
        this.UNIQUE = false;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = false;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT, final boolean UNSIGNED) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowUnsigned() && UNSIGNED){
            throw new Exception("UNSIGNED Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = UNSIGNED;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT, final boolean UNSIGNED, final boolean ZEROFILL) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowUnsigned() && UNSIGNED){
            throw new Exception("UNSIGNED Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowZerofill() && ZEROFILL){
            throw new Exception("ZEROFILL Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = -1;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = UNSIGNED;
        this.ZEROFILL = ZEROFILL;
    }

    public SqlColumn(final String NAME, final int WIDTH, final int DECIMAL_WIDTH, final SqlDataType DATA_TYPE) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowDecimalWidth()){
            throw new Exception("Decimal Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = DECIMAL_WIDTH;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = false;
        this.UNIQUE = false;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final int DECIMAL_WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowDecimalWidth()){
            throw new Exception("Decimal Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = DECIMAL_WIDTH;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = false;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final int DECIMAL_WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowDecimalWidth()){
            throw new Exception("Decimal Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = DECIMAL_WIDTH;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = false;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final int DECIMAL_WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowDecimalWidth()){
            throw new Exception("Decimal Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = DECIMAL_WIDTH;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = false;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final int DECIMAL_WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowDecimalWidth()){
            throw new Exception("Decimal Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = DECIMAL_WIDTH;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = false;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final int DECIMAL_WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT, final boolean UNSIGNED) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowDecimalWidth()){
            throw new Exception("Decimal Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowUnsigned() && UNSIGNED){
            throw new Exception("UNSIGNED Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = DECIMAL_WIDTH;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = UNSIGNED;
        this.ZEROFILL = false;
    }

    public SqlColumn(final String NAME, final int WIDTH, final int DECIMAL_WIDTH, final SqlDataType DATA_TYPE, final boolean PRIMARY_KEY, final boolean UNIQUE, final boolean NOT_NULL, final boolean AUTO_INCREMENT, final boolean UNSIGNED, final boolean ZEROFILL) throws Exception{
        if(!DATA_TYPE.allowWidth()){
            throw new Exception("Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowDecimalWidth()){
            throw new Exception("Decimal Width Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowUnsigned() && UNSIGNED){
            throw new Exception("UNSIGNED Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        if(!DATA_TYPE.allowZerofill() && ZEROFILL){
            throw new Exception("ZEROFILL Not allowed for SqlDataType "+DATA_TYPE.toString());
        }
        this.NAME = NAME;
        this.WIDTH = WIDTH;
        this.DECIMAL_WIDTH = DECIMAL_WIDTH;
        this.DATA_TYPE = DATA_TYPE;
        this.PRIMARY_KEY = PRIMARY_KEY;
        this.UNIQUE = UNIQUE;
        this.NOT_NULL = NOT_NULL;
        this.AUTO_INCREMENT = AUTO_INCREMENT;
        this.UNSIGNED = UNSIGNED;
        this.ZEROFILL = ZEROFILL;
    }

    public final String toString(){
        StringBuilder BUFFER = new StringBuilder();
        BUFFER.append(NAME).append(" ").append(DATA_TYPE.toString(WIDTH,DECIMAL_WIDTH,UNSIGNED,ZEROFILL,NOT_NULL,AUTO_INCREMENT,UNIQUE));
        if(comment != null){
            BUFFER.append(" COMMENT '").append(comment).append("'");
        }
        if(format != null){
            BUFFER.append(" COLUMN_FORMAT ").append(format.toString());
        }
        if(storage != null){
            BUFFER.append(" STORAGE  ").append(storage.toString());
        }
        if(defaultValue != null){
            BUFFER.append(" DEFAULT ").append(defaultValue);
        }
        return BUFFER.toString();
    }
}
