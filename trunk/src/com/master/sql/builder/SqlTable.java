package com.master.sql.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by subasteve on 5/16/2014.
 */
public class SqlTable {

    private final String NAME;
    private final boolean TEMPORAY, CREATE_IF_NOT_EXISTS;
    private SqlColumn[] columns = null;
    private final Map<String,List<String>> MULTI_COLUMN_INDEX = new HashMap<String, List<String>>();
    private final List<SqlForeignKey> FOREIGN_KEYS = new ArrayList<SqlForeignKey>();

    public SqlTable(final String NAME){
        this.NAME = NAME;
        TEMPORAY = false;
        CREATE_IF_NOT_EXISTS = false;
    }

    public final String dropTable(){
        return "DROP TABLE "+NAME;
    }

    public final String truncateTable(){
        return "TRUNCATE TABLE "+NAME;
    }

    public final static String dropTable(final String NAME){
        return "DROP TABLE "+NAME;
    }

    public final static String truncateTable(final String NAME){
        return "TRUNCATE TABLE "+NAME;
    }

    public final List<SqlForeignKey> getForeignKeys(){
        return FOREIGN_KEYS;
    }

    public final Map<String,List<String>> getMultiIndexs(){
        return MULTI_COLUMN_INDEX;
    }

    public final void addMultiIndex(final String NAME, final List<String> COLUMNS){
        MULTI_COLUMN_INDEX.put(NAME,COLUMNS);
    }

    public final SqlColumn[] getColumns(){
        return columns;
    }

    public final SqlTable setColumns(final SqlColumn[] COLUMNS){
        columns = COLUMNS;
        return this;
    }

    public SqlTable(final String NAME, final boolean CREATE_IF_NOT_EXISTS){
        this.NAME = NAME;
        TEMPORAY = false;
        this.CREATE_IF_NOT_EXISTS = CREATE_IF_NOT_EXISTS;
    }

    public SqlTable(final String NAME, final boolean CREATE_IF_NOT_EXISTS, final boolean TEMPORAY){
        this.NAME = NAME;
        this.TEMPORAY = TEMPORAY;
        this.CREATE_IF_NOT_EXISTS = CREATE_IF_NOT_EXISTS;
    }

    public final String toString(){
        StringBuilder BUFFER = new StringBuilder();
        BUFFER.append("CREATE");
        if(TEMPORAY){
            BUFFER.append(" TEMPORARY");
        }
        BUFFER.append(" TABLE");
        if(CREATE_IF_NOT_EXISTS){
            BUFFER.append(" IF NOT EXISTS");
        }
        BUFFER.append(" ").append(NAME).append("(");
        int tmp = 0, indexes = 0, primaryKeys = 0, unique = 0;
        for (final SqlColumn COLUMN : columns){
            BUFFER.append(COLUMN.toString());
            if(++tmp < columns.length){
                BUFFER.append(",");
            }
            if(COLUMN.hasIndex()){
                indexes++;
            }
            if(COLUMN.hasPrimaryKey()){
                primaryKeys++;
            }
            if(COLUMN.hasUnique()){
                unique++;
            }
        }
        tmp = 0;
        if(indexes > 0){
            BUFFER.append(",");
            for (final SqlColumn COLUMN : columns){
                if(COLUMN.hasIndex()){
                    BUFFER.append(COLUMN.getIndex().toString(COLUMN.getName()));
                    if(++tmp < indexes){
                        BUFFER.append(",");
                    }
                }
            }
        }
        tmp = 0;
        if(primaryKeys > 0){
            BUFFER.append(",PRIMARY KEY (");
            for (final SqlColumn COLUMN : columns){
                if(COLUMN.hasPrimaryKey()){
                    BUFFER.append(COLUMN.getName());
                    if(++tmp < primaryKeys){
                        BUFFER.append(",");
                    }
                }
            }
            BUFFER.append(")");
        }
        tmp = 0;
        int tmp2 = 0;
        if(MULTI_COLUMN_INDEX.size() > 0){
            BUFFER.append(",");
            for(final String KEY : MULTI_COLUMN_INDEX.keySet()){
                BUFFER.append("UNIQUE KEY ").append(KEY).append(" (");
                final List<String> COLUMNS = MULTI_COLUMN_INDEX.get(KEY);
                for (final String COLUMN : COLUMNS){
                    BUFFER.append(COLUMN);
                    if(++tmp2 < COLUMNS.size()){
                        BUFFER.append(",");
                    }
                }
                BUFFER.append(")");
                if(++tmp < MULTI_COLUMN_INDEX.size()){
                    BUFFER.append(",");
                }
            }
        }
        tmp = 0;
        if(FOREIGN_KEYS.size() > 0){
            BUFFER.append(",");
            for (final SqlForeignKey KEY : FOREIGN_KEYS){
                BUFFER.append(KEY.toString());
                if(++tmp < FOREIGN_KEYS.size()){
                    BUFFER.append(",");
                }
            }
        }
        return BUFFER.append(");").toString();
    }
}
