package com.master.sql.builder;

/**
 * Created by subasteve on 5/17/2014.
 */
public enum SqlStorage {
    DISK("DISK"),
    MEMORY("MEMORY"),
    DEFAULT("DEFAULT");

    private final String NAME;

    private SqlStorage(final String NAME){
        this.NAME = NAME;
    }

    public final String toString(){
        return NAME;
    }
}
