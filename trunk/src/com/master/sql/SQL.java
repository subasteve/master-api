package com.master.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;

import com.master.cache.LocalCache;
import com.master.data.interfaces.SQLToCache;
import com.master.data.interfaces.SQLToLocalCache;
import com.master.net.CacheServer;
import com.master.util.Logger;
import com.master.util.interfaces.LoggerHook;

public abstract class SQL{

	private Connection myConnection = null;
	private Logger logger = null;
	
	public SQL(){}
	
	public abstract void reconnect();
	
	public final void setLogger(final Logger LOGGER){
		logger = LOGGER;
	}

    public final Logger getLogger(){
        return logger;
    }
	
	public final void setConnection(final Connection CONN){
		myConnection = CONN;
	}
	
	public final Connection getConnection(){
		return myConnection;
	}
	
	public final void closeConnection(){
		try{
			if(myConnection != null){
				myConnection.close();
			}
		} catch (Exception e) { ErrorHandler(e); }
	}

	public final void closeQuery(final Statement STATEMENT, final ResultSet RS){
		if(STATEMENT != null){
			try{
				STATEMENT.close();
			} catch (final Exception E) {
				ErrorHandler(E);
			}
		}
		if(RS != null){
			try{
				RS.close();
			} catch (final Exception E) {
				ErrorHandler(E);
			}
		}
	}

    public final void execute(final String query) {
		Statement statement = null;
        try{
			statement = myConnection.createStatement();
			statement.execute(query);
        } catch (Exception e) {
			ErrorHandler(e);
		} finally {
			closeQuery(statement, null);
		}
	}
	
	public final void executeUpdate(final String query) {
		Statement statement = null;
		try{
			statement = myConnection.createStatement();
			statement.executeUpdate(query);
		} catch (Exception e) {
			ErrorHandler(e);
		} finally {
			closeQuery(statement, null);
		}
	}
	
	public final boolean queryResults(final String query){
		Statement statement = null;
		ResultSet rs = null;
		try{
			statement = myConnection.createStatement();
			rs = statement.executeQuery(query);
			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			ErrorHandler(e);
		} finally {
			closeQuery(statement, rs);
		}
		return false;
	}

	public void executeQuery(final String QUERY){
		Statement statement = null;
		try{
			statement = getConnection().createStatement();
			statement.executeQuery(QUERY);
		} catch (Exception e) {
			ErrorHandler(e);
		} finally {
			closeQuery(statement,null);
		}
	}

	public void executeQuery(final String QUERY, final com.master.data.interfaces.SQL DB){
		Statement statement = null;
		ResultSet rs = null;
		try{
			statement = getConnection().createStatement();
			rs = statement.executeQuery(QUERY);
			DB.fromSQL(rs);
		} catch (Exception e) {
			ErrorHandler(e);
		} finally {
			closeQuery(statement,rs);
		}
	}

	public void executeStatement(final PreparedStatement STATEMENT, final com.master.data.interfaces.SQL DB){
		ResultSet rs = null;
		try{
			rs = STATEMENT.executeQuery();
			DB.fromSQL(rs);
		} catch (Exception e) {
			ErrorHandler(e);
		} finally {
			closeQuery(STATEMENT,rs);
		}
	}

	public void executeQueryToCache(final String QUERY,final SQLToCache INTERFACE, final CacheServer CACHE){
		Statement statement = null;
		ResultSet rs = null;
		try{
			statement = getConnection().createStatement();
			rs = statement.executeQuery(QUERY);
			INTERFACE.fromSQLToCache(rs, CACHE);
		} catch (final Exception E) {
			ErrorHandler(E);
		}finally{
			closeQuery(statement,rs);
		}
	}

	public void executeStatementToCache(final PreparedStatement STATEMENT, final SQLToCache INTERFACE, final CacheServer CACHE){
		ResultSet rs = null;
		try{
			rs = STATEMENT.executeQuery();
			INTERFACE.fromSQLToCache(rs, CACHE);
		} catch (final Exception E) {
			ErrorHandler(E);
		}finally{
			closeQuery(STATEMENT,rs);
		}
	}

	public void executeQueryToLocalCache(final String QUERY,final SQLToLocalCache INTERFACE, final LocalCache CACHE){
		Statement statement = null;
		ResultSet rs = null;
		try{
			statement = getConnection().createStatement();
			rs = statement.executeQuery(QUERY);
			INTERFACE.fromSQLToCache(rs, CACHE);
		} catch (final Exception E) {
			ErrorHandler(E);
		}finally{
			closeQuery(statement,rs);
		}
	}

	public void executeStatementToLocalCache(final PreparedStatement STATEMENT, final SQLToLocalCache INTERFACE, final LocalCache CACHE){
		ResultSet rs = null;
		try{
			rs = STATEMENT.executeQuery();
			INTERFACE.fromSQLToCache(rs, CACHE);
		} catch (final Exception E) {
			ErrorHandler(E);
		}finally{
			closeQuery(STATEMENT,rs);
		}
	}

	public final void ErrorHandler(final Exception e){
		try{
			if (myConnection != null){
                if (myConnection.isClosed()){
                    if(logger != null){
                        logger.writeLine("Database ErrorHandler: myConnection is not Connected - Reconnecting", LoggerHook.LogLevel.Warn);
                    }
                    reconnect();
                    return;
                }
			}
            if(logger != null){
                logger.writeError(e);
            }
		}catch (Exception ex) {
			if(logger != null){
				logger.writeError(ex);
			}
		}
	}

}