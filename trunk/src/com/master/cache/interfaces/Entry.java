package com.master.cache.interfaces;

import com.master.cache.CacheEntry;

import java.util.Collection;
import java.util.Map;

public interface Entry<K, V> {

    void put(K key, V value, int expires);
    CacheEntry<V> get(K key);
    Map<K, V> getAll(Collection<K> collection);
    void clear();
    boolean remove(K key);
    V removeAndGet(K key);
    int size();

}
