package com.master.cache;

public class CacheEntry<V> {
    private final long EXPIRES;
    private final V VALUE_1;

    public CacheEntry(final long EXPIRES, final V VALUE_1) {
        super();
        this.EXPIRES = EXPIRES;
        this.VALUE_1 = VALUE_1;
    }

    public final long getEXPIRES() {
        return EXPIRES;
    }

    public final V getEntry() {
        return VALUE_1;
    }

}