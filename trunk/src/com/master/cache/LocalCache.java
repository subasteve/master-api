package com.master.cache;

import com.master.cache.interfaces.Entry;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class LocalCache<K, V> implements Entry<K, V> {

    private final Queue<K> QUEUE = new ConcurrentLinkedQueue<K>();
    private final Map<K, CacheEntry<V>> CACHE;
    private final int SIZE;

    private final AtomicInteger CACHE_SIZE = new AtomicInteger();

    public LocalCache(final int SIZE) {
        this.SIZE = SIZE;
        CACHE = new ConcurrentHashMap<K, CacheEntry<V>>(SIZE);
    }

    @Override
    public final CacheEntry<V> get(final K KEY_1) {
        if (KEY_1 != null) {
            final CacheEntry<V> ENTRY = CACHE.get(KEY_1);

            if (ENTRY != null) {
                long timestamp = ENTRY.getEXPIRES();
                if (timestamp != -1 && System.currentTimeMillis() > timestamp) {
                    remove(KEY_1);
                    return null;
                }
                return ENTRY;
            }
        }
        return null;
    }

    @Override
    public final V removeAndGet(final K KEY_1) {
        if (KEY_1 != null) {
            final CacheEntry<V> ENTRY = CACHE.get(KEY_1);

            if (ENTRY != null) {
                CACHE_SIZE.decrementAndGet();
                return CACHE.remove(KEY_1).getEntry();
            }
        }
        return null;
    }

    @Override
    public final void put(final K KEY_1, final V VALUE_1, final int EXPIRES_IN) {
        if (KEY_1 != null && VALUE_1 != null) {
            final long EXPIRES = EXPIRES_IN != -1 ? System.currentTimeMillis()
                    + (EXPIRES_IN * 1000) : EXPIRES_IN;
            if (!CACHE.containsKey(KEY_1)) {
                CACHE_SIZE.incrementAndGet();
                while (CACHE_SIZE.get() > SIZE) {
                    remove(QUEUE.poll());
                }
            }
            CACHE.put(KEY_1, new CacheEntry<V>(EXPIRES, VALUE_1));
            QUEUE.add(KEY_1);
        }
    }

    @Override
    public final boolean remove(final K KEY_1) {
        return removeAndGet(KEY_1) != null;
    }

    @Override
    public final int size() {
        return CACHE_SIZE.get();
    }

    @Override
    public final Map<K, V> getAll(final Collection<K> COLLECTION) {
        final Map<K, V> TMP = new HashMap<K, V>();
        for (final K KEY_1 : COLLECTION) {
            TMP.put(KEY_1, get(KEY_1).getEntry());
        }
        return TMP;
    }

    @Override
    public final void clear() {
        CACHE.clear();
        CACHE_SIZE.set(0);
    }

}
