package com.master.html;

import com.master.util.error.StringNotFoundException;
import com.master.util.SearchableString;
import com.master.util.StringElement;

public final class A{

	private final String HREF, NAME, CLASS, ID, STYLE, TITLE, TEXT;

	public A(final SearchableString STRING_SEARCH) throws StringNotFoundException{
		final StringElement IMG_ELEMENT = STRING_SEARCH.findIndex("<a",true);
		if(IMG_ELEMENT == null){
			throw new StringNotFoundException();
		}
		String tmp = "";
		try{
			tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("href=\"").getString("\"");
		}catch(final Exception e){}
		HREF = tmp;
		tmp = "";
		try{
			tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("name=\"").getString("\"");
		}catch(final Exception e){}
		NAME = tmp;
		tmp = "";
		try{
			tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("class=\"").getString("\"");
		}catch(final Exception e){}
		CLASS = tmp;
		tmp = "";
		try{
			tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("id=\"").getString("\"");
		}catch(final Exception e){}
		ID = tmp;
		tmp = "";
		try{
			tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("style=\"").getString("\"");
		}catch(final Exception e){}
		STYLE = tmp;
		tmp = "";
		try{
			tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("title=\"").getString("\"");
		}catch(final Exception e){}
		TITLE = tmp;
		tmp = "";
		try{
			tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position(">").getString("</a>");
		}catch(final Exception e){}
		TEXT = tmp;
		tmp = "";
	}
	
	public final String getHref(){
		return HREF;
	}
	
	public final String getText(){
		return TEXT;
	}
	
	public final String getName(){
		return NAME;
	}
	
	public final String getClasses(){
		return CLASS;
	}
	
	public final String getID(){
		return ID;
	}
	
	public final String getStyle(){
		return STYLE;
	}
	
	
	public final String toString(){
		return TEXT+" "+NAME+" "+TITLE+" "+HREF+" "+CLASS+" "+ID+" "+STYLE;
	}

}
