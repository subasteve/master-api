package com.master.net;

import com.master.net.socket.ClientSocket;

public final class Server{

	private final String server;
	private final int port;
	
	public Server (final String server,final int port){
		this.server = server;
		this.port = port;
	}
	
	public final String getServer(){
		return server;
	}
	
	public final int getPort(){
		return port;
	}
	
	public final ClientSocket createConnection(){
		try{
			return new ClientSocket(server,port);
		}catch(Exception e){ return null; }
	}
	
	public final ClientSocket createConnection(final boolean TIMEOUT){
		try{
			return new ClientSocket(TIMEOUT,server,port);
		}catch(Exception e){ return null; }
	}
	
}