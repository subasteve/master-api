package com.master.net;

import com.master.io.socket.Buffer;
import com.master.util.SearchableString;
import java.awt.image.BufferedImage;
import java.io.IOException;

public final class Link{

	private final String LINK_URL;
	private final String LINK_TITLE;
	private boolean valid = true;
	private int[] expires = null;
	private long expiresOffset = -1;
	
	public Link(final Buffer BUFFER, final long FILE_POSITION){
		
		byte firstByte = (byte)BUFFER.get();
		LINK_URL = BUFFER.getString();
		boolean INCLUDE_TITLE = (firstByte & (1 << 1)) != 0;
		valid = (firstByte & (1 << 2)) != 0;
		boolean INCLUDE_EXPIRES = (firstByte & (1 << 3)) != 0;
		if(INCLUDE_TITLE){
			LINK_TITLE = BUFFER.getString();
		}else{
			LINK_TITLE = null;
		}
		if(INCLUDE_EXPIRES){
			expiresOffset = FILE_POSITION+BUFFER.position();
			final int[] DATE = new int[6];
			DATE[0] = BUFFER.get(); //Month
			DATE[1] = BUFFER.get(); //Day
			DATE[2] = BUFFER.getInteger(); //Year
			DATE[3] = BUFFER.get(); //Hour
			DATE[4] = BUFFER.get(); //Minute
			DATE[5] = BUFFER.get(); //Second
			expires = DATE;
		}else{
			expires = null;
		}
	}
	
	public Link(final String LINK_URL){
		this.LINK_URL = LINK_URL;
		this.LINK_TITLE = null;
	}
	
	public Link(final String LINK_URL, final boolean VALID){
		this.LINK_URL = LINK_URL;
		this.LINK_TITLE = null;	
		valid = VALID;
	}
	
	public Link(final String LINK_TITLE, final String LINK_URL){
		this.LINK_URL = LINK_URL;
		this.LINK_TITLE = LINK_TITLE;	
	}
	
	public Link(final String LINK_TITLE, final String LINK_URL, final boolean VALID){
		this.LINK_URL = LINK_URL;
		this.LINK_TITLE = LINK_TITLE;	
		valid = VALID;
	}
	
	public final void setExpires(final int[] DATE){
		expires = DATE;
	}
	
	public final long getExpiresOffset(){
		return expiresOffset;
	}
	
	public final void setExpires(final int[] DATE, final Buffer BUFFER){
		expires = DATE;
		if(expiresOffset != -1){
			if(expires != null){
				BUFFER.put((byte)expires[0]).put((byte)expires[1]).putInteger(expires[2]).put((byte)expires[3]).put((byte)expires[4]).put((byte)expires[5]);
			}
		}
	}
	
	public final int[] getExpires(){
		return expires;
	}
	
	public final void setVaild(final boolean VALID){
		valid = VALID;	
	}
	
	public final boolean getVaild(){
		return valid;
	}
	
	public final String getURL(){
		return LINK_URL;
	}
	
	public final String getTitle(){
		return LINK_TITLE;
	}
	
	public final SearchableString getPage(final WebPage WEBPAGE) throws IOException{
		return WEBPAGE.get(LINK_URL);
	}
	
	public final BufferedImage getImage(final WebPage WEBPAGE) throws IOException{
		WEBPAGE.get(LINK_URL);
		return WEBPAGE.getImage();
	}

	public final void writeTo(final Buffer BUFFER, final boolean INCLUDE_TITLE, final boolean INCLUDE_EXPIRES, final long FILE_POSITION){
		if(LINK_URL != null){
			byte firstByte = 0;
			if(LINK_TITLE != null && INCLUDE_TITLE){
				firstByte |= 1 << 1;
			}
			if(valid){
				firstByte |= 1 << 2;
			}
			if(INCLUDE_EXPIRES){
				firstByte |= 1 << 3;
			}
			BUFFER.put(firstByte).putString(LINK_URL);
			if(LINK_TITLE != null && INCLUDE_TITLE){
				BUFFER.putString(LINK_TITLE);
			}
			if(INCLUDE_EXPIRES){
				expiresOffset = FILE_POSITION+BUFFER.position();
				if(expires != null){
					BUFFER.put((byte)expires[0]).put((byte)expires[1]).putInteger(expires[2]).put((byte)expires[3]).put((byte)expires[4]).put((byte)expires[5]);
				}else{
					BUFFER.put((byte)0).put((byte)0).putInteger(0).put((byte)0).put((byte)0).put((byte)0);
				}
			}
		}
	}

}
