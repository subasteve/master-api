package com.master.net;

import com.master.util.error.StringNotFoundException;
import com.master.util.CreateString;
import com.master.util.ProcessTime;
import com.master.util.Logger;
import com.master.util.SearchableString;
import com.master.util.StringElement;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.ByteBuffer;
import java.io.IOException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import java.security.NoSuchAlgorithmException;
import java.security.KeyManagementException;
import java.nio.BufferOverflowException;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;

import java.util.zip.GZIPInputStream;

import java.util.Map;
import java.util.HashMap;

public final class WebPage{


    public class WebRequest{
        private final WebMethod METHOD;
        private final int PORT;
        private final Host HOST;
        private final String PAGE, AUTH, DATA, DATA_CONTENT_TYPE;

        public WebRequest(final WebMethod METHOD, final String PAGE, final Host HOST){
            this.METHOD = METHOD;
            this.PAGE = PAGE;
            this.HOST = HOST;
            PORT = 80;
            AUTH = null;
            DATA = null;
            DATA_CONTENT_TYPE = null;
        }

        public WebRequest(final WebMethod METHOD, final String PAGE, final Host HOST, final String DATA, final String DATA_CONTENT_TYPE){
            this.METHOD = METHOD;
            this.PAGE = PAGE;
            this.HOST = HOST;
            PORT = 80;
            this.AUTH = null;
            this.DATA = DATA;
            this.DATA_CONTENT_TYPE = DATA_CONTENT_TYPE;
        }

        public WebRequest(final WebMethod METHOD, final String PAGE, final Host HOST, final int PORT, final String DATA, final String DATA_CONTENT_TYPE){
            this.METHOD = METHOD;
            this.PAGE = PAGE;
            this.HOST = HOST;
            this.PORT = PORT;
            this.AUTH = null;
            this.DATA = DATA;
            this.DATA_CONTENT_TYPE = DATA_CONTENT_TYPE;
        }

        public WebRequest(final WebMethod METHOD, final String PAGE, final Host HOST, final String AUTH, final String DATA, final String DATA_CONTENT_TYPE){
            this.METHOD = METHOD;
            this.PAGE = PAGE;
            this.HOST = HOST;
            PORT = 80;
            this.AUTH = AUTH;
            this.DATA = DATA;
            this.DATA_CONTENT_TYPE = DATA_CONTENT_TYPE;
        }

        public WebRequest(final WebMethod METHOD, final String PAGE, final Host HOST, final int PORT, final String AUTH, final String DATA, final String DATA_CONTENT_TYPE){
            this.METHOD = METHOD;
            this.PAGE = PAGE;
            this.HOST = HOST;
            this.PORT = PORT;
            this.AUTH = AUTH;
            this.DATA = DATA;
            this.DATA_CONTENT_TYPE = DATA_CONTENT_TYPE;
        }

        public Host getHost(){
            return HOST;
        }

        public String getPage(){
            return PAGE;
        }

        public int getPort(){
            return PORT;
        }

        public String getAuth(){
            return AUTH;
        }

        public String getData(){
            return DATA;
        }

        public String getDataContentType(){
            return DATA_CONTENT_TYPE;
        }

        public WebMethod getMethod(){
            return METHOD;
        }

        public CreateString toCreateString(){
            final CreateString REQUEST = new CreateString(METHOD.toString()," ",((!relative) ? "http://"+HOST.getHostName()+PAGE : PAGE)," HTTP/1.1\r\nHost: ",HOST.getHostName(),"\r\nAccept: */*\r\nConnection: close\r\nUser-Agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11\r\nAccept-Charset: UTF-8;q=0.7\r\nCache-Control: no-cache\r\n");
            REQUEST.put("Cookie: ");
            REQUEST.put(createCookie());
            REQUEST.put("\r\n");
            if(AUTH != null && !AUTH.equals("")){
                REQUEST.put("Authorization: Basic ",AUTH,"\r\n");
            }
            if(METHOD.canHaveData() && DATA != null && DATA_CONTENT_TYPE != null){
                REQUEST.put("Content-Type: "+DATA_CONTENT_TYPE+"\r\n");
                REQUEST.put("Content-Length: "+DATA.length()+"\r\n");
            }
            REQUEST.put("\r\n");
            if(METHOD.canHaveData() && DATA != null && DATA_CONTENT_TYPE != null){
                REQUEST.put(DATA);
            }
            return REQUEST;
        }
    }

    public enum WebMethod{
        GET("GET",false),
        POST("POST",true),
        PATCH("PATCH",true),
        PUT("PUT",true),
        DELETE("DELETE",true),
        TRACE("TRACE",false),
        OPTIONS("OPTIONS",false),
        HEAD("HEAD",false);

        private final String NAME;
        private final boolean CAN_HAVE_DATA;

        private WebMethod(final String NAME, final boolean CAN_HAVE_DATA){
            this.NAME = NAME;
            this.CAN_HAVE_DATA = CAN_HAVE_DATA;
        }

        public static WebMethod guessType(final String INPUT){
            if(INPUT.equalsIgnoreCase("get")){
                return WebMethod.GET;
            }else if(INPUT.equalsIgnoreCase("post")){
                return WebMethod.POST;
            }else if(INPUT.equalsIgnoreCase("patch")){
                return WebMethod.PATCH;
            }else if(INPUT.equalsIgnoreCase("put")){
                return WebMethod.PUT;
            }else if(INPUT.equalsIgnoreCase("delete")){
                return WebMethod.DELETE;
            }else if(INPUT.equalsIgnoreCase("trace")){
                return WebMethod.TRACE;
            }else if(INPUT.equalsIgnoreCase("options")){
                return WebMethod.OPTIONS;
            }else if(INPUT.equalsIgnoreCase("head")){
                return WebMethod.HEAD;
            }else{
                return null;
            }
        }

        public boolean canHaveData(){
            return CAN_HAVE_DATA;
        }

        public String toString(){
            return NAME;
        }
    }

	/* valid HTTP methods */
	private final Map<String,String> COOKIES = new HashMap<String,String>();
	//private final String[] METHODS = { "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE"};
	final ByteBuffer BUFFER = ByteBuffer.allocateDirect(1024*1024*10); //10MB
	private final Logger LOGGER;
	private final int HTTP_PORT = 80, HTTPS_PORT = 443;
	private final SearchableString STRING_SEARCH = new SearchableString();
	private boolean debug = false;
	private final ProcessTime TIMER;
	private HostCache dnsCache = new HostCache();
	private int lastIndex = 0;
	private boolean relative = false;

	public WebPage(){
		LOGGER = null;
		TIMER = new ProcessTime("Get WebPage");
	}

	public WebPage(final boolean DEBUG){
		debug = DEBUG;
		LOGGER = null;
		TIMER = new ProcessTime("Get WebPage");
	}
	
	public WebPage(final Logger LOGGER){
		this.LOGGER = LOGGER;
		TIMER = new ProcessTime("Get WebPage",LOGGER);
	}
	
	public WebPage(final Logger LOGGER,final boolean DEBUG){
		debug = DEBUG;
		this.LOGGER = LOGGER;
		TIMER = new ProcessTime("Get WebPage",LOGGER);
	}

	public WebPage(final String URL) throws IOException{
		get(URL);
		LOGGER = null;
		TIMER = new ProcessTime("Get WebPage");
	}

	public WebPage(final String URL, final boolean DEBUG) throws IOException{
		debug = DEBUG;
		LOGGER = null;
		TIMER = new ProcessTime("Get WebPage");
		get(URL);
	}
	
	public WebPage(final String URL, final Logger LOGGER, final boolean DEBUG) throws IOException{
		debug = DEBUG;
		this.LOGGER = LOGGER;
		TIMER = new ProcessTime("Get WebPage",LOGGER);
		get(URL);
	}
	
	public final WebPage setHostCache(final HostCache HOST_CACHE){
		dnsCache = HOST_CACHE;
		return this;
	}
	
	public final HostCache getHostCache(){
		return dnsCache;
	}

	public final WebPage setDebug(final boolean DEBUG){
		debug = DEBUG;
		return this;
	}
	
	public final boolean isWebServer(final String URL) throws IOException,BufferOverflowException{
		get(URL);
		return isWebServer();
	}
	
	public final boolean isWebServer(){
		final StringElement ELEMENT_1 = STRING_SEARCH.position(0).findIndex("HTTP/1.1 ");
		final StringElement ELEMENT_2 = STRING_SEARCH.position(0).findIndex("HTTP/1.0 ");
		if(ELEMENT_1 != null){
			if(ELEMENT_1.getStartPos() == 1){
				return true;
			}
		}else if(ELEMENT_2 != null){
			if(ELEMENT_2.getStartPos() == 1){
				return true;
			}
		}
		return false;
	}
	
	public final int getResponseCode() throws StringNotFoundException{
		final StringElement ELEMENT_1 = STRING_SEARCH.position(0).findIndex("HTTP/1.1 ");
		final StringElement ELEMENT_2 = STRING_SEARCH.position(0).findIndex("HTTP/1.0 ");
		if(ELEMENT_1 != null){
			return STRING_SEARCH.position(ELEMENT_1).getInt(" ");
		}else if(ELEMENT_2 != null){
			return STRING_SEARCH.position(ELEMENT_2).getInt(" ");
		}
		return -1;
	}
	
	public final void addCookie(final String KEY, final String VALUE){
		COOKIES.put(KEY,VALUE);
	}
	
	public final String getCookie(final String KEY){
		return COOKIES.get(KEY);
	}
	
	public final void removeCookie(final String KEY){
		COOKIES.remove(KEY);
	}
	
	public final boolean hasCookies(){
        return (COOKIES.size() > 0);
	}
	
	public final void clearCookies(final String KEY){
		COOKIES.clear();
	}

	public final SearchableString get(final String URL) throws IOException,BufferOverflowException{
		if(URL.startsWith("http://")){
			final String REMOVED_HTTP = URL.substring("http://".length());
			final int POSTION = REMOVED_HTTP.indexOf("/");
			if(POSTION  <= -1){
				return get(dnsCache.get(REMOVED_HTTP),"/");
			}else{
				return get(dnsCache.get(REMOVED_HTTP.substring(0,POSTION)),REMOVED_HTTP.substring(POSTION));
			}
		}else if(URL.startsWith("https://")){
			final String REMOVED_HTTP = URL.substring("https://".length());
			final int POSTION = REMOVED_HTTP.indexOf("/");
			if(POSTION  <= -1){
				return get(dnsCache.get(REMOVED_HTTP),"/",HTTPS_PORT);
			}else{
				return get(dnsCache.get(REMOVED_HTTP.substring(0,POSTION)),REMOVED_HTTP.substring(POSTION),HTTPS_PORT);
			}
		}else{
			final int POSTION = URL.indexOf("/");
			if(POSTION > -1){
				return get(dnsCache.get(URL.substring(0,POSTION)),URL.substring(POSTION));
			}else{
				return get(dnsCache.get(URL),"/");
			}
		}
	}

    public final SearchableString post(final String URL, final String DATA) throws IOException,BufferOverflowException{
        return post(URL,DATA,null);
    }
	
	public final SearchableString post(final String URL, final String DATA, final String DATA_CONTENT_TYPE) throws IOException,BufferOverflowException{
		if(URL.startsWith("http://")){
			final String REMOVED_HTTP = URL.substring("http://".length());
			final int POSTION = REMOVED_HTTP.indexOf("/");
			if(POSTION  <= -1){
				return get(WebMethod.POST,dnsCache.get(REMOVED_HTTP),"/",HTTP_PORT,null,DATA,(DATA_CONTENT_TYPE == null) ? "application/x-www-form-urlencoded" : DATA_CONTENT_TYPE);
			}else{
				return get(WebMethod.POST,dnsCache.get(REMOVED_HTTP.substring(0,POSTION)),REMOVED_HTTP.substring(POSTION),HTTP_PORT,null,DATA,(DATA_CONTENT_TYPE == null) ? "application/x-www-form-urlencoded" : DATA_CONTENT_TYPE);
			}
		}else if(URL.startsWith("https://")){
			final String REMOVED_HTTP = URL.substring("https://".length());
			final int POSTION = REMOVED_HTTP.indexOf("/");
			if(POSTION  <= -1){
				return get(WebMethod.POST,dnsCache.get(REMOVED_HTTP),"/",HTTPS_PORT,null,DATA,(DATA_CONTENT_TYPE == null) ? "application/x-www-form-urlencoded" : DATA_CONTENT_TYPE);
			}else{
				return get(WebMethod.POST,dnsCache.get(REMOVED_HTTP.substring(0,POSTION)),REMOVED_HTTP.substring(POSTION),HTTPS_PORT,null,DATA,(DATA_CONTENT_TYPE == null) ? "application/x-www-form-urlencoded" : DATA_CONTENT_TYPE);
			}
		}else{
			final int POSTION = URL.indexOf("/");
			if(POSTION > -1){
				return get(WebMethod.POST,dnsCache.get(URL.substring(0,POSTION)),URL.substring(POSTION),HTTP_PORT,null,DATA,(DATA_CONTENT_TYPE == null) ? "application/x-www-form-urlencoded" : DATA_CONTENT_TYPE);
			}else{
				return get(WebMethod.POST,dnsCache.get(URL),"/",HTTP_PORT,null,DATA,(DATA_CONTENT_TYPE == null) ? "application/x-www-form-urlencoded" : DATA_CONTENT_TYPE);
			}
		}
	}
	
	public final WebPage setPathRelative(final boolean BOOL){
		relative = BOOL;
		return this;
	}
	
	public final SearchableString get(final Link LINK) throws IOException,BufferOverflowException{
		return get(LINK.getURL());
	}
	
	public final SearchableString get(final Host HOST) throws IOException,BufferOverflowException{
		return get(WebMethod.GET,HOST,"/",HTTP_PORT,null,null);
	}

	public final SearchableString get(final String HOST, final String PAGE) throws IOException,BufferOverflowException{
		return get(WebMethod.GET,dnsCache.get(HOST),PAGE,HTTP_PORT,null,null);
	}
	
	public final SearchableString get(final Host HOST, final String PAGE) throws IOException,BufferOverflowException{
		return get(WebMethod.GET,HOST,PAGE,HTTP_PORT,null,null);
	}

	public final SearchableString get(final String HOST, final String PAGE, final int PORT) throws IOException,BufferOverflowException{
		return get(WebMethod.GET,dnsCache.get(HOST),PAGE,PORT,null,null);
	}
	
	public final SearchableString get(final Host HOST, final String PAGE, final int PORT) throws IOException,BufferOverflowException{
		return get(WebMethod.GET,HOST,PAGE,PORT,null,null);
	}
	
	public final SearchableString get(final Host HOST, final String PAGE, final int PORT, final String BASE64) throws IOException,BufferOverflowException{
		return get(WebMethod.GET,HOST,PAGE,PORT,BASE64,null);
	}

    public final SearchableString get(final WebMethod WEBMETHOD, final Host HOST, final String PAGE, final int PORT, final String BASE64, final String DATA) throws IOException,BufferOverflowException{
        return get(new WebRequest(WEBMETHOD,PAGE,HOST,PORT,BASE64,DATA,(DATA == null) ? null : "application/x-www-form-urlencoded"));
    }

    public final SearchableString get(final String METHOD, final Host HOST, final String PAGE, final int PORT, final String BASE64, final String DATA) throws Exception{
        final WebMethod WEBMETHOD = WebMethod.guessType(METHOD);
        if(WEBMETHOD == null){
            throw new Exception("No Such WebMethod["+METHOD+"]");
        }
        return get(new WebRequest(WEBMETHOD,PAGE,HOST,PORT,BASE64,DATA,(DATA == null) ? null : "application/x-www-form-urlencoded"));
    }

    public final SearchableString get(final WebMethod WEBMETHOD, final Host HOST, final String PAGE, final int PORT, final String BASE64, final String DATA, final String DATA_CONTENT_TYPE) throws IOException,BufferOverflowException{
        return get(new WebRequest(WEBMETHOD,PAGE,HOST,PORT,BASE64,DATA,DATA_CONTENT_TYPE));
    }

    public final SearchableString get(final String METHOD, final Host HOST, final String PAGE, final int PORT, final String BASE64, final String DATA, final String DATA_CONTENT_TYPE) throws Exception{
        final WebMethod WEBMETHOD = WebMethod.guessType(METHOD);
        if(WEBMETHOD == null){
            throw new Exception("No Such WebMethod["+METHOD+"]");
        }
        return get(new WebRequest(WEBMETHOD,PAGE,HOST,PORT,BASE64,DATA,DATA_CONTENT_TYPE));
    }
	
	public final SearchableString get(final WebRequest REQUEST) throws IOException,BufferOverflowException{
		if(debug){
			if(LOGGER != null){
				LOGGER.writeLine("Getting WebPage["+REQUEST.getHost().getHostName()+"]["+REQUEST.getPage()+"]");
				LOGGER.write("IPs");
				for(final String STRING : REQUEST.getHost().getIPs()){
					LOGGER.write("["+STRING+"]");
				}
				LOGGER.writeLine();
			}else{
				//System.out.println("Getting WebPage["+HOST.getHostName()+"]["+PAGE+"]");
			}
			TIMER.start();
		}
		if(debug){
			if(LOGGER != null){
				LOGGER.writeLine(REQUEST.toCreateString().toString());
			}else{
				//System.out.println(REQUEST.toString());
			}
		}
		final ByteBuffer HEADER = REQUEST.toCreateString().toByteBuffer();
		return get(REQUEST.getHost(),REQUEST.getPort(),HEADER);
	}
	
	public final String createCookie(){
		final StringBuilder REQUEST = new StringBuilder();
		int tmp = 0;
		if(COOKIES.size() > 0){
			for(final String KEY : COOKIES.keySet()){
				if(COOKIES.get(KEY) != null){
					final String VALUE = COOKIES.get(KEY);
					REQUEST.append(KEY).append("=").append(VALUE);
					if(tmp++ < COOKIES.size()){
						REQUEST.append("; ");
					}
				}
			}
		}
		return REQUEST.toString();
	}

	public final SearchableString get(final Host HOST, final int PORT, final ByteBuffer HEADER) throws IOException,BufferOverflowException{
		/* TODO: FINISH SSL http://docs.oracle.com/javase/6/docs/technotes/guides/security/jsse/JSSERefGuide.html#SSLENG */
		SocketAddress REMOTE_ADDRESS = null;
		for(final String IP : HOST.getIPs()){
			try{
				REMOTE_ADDRESS = new InetSocketAddress(IP, PORT);
				break;
			}catch(final Exception ex){}
		}
		final SocketChannel CHANNEL = SocketChannel.open(REMOTE_ADDRESS);
		if(PORT == 443){
			try{
				final SSLContext SSL_CONTEXT = SSLContext.getInstance("TLS");
				SSL_CONTEXT.init(null,null,null);
				final SSLEngine CLIENT_ENGINE = SSL_CONTEXT.createSSLEngine(HOST.getIPs().get(0), PORT);
				CLIENT_ENGINE.setUseClientMode(true);
				CLIENT_ENGINE.beginHandshake();
				final ByteBuffer DATA = ByteBuffer.allocateDirect(CLIENT_ENGINE.getSession().getPacketBufferSize());
				SSLEngineResult.HandshakeStatus hs = CLIENT_ENGINE.getHandshakeStatus();
				while (hs != SSLEngineResult.HandshakeStatus.FINISHED && hs != SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING) {
					switch (hs) {
						case NEED_UNWRAP:
							System.out.println("Need Unwrap");
						break;
						case NEED_WRAP:
							System.out.println("Need Wrap");
							final SSLEngineResult RESULT = CLIENT_ENGINE.wrap(HEADER, DATA);
							// Check status
							if (RESULT.getStatus() == SSLEngineResult.Status.OK) {
								while(DATA.hasRemaining()) {
									final int AMOUNT = CHANNEL.write(DATA);
									if(AMOUNT == -1){
										break;
									}else if(AMOUNT == 0){
										// no bytes written; try again later
									}
								}
								System.out.println("STATUS OK");
							}
						break;
						case NEED_TASK:
							System.out.println("Need Task");
						break;
					}
				}
			}catch(final NoSuchAlgorithmException e){
			
			}catch(final KeyManagementException ex){
			
			}
		}
		CHANNEL.write(HEADER);
		BUFFER.clear();
		while(CHANNEL.read(BUFFER) != -1){
			try{
				Thread.sleep(50);
			}catch(final Exception e){}
		}
		BUFFER.flip();
		STRING_SEARCH.setBuffer(BUFFER,false,false);
		CHANNEL.close();
		if(STRING_SEARCH.contains("Content-Encoding: gzip")){
			final ByteArrayInputStream IN = new ByteArrayInputStream(STRING_SEARCH.toBytes(STRING_SEARCH.position(0).findIndex("\r\n\r\n",true).getEndPos()-1));
			final GZIPInputStream GZIP = new GZIPInputStream(IN);
			final byte[] BYTES = new byte[IN.available()];
			GZIP.read(BYTES,0,IN.available());
			STRING_SEARCH.setBuffer(ByteBuffer.wrap(BYTES));
		}
		if(debug){
			TIMER.stop();
			//TIMER.writeProcessingTime();
			if(LOGGER != null){
				LOGGER.writeLine(STRING_SEARCH.toString());
			}
		}
		return STRING_SEARCH;
	}
	
	public final ProcessTime getProcessTime(){
		return TIMER;
	}

	public final SearchableString getSource(){
		return STRING_SEARCH;
	}
	
	public final BufferedImage getImage(){
		try{
			return ImageIO.read(new ByteArrayInputStream(STRING_SEARCH.toBytes(STRING_SEARCH.position(0).findIndex("\r\n\r\n",true).getEndPos()-1)));
		}catch(final Exception e){
			//e.printStackTrace();
		}
		return null;
	}

}
