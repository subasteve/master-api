package com.master.net;

public final class IP{

	private final String IP_STRING;
	private final int IP_INT;
    //Int because java has no notation of unsigned byte
    private final int[] IP_BYTES;

	public IP(final String IP_STRING){
		this.IP_STRING = IP_STRING;
		this.IP_INT = toInt(IP_STRING);
        IP_BYTES = toBytes(IP_INT);
	}

    public IP(final String IP_STRING, final boolean REVERSE){
        this.IP_STRING = IP_STRING;
        this.IP_INT = toInt(IP_STRING,REVERSE);
        IP_BYTES = toBytes(IP_INT,REVERSE);
    }

    public IP(final int[] IP_BYTES){
        this.IP_BYTES = IP_BYTES;
        this.IP_INT = toInt(IP_BYTES);
        this.IP_STRING = toString(IP_INT,false);
    }

    public IP(final int[] IP_BYTES, final boolean REVERSE){
        this.IP_BYTES = IP_BYTES;
        this.IP_INT = toInt(IP_BYTES,REVERSE);
        this.IP_STRING = toString(IP_INT,REVERSE);
    }

	public IP(final int IP_INT){
		this.IP_INT = IP_INT;
		this.IP_STRING = toString(IP_INT,false);
        IP_BYTES = toBytes(IP_INT);
	}

	public IP(final int IP_INT, final boolean REVERSE){
        this.IP_INT = IP_INT;
        this.IP_STRING = toString(IP_INT,REVERSE);
        IP_BYTES = toBytes(IP_INT,REVERSE);
    }


	public IP(final int IP_INT, final String IP_STRING){
		this.IP_INT = IP_INT;
		this.IP_STRING = IP_STRING;
        IP_BYTES = toBytes(IP_INT);
	}

    public IP(final int IP_INT, final String IP_STRING, final int[] IP_BYTES){
        this.IP_INT = IP_INT;
        this.IP_STRING = IP_STRING;
        this.IP_BYTES = IP_BYTES;
    }


	public final String toString(){
		return IP_STRING;
	}

	public final int toInt(){
		return IP_INT;
	}

    public final int[] toBytes(){
        return IP_BYTES;
    }

    public static final int[] toBytes(final int ADDR) {
        return toBytes(ADDR,false);
    }

    public static final int[] toBytes(final int ADDR, final boolean REVERSE) {
        try{
            final int[] BYTES = new int[4];
            if(!REVERSE){
                BYTES[0] = (ADDR >> 24) & 0xFF;
                BYTES[1] = (ADDR >> 16) & 0xFF;
                BYTES[2] = (ADDR >> 8) & 0xFF;
                BYTES[3] = (ADDR & 0xFF);
            }else{
                BYTES[0] = (ADDR & 0xFF);
                BYTES[1] = (ADDR >> 8) & 0xFF;
                BYTES[2] = (ADDR >> 16) & 0xFF;
                BYTES[3] = (ADDR >> 24) & 0xFF;
            }
            return BYTES;
        }catch(final Exception e){
            //logger.writeError(e);
            return null;
        }
    }

    public static final int[] toBytes(final String ADDR) {
        return toBytes(ADDR,false);
    }

    public static final int[] toBytes(final String ADDR, final boolean REVERSE) {
        try{
            final String[] ADDR_BYTES = ADDR.split("\\.");
            final int[] BYTES = new int[4];
            if(!REVERSE){
                BYTES[0] = Integer.parseInt(ADDR_BYTES[0].trim());
                BYTES[1] = Integer.parseInt(ADDR_BYTES[1].trim());
                BYTES[2] = Integer.parseInt(ADDR_BYTES[2].trim());
                BYTES[3] = Integer.parseInt(ADDR_BYTES[3].trim());
            }else{
                BYTES[0] = Integer.parseInt(ADDR_BYTES[3].trim());
                BYTES[1] = Integer.parseInt(ADDR_BYTES[2].trim());
                BYTES[2] = Integer.parseInt(ADDR_BYTES[1].trim());
                BYTES[3] = Integer.parseInt(ADDR_BYTES[0].trim());
            }
            return BYTES;
        }catch(final Exception e){
            //logger.writeError(e);
            return null;
        }
    }

    public static final int toInt(final int[] ADDR) {
        return toInt(ADDR,false);
    }

    public static final int toInt(final int[] ADDR, final boolean REVERSE) {
        try{
            int ip = 0;
            if(!REVERSE){
                ip = (ADDR[0] << 24)+
                        (ADDR[1] << 16)+
                        (ADDR[2] << 8)+
                        (ADDR[3]);
            }else{
                ip = ADDR[3]+
                        (ADDR[2] << 8)+
                        (ADDR[1] << 16)+
                        (ADDR[0] << 24);
            }
            return ip;
        }catch(final Exception e){
            //logger.writeError(e);
            return -1;
        }
    }

    public static final int toInt(final String ADDR) {
        return toInt(ADDR,false);
    }

    public static final int toInt(final String ADDR, final boolean REVERSE) {
		try{
			final String[] ADDR_BYTES = ADDR.split("\\.");
            int ip = 0;
            if(!REVERSE){
                ip = (Integer.parseInt(ADDR_BYTES[0]) << 24)+
                        (Integer.parseInt(ADDR_BYTES[1]) << 16)+
                        (Integer.parseInt(ADDR_BYTES[2]) << 8)+
                        (Integer.parseInt(ADDR_BYTES[3]));
            }else{
                ip = (Integer.parseInt(ADDR_BYTES[3]))+
                        (Integer.parseInt(ADDR_BYTES[2]) << 8)+
                        (Integer.parseInt(ADDR_BYTES[1]) << 16)+
                        (Integer.parseInt(ADDR_BYTES[0]) << 24);
            }
			return ip;
		}catch(final Exception e){
			//logger.writeError(e);
			return -1;
		}
	}

    public static final String toString(final int[] IP_BYTES) {
        return toString(IP_BYTES,false);
    }

    public static final String toString(final int[] IP_BYTES, final boolean REVERSE) {
        try{
            final StringBuilder BUFFER = new StringBuilder(15);
            if(!REVERSE){
                BUFFER.append(IP_BYTES[0]).append(".");
                BUFFER.append(IP_BYTES[1]).append(".");
                BUFFER.append(IP_BYTES[2]).append(".");
                BUFFER.append(IP_BYTES[3]);
            }else{
                BUFFER.append(IP_BYTES[3]).append(".");
                BUFFER.append(IP_BYTES[2]).append(".");
                BUFFER.append(IP_BYTES[1]).append(".");
                BUFFER.append(IP_BYTES[0]);
            }
            return BUFFER.toString();
        }catch(final Exception e){
            //logger.writeError(e);
            return null;
        }
    }

    public static final String toString(final int i, boolean reverse) {
		try{
			final StringBuilder BUFFER = new StringBuilder(15);
			if(!reverse){
                BUFFER.append((i >> 24) & 0xFF).append(".");
                BUFFER.append((i >> 16) & 0xFF).append(".");
                BUFFER.append((i >>  8) & 0xFF).append(".");
                BUFFER.append(i & 0xFF);
			}else{
                BUFFER.append(i & 0xFF).append(".");
                BUFFER.append((i >>  8) & 0xFF).append(".");
                BUFFER.append((i >> 16) & 0xFF).append(".");
                BUFFER.append((i >> 24) & 0xFF);
			}
			return BUFFER.toString();
		}catch(final Exception e){
			//logger.writeError(e);
			return null;
		}
    }

}
