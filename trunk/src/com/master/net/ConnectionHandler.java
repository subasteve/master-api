package com.master.net;

import com.master.net.socket.ClientSocket;
import com.master.net.socket.ConnectionSocket;

public abstract class ConnectionHandler extends Thread{

	private final int port;
	private boolean portOpened = false;

	public ConnectionHandler(final int port){
		this.port = port;
		(new Thread(this, "ConnectionHandler")).start();			// launch server listener
	}
	
	public void destruct(){
		portOpened = false;
	}
	
	
	public abstract void newClient(ClientSocket sock);
	
	public final void run() {
		ConnectionSocket clientListener = null;
		portOpened = false;
		do{ 
			try{	
				clientListener = new ConnectionSocket(port); 
				portOpened = true;
				System.out.println("ConnectionHandler: " + clientListener.getLocalPort());
			} catch(final Exception _ex) { portOpened = false; }

			if(!portOpened)
				try { Thread.sleep(3000); } catch(Exception ex) { } 

		} while (!portOpened);
		while(portOpened) {
			try{
				ClientSocket sock = clientListener.accept();
				newClient(sock);
			} catch(final Exception ex){ ex.printStackTrace(); }
		}
	}

}