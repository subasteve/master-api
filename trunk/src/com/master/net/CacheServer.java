package com.master.net;

import com.master.util.LoadIni;
import com.master.util.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Created by subasteve on 10/17/13.
 */
public final class CacheServer {

    private final String NAME, SERVER, PREFIX;
    private final int PORT;

    public Logger getLogger() {
        return logger;
    }

    public CacheServer setLogger(final Logger LOGGER) {
        logger = LOGGER;
        return this;
    }

    private Logger logger = null;

    public CacheServer(final String NAME, final String SERVER, final int PORT) throws IOException{
        this.NAME = NAME;
        this.SERVER = SERVER;
        this.PORT = PORT;
        PREFIX = "";
    }

    public CacheServer(final String NAME, final String SERVER, final String PREFIX, final int PORT) throws IOException{
        this.NAME = NAME;
        this.SERVER = SERVER;
        this.PORT = PORT;
        this.PREFIX = PREFIX;
    }

    public CacheServer generateConfig(final LoadIni SETTINGS){
        if(SETTINGS != null){
            boolean changes = false;
            if(!SETTINGS.hasProperty("cacheServerName")){
                SETTINGS.setProperty("cacheServerName", "");
                changes = true;
            }
            if(!SETTINGS.hasProperty("cacheServer")){
                SETTINGS.setProperty("cacheServer", "localhost");
                changes = true;
            }
            if(!SETTINGS.hasProperty("cacheServerPort")){
                SETTINGS.setProperty("cacheServerPort", "11211");
                changes = true;
            }
            if(changes){
                SETTINGS.save();
            }
        }
        return this;
    }

    public CacheServer(final LoadIni SETTINGS) throws Exception {
        if(SETTINGS.hasProperty("cacheServerName")) {
            NAME = SETTINGS.loadString("cacheServerName");
        }else{
            NAME = "";
        }
        if(SETTINGS.hasProperty("cacheServer")) {
            SERVER = SETTINGS.loadString("cacheServer");
        }else{
            SERVER = "";
        }
        if(SETTINGS.hasProperty("cacheServerPort")) {
            PORT = SETTINGS.loadInt("cacheServerPort");
        }else{
            PORT = -1;
        }
        String prefix = "";
        try{
            prefix = SETTINGS.loadString("cachePrefix");
        }catch (final Exception e){}
        PREFIX = prefix;
    }

    public final String getName(){
        return NAME;
    }

    public final String getServer(){
        return SERVER;
    }

    public final String getPrefix(){ return PREFIX; }

    public final int getPort(){
        return PORT;
    }
}
