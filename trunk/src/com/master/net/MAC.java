package com.master.net;

public final class MAC{

	private final String MAC_STRING;
	private final long MAC_LONG;

	public MAC(final String MAC_STRING){
		this.MAC_STRING = MAC_STRING.toUpperCase();
		this.MAC_LONG = toLong(MAC_STRING);
	}

	public MAC(final long MAC_LONG){
		this.MAC_LONG = MAC_LONG;
		this.MAC_STRING = toString(MAC_LONG).toUpperCase();
	}

	public final String toString(){
		return MAC_STRING;
	}

	public final long toLong(){
		return MAC_LONG;
	}
	
	public final boolean equals(final MAC mac){
        return mac.toString().equalsIgnoreCase(MAC_STRING);
	}

	private final String toString(final long mac){
		final StringBuilder sb = new StringBuilder( 17 );
        for ( int i=44; i>=0; i-=4 ) {
            final int nibble =  ((int)( mac >>> i )) & 0xf;
            final char nibbleChar = (char)( nibble > 9 ? nibble + ('A'-10) : nibble + '0' );
            sb.append( nibbleChar );
            if ( (i & 0x7) == 0 && i != 0 ) {
                sb.append( ":" );
            }
        }
        return sb.toString();
	}

	private final long toLong(String mac){
		if(mac == null || mac.equals("")){
                        return -1;
                }
                final String[] hex = mac.split("(\\:|\\-)");
                final byte byteArray[] = new byte[6];
                for (int i=0; i<6; i++){
                        //System.out.println(hex[i]);
                        //logger.write("HEX["+i+"]"+hex[i]+" ");
                        byteArray[i] = (byte) Integer.parseInt(hex[i], 16);
                }
                return byte2Long(byteArray);
	}

	private final long byte2Long(final byte addr[]) {
            long address = 0;
                if (addr != null) {
                    if (addr.length == 6) {
                                int offset = 0;
                                address += ((addr[offset++] & 0xffl) << 40);
                                address += ((addr[offset++] & 0xffl) << 32);
                                address += ((addr[offset++] & 0xffl) << 24);
                                address += ((addr[offset++] & 0xffl) << 16);
                                address += ((addr[offset++] & 0xffl) << 8);
                                address += (addr[offset++] & 0xffl);
                                //address = (addr[5] & 0xffl) + ((addr[4] & 0xf$
                    }
                }
                return address;
        }

}
