package com.master.net;

import java.io.*;
import java.net.*;
import com.master.util.CreateString;
import com.master.util.Logger;

public final class PageSource{

	final CreateString source = new CreateString();
	private boolean debug = false;
	private Logger logger = null;
	private String currentPath = "";
	private String currentHost = "";
	
	public PageSource(){
	}
	
	public PageSource(final Logger logger){
		this.logger = logger;
	}
	
	public final void setLogger(final Logger logger){
		this.logger = logger;
	}
	
	public final boolean getURL(final String host, final String path){
		return getURL(host, path, null, null, null);
	}
	
	public final boolean getURL(final String host, final String path, final String cookie){
		return getURL(host, path, null, null, cookie);
	}
	
	public final boolean getAuthURL(final String host, final String path, final String base64){
		return getURL(host, path, null, base64, null);
	}
	
	public final String getPath(){
		return currentPath;
	}
	
	public final String getHost(){
		return currentHost;
	}
	
	public final String getFileName(){
		String str = currentPath.substring(currentPath.lastIndexOf("/")+1);
		if(str == null || str.equals("") || str.equals(" ")){
			return null;
		}else{
			return str;
		}		
	}
	
	public final boolean requiresAuth(final String host, final String path){
		try{
			URL url = new URL(new CreateString("http://",host,path).toString());
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.connect();
            return (conn.getResponseCode() == 403 || conn.getResponseCode() == 401);
		}catch(final Exception e){
			return true;
		}
	}
	
	public final String getRedirect(final String host, final String path, final String data, final String base64, final String cookie){
		if(host == null){
			return null;
		}
		final String page = new CreateString("http://",host,path).toString();
		OutputStreamWriter wr = null;
		debug = false;
		try{
			URL url = new URL(page);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setInstanceFollowRedirects(false);
			if(base64 != null){
				conn.setRequestProperty("Authorization", "basic " +base64);
			}
			if(cookie != null){
				conn.setRequestProperty("Cookie", cookie);
			}
			conn.setRequestProperty("Host",host);
			conn.setRequestProperty("Referer",page);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
			if(data != null){
				conn.setDoOutput(true);
				wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(data);
				wr.flush();
			}else{
				conn.connect();
			}
			final int responseCode = conn.getResponseCode();
			if(responseCode == HttpURLConnection.HTTP_MOVED_TEMP || responseCode == HttpURLConnection.HTTP_MOVED_PERM){
				final String location = conn.getHeaderField("Location");
				if(location != null){
					if(logger != null){
						logger.writeLine("Redirecting: "+location);
					}
					if(wr != null){
						try{
							wr.close();
						}catch(final Exception ex){}
						wr = null;
					}
					return location;
				}
			}
		}catch(final Exception e){
			if(logger != null){
				logger.writeLine("Error Url: "+page);
				logger.writeError(e);
			}
		}finally{
			if(wr != null){
				try{
					wr.close();
				}catch(final Exception ex){}
				wr = null;
			}
		}
		return null;
	}
	
	
	public final boolean isWebserver(final String host, final String path){
		try{
			String page = new CreateString("http://",host,path).toString();
			URL url = new URL(page);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setInstanceFollowRedirects(false);
			conn.connect();
			final int responseCode = conn.getResponseCode();
			if(responseCode != -1){
				return true;
			}
		}catch(final Exception e){}
		return false;
	}
	
	public final int getResponseCode(final String host, final String path){
		try{
			String page = new CreateString("http://",host,path).toString();
			URL url = new URL(page);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setInstanceFollowRedirects(false);
			conn.connect();
			return conn.getResponseCode();
		}catch(final Exception e){}
		return -1;
	}
	
	public final boolean getURL(final String page){
		if(page == null){
			return false;
		}
		source.clear();
		BufferedReader rd = null;
		try{
			URL url = new URL(page);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setInstanceFollowRedirects(false);
			conn.setRequestProperty("Host",page.substring(page.indexOf("http://"),page.indexOf("/",page.indexOf("http://"))));
			conn.setRequestProperty("Referer",page);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			int index = 0;
			while (true){
				final String tmp = rd.readLine();
				if(tmp != null){
					source.put(tmp);
				}else{
					break;
				}
			}
			if(rd != null){
				try{
					rd.close();
				}catch(final Exception ex){}
				rd = null;
			}
			return true;
		}catch(final Exception e){
			if(logger != null){
				logger.writeLine("Error Url: "+page);
				logger.writeError(e);
			}
		}finally{
			if(rd != null){
				try{
					rd.close();
				}catch(final Exception ex){}
				rd = null;
			}
		}
		return false;
	}

	public final boolean getURL(final String host, final String path, final String data, final String base64, final String cookie){
		if(host == null){
			return false;
		}
		currentHost = host;
		currentPath = path;
		source.clear();
		String page = new CreateString("http://",host,path).toString();
		DataOutputStream wr = null;
		BufferedReader rd = null;
		try{
			URL url = new URL(page);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setInstanceFollowRedirects(false);
			if(base64 != null){
				conn.setRequestProperty("Authorization", "Basic " +base64);
			}
			if(cookie != null){
				conn.setRequestProperty("Cookie", cookie);
			}
			conn.setRequestProperty("Host",host);
			conn.setRequestProperty("Referer",page);
			conn.setRequestProperty("Accept-Charset","ISO-8859-1,utf-8;q=0.7,*;q=0.3");
			conn.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			//conn.setRequestProperty("Origin", "http://10.32.0.21");
			//conn.setRequestProperty("Referer", "http://10.32.0.21/SalesTool/login.seam");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
			if(data != null){
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
				conn.setRequestProperty("charset", "utf-8");
				conn.setRequestProperty("Content-Length",""+data.getBytes().length);
				conn.setRequestProperty("Connection","keep-alive");
				if(logger != null){
					logger.writeLine("Content-Length: "+data.getBytes().length);
				}
				conn.setDoOutput(true);
				conn.setDoInput(true);
				wr = new DataOutputStream(conn.getOutputStream());
				wr.writeBytes(data);
				wr.flush();
			}else{
				conn.connect();
			}
			
			final int responseCode = conn.getResponseCode();
			if(responseCode == HttpURLConnection.HTTP_MOVED_TEMP || responseCode == HttpURLConnection.HTTP_MOVED_PERM){
				final String location = conn.getHeaderField("Location");
				if(location != null){
					if(location.contains("http://") || location.contains("https://")){
						if(logger != null){
							logger.writeLine("Redirecting: "+location);
						}
						currentHost = host;
						currentPath = location;
						page = location;
						url = new URL(location);
					}else{
						page = new CreateString("http://",host,location).toString();
						if(logger != null){
							logger.writeLine(new CreateString("Redirecting: ",page).toString());
						}
						currentPath = location;
						url = new URL(page);
					}
					conn = (HttpURLConnection)url.openConnection();
					if(base64 != null){
						conn.setRequestProperty("Authorization", "Basic " +base64);
					}
					if(cookie != null){
						conn.setRequestProperty("Cookie", cookie);
					}
					conn.setRequestProperty("Host",host);
					conn.setRequestProperty("Referer",page);
					conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
				}
			}else if(responseCode != HttpURLConnection.HTTP_OK){
				if(responseCode != -1){
					if(logger != null){
						logger.writeLine(new CreateString("getURL[",new Integer(responseCode).toString(),"] ",page).toString());
					}
				}
				return false;
			}
			
			/*
			for (String key : conn.getHeaderFields().keySet()) {
				System.out.println(key+": "+conn.getHeaderField(key));
			}
			*/
			
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			int index = 0;
			while (true){
				final String tmp = rd.readLine();
				if(tmp != null){
					source.put(tmp);
				}else{
					break;
				}
			}
			if(wr != null){
				try{
					wr.close();
				}catch(final Exception ex){}
				wr = null;
			}
			if(rd != null){
				try{
					rd.close();
				}catch(final Exception ex){}
				rd = null;
			}
			return true;
		}catch(final Exception e){
			if(logger != null){
				logger.writeLine("Error Url: "+page);
				logger.writeError(e);
			}
		}finally{
			if(wr != null){
				try{
					wr.close();
				}catch(final Exception ex){}
				wr = null;
			}
			if(rd != null){
				try{
					rd.close();
				}catch(final Exception ex){}
				rd = null;
			}
		}
		return false;
	}
	
	public final CreateString getSource(){
		return source;
	}
	
	public final String printLines(){
		//TODO: Finish fixing CreateString
	/*
		int line = 0;
		for(String str : source.values()){
			
		}
		final StringBuffer buf = new StringBuffer(5000);
		for(int i = 0; i < cache.length; i++){
			if(cache[i] != null){
				buf.append("[");
				buf.append(i);
				buf.append("]");
				buf.append(cache[i].getString());
				buf.append("\n");
			}
		}
		return buf.toString();
		*/
		return "";
	}
	
	public final void getFile(final String file){
		BufferedReader in = null;
		source.clear();
		try{
			in = new BufferedReader(new FileReader(file));
			int index = 0;
			while (true){
				final String tmp = in.readLine();
				if(tmp != null){
					source.put(tmp);
				}else{
					break;
				}
			}
		}catch(final Exception e){
			if(logger != null){
				logger.writeLine("Error Loading: "+file);
				logger.writeError(e);
			}
		}finally{
			if(in != null){
				try{
					in.close();
				}catch(final Exception ex){}
				in = null;
			}
		}
	}
	
	public final void save(final String path, boolean lines){
		if(source == null || source.equals("")){
			return;
		}
		FileOutputStream fos = null;
		try{
			//File file = new File(path.substring(0,path.length()-getFileName().length()));
			//if(!file.exists()){
			//	file.mkdirs();
			//}
			File file = new File(path);
			fos = new FileOutputStream(file);
			//if(lines){
			//	fos.write(printLines().getBytes());
			//}else{
			//	fos.write(source.toString().getBytes());
			//}
			for(final String STRING : source.values()){
				fos.write(STRING.getBytes());
			}
		}catch(final Exception e) {
			if(logger != null){
				logger.writeLine("Error Saving: "+path);
				logger.writeError(e);
			}
		}finally{
			if(fos != null){
				try{
					fos.close();
				}catch(final Exception ex){}
				fos = null;
			}
		}
	}
	
	public final void setDebug(final boolean bool){
		debug = bool;
	}

}