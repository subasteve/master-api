package com.master.net.socket;

import com.master.net.socket.stream.in.In;
import com.master.net.socket.stream.out.Out;
import com.master.net.socket.stream.Cryption;
import com.master.net.socket.stream.in.packets.Parse;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketImpl;
import java.net.InetAddress;
import java.net.SocketAddress;

public final class ClientSocket extends Socket implements Runnable{

	private Cryption inStreamDecryption = null, outStreamDecryption = null;
	private In inStream = null;
	private Out outStream = null;
	private Parse parse = null;
	private boolean initlized = false, running = false;
	
	public final String getHostName(){
		return getInetAddress().getHostName();
	}
	
	public final String getIP(){
		return getInetAddress().getHostAddress();
	}
	
	public ClientSocket(){ //unconnected socket
		super();
	}
	
	public ClientSocket(final SocketImpl impl) throws IOException{ //unconnected socket 
		super(impl);
	}
	
	public ClientSocket(final String host, final int port) throws IOException{
		super(host,port);
		initialize();
	}
	
	public ClientSocket(final boolean TIMEOUT, final String host, final int port) throws IOException{
		super(host,port);
		initialize(TIMEOUT);
	}
	
	public ClientSocket(final InetAddress address, final int port) throws IOException{
		super(address,port);
		initialize();
	}
	
	public ClientSocket(final boolean TIMEOUT, final InetAddress address, final int port) throws IOException{
		super(address,port);
		initialize(TIMEOUT);
	}
	
	public ClientSocket(final String host, final int port, final boolean stream) throws IOException{
		super(host,port,stream);
		initialize();
	}
	
	public ClientSocket(final InetAddress host, final int port, final boolean stream) throws IOException{
		super(host,port,stream);
		initialize();
	}
	
	public ClientSocket(final InetAddress address, final int port, final InetAddress localAddr, final int localPort) throws IOException{
		super(address,port,localAddr,localPort);
		initialize();
	}
	
	public ClientSocket(final String host, final int port, final InetAddress localAddr, final int localPort) throws IOException{
		super(host,port,localAddr,localPort);
		initialize();
	}
	
	public final void connect(SocketAddress endpoint) throws IOException{
		super.connect(endpoint, 0);
		initialize();
	}
	
	public final void connect(final SocketAddress endpoint, final int timeout) throws IOException{
		super.connect(endpoint, timeout);
		initialize();
	}
	
	public final void initialize() throws IOException{
		initialize(true);
	}
	
	public final void initialize(final boolean TIMEOUT) throws IOException{
		if(TIMEOUT){
			setSoTimeout(10000);
			setSoLinger(true,10000);
		}
		setTcpNoDelay(true);
		inStream = new In(getInputStream());
		outStream = new Out();
		initlized = true;
	}
	
	public final Out getOutStream(){
		return outStream;
	}
	
	public final In getInStream(){
		return inStream;
	}
	
	public final void setParse(final Parse parse){
		this.parse = parse;
	}
	
	public final void setCryption(final long IN_KEY, long OUT_KEY){
		setCryption(new Cryption(IN_KEY,OUT_KEY));
	}
	
	public final void setCryption(int[] key){
		final Cryption cry = new Cryption(key);
		inStreamDecryption = cry;
		outStreamDecryption = cry;
		outStream.setCryption(outStreamDecryption);
	}
	
	public boolean isCryptionSet(){
		return (inStreamDecryption != null && outStreamDecryption != null);
	}
	public final void setCryption(final Cryption cry){
		setInCryption(cry);
		setOutCryption(cry);
	}

	public final void setInCryption(final Cryption cry){
		inStreamDecryption = cry;
		inStream.setCryption(cry);
	}
	public final void setOutCryption(final Cryption cry){
		outStreamDecryption = cry;
		outStream.setCryption(cry);
	}
	
	public final Cryption getInCryption(){
		return inStreamDecryption;
	}

	public synchronized final void writePacket(final int packet){
		try{
			Out OUT = new Out();
			if(outStreamDecryption != null){
				OUT.writeByte(packet+outStreamDecryption.getNextKey());
			}else{
				OUT.writeByte(packet);
			}
			OUT.writeByte(0);
			OUT.writeTo(getOutputStream());
			OUT.reset();
            		getOutputStream().flush();
		}catch(Exception ex) { destruct(ex); }
	}
	
	// two methods that are only used for  procedure
	public synchronized final void writePacket(final int packet, final Out OUT){
		try{
			Out OUT2 = new Out();
			if(outStreamDecryption != null){
				OUT2.writeByte(packet+outStreamDecryption.getNextKey());
			}else{
				OUT2.writeByte(packet);
			}
			final long offset = OUT.getCurrentOffset();
			if(offset >= 255){
				OUT2.writeByte(255);
				OUT2.writeLong(offset); //size constrants type
			}else{
				OUT2.writeByte(offset);
			}
			OUT.writeTo(OUT2);
			OUT2.writeTo(getOutputStream());
			OUT2.reset();
			OUT.reset();
			getOutputStream().flush();
		}catch(Exception ex) { destruct(ex); }
	}
/*
	// two methods that are only used for  procedure
	public synchronized final void directWriteOutputStream(final byte[] buffer, final int offset){
		try{
			getOutputStream().write(buffer, 0, offset);
			getOutputStream().flush();
		}catch(Exception ex) { destruct(ex); }
	}

	// two methods that are only used for  procedure
	public final void directFlushOutStream(){
		try{
			getOutStream().writeTo(getOutputStream());
			getOutputStream().flush();
			getOutStream().reset();
		}catch(Exception ex) { destruct(ex); }
	}

	// two methods that are only used for  procedure
	public final void directFlushOutStream(final int forceOut){
		try{
			getOutputStream().write(getOutStream().getBytes(), 0, forceOut);
			getOutputStream().flush();
			getOutStream().setCurrentOffset(forceOut);		// reset
		}catch(Exception ex) { destruct(ex); }
	}
*//*
	// forces to read forceRead bytes from the client - block until we have received those
	public final void fillInStream(final int forceRead){
		try{
			if(forceRead > 5000){
				getInStream().setBufferSize(forceRead);
			}
			getInStream().setCurrentOffset(0);
			getInputStream().read(getInStream().getBuffer(), 0, forceRead);
		}catch(Exception ex) { destruct(ex); }
	}
*//*
	// forces to read forceRead bytes from the client - block until we have received those
	public final void appendInStream(final int offset, final int forceRead){
		try{
			getInputStream().read(getInStream().getBuffer(), offset, forceRead);
		}catch(Exception ex) { destruct(ex); }
	}
	// forces to read forceRead bytes from the client - block until we have received those
	public final void fillInStream(){
		try{
			getInStream().setCurrentOffset(0);
			final int size = getInputStream().available();
			for(int i = 0; i < size; i++){
				getOutStream().writeByte(getInputStream().read());
			}
		}catch(Exception ex) { destruct(ex); }
	}
*/
	public final void start(){
		if(!running){
			running = true;
			new Thread(this).start();
		}
	}

	public final void run(){
		do{
			process();
			//try { Thread.sleep(1); } catch(Exception ex) { } 
		}while(isAlive() && running);
	}

	public final int getPacketType() throws IOException{
		final int AVAILABLE = getInputStream().available();
		if(AVAILABLE > 0){
			if(inStreamDecryption != null){
				return (getInStream().readUnsignedByte()-inStreamDecryption.getNextKey() & 0xff);
			}else{
				return getInStream().readUnsignedByte();
			}
		}
		return -1;
	}

	public final long getPacketSize() throws IOException{
		final int AVAILABLE = getInputStream().available();
		if(AVAILABLE > 0){
			long packetSize = getInStream().readUnsignedByte();
			if(packetSize == 255){
				packetSize = getInStream().readLong();
			}
			return packetSize;
		}
		return -1;
	}

	public final void process(){
	/*
		writeData();
		flushOutStream();
		*/
		if(parse != null){
			//while(parse.getPackets());
			if(!isAlive()){
				destruct();
				return;
			}
			try{
				final int AVAILABLE = getInputStream().available();
				if(AVAILABLE > 1){
					final int PACKET_TYPE = getPacketType();
					final long PACKET_SIZE = getPacketSize();
					if(PACKET_TYPE != -1){
						//System.out.println("Parse Packet["+PACKET_TYPE+"]["+PACKET_SIZE+"]");
						parse.processPacket(PACKET_TYPE,PACKET_SIZE);
					}
				}
			}catch(final Exception e){ destruct(e); }
			//while(parse.getPackets());
		}
	}
	
	public final Cryption getOutCryption(){
		return outStreamDecryption;
	}
	
	public final void destruct(){
		destruct(null);
	}
	
	public final boolean isAlive(){
		if(initlized){
			if(outStream == null){
				return false;
			}else if(inStream == null){
				return false;
			}
		}
        return (isConnected() && !isInputShutdown() && !isOutputShutdown() && !isClosed());
	}
	
	public final void destruct(final Exception ex){
		running = false;
		if(ex != null){
			ex.printStackTrace();
		}
		if(parse != null){
			parse = null;
		}
		if(outStream != null){
			outStream.destruct();
			outStream = null;
		}
		if(inStream != null){
			inStream = null;
		}
		if(outStreamDecryption != null){
			outStreamDecryption = null;
		}
		if(inStreamDecryption != null){
			inStreamDecryption = null;
		}
		if(!isInputShutdown()){
			try {
				shutdownInput();
			} catch(Exception e) { }
		}
		if(!isOutputShutdown()){
			try {
				shutdownOutput();
			} catch(Exception e) { }
		}
		if(!isClosed()){
			try {
				close();
			} catch(Exception e) { }
		}
	}
}
