package com.master.net.socket.stream.out.packets;

import java.io.IOException;
import com.master.net.socket.stream.out.Out;
import com.master.net.socket.ClientSocket;

public final class SendServerSessionKey{

	public SendServerSessionKey(final ClientSocket SOCK, final long KEY) throws IOException{
		final Out OUT = new Out();
		OUT.writeByte(0); //type 0 send serversession key
		OUT.writeLong(KEY);
		SOCK.writePacket(0,OUT);
	}
	
}
