package com.master.net.socket.stream.out.packets;

import java.io.IOException;
import com.master.net.socket.stream.out.Out;
import com.master.net.socket.ClientSocket;

public final class RequestServerSessionKey{

	public RequestServerSessionKey(final ClientSocket SOCK) throws IOException{
		final Out OUT = new Out();
		OUT.writeByte(0); //type 0 get serversession key
		SOCK.writePacket(0,OUT);
	}
	
}
