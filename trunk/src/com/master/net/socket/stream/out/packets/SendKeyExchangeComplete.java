package com.master.net.socket.stream.out.packets;

import java.io.IOException;
import com.master.net.socket.stream.out.Out;
import com.master.net.socket.ClientSocket;

public final class SendKeyExchangeComplete{

	public SendKeyExchangeComplete(final ClientSocket SOCKET, final long CLIENT_KEY, final long SERVER_KEY) throws IOException{
		final Out OUT = new Out();
		OUT.writeByte(1); //type 1 key exchange complete
		SOCKET.writePacket(0,OUT);
		//SOCKET.setCryption(CLIENT_KEY,SERVER_KEY);
	}
	
}
