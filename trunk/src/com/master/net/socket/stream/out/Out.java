package com.master.net.socket.stream.out;

import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import com.master.net.MAC;
import com.master.net.IP;
import com.master.net.socket.stream.Cryption;

import java.io.ByteArrayOutputStream;

public final class Out{

	//private final Map<Integer, Byte> BUFFER = new HashMap<Integer, Byte>();
	private final OutputStream OUTPUT_STREAM;
	private final ByteArrayOutputStream BUFFER;
	//private static final int frameStackSize = 10;
	//private int frameStackPtr = -1;
	//private int frameStack[] = new int[frameStackSize];
	//private byte buffer[] = null;
    	private int currentOffset = 0;
    	//private int bitMaskOut[] = new int[32];
    	private Cryption packetEncryption = null;
	
	public final void setCryption(final Cryption cry){
		packetEncryption = cry;
	}
	
	public final Cryption getCryption(){
		return packetEncryption;
	}
	
	//public final byte getByte(final int POSITION){
	//	return BUFFER.get(POSITION);
	//}
/*
	public final byte[] getBytes(){
		final byte[] BYTES = new byte[currentOffset];
		for(long i = 0; i < currentOffset; i++){
			BYTES[i] = BUFFER.get(i).byteValue();
		}
		return BYTES;
	}
*/
	public final void writeTo(final Out OUT) throws IOException{
		for(final byte BYTE : BUFFER.toByteArray()){
			OUT.writeByte(BYTE);
		}
	}

	public final void writeTo(final OutputStream OUTPUT_STREAM) throws IOException{
		BUFFER.writeTo(OUTPUT_STREAM);
		//for(final byte BYTE : BUFFER.values()){
		//	try{
		//		OUTPUT_STREAM.write(BYTE);
		//	}catch(final Exception e){}
		//}
	}

	public final void destruct(){
		//frameStackPtr = -1;
		//frameStack = null;
		//buffer = null;
		currentOffset = 0;
		//bitPosition = 0;
		//bitMaskOut = null;
		packetEncryption = null;
	}

	public final void reset(){
		BUFFER.reset();
		currentOffset = 0;
		//for(int i = 0; i < 32; i++)
		//	bitMaskOut[i] = (1 << i) - 1;
	}

	public final void setCurrentOffset(final int POSITION){
		currentOffset = POSITION;
	}

	public final long getCurrentOffset(){
		return currentOffset;
	}

	public final byte[] getBuffer(){
		return BUFFER.toByteArray();
	}

	public Out(final Cryption cry){
		OUTPUT_STREAM = null;
		BUFFER = new ByteArrayOutputStream();
		packetEncryption = cry;
	}

	public Out(){
		OUTPUT_STREAM = null;
        	BUFFER = new ByteArrayOutputStream();
   	}

	public Out(final int SIZE){
		OUTPUT_STREAM = null;
		BUFFER = new ByteArrayOutputStream(SIZE);
	}

	public Out(final byte BYTES[]) throws IOException{
		OUTPUT_STREAM = null;
		BUFFER = new ByteArrayOutputStream(BYTES.length);
		writeBytes(BYTES);
	}

	public Out(final OutputStream OUTPUT_STREAM){
		this.OUTPUT_STREAM = OUTPUT_STREAM;
		BUFFER = null;
	}

	public Out(final String FILE) throws FileNotFoundException{
		this.OUTPUT_STREAM = new FileOutputStream(FILE);
		BUFFER = null;
	}

	public final void write(final byte BYTE) throws IOException{ //Max unsigned 256 | Max signed 128
		writeByte(BYTE);
	}

	public final void write(final int INTEGER) throws IOException{
		writeInteger(INTEGER);
	}

	public final void write(final long LONG) throws IOException{
		writeLong(LONG);
	}

	public final void writeByte(final byte BYTE) throws IOException{ //Max unsigned 256 | Max signed 128
		if(OUTPUT_STREAM != null){
			OUTPUT_STREAM.write((int)BYTE);
		}else{
			BUFFER.write(BYTE);
			//BUFFER.put(currentOffset++, new Byte(BYTE));
		}
	}

	public final void writeByte(final int INTEGER) throws IOException{ //Max unsigned 256 | Max signed 128
		if(OUTPUT_STREAM != null){
            OUTPUT_STREAM.write((byte)INTEGER);
        }else{
			BUFFER.write(INTEGER);
			//BUFFER.put(currentOffset++, new Byte((byte)INTEGER));
        }
	}

	public final void writeByte(final int INTEGER, final int SHIFT) throws IOException{ //Max unsigned 256 | Max signed 128
		if(OUTPUT_STREAM != null){
			OUTPUT_STREAM.write((byte)(INTEGER >> SHIFT));
		}else{
			BUFFER.write((INTEGER >> SHIFT));
			//BUFFER.put(currentOffset++, new Byte((byte)(INTEGER >> SHIFT)));
		}
	}

	public final void writeByte(final long LONG) throws IOException{
		if(OUTPUT_STREAM != null){
			OUTPUT_STREAM.write((byte)(int)LONG);
		}else{
			BUFFER.write((int)(LONG));
			//BUFFER.put(currentOffset++, new Byte((byte)(int)LONG));
		}
	}

	public final void writeByte(final long LONG, final int SHIFT) throws IOException{
		if(OUTPUT_STREAM != null){
			OUTPUT_STREAM.write((byte)(int)(LONG >> SHIFT));
		}else{
			BUFFER.write((int)(LONG >> SHIFT));
			//BUFFER.put(currentOffset++, new Byte((byte)(int)(LONG >> SHIFT)));
		}
	}
/*
    public final void writeByteA(final int INTEGER){
	writeByte(INTEGER+128);
    }

    public final void writeByteS(final int INTEGER){
	writeByte(128-INTEGER);
    }

    public final void writeByteC(final int INTEGER){
	writeByte(-INTEGER);
    }

    public final void writeWordBigEndianA(final int INTEGER){
	writeByteA(INTEGER);
	writeByte(INTEGER,8);
    }

    public final void writeWordA(final int INTEGER){
	writeByte(INTEGER,8);
	writeByteA(INTEGER);
    }

    public final void writeWordBigEndian_dup(final int INTEGER){
	writeByte(INTEGER);
	writeByte(INTEGER,8);
   }

    public final void writeDWord_v1(final int INTEGER){
	writeByte(INTEGER,8);
	writeByte(INTEGER);
	writeByte(INTEGER,24);
	writeByte(INTEGER,16);
    }

    public final void writeDWord_v2(final int INTEGER){
	writeByte(INTEGER,16);
	writeByte(INTEGER,24);
	writeByte(INTEGER);
	writeByte(INTEGER,8);
    }
*/
	/* INCOMPLETE
    public final void writeBytes_reverse(final byte BYTES[], final int i, final int j){
	for(final byte BYTE : BYTES){
		writeByte(BYTE);
	}
		try{
			for(int k = (j + i) - 1; k >= j; k--)
				buffer[currentOffset++] = abyte0[k];
		}catch(Exception ex) {return;}
    }

    public final void writeBytes_reverseA(byte abyte0[], final int i, final int j){
		try{
			for(int k = (j + i) - 1; k >= j; k--)
				buffer[currentOffset++] = (byte)(abyte0[k] + 128);
		}catch(Exception ex) {return;}
    }
*/
/* OLD RUNESCAPE CODE
    public final void createFrame(final int id){
		try{
			buffer[currentOffset++] = (byte)(id);
		}catch(Exception ex) {return;}
	}
	
	public final void createFrameVarSize(final int id){// creates a variable sized frame
		try{
			buffer[currentOffset++] = (byte)(id);
			buffer[currentOffset++] = 0;		// placeholder for size byte
			if(frameStackPtr >= frameStackSize-1) {
				//throw new RuntimeException("Stack overflow");
				return;
			}
			else frameStack[++frameStackPtr] = currentOffset;
		}catch(Exception ex) {return;}
	}
	
	public final void createFrameVarSizeWord(final int id){ // creates a variable sized frame
		try{
			buffer[currentOffset++] = (byte)(id);
			writeWord(0);		// placeholder for size word
			if(frameStackPtr >= frameStackSize-1) {
				//throw new RuntimeException("Stack overflow");
				return;
			}
			else frameStack[++frameStackPtr] = currentOffset;
		}catch(Exception ex) {return;}
	}

	public final void endFrameVarSize(){ // ends a variable sized frame
		try{
			//if(frameStackPtr < 0) throw new RuntimeException("Stack empty");
			if(frameStackPtr < 0) return;
			else writeFrameSize(currentOffset - frameStack[frameStackPtr--]);
		}catch(Exception ex) {return;}
	}

	public final void endFrameVarSizeWord(){ // ends a variable sized frame
		try{
			//if(frameStackPtr < 0) throw new RuntimeException("Stack empty");
			if(frameStackPtr < 0) return;
			else writeFrameSizeWord(currentOffset - frameStack[frameStackPtr--]);
		}catch(Exception ex) {return;}
	}
*/
	public final void writeBoolean(final boolean BOOL) throws IOException{ //Max unsigned 256 | Max signed 128
		writeByte((BOOL ? 1 : 0));
	}
	
	public final void writeDouble(final double DOUBLE) throws IOException{
		try{
			writeLong(Double.doubleToRawLongBits(DOUBLE));
		}catch(Exception ex) {}
	}
	
	public final void writeFloat(final float FLOAT) throws IOException{
		try{
			writeInteger(Float.floatToIntBits(FLOAT));
		}catch(Exception ex) {}
	}
	
	public final void writeWord(final int INTEGER) throws IOException{ //Max unsigned 256^2 | Max signed 128^2
		writeByte(INTEGER,8);
		writeByte(INTEGER);
	}

	public final void writeWordBigEndian(final int INTEGER) throws IOException{ //Max unsigned 256^2 | Max signed 128^2
		writeByte(INTEGER);
		writeByte(INTEGER,8);
	}

    	public final void write3Byte(final int INTEGER) throws IOException{ //Max unsigned 256^3 | Max signed 128^3
		writeByte(INTEGER,16);
		writeByte(INTEGER,8);
		writeByte(INTEGER);
	}

	public final void write3Byte(final byte BYTE_1, final byte BYTE_2, final byte BYTE_3) throws IOException{
		writeByte(BYTE_1);
		writeByte(BYTE_2);
		writeByte(BYTE_3);
	}

	public final void write3Byte(final int INTEGER_1, final int INTEGER_2, final int INTEGER_3) throws IOException{
		writeByte(INTEGER_1,16);
		writeByte(INTEGER_2,8);
		writeByte(INTEGER_3);
	}

	public final void writeMAC(final MAC mac) throws IOException{
		if(mac == null){
			return;
		}
		writeMAC(mac.toLong());
	}
	
	public final void writeMAC(final long MAC_ADDRESS) throws IOException{
		writeByte(MAC_ADDRESS,40);
		writeByte(MAC_ADDRESS,32);
		writeByte(MAC_ADDRESS,24);
		writeByte(MAC_ADDRESS,16);
		writeByte(MAC_ADDRESS,8);
		writeByte(MAC_ADDRESS);
	}

	public final void writeIP(final IP ip) throws IOException{
		if(ip == null){
			return;
		}
		writeInteger(ip.toInt());
	}

	public final void writeInteger(final int INTEGER) throws IOException{ //Max unsigned 256^4 | Max signed 128^4
		writeByte(INTEGER,24);
		writeByte(INTEGER,16);
		writeByte(INTEGER,8);
		writeByte(INTEGER);
	}
/*
	public final void writeDWordBigEndian(final int INTEGER){ //Max unsigned 256^4 | Max signed 128^4
		writeByte(INTEGER);
		writeByte(INTEGER,8);
		writeByte(INTEGER,16);
		writeByte(INTEGER,24);
	}
*/
	public final void writeLong(final long LONG) throws IOException{ //Max unsigned 256^8 | Max signed 128^8
		writeByte(LONG,56);
		writeByte(LONG,48);
		writeByte(LONG,40);
		writeByte(LONG,32);
		writeByte(LONG,24);
		writeByte(LONG,16);
		writeByte(LONG,8);
		writeByte(LONG);
	}

	public final void writeString(final String STR) throws IOException{
		writeString(STR,false);
	}

	public final void writeString(final String STR, final boolean ENCRYPT) throws IOException{
		final byte[] BYTES = STR.getBytes();
		writeInteger(BYTES.length);
		if(ENCRYPT){
			if(packetEncryption != null){
				System.out.print("Socket.out.writeString["+STR+"][Encrypted]");
			}else{
				System.out.print("Socket.out.writeString["+STR+"]");
			}
		}else{
			System.out.print("Socket.out.writeString["+STR+"]");
		}
		for(final byte BYTE : BYTES){
			if(!ENCRYPT){
				writeByte(BYTE);
				System.out.print("["+BYTE+"]");
			}else{
				if(packetEncryption != null){
					final byte ENCRYPTED_BYTE = (byte)(BYTE+packetEncryption.getNextKey());
					writeByte(ENCRYPTED_BYTE);
					System.out.print("["+ENCRYPTED_BYTE+"]");
				}else{
					writeByte(BYTE);
					System.out.print("["+BYTE+"]");
				}
			}
		}
		System.out.print("\n");
		//s.getBytes(0, s.length(), buffer, currentOffset);
               	//currentOffset += s.length();
	}

    public final void writeBytes(final byte BYTES[]) throws IOException{
	for(final byte BYTE : BYTES){
		writeByte(BYTE);
	}
    }
/* OLD RUNESCAPE CODE
    public final void writeFrameSize(final int i){
	try{
	        buffer[currentOffset - i - 1] = (byte)i;
	    }catch(Exception ex) {return;}
	}

    public final void writeFrameSizeWord(final int i){
		try{
	        buffer[currentOffset - i - 2] = (byte)(i >> 8);
	        buffer[currentOffset - i - 1] = (byte)i;
	    }catch(Exception ex) {return;}
	}
*/
/* BIT ACCESS TODO FIX
	public final long getBitPosition(){
		return (currentOffset*8);
	}

	public final void writeBits(int numBits, final int value){
		try{
			int bitPosition = getBitPosition();
			int bytePos = bitPosition >> 3;
			int bitOffset = 8 - (bitPosition & 7);
			bitPosition += numBits;

			for(; numBits > bitOffset; bitOffset = 8) {
				buffer[bytePos] &= ~ bitMaskOut[bitOffset];		// mask out the desired area
				buffer[bytePos++] |= (value >> (numBits-bitOffset)) & bitMaskOut[bitOffset];

				numBits -= bitOffset;
			}
			if(numBits == bitOffset) {
				buffer[bytePos] &= ~ bitMaskOut[bitOffset];
				buffer[bytePos] |= value & bitMaskOut[bitOffset];
			}
			else {
				buffer[bytePos] &= ~ (bitMaskOut[numBits]<<(bitOffset - numBits));
				buffer[bytePos] |= (value&bitMaskOut[numBits]) << (bitOffset - numBits);
			}
			currentOffset = (bitPosition + 7) / 8;
		}catch(Exception ex) {return;}
	}
*/
}
