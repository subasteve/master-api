package com.master.net.socket.stream.out.packets;

import java.io.IOException;
import com.master.net.socket.stream.out.Out;
import com.master.net.socket.ClientSocket;

public final class SendUserPassResult{

	public SendUserPassResult(final ClientSocket SOCKET, final boolean LOGGED_IN) throws IOException{
		final Out OUT = new Out();
		OUT.writeByte(2); //type 2 User & Pass Result
		OUT.writeBoolean(LOGGED_IN);
		SOCKET.writePacket(0,OUT);
	}
	
}
