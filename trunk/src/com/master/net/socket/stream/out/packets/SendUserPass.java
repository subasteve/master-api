package com.master.net.socket.stream.out.packets;

import java.io.IOException;
import com.master.net.socket.stream.out.Out;
import com.master.net.socket.ClientSocket;

public final class SendUserPass{

	public SendUserPass(final ClientSocket SOCK, final String USER, final String PASS) throws IOException{
		final Out OUT = new Out();
		OUT.writeByte(2); //type 2 send username & password
		OUT.writeString(USER,false);
		OUT.writeString(PASS,false);
		SOCK.writePacket(0,OUT);
	}
	
}
