package com.master.net.socket.stream.in.packets;

import java.io.IOException;
import com.master.net.socket.ClientSocket;
import com.master.net.socket.stream.out.packets.SendServerSessionKey;
import com.master.net.socket.stream.out.packets.SendKeyExchangeComplete;
import com.master.net.socket.stream.out.packets.SendUserPassResult;

public abstract class LoginServer{
	
	private final ClientSocket SOCKET;
	private final long SERVER_KEY;
	
	public final boolean validNick(final String USER){
		try{
			if(USER == null) return false;
			final char[] VALID_CHARS = getVaildChars();
			if(VALID_CHARS == null) return true;
			for(int i = 0; i < USER.length(); i++){
				boolean valid = false;
				for(int x = 0; x < VALID_CHARS.length; x++){
					if(USER.charAt(i) == VALID_CHARS[x]){
						valid = true;
						break;
					}
				}
				if(!valid){
					System.out.println("Char: "+USER.charAt(i)+" Not Valid");
					return false;
				}
			}
			return true;
		}catch(Exception e) { return false; }
	}
	
	public abstract char[] getVaildChars();
	public abstract boolean login(final String USER, final String PASS);

	public LoginServer(final ClientSocket SOCKET){
		this.SOCKET = SOCKET;
		SERVER_KEY = ((long)(java.lang.Math.random() * 99999999D) << 32) + (long)(java.lang.Math.random() * 99999999D);
	}
	
	public final void parse() throws IOException{
		final int TYPE = SOCKET.getInStream().readUnsignedByte(); // 0 = key exchange | 1 = key exchange & cryption set | 2 = login
		switch(TYPE){
			case 0:
				System.out.println("Sending Server Key...");
				new SendServerSessionKey(SOCKET,SERVER_KEY);
			break;
			case 1:
				System.out.println("Recieving Client Key...");
				final long CLIENT_KEY = SOCKET.getInStream().readLong();
				System.out.println("Sending key exchange complete...");
				new SendKeyExchangeComplete(SOCKET,CLIENT_KEY,SERVER_KEY);
			break;
			case 2:
				System.out.println("Recieving User & Pass..");
				final String USER = SOCKET.getInStream().readString();
				final String PASS = SOCKET.getInStream().readString();
				System.out.println("U["+USER+"]P["+PASS+"]Sending User & Pass result...");
				if(validNick(USER)){
					if(login(USER,PASS)){
						new SendUserPassResult(SOCKET,true);
					}
				}else{
					System.out.println("Non-Valid Username...");
					new SendUserPassResult(SOCKET,false);
				}
			break;
			default:
				System.out.println("Unknown login type ["+TYPE+"] killing socket...");
				SOCKET.destruct();
			break;
		}
	}

}
