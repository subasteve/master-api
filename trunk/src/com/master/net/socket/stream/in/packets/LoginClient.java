package com.master.net.socket.stream.in.packets;

import java.io.IOException;
import com.master.net.socket.ClientSocket;
import com.master.net.socket.stream.out.packets.SendClientSessionKey;
import com.master.net.socket.stream.out.packets.SendUserPass;
import com.master.net.socket.stream.out.packets.RequestServerSessionKey;

public abstract class LoginClient{

	private final ClientSocket SOCKET;
	private final long CLIENT_KEY;
	private long serverKey = -1;

	public final boolean validNick(final String USER){
		try{
			if(USER == null) return false;
			final char[] VALID_CHARS = getVaildChars();
			if(VALID_CHARS == null) return true;
			for(int i = 0; i < USER.length(); i++){
				boolean valid = false;
				for(int x = 0; x < VALID_CHARS.length; x++){
					if(USER.charAt(i) == VALID_CHARS[x]){
						valid = true;
						break;
					}
				}
				if(!valid)
					return false;
			}
			return true;
		}catch(Exception e) { return false; }
	}
	
	public abstract char[] getVaildChars();
	public abstract String getUser();
	public abstract String getPass();
	public abstract void login(final boolean SUCCESS);
	
	public LoginClient(final ClientSocket SOCKET){
		this.SOCKET = SOCKET;
		CLIENT_KEY = ((long)(java.lang.Math.random() * 99999999D) << 32) + (long)(java.lang.Math.random() * 99999999D);
	}
	
	public LoginClient(final ClientSocket SOCKET, final boolean AUTO_START) throws IOException{
		this.SOCKET = SOCKET;
		CLIENT_KEY = ((long)(java.lang.Math.random() * 99999999D) << 32) + (long)(java.lang.Math.random() * 99999999D);
		if(AUTO_START){
			start();
		}
	}
	
	public final void start() throws IOException{
		new RequestServerSessionKey(SOCKET);
	}
	
	public final void parse() throws IOException{
		final int TYPE = SOCKET.getInStream().readUnsignedByte(); // 0 = key exchange | 1 = key exchange & cryption set | 2 = login
		switch(TYPE){
			case 0:
				System.out.println("Recieved Server Key sending Client Key...");
				serverKey = SOCKET.getInStream().readLong();
				new SendClientSessionKey(SOCKET,CLIENT_KEY);
			break;
			case 1:
				if(serverKey != -1){
					System.out.println("Sending User & Pass...");
					//SOCKET.setCryption(CLIENT_KEY,serverKey);
					new SendUserPass(SOCKET,getUser(),getPass());
				}else{
					System.out.println("No Server Key Recieved killing socket....");
					SOCKET.destruct();
				}
			break;
			case 2:
				final boolean SUCCESS = SOCKET.getInStream().readBoolean();
				login(SUCCESS);
			break;
			default:
				System.out.println("Unknown login type ["+TYPE+"] killing socket...");
				SOCKET.destruct();
			break;
		}
	}
	
}
