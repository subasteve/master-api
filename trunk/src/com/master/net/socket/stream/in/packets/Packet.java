package com.master.net.socket.stream.in.packets;
import com.master.net.socket.ClientSocket;

public interface Packet{
	int read[] = new int[15];
	long readLong[] = new long[2];
	String readString[] = new String[1];
	void process(final ClientSocket sock);
}