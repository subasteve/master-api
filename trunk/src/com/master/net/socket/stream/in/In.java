package com.master.net.socket.stream.in;

import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import com.master.net.IP;
import com.master.net.MAC;
import com.master.net.socket.stream.Cryption;
//import java.util.Map;
//import java.util.HashMap;

public final class In{

	//private final Map<Integer, Byte> BUFFER = new HashMap<Integer, Byte>();
	private final InputStream INPUT_STREAM;
	private byte buffer[] = null;
	private int currentOffset = 0;
	//private final int bufferSize = 5000;
	private Cryption packetEncryption = null;

	public final void setCurrentOffset(int i){
		currentOffset = i;
	}
/*
	public final void setBufferSize(final int size){
		buffer = new byte[size];
	    	currentOffset = 0;
	}
*/
	public final void setCryption(final Cryption CRYPTION){
		packetEncryption = CRYPTION;
	}

	public final void reset(){
		buffer = new byte[buffer.length];
	    currentOffset = 0;
	}
	
	public final void reset(final int SIZE){
		buffer = new byte[SIZE];
	    currentOffset = 0;
	}

	public final byte getByte(final int POSITION){
		return buffer[POSITION];
		//return BUFFER.get(POSITION);
	}

	public final int getCurrentOffset(){
		return currentOffset;
	}
	/*
	public final Map<Integer, Byte> getBuffer(){
                return BUFFER;
        }
	*/
	public final void writeBytes(final byte BYTES[]){
        for(final byte BYTE : BYTES){
			writeByte(BYTE);
        }
    }
		
	public final void writeByte(final byte BYTE){ //Max unsigned 256 | Max signed 128
		//BUFFER.put(currentOffset++, new Byte(BYTE));
		buffer[currentOffset++] = BYTE;
	}
	
	public In(){
		INPUT_STREAM = null;
		reset(5000);
	}

	public In(final byte[] BYTES){
		INPUT_STREAM = null;
		reset(BYTES.length);
		writeBytes(BYTES);
	}

	public In(final InputStream INPUT_STREAM){
		this.INPUT_STREAM = INPUT_STREAM;
	}

	public In(final String file) throws IOException{
		INPUT_STREAM = new FileInputStream(file);
	}

	public In(final File FILE) throws FileNotFoundException{
		INPUT_STREAM = new FileInputStream(FILE);
	}

	public final int readUnsignedByte() throws IOException{ //Max unsigned 256
		if(INPUT_STREAM != null){
			return (INPUT_STREAM.read() & 0xff);
		}
		return (buffer[currentOffset++] & 0xff);
	}

	public final int readUnsignedByte(final int SHIFT)  throws IOException{ //Max unsigned 256
		if(INPUT_STREAM != null){
			return ((INPUT_STREAM.read() & 0xff) << SHIFT);
		}
		return ((buffer[currentOffset++] & 0xff) << SHIFT);
	}

	public final long readUnsignedByteLong()  throws IOException{ //Max unsigned 256
		if(INPUT_STREAM != null){
			return (INPUT_STREAM.read() & 0xffl);
		}
		return (buffer[currentOffset++] & 0xffl);
	}

	public final long readUnsignedByteLong(final int SHIFT)  throws IOException{ //Max unsigned 256
		if(INPUT_STREAM != null){
			return ((INPUT_STREAM.read() & 0xffl) << SHIFT);
		}
		return ((buffer[currentOffset++] & 0xffl) << SHIFT);
	}

	public final int readSignedByte()  throws IOException{ //Max signed 128
		if(INPUT_STREAM != null){
			return INPUT_STREAM.read();
		}
        return buffer[currentOffset++];
	}

	public final int readSignedByte(final int SHIFT) throws IOException{ //Max signed 128
		if(INPUT_STREAM != null){
			return (INPUT_STREAM.read() << SHIFT);
		}
		return (buffer[currentOffset++] << SHIFT);
	}

	public final int readInteger() throws IOException{
		int integer = 0;
		integer += readUnsignedByte(24);
		integer += readUnsignedByte(16);
		integer += readUnsignedByte(8);
		integer += readUnsignedByte();
		return integer;
	}

	public final long readLong() throws IOException{
		long Long = 0;
        Long += readUnsignedByte(56);
        Long += readUnsignedByte(48);
        Long += readUnsignedByte(40);
        Long += readUnsignedByte(32);
        Long += readUnsignedByte(24);
        Long += readUnsignedByte(16);
        Long += readUnsignedByte(8);
        Long += readUnsignedByte();
        return Long;
	}

	public final boolean readBoolean() throws IOException{
		if(INPUT_STREAM != null){
			return (INPUT_STREAM.read() != 0);
		}
		return (buffer[currentOffset++] != 0);
	}

	public final double readDouble() throws IOException{
		try{
			return Double.longBitsToDouble(readLong());
		}catch(Exception ex) {return 0;}
	}

	public final float readFloat() throws IOException{
		try{
			return Float.intBitsToFloat(readInteger());
		}catch(Exception ex) {return 0;}
	}

	public final int readUnsignedWord() throws IOException{ //Max unsigned 256^2
		int integer = 0;
		integer += readUnsignedByte(8);
		integer += readUnsignedByte();
		return integer;
	}

	public final int readSignedWord() throws IOException{ //Max signed 128^2
		int integer = 0;
		integer += readSignedByte(8);
		integer += readSignedByte();
		return integer;
	}
	
	public final int read3Bytes() throws IOException{ //Max unsigned 256^3
		int integer = 0;
		integer += readUnsignedByte(16);
		integer += readUnsignedByte(8);
		integer += readUnsignedByte();
		return integer;
	}
	
	public final MAC getMAC(){
		try{
			final long address = readMAC();
			if(address != 0){
				return new MAC(address);
			}
		}catch(final Exception e){}
		return null;
	}

	public final long readMAC() throws IOException{
		long address = 0;
		address += readUnsignedByteLong(40);
		address += readUnsignedByteLong(32);
		address += readUnsignedByteLong(24);
		address += readUnsignedByteLong(16);
		address += readUnsignedByteLong(8);
		address += readUnsignedByteLong();
		return address;
	}

	public final IP getIP(){
		try{
			final int address = readInteger();
			if(address != 0){
	                        return new IP(address);
                	}
		}catch(final Exception e){}
		return null;
	}
/*
	public final int readDWord() throws IOException{ //Max unsigned 256^4
		int integer = 0;
		integer += readUnsignedByte(24);
		integer += readUnsignedByte(16);
		integer += readUnsignedByte(8);
		integer += readUnsignedByte();
		return integer;
	}

	public final long readQWord() throws IOException{ //Max unsigned 256^8
		long Long = 0;
		Long += readUnsignedByte(56);
		Long += readUnsignedByte(48);
		Long += readUnsignedByte(40);
		Long += readUnsignedByte(32);
		Long += readUnsignedByte(24);
		Long += readUnsignedByte(16);
		Long += readUnsignedByte(8);
		Long += readUnsignedByte();
		return Long;
	}
*/
	public final String readString(){
		return readString(false);
	}

	public final String readString(final boolean ENCRYPT){
		try{
			final int STRING_LENGTH = readInteger();
			final byte[] BYTES = new byte[STRING_LENGTH];
			int tmpOffset = 0;
			for(int i = 0; i < STRING_LENGTH; i++){
				if(!ENCRYPT){
					final byte BYTE = (byte)readSignedByte();
					BYTES[tmpOffset++] = BYTE;
				}else{
					if(packetEncryption != null){
						final byte BYTE = (byte)(readSignedByte()+packetEncryption.getNextKey());
                        BYTES[tmpOffset++] = BYTE;
					}else{
						final byte BYTE = (byte)readSignedByte();
                        BYTES[tmpOffset++] = BYTE;
					}
				}
			}
			return new String(BYTES);
		}catch(final Exception e){}
		return "";
	}

/*
    public final byte[] readBytes(byte abyte0[], int i, int j){
		try{
			for(int k = j; k < j + i; k++)
				abyte0[k] = buffer[currentOffset++];
			return abyte0;
		}catch(Exception ex) {return null;}
    }
	
	public final byte[] readBytes_reverseA(byte abyte0[], int i, int j){
		try{
			for(int k = (j + i) - 1; k >= j; k--)
				abyte0[k] = (byte)(buffer[currentOffset++] - 128);
			return abyte0;
		}catch(Exception ex) {return null;}
    }
	
	public final byte[] readBytes_reverse(byte abyte0[], int i, int j){
		try{
			for(int k = (j + i) - 1; k >= j; k--)
				abyte0[k] = buffer[currentOffset++];
			return abyte0;
		}catch(Exception ex) {return null;}
    }
	public final int readDWord_v1() throws IOException{ //Max unsigned 256^4
		int integer = 0;
		integer += readUnsignedByte(8);
		integer += readUnsignedByte();
		integer += readUnsignedByte(24);
		integer += readUnsignedByte(16);
		return integer;
	}

	public final int readDWord_v2() throws IOException{ //Max unsigned 256^4
		int integer = 0;
		integer += readUnsignedByte(16);
		integer += readUnsignedByte(24);
		integer += readUnsignedByte();
		integer += readUnsignedByte(8);
		return integer;
	}

	public final int readSignedWordBigEndian() throws IOException{ //Max unsigned 256^2
		int integer = 0;
		integer += readSignedByte();
		integer += readSignedByte(8);
		return integer;
	}

    public final int readSignedWordA(){ //Max unsigned 256^2
		int integer = 0;
		integer += 
		try{
			currentOffset += 2;
			int i = ((buffer[currentOffset - 2] & 0xff) << 8) + (buffer[currentOffset - 1] - 128 & 0xff);
			if(i > 32767)
				i -= 0x10000;
			return i;
		}catch(Exception ex) {return 0;}
    }

    public final int readSignedWordBigEndianA(){ //Max unsigned 256^2
		try{
		   currentOffset += 2;
			int i = ((buffer[currentOffset - 1] & 0xff) << 8) + (buffer[currentOffset - 2] - 128 & 0xff);
			if(i > 32767)
				i -= 0x10000;
			return i;
		}catch(Exception ex) {return 0;}
    }

    public final int readUnsignedWordBigEndian(){ //Max unsigned 256^2
		try{
			currentOffset += 2;
			return ((buffer[currentOffset - 1] & 0xff) << 8) + (buffer[currentOffset - 2] & 0xff);
		}catch(Exception ex) {return 0;}
    }

    public final int readUnsignedWordA(){ //Max unsigned 256^2
		try{
			currentOffset += 2;
			return ((buffer[currentOffset - 2] & 0xff) << 8) + (buffer[currentOffset - 1] - 128 & 0xff);
		}catch(Exception ex) {return 0;}
    }

    public final int readUnsignedWordBigEndianA(){ //Max unsigned 256^2
		try{
			currentOffset += 2;
			return ((buffer[currentOffset - 1] & 0xff) << 8) + (buffer[currentOffset - 2] - 128 & 0xff);
		}catch(Exception ex) {return 0;}
    }
	
	public final byte readSignedByteA(){ //Max signed 128
		try{
			return (byte)(buffer[currentOffset++] - 128);
		}catch(Exception ex) {return 0;}
    }

    public final byte readSignedByteC(){ //Max signed 128
		try{
			return (byte)(-buffer[currentOffset++]);
		}catch(Exception ex) {return 0;}
    }

    public final byte readSignedByteS(){ //Max signed 128
		try{
			return (byte)(128 - buffer[currentOffset++]);
		}catch(Exception ex) {return 0;}
    }

    public final int readUnsignedByteA(){ //Max unsigned 256
		try{
			return buffer[currentOffset++] - 128 & 0xff;
		}catch(Exception ex) {return 0;}
    }

    public final int readUnsignedByteC(){ //Max unsigned 256
		try{
			return -buffer[currentOffset++] & 0xff;
		}catch(Exception ex) {return 0;}
    }

    public final int readUnsignedByteS(){ //Max unsigned 256
		try{
			return 128 - buffer[currentOffset++] & 0xff;
		}catch(Exception ex) {return 0;}
    }
*/
}
