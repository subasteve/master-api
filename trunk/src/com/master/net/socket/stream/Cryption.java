package com.master.net.socket.stream;

public final class Cryption{

    public Cryption(final int SESSION_KEY[]){
        CRYPT = new int[256];
        KEY_SET = new int[256];
        System.arraycopy(SESSION_KEY,0,KEY_SET,0,SESSION_KEY.length);
        initializeKeySet();
    }

	public Cryption(final long IN_KEY, final long OUT_KEY){
		final int SESSION_KEY[] = new int[4];
        SESSION_KEY[0] = (int)(IN_KEY >> 32);
        SESSION_KEY[1] = (int)IN_KEY;
        SESSION_KEY[2] = (int)(OUT_KEY >> 32);
        SESSION_KEY[3] = (int)OUT_KEY;
        CRYPT = new int[256];
        KEY_SET = new int[256];
        System.arraycopy(SESSION_KEY,0,KEY_SET,0,SESSION_KEY.length);
        initializeKeySet();
	}

    public final synchronized int getNextKey(){
        if(keyArrayIdx-- == 0){
            generateNextKeySet();
            keyArrayIdx = 255;
        }
        return KEY_SET[keyArrayIdx];
    }

    private final void generateNextKeySet(){
        cryptVar2 += ++cryptVar3;
        for(int i = 0; i < 256; i++){
            int j = CRYPT[i];
            if((i & 3) == 0)
                cryptVar1 ^= cryptVar1 << 13;
            else
            if((i & 3) == 1)
                cryptVar1 ^= cryptVar1 >>> 6;
            else
            if((i & 3) == 2)
                cryptVar1 ^= cryptVar1 << 2;
            else
            if((i & 3) == 3)
                cryptVar1 ^= cryptVar1 >>> 16;
            cryptVar1 += CRYPT[i + 128 & 0xff];
            int k;
            CRYPT[i] = k = CRYPT[(j & 0x3fc) >> 2] + cryptVar1 + cryptVar2;
            KEY_SET[i] = cryptVar2 = CRYPT[(k >> 8 & 0x3fc) >> 2] + j;
        }

    }

    private final void initializeKeySet(){
        int i1;
        int j1;
        int k1;
        int l1;
        int i2;
        int j2;
        int k2;
        int l = i1 = j1 = k1 = l1 = i2 = j2 = k2 = 0x9e3779b9;
        for(int i = 0; i < 4; i++){
            l ^= i1 << 11;
            k1 += l;
            i1 += j1;
            i1 ^= j1 >>> 2;
            l1 += i1;
            j1 += k1;
            j1 ^= k1 << 8;
            i2 += j1;
            k1 += l1;
            k1 ^= l1 >>> 16;
            j2 += k1;
            l1 += i2;
            l1 ^= i2 << 10;
            k2 += l1;
            i2 += j2;
            i2 ^= j2 >>> 4;
            l += i2;
            j2 += k2;
            j2 ^= k2 << 8;
            i1 += j2;
            k2 += l;
            k2 ^= l >>> 9;
            j1 += k2;
            l += i1;
        }

        for(int j = 0; j < 256; j += 8){
            l += KEY_SET[j];
            i1 += KEY_SET[j + 1];
            j1 += KEY_SET[j + 2];
            k1 += KEY_SET[j + 3];
            l1 += KEY_SET[j + 4];
            i2 += KEY_SET[j + 5];
            j2 += KEY_SET[j + 6];
            k2 += KEY_SET[j + 7];
            l ^= i1 << 11;
            k1 += l;
            i1 += j1;
            i1 ^= j1 >>> 2;
            l1 += i1;
            j1 += k1;
            j1 ^= k1 << 8;
            i2 += j1;
            k1 += l1;
            k1 ^= l1 >>> 16;
            j2 += k1;
            l1 += i2;
            l1 ^= i2 << 10;
            k2 += l1;
            i2 += j2;
            i2 ^= j2 >>> 4;
            l += i2;
            j2 += k2;
            j2 ^= k2 << 8;
            i1 += j2;
            k2 += l;
            k2 ^= l >>> 9;
            j1 += k2;
            l += i1;
            CRYPT[j] = l;
            CRYPT[j + 1] = i1;
            CRYPT[j + 2] = j1;
            CRYPT[j + 3] = k1;
            CRYPT[j + 4] = l1;
            CRYPT[j + 5] = i2;
            CRYPT[j + 6] = j2;
            CRYPT[j + 7] = k2;
        }

        for(int k = 0; k < 256; k += 8){
            l += CRYPT[k];
            i1 += CRYPT[k + 1];
            j1 += CRYPT[k + 2];
            k1 += CRYPT[k + 3];
            l1 += CRYPT[k + 4];
            i2 += CRYPT[k + 5];
            j2 += CRYPT[k + 6];
            k2 += CRYPT[k + 7];
            l ^= i1 << 11;
            k1 += l;
            i1 += j1;
            i1 ^= j1 >>> 2;
            l1 += i1;
            j1 += k1;
            j1 ^= k1 << 8;
            i2 += j1;
            k1 += l1;
            k1 ^= l1 >>> 16;
            j2 += k1;
            l1 += i2;
            l1 ^= i2 << 10;
            k2 += l1;
            i2 += j2;
            i2 ^= j2 >>> 4;
            l += i2;
            j2 += k2;
            j2 ^= k2 << 8;
            i1 += j2;
            k2 += l;
            k2 ^= l >>> 9;
            j1 += k2;
            l += i1;
            CRYPT[k] = l;
            CRYPT[k + 1] = i1;
            CRYPT[k + 2] = j1;
            CRYPT[k + 3] = k1;
            CRYPT[k + 4] = l1;
            CRYPT[k + 5] = i2;
            CRYPT[k + 6] = j2;
            CRYPT[k + 7] = k2;
        }

        generateNextKeySet();
        keyArrayIdx = 256;
    }

    private  int keyArrayIdx = 0;
    private final int KEY_SET[];
    private final int CRYPT[];
    private int cryptVar1 = 0;
    private int cryptVar2 = 0;
    private int cryptVar3 = 0;
}
