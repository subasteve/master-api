package com.master.net.socket;
import java.net.ServerSocket;
import java.net.SocketImpl;
import java.io.IOException;
import java.net.SocketException;
public final class ConnectionSocket extends ServerSocket{
	public ConnectionSocket(final int Port) throws IOException{
		super(Port);
	}
	public ConnectionSocket(final int Port, final int que) throws IOException{
		super(Port,que);
	}
	public final ClientSocket accept() throws IOException{
		if (isClosed())
			throw new SocketException("Socket is closed");
		if (!isBound())
			throw new SocketException("Socket is not bound yet");
		final ClientSocket s = new ClientSocket((SocketImpl) null);
		implAccept(s);
		return s;
	}
}