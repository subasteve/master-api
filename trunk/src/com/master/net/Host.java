package com.master.net;

import javax.naming.Context;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public final class Host{

	private final String HOST_NAME;
	private final List<String> IPS = new ArrayList<String>();
	private final List<String> NSS = new ArrayList<String>();
	private InitialDirContext idc;


	private final static Properties ENV = new Properties();
	static {
		ENV.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
	}
	private static final String ADDR_ATTRIB = "A";
	private static String[] ADDR_ATTRIBS = {ADDR_ATTRIB};
	private static final String NS_ATTRIB = "NS";
	private static String[] NS_ATTRIBS = {NS_ATTRIB};


	private void setupDns(){
		try {
			idc = new InitialDirContext(ENV);
		}catch (final Exception E){
			idc = null;
		}
	}

	public Host(final String HOST_NAME){
		this.HOST_NAME = HOST_NAME;
		setupDns();
	}

	public Host(final String HOST_NAME, final boolean LOOKUP_NS){
		this.HOST_NAME = HOST_NAME;
		setupDns();
		updateDNSRecords(LOOKUP_NS);
	}

	public Host(final String HOST_NAME, final String IP){
		this.HOST_NAME = HOST_NAME;
		IPS.add(IP);
		setupDns();
	}

	public final void updateDNSRecords(){
		updateDNSRecords(false);
	}

	public final void updateDNSRecords(final boolean LOOKUP_NS){
		if(!IPS.isEmpty()){
			IPS.clear();
		}
		for(String IP : getCurrentIPAddresses()){
			IPS.add(IP);
		}
		if(LOOKUP_NS){
			if(!NSS.isEmpty()){
				NSS.clear();
			}
			for(String IP : getCurrentNSAddresses()){
				NSS.add(IP);
			}
		}
	}

	public final List<String> getCurrentIPAddresses() {
		return getCurrentAddresses(ADDR_ATTRIBS,ADDR_ATTRIB);
	}

	public final List<String> getCurrentNSAddresses() {
		return getCurrentAddresses(NS_ATTRIBS,NS_ATTRIB);
	}

	public final List<String> getCurrentAddresses(final String[] TYPES, final String TYPE) {

		try {
			final List<String> ADDRESSES = new ArrayList<String>();
			final Attributes ATTRS = idc.getAttributes(HOST_NAME, TYPES);

			if(ATTRS != null && ATTRS.size() > 0){
				final Attribute ATTR = ATTRS.get(TYPE);

				if (ATTR != null) {
					for (int i = 0; i < ATTR.size(); i++) {
						ADDRESSES.add((String) ATTR.get(i));
					}
				}
			}

			return ADDRESSES;
		}catch (final Exception E){
		}
		return null;
	}

	public final List<String> getIPs(){
		return IPS;
	}

	public final List<String> getNSs(){
		return NSS;
	}

	public final String getHostName(){
		return HOST_NAME;
	}

}
