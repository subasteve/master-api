package com.master.net;

import java.util.Map;
import java.util.HashMap;
import com.master.util.Logger;

public final class HostCache{

	private final Map<String, Host> STORED_HOSTS = new HashMap<String, Host>();
	private final Logger LOGGER;
	private boolean debug = false;
	
	public HostCache(){
		LOGGER = null;
	}
	
	public HostCache(final Logger LOGGER){
		this.LOGGER = LOGGER;
	}
	
	public HostCache(final Logger LOGGER, final boolean DEBUG){
		this.LOGGER = LOGGER;
		debug = DEBUG;
	}
	
	public void debug(final boolean DEBUG){
		debug = DEBUG;
	}
	
	public final void put(final String HOST_NAME, final String IP){
		final Host HOST = STORED_HOSTS.get(HOST_NAME);
		if(HOST == null){
			STORED_HOSTS.put(HOST_NAME, new Host(HOST_NAME,IP));
		}
	}

    public boolean validIP (final String INPUT) {
        try {
            if (INPUT == null || INPUT.isEmpty() || !INPUT.contains(".")) {
                return false;
            }

            String[] parts = INPUT.split( "\\." );
            if ( parts.length != 4 ) {
                return false;
            }

            for ( String s : parts ) {
                int i = Integer.parseInt( s );
                if ( (i < 0) || (i > 255) ) {
                    return false;
                }
            }
            if(INPUT.endsWith(".")) {
                return false;
            }

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
	
	public final Host get(final String HOST_NAME){
		Host host = STORED_HOSTS.get(HOST_NAME);
		if(host != null){
			if(debug){
				if(LOGGER != null){
					LOGGER.writeLine("[HostCache][get]("+HOST_NAME+") HOST FOUND");
				}
			}
			return host;
		}
		if(debug){
			if(LOGGER != null){
				LOGGER.writeLine("[HostCache][get]("+HOST_NAME+") HOST NOT FOUND... CREATING");
			}
		}
        if(!validIP(HOST_NAME)) {
            if(debug){
                if(LOGGER != null){
                    LOGGER.writeLine("[HostCache][get]("+HOST_NAME+") HOST NOT IP... CREATING");
                }
            }
            do {
                host = new Host(HOST_NAME, true);
                try {
                    Thread.sleep(50);
                } catch (final Exception e) {
                }
            } while (host.getIPs().isEmpty());
        }else{
            if(debug){
                if(LOGGER != null){
                    LOGGER.writeLine("[HostCache][get]("+HOST_NAME+") HOST IP... CREATING");
                }
            }
            host = new Host(HOST_NAME, HOST_NAME);
        }
		if(debug){
			if(LOGGER != null){
				LOGGER.writeLine("[HostCache][get]("+HOST_NAME+") HOST CREATED");
			}
		}
		STORED_HOSTS.put(HOST_NAME, host);
		if(debug){
			if(LOGGER != null){
				LOGGER.writeLine("[HostCache][get]("+HOST_NAME+") HOST ADDED");
			}
		}
		return host;
	}

}
