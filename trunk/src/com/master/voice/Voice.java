package com.master.voice;

import com.master.util.SearchableString;
import com.master.util.Base64;
import com.master.util.Logger;
import com.master.net.WebPage;

public final class Voice{

	private static final WebPage WEBPAGE = new WebPage();

	public static final String getSpeechURL(final Logger LOGGER, final String TEXT_TO_SPEECH){
    	if(!WEBPAGE.hasCookies()){
			try{
				if(LOGGER != null){
					LOGGER.writeLine("Getting Cookies... ");
				}
				final String COOKIE = WEBPAGE.get("www.ivona.com/voicetest.php").position(0).position("Set-Cookie: ").getString("\r\n").trim();
				if(COOKIE.contains(";")){
					final String[] COOKIES = COOKIE.split(";");
					for(final String COOKIE_1 : COOKIES){
						if(COOKIE_1.contains("HttpOnly")){
							continue;
						}
						final String[] KEY_VALUE = COOKIE_1.split("=");
						WEBPAGE.addCookie(KEY_VALUE[0].trim(),KEY_VALUE[1].trim());
						if(LOGGER != null){
							LOGGER.writeLine(KEY_VALUE[0].trim()+"="+KEY_VALUE[1].trim());
						}
					}
				}else{
					final String[] KEY_VALUE = COOKIE.split("=");
					WEBPAGE.addCookie(KEY_VALUE[0].trim(),KEY_VALUE[1].trim());
					if(LOGGER != null){
						LOGGER.writeLine(KEY_VALUE[0].trim()+"="+KEY_VALUE[1].trim());
					}
				}
				WEBPAGE.addCookie("cc3_ext","us");
				WEBPAGE.addCookie("cc3","us");
			}catch(final Exception e){
				if(LOGGER != null){
					LOGGER.writeError(e);
				}
			}
		}
		
		SearchableString STRING_SEARCH = null;
		String url = null;
		if(LOGGER != null){
			LOGGER.writeLine("Getting Voice... "+TEXT_TO_SPEECH);
		}
		final String BASE64 = Base64.encodeBytes(TEXT_TO_SPEECH.getBytes());
		final String BASE64_EDITED = BASE64.substring(0,BASE64.length()-2)+"..";
		final String REQUEST_URL = "www.ivona.com/voicetest.php?rtr=1&t2r="+BASE64_EDITED+"&v2r=ZW5fdXNfc2FsbGk.&lang=us&add32cac3e068d7083578eab698775fb5f8=05491432c201292a3687740ad8c1c1d7";
		if(LOGGER != null){
			LOGGER.writeLine("BASE64: "+BASE64_EDITED);
			LOGGER.writeLine("URL: "+REQUEST_URL);
		}
		do{
			try{
					STRING_SEARCH = new SearchableString(WEBPAGE.get(REQUEST_URL).toString());
					break;
			}catch(final Exception e){
				if(LOGGER != null){
					LOGGER.writeError(e);
				}
				return null;
			}
		}while(true);
		if(LOGGER != null){
			LOGGER.writeLine("Voice Results: "+STRING_SEARCH.position(0).toString());
		}
		do{
			try{
        		if(STRING_SEARCH.position(0).contains("Location: ")){
        			final String LOCATION = STRING_SEARCH.position(0).position("Location: ").getString("\r\n");
        			if(LOGGER != null){
						LOGGER.writeLine("Voice Location: "+LOCATION);
					}
        			try{
        				if(LOCATION.contains("http://www.ivona.com")){
        					STRING_SEARCH = new SearchableString(WEBPAGE.get(LOCATION).toString());
        					if(LOGGER != null){
								LOGGER.writeLine("Voice Results2: "+STRING_SEARCH.position(0).toString());
							}
        				}else{
        					//String hostname = LOCATION.substring("http://".length());
        					//final int FILE_INDEX = hostname.indexOf("/");
        					//hostname = hostname.substring(0,FILE_INDEX);
        					//final Host HOST = new Host(hostname,java.net.InetAddress.getByName(hostname).getHostAddress());
        					//final String FILE = LOCATION.substring(FILE_INDEX);
        					//logDebug("Voice Location: "+hostname+FILE);
        					//STRING_SEARCH = new SearchableString(WEBPAGE.get(HOST,FILE,80,"PHPSESSID=hdb1ppjuhd8uu3d266c618q4d4; cc3_ext=en; cc3=en",null).toString());
        					if(LOGGER != null){
								LOGGER.writeLine("Adding to Voice que...");
							}
							url = LOCATION;
        					break;
        				}
        			}catch(final Exception e){
						if(LOGGER != null){
							LOGGER.writeError(e);
						}
        			}
        		}else{
        			break;
        		}
			}catch(final Exception e){
				if(LOGGER != null){
					LOGGER.writeError(e);
				}
				break;
			}
		}while(true);
		if(LOGGER != null){
			LOGGER.writeLine("Voice Done...");
		}
		return url;
		//if(STRING_SEARCH.position(0).contains("Content-Type: ") && STRING_SEARCH.contains("audio")){
    	//	logDebug("BINGO");
    	//}
    }

}