package com.master.automation;

import java.awt.Color;
import java.awt.Point;
import java.awt.Robot;
import java.awt.GraphicsEnvironment;
import java.awt.GraphicsDevice;
import java.awt.MouseInfo;
import java.util.Map;
import java.util.Collection;
import java.util.HashMap;
import java.awt.event.KeyEvent;

public final class ControlComputer{

	private final Map<Character,Integer> KEYS = new HashMap<Character,Integer>();
	private final Map<Integer,GraphicsDevice> SCREENS = new HashMap<Integer,GraphicsDevice>();
	private final Map<Integer,Robot> ROBOTS = new HashMap<Integer,Robot>();

	public ControlComputer(){
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		for (int i = 0; i < gs.length; i++) {
			SCREENS.put(i,gs[i]);
		}
		int tmp = 0;
		for(final GraphicsDevice DEVICE : SCREENS.values()){
			try{
				ROBOTS.put(tmp++,new Robot(DEVICE));
			}catch(final Exception e){}
		}
		createKeyBindings();
	}
	
	public final void createKeyBindings(){
		KEYS.put('a',KeyEvent.VK_A);
		KEYS.put('b',KeyEvent.VK_B);
		KEYS.put('c',KeyEvent.VK_C);
		KEYS.put('d',KeyEvent.VK_D);
		KEYS.put('e',KeyEvent.VK_E);
		KEYS.put('f',KeyEvent.VK_F);
		KEYS.put('g',KeyEvent.VK_G);
		KEYS.put('h',KeyEvent.VK_H);
		KEYS.put('i',KeyEvent.VK_I);
		KEYS.put('j',KeyEvent.VK_J);
		KEYS.put('k',KeyEvent.VK_K);
		KEYS.put('l',KeyEvent.VK_L);
		KEYS.put('m',KeyEvent.VK_M);
		KEYS.put('n',KeyEvent.VK_N);
		KEYS.put('o',KeyEvent.VK_O);
		KEYS.put('p',KeyEvent.VK_P);
		KEYS.put('q',KeyEvent.VK_Q);
		KEYS.put('r',KeyEvent.VK_R);
		KEYS.put('s',KeyEvent.VK_S);
		KEYS.put('t',KeyEvent.VK_T);
		KEYS.put('u',KeyEvent.VK_U);
		KEYS.put('v',KeyEvent.VK_V);
		KEYS.put('w',KeyEvent.VK_W);
		KEYS.put('x',KeyEvent.VK_X);
		KEYS.put('y',KeyEvent.VK_Y);
		KEYS.put('z',KeyEvent.VK_Z);
		KEYS.put('0',KeyEvent.VK_0);
		KEYS.put('1',KeyEvent.VK_1);
		KEYS.put('2',KeyEvent.VK_2);
		KEYS.put('3',KeyEvent.VK_3);
		KEYS.put('4',KeyEvent.VK_4);
		KEYS.put('5',KeyEvent.VK_5);
		KEYS.put('6',KeyEvent.VK_6);
		KEYS.put('7',KeyEvent.VK_7);
		KEYS.put('8',KeyEvent.VK_8);
		KEYS.put('9',KeyEvent.VK_9);
	}
	public final Collection<Robot> getRobots(){
		return ROBOTS.values();
	}
	
	public final Robot getRobot(final int KEY){
		return ROBOTS.get(KEY);
	}
	
	public final Point getMouseCoords(){
		return MouseInfo.getPointerInfo().getLocation();
	}
	
	public final int getMouseOnScreen(){
		final GraphicsDevice DEVICE = MouseInfo.getPointerInfo().getDevice();
		for(final int KEY : SCREENS.keySet()){
			final GraphicsDevice DEVICE_2 = SCREENS.get(KEY);
			if(DEVICE_2 != null){
				if(DEVICE_2.equals(DEVICE)){
					return KEY;
				}
			}
		}
		return -1;
	}
	
	public final Color getColorWhereMouseIs(final Robot ROBOT){
		final Point POINT = getMouseCoords();
		if(POINT != null){
			if(ROBOT != null){
				return ROBOT.getPixelColor((int)POINT.getX(),(int)POINT.getY());
			}
		}
		return null;
	}
	
	public final void typeString(final Robot ROBOT, final String STR) throws Exception{
		if(ROBOT != null){
			for(final char CHAR : STR.toCharArray()){
				if(!Character.isLowerCase(CHAR)){
					ROBOT.keyPress(KeyEvent.VK_SHIFT);
					ROBOT.delay(100);
				}
				if(!Character.isLowerCase(CHAR)){
					final char CHAR_LOWER = Character.toLowerCase(CHAR);
					if(KEYS.get(CHAR_LOWER) != null){ 
						final int KEY_CODE = KEYS.get(CHAR_LOWER);
						ROBOT.keyPress(KEY_CODE);
						ROBOT.delay(100);
					}else{
						throw new Exception("Key["+CHAR_LOWER+"] Not Mapped!");
					}
				}else{
					if(KEYS.get(CHAR) != null){ 
						final int KEY_CODE = KEYS.get(CHAR);
						ROBOT.keyPress(KEY_CODE);
						ROBOT.delay(100);
					}else{
						throw new Exception("Key["+CHAR+"] Not Mapped!");
					}
				}
				if(!Character.isLowerCase(CHAR)){
					ROBOT.keyRelease(KeyEvent.VK_SHIFT);
					ROBOT.delay(100);
				}
			}
		}
	}
	
	

}