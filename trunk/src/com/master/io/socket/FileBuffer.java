package com.master.io.socket;

import com.master.io.socket.Buffer;

import java.nio.channels.SeekableByteChannel;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.ByteBuffer;
import java.nio.file.StandardOpenOption;
import java.io.IOException;

public final class FileBuffer extends Buffer {

	private final SeekableByteChannel SEEKABLE_BYTE_CHANNEL;
	
	public FileBuffer(final String FILE_NAME, final int BUFFER_SIZE_MB) throws IOException{
		final Path FILE = Paths.get(FILE_NAME);
		reset(1024*1024*BUFFER_SIZE_MB);
		SEEKABLE_BYTE_CHANNEL = Files.newByteChannel(FILE, StandardOpenOption.READ, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
		setByteChannel(SEEKABLE_BYTE_CHANNEL);
	}
	
	public final long filePosition() throws IOException {
		return SEEKABLE_BYTE_CHANNEL.position();
	}
	
	public final Buffer filePosition(final long POSITION) throws IOException {
		SEEKABLE_BYTE_CHANNEL.position(POSITION);
		return this;
	}
	
	public final long size() throws IOException {
		return SEEKABLE_BYTE_CHANNEL.size();
	}

	public final int putToChannel(final long OFFSET) throws IOException, NotYetConnectedException, ClosedChannelException {
		filePosition(OFFSET);
		return putToChannel();
	}

	public final int putToChannel(final ByteBuffer BYTE_BUFFER, final long OFFSET) throws IOException, NotYetConnectedException, ClosedChannelException {
		filePosition(OFFSET);
		return putToChannel(BYTE_BUFFER);
	}


}
