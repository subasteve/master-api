package com.master.io.socket;

import com.master.io.socket.Buffer;

import java.nio.channels.ServerSocketChannel;
import java.nio.channels.DatagramChannel;
import java.nio.channels.Selector;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.channels.SelectionKey;
import java.io.IOException;
import java.util.*;

public abstract class ServerSocket implements Runnable{

    private final Map<SocketChannel,List<Buffer>> DATA_MAP = new HashMap<SocketChannel,List<Buffer>>();
	private final DatagramChannel SERVER_UDP_CHANNEL;
	private final ServerSocketChannel SERVER_CHANNEL;
	private final Selector SELECTOR;
	private boolean running = true;

	public ServerSocket(final int PORT) throws IOException{
		SELECTOR = Selector.open();
		SERVER_UDP_CHANNEL = null;
		SERVER_CHANNEL = ServerSocketChannel.open();
		SERVER_CHANNEL.configureBlocking(false);
		SERVER_CHANNEL.socket().bind(new InetSocketAddress(PORT));
		SERVER_CHANNEL.register(SELECTOR, SelectionKey.OP_ACCEPT);
		new Thread(this, "ServerSocket").start();
	}
	
	public ServerSocket(final int PORT, final boolean UDP) throws IOException{
		SELECTOR = Selector.open();
		if(UDP){
			SERVER_CHANNEL = null;
			SERVER_UDP_CHANNEL = DatagramChannel.open();
			SERVER_UDP_CHANNEL.configureBlocking(false);
			SERVER_UDP_CHANNEL.socket().bind(new InetSocketAddress(PORT));
			SERVER_UDP_CHANNEL.register(SELECTOR, SelectionKey.OP_READ);
		}else{
			SERVER_UDP_CHANNEL = null;
			SERVER_CHANNEL = ServerSocketChannel.open();
			SERVER_CHANNEL.configureBlocking(false);
			SERVER_CHANNEL.socket().bind(new InetSocketAddress(PORT));
			SERVER_CHANNEL.register(SELECTOR, SelectionKey.OP_ACCEPT);
		}
		new Thread(this, "ServerSocket").start();
	}
	
	public ServerSocket(final int PORT, final boolean TCP, final boolean UDP) throws IOException{
		SELECTOR = Selector.open();
		if(UDP){
			SERVER_UDP_CHANNEL = DatagramChannel.open();
			SERVER_UDP_CHANNEL.configureBlocking(false);
			SERVER_UDP_CHANNEL.socket().bind(new InetSocketAddress(PORT));
			SERVER_UDP_CHANNEL.register(SELECTOR, SelectionKey.OP_ACCEPT);
		}else{
			SERVER_UDP_CHANNEL = null;
		}
		if(TCP){
			SERVER_CHANNEL = ServerSocketChannel.open();
			SERVER_CHANNEL.configureBlocking(false);
			SERVER_CHANNEL.socket().bind(new InetSocketAddress(PORT));
			SERVER_CHANNEL.register(SELECTOR, SelectionKey.OP_READ);
		}else{
			SERVER_CHANNEL = null;
		}
		new Thread(this, "ServerSocket").start();
	}

	public final void destruct(){
		running = false;
		try{
			if(SERVER_CHANNEL != null){
				SERVER_CHANNEL.close();
			}
		}catch(final Exception e){}
		try{
			if(SERVER_UDP_CHANNEL != null){
				SERVER_CHANNEL.close();
			}
		}catch(final Exception e){}
		try{
			SELECTOR.close();
		}catch(final Exception e){}
	}

	public abstract ClientSocket accept(final SelectionKey SELECTION_KEY, final SocketChannel SOCKET_CHANNEL);
	
	public abstract void read(final Buffer BUFFER, final DatagramChannel CHANNEL);

    public final void writePacket(final SelectionKey KEY, Buffer data) {
        List<Buffer> pendingData = DATA_MAP.get(KEY.channel());
        pendingData.add(data);
        KEY.interestOps(SelectionKey.OP_WRITE);
    }

    private void write(final SelectionKey KEY) throws IOException {
        final SocketChannel CHANNEL = (SocketChannel) KEY.channel();
        List<Buffer> pendingData = DATA_MAP.get(CHANNEL);
        Iterator<Buffer> items = pendingData.iterator();
        while (items.hasNext()) {
            Buffer item = items.next();
            items.remove();
            CHANNEL.write(item.getByteBuffer());
        }
        KEY.interestOps(SelectionKey.OP_READ);
    }

	public final void run(){
		while(running){
			try{
				final int CHANNELS_READY = SELECTOR.select();
				if(CHANNELS_READY <= 0){
					continue;
				}
				final Iterator<SelectionKey> KEY_ITERATOR = SELECTOR.selectedKeys().iterator();
				while(KEY_ITERATOR.hasNext()) {
					final SelectionKey SELECTION_KEY = KEY_ITERATOR.next();
					KEY_ITERATOR.remove();
					if(SELECTION_KEY.isValid()){
						if(SELECTION_KEY.isAcceptable()) {
							// a connection was accepted by a ServerSocketChannel.
							final SocketChannel CHANNEL = SERVER_CHANNEL.accept();
							if(CHANNEL != null){
								final ClientSocket CS = accept(SELECTION_KEY, CHANNEL);
								if(CS != null){
                                    DATA_MAP.put(CHANNEL, new ArrayList<Buffer>());
                                    final SelectionKey SELECTION_KEY_2 = CS.getSocket().register(SELECTOR, SelectionKey.OP_READ);
									SELECTION_KEY_2.attach(CS);
								}
								//SELECTION_KEY.cancel();
							}
						} else if (SELECTION_KEY.isConnectable()) {
							// a connection was established with a remote server.
						} else if (SELECTION_KEY.isReadable()) {
							// a channel is ready for reading
							final Object OBJ = SELECTION_KEY.attachment();
							if(OBJ != null){
								if(OBJ instanceof ClientSocket){
									final ClientSocket SOCKET = (ClientSocket)OBJ;
									if(!SOCKET.read(SELECTION_KEY)){
                                        DATA_MAP.remove(SELECTION_KEY.channel());
										SELECTION_KEY.cancel();
									}
								}
							}else{
								try{
									DatagramChannel CHANNEL = (DatagramChannel)SELECTION_KEY.channel();
									if(CHANNEL != null){
										final Buffer BUFFER = new Buffer(512);
										CHANNEL.receive(BUFFER.getByteBuffer());
										BUFFER.flip();
										read(BUFFER,CHANNEL);
									}
								}catch(final Exception e){
								}
							}
						} else if (SELECTION_KEY.isWritable()) {
							// a channel is ready for writing
                            write(SELECTION_KEY);
						}
					}
				}
				/*
				for(final SelectionKey SELECTION_KEY : SELECTOR.selectedKeys()){
					if(SELECTION_KEY.isValid()){
						if (SELECTION_KEY.isAcceptable()) {
							final SocketChannel CHANNEL = SERVER_CHANNEL.accept();
							if(CHANNEL != null){
								accept(CHANNEL);
							}
						}
					}
				}
				*/
			}catch(final Exception e){
				e.printStackTrace();
			}
		}
	}
}
