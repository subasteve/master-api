/* TODO: REWRITE WITH ENCRYPTION */
package com.master.io.socket;

import java.nio.BufferUnderflowException;
import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import java.io.IOException;
import java.nio.channels.ByteChannel;

import com.master.data.UnsignedByte;
import com.master.net.IP;
import com.master.net.MAC;
import com.master.util.Logger;

import java.nio.ByteBuffer;

public class Buffer{

	private ByteBuffer buffer = null;
	private ByteChannel byteChannel = null;
	
	public Buffer(){}
	
	public Buffer(final int SIZE){
		reset(SIZE);
	}
	
	public Buffer(final ByteBuffer BYTE_BUFFER){
		buffer = BYTE_BUFFER;
	}

	public Buffer(final byte[] BYTES){
		reset(BYTES.length);
		put(BYTES);
	}
	
	public Buffer(final ByteChannel BYTE_CHANNEL){
		byteChannel = BYTE_CHANNEL;
		reset(1024*1024);
	}
	
	public final Buffer setByteChannel(final ByteChannel BYTE_CHANNEL){
		byteChannel = BYTE_CHANNEL;
		return this;
	}

	public final void close() throws IOException{
		if(byteChannel != null){
			byteChannel.close();
		}
	}
	
	public final Buffer reset(){
		buffer.clear();
		return this;
	}
	
	public final Buffer rewind(){
		buffer.rewind();
		return this;
	}
	
	public final Buffer flip(){
		buffer.flip();
		return this;
	}
	
	public final boolean hasRemaining(){
		return buffer.hasRemaining();
	}
	
	public int position(){
		return buffer.position();
	}
	
	public Buffer position(final int POS){
		buffer.position(POS);
		return this;
	}
	
	public final Buffer reset(final int SIZE){
		buffer = ByteBuffer.allocateDirect(SIZE);
		return this;
	}
	
	public final ByteBuffer getByteBuffer(){
		return buffer;
	}
	
	public final int getFromChannel() throws NotYetConnectedException, ClosedChannelException{
		return getFromChannel(true);
	}
	
	public final int getFromChannel(final boolean CLEAR_AND_FLIP) throws NotYetConnectedException, ClosedChannelException{
		if(byteChannel != null){
			//final int REMAINING = buffer.remaining();
			//System.out.println("Remaining: "+REMAINING);
			//if(REMAINING < 0){
				try{
					if(CLEAR_AND_FLIP){
						buffer.clear();
					}
					final int AMOUNT = byteChannel.read(buffer);
					if(CLEAR_AND_FLIP){
						buffer.flip();
					}
					return AMOUNT;
				}catch(final NotYetConnectedException NOT_CONNECTED){
					throw NOT_CONNECTED;
				}catch(final Exception e){
					try{
						byteChannel.close();
					}catch(final Exception ex){}
					throw new ClosedChannelException();
				}
			//}
			//return REMAINING;
		}
		return -1;
	}
	
	public final byte[] getBytes(final int LENGTH){
		final byte[] BYTES = new byte[LENGTH];
		buffer.get(BYTES);
		return BYTES;
	}
	
	public final int get() throws BufferUnderflowException{
		return get(false,0);
	}
	
	public final int get(final boolean SIGNED) throws BufferUnderflowException{
		return get(SIGNED,0);
	}
	
	public final int get(final int SHIFT) throws BufferUnderflowException{
		return get(false,SHIFT);
	}
	
	public final int get(final boolean SIGNED, final int SHIFT) throws BufferUnderflowException{ //Signed Byte MAX=128 MIN=-128 | Unsigned Byte MAX=256
		if(SIGNED){
			if(SHIFT == 0){
				return buffer.get();
			}else{
				return (buffer.get() << SHIFT);
			}
		}else{
			if(SHIFT == 0){
				return (buffer.get() & 0xff);
			}else{
				return ((buffer.get() & 0xff) << SHIFT);
			}
		}
	}

	public final byte getAsByte() throws BufferUnderflowException{
		return buffer.get();
	}

	public final UnsignedByte getAsUnsignedByte(){
		return new UnsignedByte(buffer.get());
	}
	
	public final long getLongByte() throws BufferUnderflowException{
		return getLongByte(false,0);
	}
	
	public final long getLongByte(final boolean SIGNED) throws BufferUnderflowException{
		return getLongByte(SIGNED,0);
	}
	
	public final long getLongByte(final int SHIFT) throws BufferUnderflowException{
		return getLongByte(false,SHIFT);
	}
	
	public final long getLongByte(final boolean SIGNED, final int SHIFT) throws BufferUnderflowException{ //Signed Byte MAX=128 MIN=-128 | Unsigned Byte MAX=256
		if(SIGNED){
			if(SHIFT == 0){
				return buffer.get();
			}else{
				return (buffer.get() << SHIFT);
			}
		}else{
			if(SHIFT == 0){
				return (buffer.get() & 0xffl);
			}else{
				return ((buffer.get() & 0xffl) << SHIFT);
			}
		}
	}

	public final int getInteger() throws BufferUnderflowException{
		return (get(24)+get(16)+get(8)+get());
	}
	
	public final int getRGB(){
		return (get(16)+get(8)+get());
	}
	
	public final int[] getRGB(final int AMOUNT){
		final int[] RGBS = new int[AMOUNT];
		for(int i = 0; i < RGBS.length; i++){
			RGBS[i] = getRGB();
		}
		return RGBS;
	}

	public final long getLong() throws BufferUnderflowException{
		return (getLongByte(56)+getLongByte(48)+getLongByte(40)+getLongByte(32)+getLongByte(24)+getLongByte(16)+getLongByte(8)+getLongByte());
	}

	public final boolean getBoolean() throws BufferUnderflowException{
		return (get(true) != 0);
	}
	
	public final double getDouble() throws BufferUnderflowException{
		return Double.longBitsToDouble(getLong());
	}
	
	public final float getFloat() throws BufferUnderflowException{
		return Float.intBitsToFloat(getInteger());
	}

	public final int getShort() throws BufferUnderflowException{
		return getShortAsShort();
	}

	public final short getShortAsShort() throws BufferUnderflowException{
		return (short)(get(8)+get());
	}

	public final MAC getMAC() throws BufferUnderflowException{
		return new MAC(getLongByte(40)+getLongByte(32)+getLongByte(24)+getLongByte(16)+getLongByte(8)+getLongByte());
	}

	public final IP getIP() throws BufferUnderflowException{
		return new IP(getInteger());
	}
	
	public final String getString() throws BufferUnderflowException{
		final int STRING_LENGTH = getInteger();
		final byte[] BYTES = new byte[STRING_LENGTH];
		buffer.get(BYTES);
		return new String(BYTES);
	}

	private final int getLength(final boolean BYTE_LENGTH){
		if(BYTE_LENGTH){
			return get();
		}else{
			return getInteger();
		}
	}

	public final int[] getByteArray(final boolean BYTE_LENGTH){
		int length = getLength(BYTE_LENGTH);
		final int[] ARRAY = new int[length];
		for(int i = 0; i < length; i++){
			ARRAY[i] = get(false);
		}
		return ARRAY;
	}

	public final byte[] getByteArrayAsBytes(final boolean BYTE_LENGTH){
		int length = getLength(BYTE_LENGTH);
		final byte[] ARRAY = new byte[length];
		for(int i = 0; i < length; i++){
			ARRAY[i] = getAsByte();
		}
		return ARRAY;
	}

	public final int[] getIntegerArray(final boolean BYTE_LENGTH){
		int length = getLength(BYTE_LENGTH);
		final int[] ARRAY = new int[length];
		for(int i = 0; i < length; i++){
                	ARRAY[i] = getInteger();
                }
		return ARRAY;
	}

	public final int[] getShortArray(final boolean BYTE_LENGTH){
		int length = getLength(BYTE_LENGTH);
		final int[] ARRAY = new int[length];
		for(int i = 0; i < length; i++){
			ARRAY[i] = getShort();
		}
		return ARRAY;
	}

	public final short[] getShortArrayAsShort(final boolean BYTE_LENGTH){
		int length = getLength(BYTE_LENGTH);
		final short[] ARRAY = new short[length];
		for(int i = 0; i < length; i++){
			ARRAY[i] = getShortAsShort();
		}
		return ARRAY;
	}

	public final long[] getLongArray(final boolean BYTE_LENGTH){
		int length = getLength(BYTE_LENGTH);
		final long[] ARRAY = new long[length];
		for(int i = 0; i < length; i++){
			ARRAY[i] = getLong();
		}
		return ARRAY;
	}

	public final String[] getStringArray(final boolean BYTE_LENGTH){
		int length = getLength(BYTE_LENGTH);
		final String[] ARRAY = new String[length];
		for(int i = 0; i < length; i++){
			ARRAY[i] = getString();
		}
		return ARRAY;
	}

	public final Buffer put(final byte BYTE) { //Max unsigned 256 | Max signed 128
		buffer.put(BYTE);
		return this;
	}

	public final Buffer put(final UnsignedByte BYTE) {
		buffer.put(BYTE.getAsByte());
		return this;
	}
	
	public final Buffer put(final byte BYTES[]) {
		buffer.put(BYTES);
		return this;
    }
	
	public final Buffer put(final Buffer BUFFER) {
		buffer.put(BUFFER.getByteBuffer());
		return this;
	}
	
	public final Buffer put(final int INTEGER) { //Max unsigned 256 | Max signed 128
		return put(INTEGER,0);
	}

	public final Buffer put(final int INTEGER, final int SHIFT) { //Max unsigned 256 | Max signed 128
		if(SHIFT == 0){
			buffer.put((byte)INTEGER);
		}else{
			buffer.put((byte)(INTEGER >> SHIFT));
		}
		return this;
	}
	
	public final Buffer put(final long LONG) {
		return put(LONG,0);
	}

	public final Buffer put(final long LONG, final int SHIFT) {
		if(SHIFT == 0){
			buffer.put((byte)LONG);
		}else{
			buffer.put((byte)(LONG >> SHIFT));
		}
		return this;
	}
	
	public final Buffer put(final boolean BOOL) {
		return put((BOOL ? 1 : 0));
	}

    public int putToChannel() throws NotYetConnectedException, ClosedChannelException{
        return putToChannel(null,buffer);
    }

	public int putToChannel(final Logger LOGGER) throws NotYetConnectedException, ClosedChannelException{
		return putToChannel(LOGGER,buffer);
	}

	public int putToChannel(final ByteBuffer BYTE_BUFFER) throws NotYetConnectedException, ClosedChannelException{
		return putToChannel(null,BYTE_BUFFER);
	}
	
	public int putToChannel(final Logger LOGGER, final ByteBuffer BYTE_BUFFER) throws NotYetConnectedException, ClosedChannelException{
		if(byteChannel != null){
			try{
				BYTE_BUFFER.flip();
				int tmp = 0;
				while(BYTE_BUFFER.hasRemaining()){
					tmp+=byteChannel.write(BYTE_BUFFER);
				}
				BYTE_BUFFER.clear();
				return tmp;
			}catch(final NotYetConnectedException NOT_CONNECTED){
				throw NOT_CONNECTED;
			}catch(final Exception e){
                if(LOGGER != null){
                    LOGGER.writeError(e);
                }
				try{
					byteChannel.close();
				}catch(final Exception ex){}
				throw new ClosedChannelException();
			}
		}
		return -1;
	}
	
	public final Buffer putBoolean(final boolean BOOL) {
		return put(BOOL);
	}
	
	public final Buffer putShort(final int INTEGER) { //Max unsigned 256^2 | Max signed 128^2
		return put(INTEGER,8).put(INTEGER);
	}

	public final Buffer putShort(final short SHORT) { //Max unsigned 256^2 | Max signed 128^2
		return put(SHORT,8).put(SHORT);
	}
	
	public final Buffer putRGB(final int RED, final int GREEN, final int BLUE){
		return put(RED).put(GREEN).put(BLUE);
	}
	
	public final Buffer putRGB(final int INTEGER){
		return put(INTEGER,16).put(INTEGER,8).put(INTEGER);
	}
	
	public final Buffer putRGB(final int[] INTEGERS){
		for(final int INTEGER : INTEGERS){
			put(INTEGER,16);
			put(INTEGER,8);
			put(INTEGER);
		}
		return this;
	}
	
	public final Buffer putInteger(final int INTEGER) { //Max unsigned 256^4 | Max signed 128^4
		return put(INTEGER,24).put(INTEGER,16).put(INTEGER,8).put(INTEGER);
	}
	
	public final Buffer putDouble(final double DOUBLE) {
		return putLong(Double.doubleToRawLongBits(DOUBLE));
	}
	
	public final Buffer putFloat(final float FLOAT) {
		return putInteger(Float.floatToIntBits(FLOAT));
	}

	public final Buffer putMAC(final MAC mac) {
		return putMAC(mac.toLong());
	}
	
	public final Buffer putMAC(final long MAC_ADDRESS) {
		return put(MAC_ADDRESS,40).put(MAC_ADDRESS,32).put(MAC_ADDRESS,24).put(MAC_ADDRESS,16).put(MAC_ADDRESS,8).put(MAC_ADDRESS);
	}

	public final Buffer writeIP(final IP ip) {
		return putInteger(ip.toInt());
	}
	
	public final Buffer putLong(final long LONG) { //Max unsigned 256^8 | Max signed 128^8
		return put(LONG,56).put(LONG,48).put(LONG,40).put(LONG,32).put(LONG,24).put(LONG,16).put(LONG,8).put(LONG);
	}
	
	public final Buffer putString(final String STR) {
		return putString(STR,true);
	}
	
	public final Buffer putString(final String STR, final boolean INCLUDE_LENGTH) {
		final byte[] BYTES = STR.getBytes();
		return (INCLUDE_LENGTH) ? putInteger(BYTES.length).put(BYTES) : put(BYTES);
	}

	private final int writeLength(final int LENGTH, final boolean BYTE_LENGTH) {
		if(BYTE_LENGTH){
			if(LENGTH > 255){
				put(255);
				return 255;
			}else{
				put(LENGTH);
			}
		}else{
			putInteger(LENGTH);
		}
		return LENGTH;
	}

	public final Buffer putByteArray(final byte[] ARRAY, final boolean BYTE_LENGTH) {
		final int LENGTH = writeLength(ARRAY.length, BYTE_LENGTH);
		for(int i = 0; i < LENGTH; i++){
			put(ARRAY[i]);
		}
		return this;
	}

	public final Buffer putByteArray(final int[] ARRAY, final boolean BYTE_LENGTH) {
		final int LENGTH = writeLength(ARRAY.length, BYTE_LENGTH);
		for(int i = 0; i < LENGTH; i++){
			put(ARRAY[i]);
		}
		return this;
	}

	public final Buffer putIntegerArray(final int[] ARRAY, final boolean BYTE_LENGTH) {
		final int LENGTH = writeLength(ARRAY.length, BYTE_LENGTH);
		for(int i = 0; i < LENGTH; i++){
			putInteger(ARRAY[i]);
		}
		return this;
	}

	public final Buffer putShortArray(final short[] ARRAY, final boolean BYTE_LENGTH) {
		final int LENGTH = writeLength(ARRAY.length, BYTE_LENGTH);
		for(int i = 0; i < LENGTH; i++){
			putShort(ARRAY[i]);
		}
		return this;
	}

	public final Buffer putShortArray(final int[] ARRAY, final boolean BYTE_LENGTH) {
		final int LENGTH = writeLength(ARRAY.length, BYTE_LENGTH);
		for(int i = 0; i < LENGTH; i++){
			putShort(ARRAY[i]);
		}
		return this;
	}

	public final Buffer putLongArray(final long[] ARRAY, final boolean BYTE_LENGTH) {
		final int LENGTH = writeLength(ARRAY.length, BYTE_LENGTH);
		for(int i = 0; i < LENGTH; i++){
			putLong(ARRAY[i]);
		}
		return this;
	}

	public final Buffer putStringArray(final String[] ARRAY, final boolean BYTE_LENGTH) {
		return putStringArray(ARRAY,BYTE_LENGTH,false);
	}

	public final Buffer putStringArray(final String[] ARRAY, final boolean BYTE_LENGTH, final boolean IGNORE_NULL) {
		int length = 0;
		if(IGNORE_NULL){
			for(int i = 0; i < ARRAY.length; i++){
				if(ARRAY[i] != null){
					length++;
				}
			}
		}else{
			length = ARRAY.length;
		}
		writeLength(length, BYTE_LENGTH);
		length = 0;
		for(int i = 0; i < ARRAY.length; i++){
			if (ARRAY[i] != null) {
				putString(ARRAY[i]);
				length++;
			}else if(!IGNORE_NULL && ARRAY[i] == null) {
				putString("");
				length++;
			}
			if(BYTE_LENGTH && length > 255){
				break;
			}
		}
		return this;
	}

}
