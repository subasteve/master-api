package com.master.io.socket;

import com.master.io.in.packets.Parse;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.BufferUnderflowException;
import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import java.net.InetSocketAddress;
import java.io.IOException;
import java.util.*;
import java.nio.channels.SelectionKey;

import com.master.io.interfaces.BufferListener;
import com.master.io.socket.Buffer;
import com.master.util.Logger;

public final class ClientSocket implements Runnable{

    private final List<Buffer> DATA_MAP = new ArrayList<Buffer>();
	private final Map<Integer,BufferListener> BUFFER_LISTENERS = new HashMap<Integer,BufferListener>();
	private int lastID = 0;
	private final Object LOCK = new Object();
	private Parse parse = null;
	private final Selector SELECTOR;
	private final SocketChannel SOCKET_CHANNEL;
	private final Buffer BUFFER_IN;
	private final Buffer BUFFER_OUT;
	private final Buffer BUFFER_TMP;
	private boolean running = false;
    private SelectionKey selectionKey = null;
	private int headerOffset = 0, bufferOffset = 0, readOffset = 0, lastPacketSize = -1, lastPacketType = -1, lastPostion = 0;
	
	public ClientSocket(final String HOST, final int PORT) throws IOException{
		SELECTOR = Selector.open();
		SOCKET_CHANNEL = SocketChannel.open(new InetSocketAddress(HOST, PORT));
		SOCKET_CHANNEL.configureBlocking(false);
		SOCKET_CHANNEL.register(SELECTOR, SelectionKey.OP_READ);
		//SOCKET_CHANNEL.register(SELECTOR, SelectionKey.OP_WRITE);
		BUFFER_IN = new Buffer(SOCKET_CHANNEL);
		BUFFER_OUT = new Buffer(SOCKET_CHANNEL);
		BUFFER_TMP = new Buffer(1024*1024);
		running = true;
		new Thread(this, "ClientSocket").start();
	}

	public ClientSocket(final SocketChannel SOCKET_CHANNEL, final Parse PARSE) throws IOException{
		SELECTOR = Selector.open();
		this.SOCKET_CHANNEL = SOCKET_CHANNEL;
		SOCKET_CHANNEL.configureBlocking(false);
		SOCKET_CHANNEL.register(SELECTOR, SelectionKey.OP_READ);
		//SOCKET_CHANNEL.register(SELECTOR, SelectionKey.OP_WRITE);
		this.parse = PARSE;
		BUFFER_IN = new Buffer(SOCKET_CHANNEL);
		BUFFER_OUT = new Buffer(SOCKET_CHANNEL);
		BUFFER_TMP = new Buffer(1024*1024);
		running = true;
		new Thread(this, "ClientSocket").start();
	}

	public ClientSocket(final SocketChannel SOCKET_CHANNEL) throws IOException{
		SELECTOR = Selector.open();
		this.SOCKET_CHANNEL = SOCKET_CHANNEL;
		SOCKET_CHANNEL.configureBlocking(false);
		SOCKET_CHANNEL.register(SELECTOR, SelectionKey.OP_READ);
		//SOCKET_CHANNEL.register(SELECTOR, SelectionKey.OP_WRITE);
		BUFFER_IN = new Buffer(SOCKET_CHANNEL);
		BUFFER_OUT = new Buffer(SOCKET_CHANNEL);
		BUFFER_TMP = new Buffer(1024*1024);
		running = true;
		new Thread(this, "ClientSocket").start();
	}

	public ClientSocket(final SocketChannel SOCKET_CHANNEL, final boolean BOOL_SELECTOR) throws IOException{
		if(BOOL_SELECTOR){
			SELECTOR = Selector.open();
		}else{
			SELECTOR = null;
		}
		this.SOCKET_CHANNEL = SOCKET_CHANNEL;
		SOCKET_CHANNEL.configureBlocking(false);
		if(BOOL_SELECTOR){
			SOCKET_CHANNEL.register(SELECTOR, SelectionKey.OP_READ);
		}
		//SOCKET_CHANNEL.register(SELECTOR, SelectionKey.OP_WRITE);
		BUFFER_IN = new Buffer(SOCKET_CHANNEL);
		BUFFER_OUT = new Buffer(SOCKET_CHANNEL);
		BUFFER_TMP = new Buffer(1024*1024);
		if(BOOL_SELECTOR){
			running = true;
			new Thread(this, "ClientSocket").start();
		}else{
			running = false;
		}
	}

	public final void blocking(final boolean BOOL) throws IOException{
		SOCKET_CHANNEL.configureBlocking(BOOL);
	}

	public final SocketChannel getSocket(){
		return SOCKET_CHANNEL;
	}

	public final void start(final Parse PARSE){
		this.parse = PARSE;
	}

	public final Buffer getTmpBuffer(){
		return BUFFER_TMP;
	}

	public final Buffer getInBuffer(){
		return BUFFER_IN;
	}

	public final Buffer getOutBuffer(){
		return BUFFER_OUT;
	}

	public final Buffer putToChannel() throws NotYetConnectedException, ClosedChannelException{
		BUFFER_OUT.putToChannel();
		return BUFFER_OUT;
	}

	public final void writePacket(final int PACKET) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		writePacket(PACKET,null,null);
	}

    public final void writePacket(final int PACKET, final Buffer BUFFER) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
        writePacket(PACKET,BUFFER,null);
    }
	
	public final void writePacket(final int PACKET, final Buffer BUFFER, final Logger LOGGER) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		BUFFER_OUT.reset();
		BUFFER_OUT.put(PACKET); //packet type
		if(BUFFER != null){
			final int SIZE = BUFFER.position();
			//System.out.println("BUFFER SIZE: "+SIZE);
			if(SIZE >= 255){
				BUFFER_OUT.put(255);
				BUFFER_OUT.putInteger(SIZE); //size
			}else{
				BUFFER_OUT.put(SIZE); //size
			}
			BUFFER.flip();
			BUFFER_OUT.put(BUFFER);
		}else{
			BUFFER_OUT.put(0); //size
		}
		//System.out.println("BUFFER_OUT SIZE: "+BUFFER_OUT.position());
		BUFFER_OUT.putToChannel(LOGGER);
		BUFFER_TMP.reset();
	}

    public final void writePacket(final SelectionKey KEY, final int PACKET) {
        writePacket(KEY,PACKET,null);
    }

    public final void writePacket(final SelectionKey KEY, final int PACKET, final Buffer BUFFER) {
        if (BUFFER != null) {
            final int SIZE = BUFFER.position();
            //System.out.println("BUFFER SIZE: "+SIZE);
            if (SIZE >= 255) {
                final Buffer FINAL_BUFFER = new Buffer(6 + BUFFER.position());
                FINAL_BUFFER.put(PACKET);
                FINAL_BUFFER.put(255);
                FINAL_BUFFER.putInteger(SIZE); //size
                BUFFER.flip();
                FINAL_BUFFER.put(BUFFER);
                FINAL_BUFFER.flip();
                DATA_MAP.add(FINAL_BUFFER);
            } else {
                final Buffer FINAL_BUFFER = new Buffer(2 + BUFFER.position());
                FINAL_BUFFER.put(PACKET);
                FINAL_BUFFER.put(SIZE);
                BUFFER.flip();
                FINAL_BUFFER.put(BUFFER);
                FINAL_BUFFER.flip();
                DATA_MAP.add(FINAL_BUFFER);
            }
        } else {
            final Buffer FINAL_BUFFER = new Buffer(1);
            FINAL_BUFFER.put(0); //size
            FINAL_BUFFER.position(0);
            DATA_MAP.add(FINAL_BUFFER);
        }
        //System.out.println("BUFFER_OUT SIZE: "+BUFFER_OUT.position());
        BUFFER_TMP.reset();
        KEY.interestOps(SelectionKey.OP_WRITE);
    }

    public final void writePacket(final SelectionKey KEY, final Buffer DATA) {
        DATA_MAP.add(DATA);
        KEY.interestOps(SelectionKey.OP_WRITE);
    }

    private void write(final SelectionKey KEY) throws IOException {
        final SocketChannel CHANNEL = (SocketChannel) KEY.channel();
        if(DATA_MAP != null) {
            final Iterator<Buffer> ITEMS = DATA_MAP.iterator();
            while (ITEMS.hasNext()) {
                final Buffer ITEM = ITEMS.next();
                ITEMS.remove();
                //System.out.println("Send: "+ITEM.getByteBuffer().limit());
                CHANNEL.write(ITEM.getByteBuffer());
            }
            KEY.interestOps(SelectionKey.OP_READ);
        }
    }
	
	public final int put(final BufferListener LISTENER){
		synchronized(LOCK){
			BUFFER_LISTENERS.put(lastID,LISTENER);
			return lastID++;
		}
	}

	public final void remove(final int ID){
		synchronized(LOCK){
			BUFFER_LISTENERS.remove(ID);
		}
	}

    public final SelectionKey getSelectionKey(){
        return selectionKey;
    }

	public final boolean read(final SelectionKey KEY){
		try{
            selectionKey = KEY;
			if(BUFFER_IN.position() > 0){
                final Buffer TMP_BUFFER = new Buffer(lastPostion-readOffset);
                BUFFER_IN.position(readOffset);
                final int LIMIT = BUFFER_IN.getByteBuffer().limit();
                BUFFER_IN.getByteBuffer().limit(lastPostion);
                TMP_BUFFER.put(BUFFER_IN);
                BUFFER_IN.position(0);
                BUFFER_IN.reset();
                BUFFER_IN.position(lastPostion-readOffset);
                if(SOCKET_CHANNEL.read(BUFFER_IN.getByteBuffer()) == -1){
                    destruct();
                    return false;
                }
                final int POSITION = BUFFER_IN.position();
                TMP_BUFFER.position(0);
                BUFFER_IN.position(0);
                BUFFER_IN.put(TMP_BUFFER);
                BUFFER_IN.getByteBuffer().limit(LIMIT);
                BUFFER_IN.position(POSITION);
                bufferOffset = 0;
                readOffset = 0;
            }else{
                if(SOCKET_CHANNEL.read(BUFFER_IN.getByteBuffer()) == -1){
                    destruct();
                    return false;
                }
            }
			
			synchronized(LOCK){
				for(final BufferListener LISTENER : BUFFER_LISTENERS.values()){
					LISTENER.bufferRecieve(BUFFER_IN);
				}
			}
			if(parse != null){
                readOffset = 0;
                lastPostion = BUFFER_IN.position();
				BUFFER_IN.position(bufferOffset);
				//if(OFFSET > 0){
				//System.out.println("Read "+lastPostion);
				try{
                    if(bufferOffset <= 0) {
                        lastPacketType = getPacketType(BUFFER_IN);
                        lastPacketSize = getPacketSize(BUFFER_IN);
                        //System.out.println("TYPE "+lastPacketType+" SIZE "+lastPacketSize);
                        bufferOffset += lastPostion;
                    }else{
                        bufferOffset += lastPostion;
                    }
                    final int SIZE_COMPARE = bufferOffset-headerOffset;
                    if(SIZE_COMPARE == lastPacketSize) {
                        bufferOffset = 0;
                        headerOffset = 0;
                        readOffset = 0;
                        if (lastPacketType != -1) {
                            parse.processPacket(lastPacketType, lastPacketSize);
                        }
                        BUFFER_IN.reset();
                    }else if(SIZE_COMPARE > lastPacketSize){
                        //System.out.println("ERROR "+SIZE_COMPARE+" > "+lastPacketSize);
                        //System.out.println("DIFF "+(SIZE_COMPARE-lastPacketSize));
                        if (lastPacketType != -1) {
                            parse.processPacket(lastPacketType, lastPacketSize);
                        }
                        bufferOffset -= lastPacketSize+headerOffset;
                        readOffset += lastPacketSize+headerOffset;
                        do {
                            if (bufferOffset >= 3){
                                headerOffset = 0;
                                lastPacketType = getPacketType(BUFFER_IN);
                                lastPacketSize = getPacketSize(BUFFER_IN);
                                final int SIZE_COMPARE_2 = bufferOffset-headerOffset;
                                if(SIZE_COMPARE_2 >= lastPacketSize){
                                    //System.out.println("TYPE "+lastPacketType+" SIZE "+lastPacketSize);
                                    if (lastPacketType != -1) {
                                        parse.processPacket(lastPacketType, lastPacketSize);
                                    }
                                    bufferOffset -= lastPacketSize+headerOffset;
                                    readOffset += lastPacketSize+headerOffset;
                                    headerOffset = 0;
                                }else{
                                    headerOffset = 0;
                                    break;
                                }
                            }else{
                                break;
                            }
                        }while(true);
                    }else{
                        headerOffset = 0;
                        //System.out.println("Packet next "+SIZE_COMPARE+" < "+lastPacketSize);
                    }
                    if(bufferOffset == 0){
                        BUFFER_IN.position(0);
                        BUFFER_IN.reset();
                    }else{
                        //System.out.println("Buffer Postion "+BUFFER_IN.position()+" Packet Type "+lastPacketType+" Packet Size "+lastPacketSize+" Buffer Offset "+bufferOffset+" Read Offset "+readOffset);
                        /*
                        final Buffer TMP_BUFFER = new Buffer(POSTION-bufferOffset);
                        BUFFER_IN.position(bufferOffset);
                        final int LIMIT = BUFFER_IN.getByteBuffer().limit();
                        BUFFER_IN.getByteBuffer().limit(POSTION);
                        TMP_BUFFER.put(BUFFER_IN);
                        BUFFER_IN.position(0);
                        BUFFER_IN.reset();
                        TMP_BUFFER.position(0);
                        BUFFER_IN.put(TMP_BUFFER);
                        BUFFER_IN.getByteBuffer().limit(LIMIT);
                        bufferOffset = 0;
                        */
                    }
				}catch(final Exception e){
					e.printStackTrace();
                    System.out.println("lastPacketType = "+lastPacketType+" lastPacketSize = "+lastPacketSize+" bufferOffset ="+bufferOffset+" headerOffset = "+headerOffset);
                    headerOffset = 0;
                    bufferOffset = 0;
                    readOffset = 0;
					BUFFER_IN.position(lastPostion);
				}
			}
			return true;
		}catch(final Exception e){
			destruct();
			return false;
		}
	}

	public final void run(){
		do{
			try{
				final int CHANNELS_READY = SELECTOR.select();
				if(CHANNELS_READY <= 0){
					continue;
				}
				final Iterator<SelectionKey> KEY_ITERATOR = SELECTOR.selectedKeys().iterator();
				while(KEY_ITERATOR.hasNext()) {
					final SelectionKey SELECTION_KEY = KEY_ITERATOR.next();
					KEY_ITERATOR.remove();
					if(SELECTION_KEY.isValid()){
						if(SELECTION_KEY.isAcceptable()) {
							// a connection was accepted
						} else if (SELECTION_KEY.isConnectable()) {
							// a connection was established with a remote server.
						} else if (SELECTION_KEY.isReadable()) {
							// a channel is ready for reading
							read(SELECTION_KEY);
						} else if (SELECTION_KEY.isWritable()) {
							// a channel is ready for writing
							write(SELECTION_KEY);
						}
					}
				}
			}catch(final Exception e){
				e.printStackTrace();
			}
		}while(isAlive() && running);
	}

	private final int getPacketType(final Buffer BUFFER) throws BufferUnderflowException{
        headerOffset++;
		return BUFFER.get();
	}

	private final int getPacketSize(final Buffer BUFFER) throws BufferUnderflowException{
		final int SIZE = BUFFER.get();
        headerOffset++;
		if(SIZE == 255){
            headerOffset+=4;
			return BUFFER.getInteger();
		}
		return SIZE;
	}

	public final boolean isAlive(){
		if(!SOCKET_CHANNEL.isConnected()){
			return false;
		}else if(!SOCKET_CHANNEL.isOpen()){
			return false;
		}
		return true;
	}

	public final void destruct(){
		destruct(null);
	}

	public final void destruct(final Exception ex){
		running = false;
		if(parse != null){
			parse.destruct();
		}
		if(ex != null){
			ex.printStackTrace();
		}
		if(SELECTOR != null){
			try {
				SELECTOR.close();
			} catch(final Exception e) { }
		}
		if(SOCKET_CHANNEL.isOpen()){
			try {
				SOCKET_CHANNEL.close();
			} catch(final Exception e) { }
		}
	}
}
