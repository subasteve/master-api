package com.master.io.interfaces;

import com.master.io.socket.Buffer;

public interface BufferInterface{

	public void put(final Buffer BUFFER) throws Exception;
	public void get(final Buffer BUFFER) throws Exception;
	
}