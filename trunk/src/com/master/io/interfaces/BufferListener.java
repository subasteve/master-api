package com.master.io.interfaces;

import com.master.io.socket.Buffer;

public interface BufferListener{

	public Buffer bufferRecieve(final Buffer BUFFER);
	
	public Buffer bufferSend(final Buffer BUFFER);

}
