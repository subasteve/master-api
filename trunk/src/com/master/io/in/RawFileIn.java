package com.master.io.in;

import java.io.File;
import java.io.FileInputStream;
import com.master.net.socket.stream.in.In;
import com.master.util.Logger;

public final class RawFileIn{
	
	private FileInputStream fis = null;
	private final File f;
	private long timeStamp;
	private byte buffer[];
	private Logger logger = null;
	
	public RawFileIn(final String file){
		f = new File(file);
		timeStamp = 0;
		reload();
	}
	
	public RawFileIn(final Logger logger, final String file){
		this.logger = logger;
		f = new File(file);
		timeStamp = 0;
		reload();
	}
	
	public final void setLogger(final Logger logger){
		this.logger = logger;
	}
	
	public final boolean reload(){
		if(f.exists()){
			if(timeStamp != f.lastModified()){
				timeStamp = f.lastModified();
				try{
					fis = new FileInputStream(f);
					buffer = new byte[fis.available()];
					fis.read(buffer);
					if(fis != null){
						try{
							fis.close();
						}catch(final Exception ex){}
						fis = null;
					}
					return true;
				}catch(final Exception e) {
					if(logger != null){
						logger.writeLine("Error loading File: "+f.getName());
						logger.writeError(e);
					}
				}finally{
					if(fis != null){
						try{
							fis.close();
						}catch(final Exception ex){}
						fis = null;
					}
				}
			}
		}
		return false;
	}
	
	public final In getIn(){
		if(f.exists()){
			return new In(buffer);
		}else{
			return null;
		}
	}

}