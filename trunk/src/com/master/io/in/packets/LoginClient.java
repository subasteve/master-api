package com.master.io.in.packets;

import com.master.io.socket.ClientSocket;
import com.master.io.out.packets.SendClientSessionKey;
import com.master.io.out.packets.SendUserPass;
import com.master.io.out.packets.RequestServerSessionKey;
import java.nio.BufferUnderflowException;
import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;

public abstract class LoginClient{

	private final long CLIENT_KEY;
	private final ClientSocket SOCKET;
	private long serverKey = -1;

	public final boolean validNick(final String USER){
		try{
			if(USER == null) return false;
			final char[] VALID_CHARS = getVaildChars();
			if(VALID_CHARS == null) return true;
			for(int i = 0; i < USER.length(); i++){
				boolean valid = false;
				for(int x = 0; x < VALID_CHARS.length; x++){
					if(USER.charAt(i) == VALID_CHARS[x]){
						valid = true;
						break;
					}
				}
				if(!valid)
					return false;
			}
			return true;
		}catch(Exception e) { return false; }
	}
	
	public abstract char[] getVaildChars();
	public abstract String getUser();
	public abstract String getPass();
	public abstract int getMemberID();
	public abstract void login(final boolean SUCCESS);
	
	public LoginClient(final ClientSocket SOCKET){
		this.SOCKET = SOCKET;
		CLIENT_KEY = ((long)(java.lang.Math.random() * 99999999D) << 32) + (long)(java.lang.Math.random() * 99999999D);
	}
	
	public LoginClient(final ClientSocket SOCKET, final boolean AUTO_START) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		this.SOCKET = SOCKET;
		CLIENT_KEY = ((long)(java.lang.Math.random() * 99999999D) << 32) + (long)(java.lang.Math.random() * 99999999D);
		if(AUTO_START){
			start();
		}
	}
	
	public final void start() throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		new RequestServerSessionKey(SOCKET);
	}
	
	public final void parse() throws BufferUnderflowException, BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		final int TYPE = SOCKET.getInBuffer().get(); // 0 = key exchange | 1 = key exchange & cryption set | 2 = login
		switch(TYPE){
			case 0:
				//System.out.println("Recieved Server Key sending Client Key...");
				serverKey = SOCKET.getInBuffer().getLong();
				new SendClientSessionKey(SOCKET,CLIENT_KEY);
			break;
			case 1:
				if(serverKey != -1){
					//System.out.println("Sending User & Pass...");
					//SOCKET.setCryption(CLIENT_KEY,serverKey);
					if(getMemberID() > -1){
						new SendUserPass(SOCKET,getMemberID(),getPass());
					}else{
						new SendUserPass(SOCKET,getUser(),getPass());
					}
				}else{
					//System.out.println("No Server Key Recieved killing socket....");
					SOCKET.destruct();
				}
			break;
			case 2:
				final boolean SUCCESS = SOCKET.getInBuffer().getBoolean();
				login(SUCCESS);
			break;
			default:
				System.out.println("Unknown login type ["+TYPE+"] killing socket...");
				SOCKET.destruct();
			break;
		}
	}
	
}
