package com.master.io.in.packets;
import com.master.io.socket.ClientSocket;
public interface Packet{
	void process(final ClientSocket SOCKET);
}