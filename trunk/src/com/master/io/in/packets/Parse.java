package com.master.io.in.packets;

public abstract class Parse{


	public Parse(){}

	public abstract void parsePacket(final int TYPE,final long SIZE);
	
	public abstract void destruct();

	public final void processPacket(final int TYPE, final long SIZE) {
		parsePacket(TYPE,SIZE);
		//new Thread(new Process(TYPE,SIZE));
	}
/*
	private class Process implements Runnable{
		final int PACKET_TYPE;
		final long PACKET_SIZE;
		public Process(final int PACKET_TYPE, final long PACKET_SIZE){
			this.PACKET_TYPE = PACKET_TYPE;
			this.PACKET_SIZE = PACKET_SIZE;
		}
		public final void run(){
			parsePacket(PACKET_TYPE,PACKET_SIZE);
		}
	}
*/
}
