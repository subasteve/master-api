package com.master.io.out.packets;

import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import com.master.io.socket.ClientSocket;

public final class SendClientSessionKey{

	public SendClientSessionKey(final ClientSocket SOCKET, final long KEY) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		SOCKET.writePacket(0,SOCKET.getTmpBuffer().put(1).putLong(KEY)); //type 1 send clientsession key
	}
	
}
