package com.master.io.out.packets;

import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import com.master.io.socket.ClientSocket;

public final class RequestServerSessionKey{

	public RequestServerSessionKey(final ClientSocket SOCKET) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		SOCKET.writePacket(0,SOCKET.getTmpBuffer().put(0)); //type 0 get serversession key
	}
	
}
