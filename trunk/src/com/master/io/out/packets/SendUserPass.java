package com.master.io.out.packets;

import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import com.master.io.socket.ClientSocket;

public final class SendUserPass{

	public SendUserPass(final ClientSocket SOCKET, final String USER, final String PASS) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		SOCKET.writePacket(0,SOCKET.getTmpBuffer().put(2).put(1).putString(USER).putString(PASS)); //type 2 send username & password
	}
	
	public SendUserPass(final ClientSocket SOCKET, final int MEMBER_ID, final String PASS) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		SOCKET.writePacket(0,SOCKET.getTmpBuffer().put(2).put(2).putInteger(MEMBER_ID).putString(PASS)); //type 2 send member ID & password
	}
	
}
