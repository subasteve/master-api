package com.master.io.out.packets;

import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import com.master.io.socket.ClientSocket;

public final class SendServerSessionKey{

	public SendServerSessionKey(final ClientSocket SOCKET, final long KEY) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		SOCKET.writePacket(0,SOCKET.getTmpBuffer().put(0).putLong(KEY)); //type 0 send serversession key
	}
	
}
