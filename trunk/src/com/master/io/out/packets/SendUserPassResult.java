package com.master.io.out.packets;

import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import com.master.io.socket.ClientSocket;

public final class SendUserPassResult{

	public SendUserPassResult(final ClientSocket SOCKET, final boolean LOGGED_IN) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		SOCKET.writePacket(0,SOCKET.getTmpBuffer().put(2).put(LOGGED_IN)); //type 2 User & Pass Result
	}
	
}
