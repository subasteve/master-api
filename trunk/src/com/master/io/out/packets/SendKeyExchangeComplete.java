package com.master.io.out.packets;

import java.nio.BufferOverflowException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ClosedChannelException;
import com.master.io.socket.ClientSocket;

public final class SendKeyExchangeComplete{

	public SendKeyExchangeComplete(final ClientSocket SOCKET, final long CLIENT_KEY, final long SERVER_KEY) throws BufferOverflowException, NotYetConnectedException, ClosedChannelException{
		SOCKET.writePacket(0,SOCKET.getTmpBuffer().put(1)); //type 1 key exchange complete
		//SOCKET.setCryption(CLIENT_KEY,SERVER_KEY);
	}
	
}
