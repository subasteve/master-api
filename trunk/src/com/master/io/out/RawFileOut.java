package com.master.io.out;

import java.io.File;
import java.io.FileOutputStream;
import com.master.net.socket.stream.out.Out;
import com.master.net.socket.stream.Cryption;
import com.master.util.Logger;

public final class RawFileOut{

	private FileOutputStream fos = null;
	private final File f;
	private long timeStamp;
	private Cryption encryption = null;
	private Logger logger = null;

	public RawFileOut(final String file){
		f = new File(file);
		timeStamp = 0;
		create(false);
	}
	
	public RawFileOut(final Logger logger, final String file){
		this.logger = logger;
		f = new File(file);
		timeStamp = 0;
		create(false);
	}
	
	public RawFileOut(final String file, boolean append){
		f = new File(file);
		timeStamp = 0;
		create(append);
	}
	
	public RawFileOut(final Logger logger, final String file, boolean append){
		this.logger = logger;
		f = new File(file);
		timeStamp = 0;
		create(append);
	}
	
	public final void setLogger(final Logger logger){
		this.logger = logger;
	}
	
	public final void destruct(){
		if(fos != null){
			try{
				fos.close();
			}catch(final Exception ex){}
			fos = null;
		}
	}
	
	public final boolean create(boolean append){
		if(timeStamp != f.lastModified() || !f.exists()){
			timeStamp = f.lastModified();
			try{
				fos = new FileOutputStream(f,append);
				return true;
			}catch(final Exception e) {
				if(logger != null){
					logger.writeLine("Error saving File: "+f.getName());
					logger.writeError(e);
				}
			}
			return false;
		}
		return false;
	}
	
	public final void setEncryption(final Cryption cry){
		encryption = cry;
	}
	
	public final void writeOut(final Out OUT){
		try{
			OUT.writeTo(fos);
			fos.flush();
			timeStamp = f.lastModified();
		}catch(final Exception e){}
	}
	
	public final void writeOut(final int packet, final Out out){
		try{
			//final byte buffer[] = out.getBuffer();
			final long offset = out.getCurrentOffset();
			Out out2 = new Out();
			if(encryption != null){
				out2.writeByte(packet+encryption.getNextKey());
			}else{
				out2.writeByte(packet);
			}
			if(offset >= 255){
				out2.writeByte(255);
				out2.writeLong(offset);
			}else{
				out2.writeByte(offset);
			}
			out.writeTo(out2);
			//fos.write(out2.getBytes(), 0, out2.getCurrentOffset());
			out2.writeTo(fos);
			fos.flush();
			timeStamp = f.lastModified();
		}catch(final Exception e){}
	}

}
