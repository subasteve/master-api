package com.master.weather;

import com.master.net.WebPage;
import com.master.util.SearchableString;

public final class WeatherUnderground implements Runnable{

	private final String WEATHER_UNDERGROUND_STATIONLOOKUP = "http://stationdata.wunderground.com/cgi-bin/stationlookup?station=";
	private final WebPage WEB_PAGE = new WebPage(false);
	private final String STATION_ID;
	private final int UPDATE_SECONDS;
	private boolean running = true;
	private double temp = -1;
	
	public WeatherUnderground(final String STATION_ID, final int UPDATE_SECONDS){
		this.STATION_ID = STATION_ID;
		this.UPDATE_SECONDS = UPDATE_SECONDS;
		WEB_PAGE.setPathRelative(true);
		if(UPDATE_SECONDS > 0){
			new Thread(this).start();
		}
	}
	
	public final void update(){
		do{
			try{
				final SearchableString SEARCH_STRING = WEB_PAGE.get(WEATHER_UNDERGROUND_STATIONLOOKUP+STATION_ID);
				//System.out.println(SEARCH_STRING.toString());
				temp = SEARCH_STRING.position(0).position("<tempf val=\"").getDouble("\"");
				break;
			}catch(final Exception e){ /*e.printStackTrace();*/ }
			try { Thread.sleep(UPDATE_SECONDS*1000); } catch(Exception ex) {}
		}while(true);
	}
	
	public final double getTemp(){
		return temp;
	}
	
	public final double getTemp(final boolean UPDATE){
		if(UPDATE){
			update();
		}
		return temp;
	}
	
	public final void destruct(){
		running = false;
	}
	
	public final void run(){
		do{
			update();
			try { Thread.sleep(UPDATE_SECONDS*1000); } catch(Exception ex) { } 
		}while(running);
	}

}
