Master API
========================================
![Dependencies: none](https://gemnasium.com/mathiasbynens/he.svg)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/873ea04bf75b41aeb1321c214299da9c)](https://www.codacy.com/app/subasteve/master-api?utm_source=subasteve@bitbucket.org&amp;utm_medium=referral&amp;utm_content=subasteve/master-api&amp;utm_campaign=Badge_Grade)
[![Coverage Status](https://coveralls.io/repos/bitbucket/subasteve/master-api/badge.svg?branch=master)](https://coveralls.io/bitbucket/subasteve/master-api?branch=master)
[![CircleCI](https://circleci.com/bb/subasteve/master-api/tree/master.svg?style=svg)](https://circleci.com/bb/subasteve/master-api/tree/master)  
![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)
[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://opensource.org/licenses/mit-license.php)  
  
[Download Latest Jar](https://circleci.com/api/v1.1/project/bitbucket/subasteve/master-api/latest/artifacts/0/$CIRCLE_ARTIFACTS/master-api.jar?branch=master)  
  
Master API is a collection of classes to help create projects quickly and efficiently.

Includes
-------------
* Automation
* Google Wrappers
* JSON construction
* Buffer
* Networking
* NIO Networking
* Encryption
* Text Searching/Parsing
* Weather
* Tray Icon
* MySQL
* MsSQL
* Commands
* Screenshots
* Program Shutdown Hooks
* Base64


Runtime Dependencies
-------------
#### MsSql aka com.microsoft.sqlserver.jdbc ####

http://msdn.microsoft.com/en-us/data/aa937724.aspx

sqljdbc4.jar

#### MySql aka com.mysql.jdbc ####

https://dev.mysql.com/downloads/connector/j/

Compile
-------------
```
#!sh
ant master_jar
```
or
```
#!sh
./gradlew jar
```

Testing
-------------
```
#!sh
ant master_test
```
or
```
#!sh
./gradlew test
```


Author
-------------
Stephen Hineline  
[stephenhineline.com](http://stephenhineline.com/ "Homepage")
